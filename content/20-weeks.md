Title: 20 Weeks
Date: 2008-05-27 10:30
Tags: life
Slug: 20-weeks

Over the last few weeks Vicky's previously invisible pregnancy has
finally popped out. Much frustration ensued as this meant most of her
clothes didn't fit any more, but that was soon relieved.

![20 Weeks](http://farm4.static.flickr.com/3015/2526738845_3f5a4ec475_m.jpg)
