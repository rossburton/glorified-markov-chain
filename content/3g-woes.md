Title: 3G Woes
Date: 2009-01-15 11:15
Tags: tech
Slug: 3g-woes

Has anyone out there used a recent Nokia phone (E65 to be precise) as a
modem with Network Manager 0.7? I can't seem to get the magic right, and
get one of two failures:

    NetworkManager: <info>  (ttyACM0): powering up... 
    NetworkManager: <info>  Registered on Home network 
    an 15 10:50:04 blackadder NetworkManager: <info>  Associated with network: +COPS: 0,2,"23415" 
      NetworkManager: <WARN>  dial_done(): Dialing timed out </WARN>

Or:

    NetworkManager: <info>  Activation (ttyACM0) Stage 1 of 5 (Device Prepare) complete. 
    NetworkManager: <info>  (ttyACM0): powering up... 
    NetworkManager: <info>  Registered on Home network 
    NetworkManager: <info>  Associated with network: +COPS: 0,2,"23415" 
    NetworkManager: <info>  Connected, Woo! 
    NetworkManager: <info>  Activation (ttyACM0) Stage 2 of 5 (Device Configure) scheduled..
    . 
    NetworkManager: <info>  Activation (ttyACM0) Stage 2 of 5 (Device Configure) starting...
     
    NetworkManager: <info>  (ttyACM0): device state change: 4 -> 5 
    NetworkManager: <info>  Starting pppd connection 
    NetworkManager: <debug> [1232015456.962700] nm_ppp_manager_start(): Command line: /usr/s
    bin/pppd nodetach lock nodefaultroute user web ttyACM0 noipdefault usepeerdns lcp-echo-failure 0 lcp-echo-interval 
    0 ipparam /org/freedesktop/NetworkManager/PPP/4 plugin /usr/lib/pppd/2.4.4/nm-pppd-plugin.so 
    NetworkManager: <debug> [1232015456.964964] nm_ppp_manager_start(): ppp started with pid
     29590 
    NetworkManager: <info>  Activation (ttyACM0) Stage 2 of 5 (Device Configure) complete. 
    pppd[29590]: Plugin /usr/lib/pppd/2.4.4/nm-pppd-plugin.so loaded.
    pppd[29590]: pppd 2.4.4 started by root, uid 0
    NetworkManager: <WARN>  pppd_timed_out(): Looks like pppd didn't initialize our dbus mod
    ule

Anyone know what the problem could be?
