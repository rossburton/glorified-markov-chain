Title: A Common Bug
Date: 2003-09-16 00:00
Tags: tech
Slug: a-common-bug

Will everyone who is about to file a bug with Sound Juicer about how it
crashes when its has extracted a song please compare their bug report
with [this one](http://bugzilla.gnome.org/show_bug.cgi?id=122416). If
its the same bug, please don't file it again. Bastien has been very good
with marking these bugs as duplicates, and I'm sure he is getting bored
now.

Everytime you file a duplicate bug, God kills a kitten.

Please, think of the kittens.
