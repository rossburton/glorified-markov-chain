Title: A New Year...
Date: 2003-01-24 12:00
Tags: life
Slug: a-new-year

A little late, but that never hurt anyone.

This year looks like being a good one on the whole. I'm off to Paris in
April for 4 days with Vicky for our anniversary. In June I want to try
and get to GUADEC 4. Then in July I'm off to Kefalonia for two weeks,
which was a birthday present (probably one of the best presents I've
ever received...) from my parents.

I've also just bought a new camera -- the Canon EOS-300V. I've always
wanted a decent SLR camera, so we bought it with all the money we
received over Christmas. It totally rocks. I suppose now I'll have to
get the scanner working...
