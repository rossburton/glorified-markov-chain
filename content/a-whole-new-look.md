Title: A Whole New Look
Date: 2002-09-04 19:00
Tags: tech
Slug: a-whole-new-look

A new style for a new site. I found the [EFF's blog]() and decided to
shamelessly steal their sexy stylesheet, which turns out to be
<cite>Georgia Blue</cite> from [Movable
Type](http://www.movabletype.com./). Thus, this site is very
<abbr title="Extensible Hypertext Markup Language">XHTML</abbr> +
<abbr title="Cascading Style Sheets">CSS2</abbr> heavy, so if you don't
have a modern browser you're missing out on the goodies. I also advise
the Microsoft Web Fonts, and a browser capable of anti-aliasing.
However, if you have a fetish for Lynx at least the content will be in a
reasonable order...

Anyway I should introduce some of the content on this site...

<div class="title">

Devil's Pie

</div>

I am writing a program to find and act upon windows as they are created
in X. Basically, its a re-implementation of the Matched Windows concept
in Sawfish, for Metacity. Version 0.2, the first release, will be
available soon.

<div class="title">

Debian on the IBM X22

</div>

I run Debian Sid on my IBM ThinkPad X22. <span class="extended">[Read
how I did it here](/computing/debian-on-x22.html)</span>.

<div class="title">

Themes

</div>

My GNOME2 theme is on the [GNOME2 screen
shots](http://www.gnome.org/start/2.0/screenshots/) site, and
unfortunately I had my email address showing in a window... I am
currently packaging the parts I have made (and finding the originals of
the parts I stole) into a sane state, and will upload it here.

<div class="title">

GTK/Python

</div>

Recently I have been playing with the GTK+ 2 bindings for Python. I have
written a few little toys and one of the good things which resulted was
a `GtkTreeModel` which can look just like a regular Python list to the
developer. Recent advances in the bindings may mean that this is
redundant now, but it can be [downloaded from
here](python/TreeModelListAdaptor.py).
