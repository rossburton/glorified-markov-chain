Title: ABI Breaking Madness
Date: 2005-07-29 11:54
Tags: tech
Slug: abi-breaking-madness

Everything I see at the moment is tinted red from The Rage...

I've just spent two days debugging Evolution and the DBus data server.
When I click on a contact in the Address Book wth the DBus backend,
Evolution crashes in a very odd manner. Stack traces are a little odd,
and Valgrind's output is just plain confusing. It can't be anything
directly related to DBus as there is no IPC occuring at this point, and
the memory management in the DBus port is if anything better than the
Bonobo version. Eventually I figure out what is going on.

A few weeks ago a new field was added to `EContact`, to store the URI of
the book the contact belongs too. This seems reasonable enough, just add
the new item at the end of the enumeration and all is good. However,
EContact does some magic for optimisation and the enumeration is sorted
by type and the position of key items in the enumeration is stored, such
as the location of the last simple string, or the location of the last
list. This means that the new field was added as the third item in the
enumeration...

Yes, really.

Also the library versioning wasn't changed, so older Evolution builds
just carry on as normal. Now when an older Evolution asks the contact
for the list of all email addresses, it expects to get a GList, which it
promptly tries to iterate over. It was actually passed a structure
representing the contact's full name, and when it tried to iterate over
that as a list... all hell breaks loose.

I really hope this somehow gets sorted out before 2.12...

<small>NP: <cite>Best Of</cite>, R.E.M. (the 90's Best Of, not the 2003
one)</small>
