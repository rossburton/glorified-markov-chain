Title: "About Freakin Time"
Date: 2008-01-30 11:12
Tags: life
Slug: about-freakin-time

> “NHS primary care trusts are slashing funding for homoeopathic
> treatment amid debate about its efficacy and the drive to cuts costs,
> a study has suggested.”

As Dan so succinctly put it, [about freakin
time](http://news.bbc.co.uk/1/hi/health/7215470.stm). That said, the BBC
are still a little wooly on the scientific side of things:

> “...and some scientists argue the solution is so diluted it does not
> contain any active ingredients at all.”

Both sides generally agree it doesn't contain any of the active
ingredient, that's pretty much the entire point of homoeopathy verses
conventional medicine or poison (depending on what the active ingredient
is). Scientists point out that a remedy can't do anything if there is
nothing but water in it, homoeopaths insist that water has a mysterious
(and bounded, unless tap water in old houses doubles as the homoeopathic
remedy <cite>Plumbum Metallicum</cite>) memory which makes it magically
work.
