Title: Additions
Date: 2006-01-06 20:24
Tags: life
Slug: additions

New year, new colleagues: [Iain Holmes](http://blogs.gnome.org/iain) and
[Tomas Frydrych](http://www.qoheleth.uklinux.net/blog/) joined [Opened
Hand](http://www.o-hand.com) this week. It's been great to watch Opened
Hand expand over the months as when I joined I was the second employee.
I think we're a pretty great team now,
[Matthew](http://www.butterfeet.org/blog/) has done well!
