Title: AirPlay/UPnP Synergy
Date: 2011-04-19 12:00
Tags: tech
Slug: airplayupnp-synergy

You know, it would be really good if someone could take
[ShairPort](https://github.com/albertz/shairport), glue that to
`gst-rtsp-server`, and then implement the Rygel
[MediaServer](https://live.gnome.org/Rygel/MediaServer2Spec)
specification, letting me play music from my iPhone on my Raumfeld UPnP
speakers.

I'd actually like this so much that I'm willing to put up some of my own
hard cash to see it happen. Are there sites that will let people pledge
money towards projects like this?

<small>NP: <cite>Central Reservation</cite>, Beth Orton</small>
