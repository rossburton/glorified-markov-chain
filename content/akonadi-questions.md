Title: Akonadi Questions
Date: 2008-01-30 10:30
Tags: tech
Slug: akonadi-questions

I've recently been looking at Akonadi again, and trying to understand
its design goals, implementation, and so on. The documentation on the
web site is pretty thin on the ground, so I have a few questions which
I'd love any friendly Akonadi developers to reply to (note that I'm
biased towards the address book side for now).

1.  IPC. Akonadi uses IMAP for most operations, with DBus used for
    notifications and other "control" messages apparently. As IMAP
    supports notifications fine, why not drop DBus entirely? To use
    Akonadi there has to be an IMAP connection, correct?
2.  Data format. How is something concrete, like a contact, represented?
    In what format would it be stored in the "local addressbook", and in
    what format is it transferred over IMAP?
3.  Dependencies. For a GNOME component C++ I can just about handle, Qt
    is pushing it, and libkde is out. What are the real dependencies of
    Akonadi, and can they be reduced?
4.  Examples. Can anyone provide example code of basic operations
    against the addressbook, such as searches, handling live views,
    adding and removing contacts?

Thanks!

<small>NP: <cite>Artists Like: Skalpel</cite>, Last.fm</small>
