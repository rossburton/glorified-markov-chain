Title: All Change Non Stop
Date: 2004-10-15 00:00
Tags: life
Slug: all-change-non-stop

Well, what a few weeks this has been. Work has been mental with the
porting work charging ahead, which I managed to pass onto other people
as the core became functional. I've mostly been working on the
documentation system, previously we had LaTeX and generated HTML API
docs, now we have an all-singing DocBook build process. In all this I
switched servers for `burtonini.com` (thanks Thom!) and my s3krit
project is going well.

Amusing things have been going on in the world around us. George Bush
Snr. called Michael Moore a ["total ass,
slimeball"](http://news.bbc.co.uk/1/hi/entertainment/film/3741954.stm);
Great Ormond St. Hospital may [sue
Disney](http://edition.cnn.com/2004/SHOWBIZ/books/10/13/peter.pan/index.html)
over copyright violation; and a private (!) voter registration company
has been [throwing away registration cards from
Democrats](http://www.klas-tv.com/Global/story.asp?S=2421595&nav=168XRvNe).

Just for fun, last week the company who we had our wedding gift list
with, [The Gift Registry](http://thegiftregistry.info/), went into
administration. As a result we've only got about a third of the gifts we
should have, and we've got to contact everyone to sort this out.

Finally, a word from Dubya: "Mars is essentially in the same orbit...
Mars is somewhat the same distance from the Sun, which is very
important. We have seen pictures where there are canals, we believe, and
water. If there is water, that means there is oxygen. If oxygen, that
means we can breathe." \[update: turns out Dan Quayle said this\]

<small>NP: <cite>Dummy</cite>, Portishead</small>
