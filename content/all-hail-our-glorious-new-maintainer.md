Title: All Hail Our Glorious New Maintainer
Date: 2008-12-10 16:55
Tags: tech
Slug: all-hail-our-glorious-new-maintainer

Or, Contact Lookup Applet 0.17 is now released. Some bug fixes and
features thanks to the core widget being used in Nautilus Send-To:

-   Pass future maintainership to Bastien Nocera
-   Don't search unopened books
-   Automatically detect sources (Bastien Nocera)
-   Only error out if all the addressbooks failed to open (BN)
-   Show one menu item for each e-mail address, and select by default in
    the contact details dialogue (BN)

The tarball is here:
[contact-lookup-applet-0.17.tar.gz](http://www.burtonini.com/computing/contact-lookup-applet-0.17.tar.gz).
