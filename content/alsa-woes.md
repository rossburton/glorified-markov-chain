Title: ALSA Woes
Date: 2006-08-09 12:20
Tags: tech
Slug: alsa-woes

Has anyone seen this, and know a solution, for ALSA on a ThinkPad X60:

    $ esd
    ALSA lib pcm_direct.c:786:(snd_pcm_direct_initialize_slave) snd_pcm_hw_params_any failed
    ALSA lib pcm_dmix.c:831:(snd_pcm_dmix_open) unable to initialize slave

The sound card is a standard Intel chipset:

    0000:00:1b.0 0403: Intel Corporation 82801G (ICH7 Family) High Definition Audio Controller (rev 02)
