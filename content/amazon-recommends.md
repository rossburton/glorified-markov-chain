Title: Amazon Recommends...
Date: 2004-03-22 00:00
Tags: life
Slug: amazon-recommends

Sometimes Amazon's recommendations are great ("hey, this band is
great!"), sometimes disturbing ("I bought a great album, why are you
recommending Blue!") and sometimes [very
amusing](http://www.burtonini.com/computing/screenshots/amazon-black-books.png).

NP: Personal Journals, Sage Francis
