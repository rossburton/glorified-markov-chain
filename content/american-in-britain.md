Title: American In Britain
Date: 2006-07-24 12:30
Tags: life
Slug: american-in-britain

> “Britain is often said to be part of "the United Kingdom", along with
> several other countries, including England. Ireland is also nearby,
> and is considered part of Scotland, which, in turn, is adjacent to,
> and included in, a small country called Wales. To first-time visitors
> this can be confusing, especially when one learns that - paradoxically
> - France is considered by the British to be its very own nation!”

A snipped from the first article in [a series in <cite>The
Guardian</cite>](http://books.guardian.co.uk/departments/generalfiction/story/0,,1826279,00.html).
It continues later:

> “The traveller must, of course, always be cautious of the overly broad
> generalisation. But I am an American, and a paucity of data does not
> stop me from making sweeping, vague, conceptual statements and, if
> necessary, following these statements up with troops.”

<small>NP: <cite>Neighbour Radio</cite>, Last.fm</small>
