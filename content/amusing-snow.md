Title: Amusing Snow
Date: 2004-03-10 00:00
Tags: life
Slug: amusing-snow

Snow has made me laugh recently, in different ways.

This morning it had decided to snow, great huge fluffy flakes coming
down into our garden. Totally unexpected (but I didn't look at the
weather report last night), and it wasn't settling at all, but it did
seem rather out of place. From inside it looked just a bit cold outside,
and the ground wasn't really wet, so these massive flakes were coming
down and having no effect whatsoever.

Of course, I get to <s>Mordor</s>Croydon, and the snowing has stopped and
we're left with sub-Arctic winds instead. Joy.

The other snow was Jon Snow, of [Channel 4
News](http://www.channel4.com/news/). Four of the British Guantanamo Bay
detainees were released last night without charges by the US government,
yet deal they did with the British government was that they had to be
treated as serious detainees on arrival. This meant the police had to
drive a van *into* the aircraft so they could be taken into custody
straight away, without the opportunity to wave to the surrounding
journalists. This is because (quoting from Jon) "...the US still
considers them guilty of whatever they haven't charged them with".

Wonderful. Two years in a camp, without actually being arrested, and no
charges bought against them but *they are still guilty*. Of what? After
two years you would think they'd have something on them.

Who knows, maybe they have something on them and are not telling anyone
but our government, who now has them in custody. My guess is that they
won't actually use the Terrorism Act 2000 and only keep them for a few
days, rather than the full 14 days they can under the act. After all,
everyone is now watching like a hawk.

NP: When It Falls, Zero 7
