Title: ...And Relax
Date: 2003-03-31 00:00
Tags: life
Slug: and-relax

It's nearly over. Vicky handed in her dissertation last Friday, all
thats left is 6 weeks of revision (or as she calls it 'learning'). We we
both tired over the weekend, and it turns out V was more stressed than
she thought -- the day after she handed it in a cold promptly developed
-- there appears to be a link between the removal of stress and illness.

Rugby. Need I say more? This year I am trying to watch more rugby, and
the Six Nations is a good series. The final was England v Ireland, but
Ireland didn't do very well... the score being 42-6 to England. I still
don't understand all of the rules, but the players appear to so that's
okay. I do however understand eight grown (in most cases rather
overgrown) men jumping on top of each other, kicking and shouting,
trying to get a funny shaped ball out of the writhing pile so someone
else can kick it towards what is possibly the highest goal posts in any
sport.

Which reminds me: whilst in the pub on Saturday we were talking about
how some sportmans skills transfer into other sports. For example, a
good British 200m sprinter (can't recall his name), is also a
bobsleigher. I was thinking: if a rugby player can make one of those
odd-shaped balls go where he wants it... how good will his shot be with
a perfectly round ball! Will we be able to define new measures of
straightness from the England rubgy team? `:)`

In other news, Sound Juicer in GNOME CVS now works and IBM finally
paided me for my article.
