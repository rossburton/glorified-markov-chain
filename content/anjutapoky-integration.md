Title: Anjuta+Poky Integration
Date: 2008-05-20 17:40
Tags: tech
Slug: anjutapoky-integration

Yesterday I tested and rolled a new release of the [Poky integration
plugin for Anjuta](http://labs.o-hand.com/anjuta-poky-sdk-plugin/),
created by our fearless [Sir Bradford](http://www.robster.org.uk/). This
is a very special piece of magic which lets you use a Poky SDK in Anjuta
to cross-compile binaries without any pain, and will even deploy,
execute and debug the binaries in a QEMU for testing. As part of the
release process I had to test it, so I'll step through what I did as a
brief tutorial on how to use Anjuta with Poky.

The prerequisites are Anjuta, the Anjuta Poky SDK plugin, and QEMU.
These are all available for installation from our [Debian
repository](http://debian.o-hand.com) for Debian/Ubuntu users, everyone
else will have to build from source, sorry! You'll also need a Poky ARM
SDK and QEMU ARM images from the [Poky](http://pokylinux.org) web site.
The SDK is a tarball which contains a cross compiler with base libraries
(glibc, GTK+, and so on) and should be extracted onto your machine (it
extracts the SDK into `/usr/local/poky`). The QEMU image consists of a
kernel and a `ext3` file system which will boot Poky inside QEMU.

To start I fetched a checkout of
[Tasks](http://pimlico-project.org/tasks.html) and loaded up Anjuta. I
don't have an existing Anjuta project for Tasks, so I used <cite>File →
New → Project From Existing Sources</cite> to create a project using the
checkout. At this point I could do native development using <cite>Build
→ Run Configure</cite> and <cite>Build → Build Project</cite> to
configure and compile the source, but we want to cross-compile.

To activate cross compiling go to <cite>Edit → Preferences → General →
Installed Plugins</cite> and enable the <cite>Poky SDK</cite> plugin.
This will add a new page <cite>Poky SDK</cite> to the preferences
dialog. We're using an external toolchain so set the SDK root to
`/usr/local/poky/eabi-glibc/arm` and the toolchain triplet to
`arm-poky-linux-gnueabi`. We're also using QEMU instead of a real device
so set the paths to the kernel and root filesystem (remembering to
uncompress the filesystem). We're now done configuring, so the
preferences dialog can be closed. However notice that if you switch from
using a SDK to building with a full Poky tree you can use the
cross-compiler it produces directly, and you can also use an external
device instead of QEMU: the only requirement is that you can SSH into
it.

Now to do the build. Use <cite>Build → Run Configure</cite> to configure
Tasks, passing any extra options you want. Note that if you want to
debug your build in the future you'll need to enter `CFLAGS=-g` here to
disable optimisation (autoconf sets `-O2 -g` by default, which isn't
useful for debugging). The configure script is then ran with the right
environment and options for cross compiling, and with any luck will
successfully configure. Then hit <cite>Build → Build Project</cite> and
watch the cross-compiler do its thing. When that has worked, you can
prove to yourself that the right thing has happened.

    $ file tasks
      tasks: ELF 32-bit LSB executable, ARM, version 1 (SYSV), for GNU/Linux 2.6.14, dynamically linked (uses shared libs), not stripped

We have an ARM binary, ready for deployment. Start the virtual machine
with <cite>Tools → Start QEMU</cite> (this may ask for your root
password to configure networking) and once it has booted you can install
the project into the VM with <cite>Tools → Deploy</cite>. This will run
`make install` to a temporary directory and then rsync it to the VM. Now
you can either interact with the VM directly (if the application
installed a new desktop file, then it should appear on the desktop), or
use <cite>Tools → Run Remote</cite> to execute a binary directly:
entering `tasks` will execute the freshly installed Tasks. Neat, huh?

For the final trick there is even GDB integration. <cite>Tools → Debug
Remote</cite> will let you specify a local binary (to extract debug
symbols from, say `src/gtk/tasks`) and a remote binary to run, and then
start a GDB on the VM and connect to it. The binary will be initially
running but paused at the entrypoint, so you can add breakpoints and
then `continue` execution.

Hopefully this post has been a good overview of the integration
available between Poky and Anjuta. In the future I hope to see Nemiver
integrated into Anjuta, and gdbserver support in Nemiver, which would be
a killer combination for Poky integration.

<small>NP: <cite>One On Twoism</cite>, Various</small>
