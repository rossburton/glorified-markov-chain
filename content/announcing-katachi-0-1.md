Title: Announcing Katachi 0.1
Date: 2007-07-22 20:50
Tags: tech
Slug: announcing-katachi-0-1

Over the last few months I've been hacking on (yet another) image viewer
for GTK+, using the hot new GVFS library (go Alex!) for asynchronous
file handling and GtkImageView because I'm lazy.

It's got a pretty lean interface at the moment and is fairly fast in
use. My goal is to use it on my Zaurus for reviewing images from a CF
card in the field, so performance is quite important to me. As the
primary users are photographers, filenames are not shown in the
interface (just thumbnails). There is a lot of work left to do, but I've
used 0.1 for some time now.

The source is being developed in a Bazaar branch at
<http://burtonini.com/bzr/katachi>. I've just tagged a 0.1 release, a
[tarball of which is
here](http://burtonini.com/computing/katachi-0.1.tar.gz). You'll need to
build GVFS from git, and GtkImageView from svn, sorry. :)

<small>NP: <cite>Closes Volume 1</cite>, Boards of Canada</small>
