Title: Announcing Pimlico
Date: 2007-04-13 10:00
Tags: tech
Slug: announcing-pimlico

![Pimlico](http://burtonini.com/images/pimlico-logo.png)

OpenedHand have announced [Pimlico](http://pimlico-project.org/), an
umbrella project for our PIM applications: Contacts, Dates, Tasks and
Sync (in progress).

Released at the same time was [Dates
0.4](http://www.pimlico-project.org/dates.html): this release is a vast
improvement on the previous release and now available in three flavours:
desktop, Maemo, and OpenMoko.

At the same time we sneaked out an updated set of
`evolution-data-server` packages for Maemo 3.0. As far as the user is
concerned the only changes are that addressbook syncing will work (using
OpenSync or SyncEvolution), and that WebCal is supported (as used by
Dates).

<small>NP: <cite>Urban Biology</cite>, Machine Drum</small>
