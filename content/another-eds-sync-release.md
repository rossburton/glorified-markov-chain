Title: Another eds-sync Release
Date: 2007-05-31 16:10
Tags: tech
Slug: another-eds-sync-release

We've just released [eds-sync
2.20070531](https://garage.maemo.org/projects/eds/), with some hot new
features.

-   Allow building against a stock evolution-data-server 1.10
-   Handle duplicate contacts correctly
-   Batch contact commits if possible (requires eds-dbus)
-   Don't work on the same contact multiple times
-   Remove code that exists in Glib
-   Expire the inspect cache when handles are removed
-   Set the alias when adding a contact to the roster

I've uploaded a tarball to Maemo Garage which should be visible soon,
but for now there is a tagged release in Subversion.
