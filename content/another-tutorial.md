Title: Another Tutorial
Date: 2006-03-02 17:45
Tags: tech
Slug: another-tutorial

I just found [Yet Another Devil's Pie
Tutorial](http://wiki.foosel.net/linux/devilspie) online. That puts the
count of unofficial tutorials up to three... obviously people out there
want to document Devil's Pie but everyone is doing it in their own
corner of the world, so I've just added a skeleton structure to [the
Devil's Pie wiki page](http://live.gnome.org/DevilsPie). If anyone out
there has a passion for Devil's Pie and wants to start adding
documentation for the configuration file format, feel free!

<small>NP: <cite>Groove Salad</cite>, SomaFM</small>
