Title: Another XICC Update
Date: 2007-11-28 17:45
Tags: tech
Slug: another-xicc-update

Another quick [XICC](http://burtonini.com/blog/computers/xicc/) update:
Jon has just committed XICC support to Inkscape. Thanks Jon!

In other news there are patches floating around to clarify multihead
usage, because my wording was pretty terrible. I'll merge these this
week, honest.
