Title: Arbitrary Content
Date: 2003-12-04 00:00
Tags: life
Slug: arbitrary-content

Quick diary update: feeling a little less tired which is good. Feeling
like a cold is slowing working its way into my head, which is bad.

Got more fabulous birthday presents -- Simpsons, [Hot
Shots!](http://uk.imdb.com/title/tt0102059/) and [circa 1980 Billy
Connolly](http://www.amazon.co.uk/exec/obidos/ASIN/B0000DG5MU/) on DVD,
a pornographic heat sensitive mug (classy...) and a lovely bottle of
whiskey (actually classy), [Last Chance To
See](http://www.amazon.co.uk/exec/obidos/ASIN/0330320025/) and [Three
Men In A Boat](http://www.amazon.co.uk/exec/obidos/ASIN/0140621334/). I
think that was all... but I probably forgot something and insulted
someone.

And now, the news update:

-   [Bad Sex Prize winner
    announced](http://books.guardian.co.uk/news/articles/0,6109,1099417,00.html)
-   [Daily Mail on the cutting edge as
    usual](http://media.guardian.co.uk/newmedia/story/0,7496,1099598,00.html)
-   [The freest nation on
    Earth?](http://www.guardian.co.uk/guantanamo/story/0,13743,1098618,00.html)

And finally, everyone should [Share the
Groove](http://www.sharingthegroove.org/). Online music trading, by
artists who don't object to recorded gigs being distributed for non
profit. Currently listening to an excellent Otis Reading gig, and
yesterday I grabbed Beth Orton at the Royal Albert Hall and Coldplay at
La Hanger.

**Update:** as expected I forgot to mention that I also got [Westwood
Platinum](http://www.amazon.co.uk/exec/obidos/ASIN/B0000E65XY) from
Bastien. Some "slamming joints" (apparently) and Westwood being, well,
himself. Also Share The Groove has an, erm, lazy-evaluated list of
artists who don't mind their gigs being distributed. In that they share
everyones, and restrict artists who complain.
