Title: Asynchronous Flickr Library, version 0.1
Date: 2007-01-07 19:30
Tags: tech
Slug: asynchronous-flickr-library-version-0-1

I'm finally releasing version 0.1 of my Flickr library, `flickrpc`. The
name is actually misleading, and I should change it for 1.0, as the
class name is `flickrest` and the transport protocol is HTTP/REST, not
XML-RPC (as was the original plan, thus the name). This library is
written in Python and uses Twisted.

Why another Flickr library?

:   Because Postr required it. All of the existing libraries that I know
    of are blocking (I used to use Beej's `flickrapi`, which was based
    upon Michele Campeotto's `flickrclient`), which makes using them in
    GUI applications painful. Early versions of Postr used threads so
    that uploading could be done in the background without blocking the
    UI, but as Postr makes more calls to Flickr whilst being used
    interactively these threads will become a maintaince nightmare. By
    using Twisted all of the network operations are handled in the main
    loop, and I can write callback-based network code just like I write
    callback-based UI code.

Why Twisted?

:   Because it works, and is popular. If someone has a HTTP client
    library that integrated into the GLib mainloop and will expose
    callbacks when operations complete, please tell me. I love what
    Twisted gives me, but at the end of the day if there is a simplier
    solution, I'll consider it.

Sounds excellent! Where can I get it?

:   There is a tarball of 0.1 available here:
    [flickrpc-0.1.tar.gz](http://burtonini.com/computing/flickrpc-0.1.tar.gz).
    If you want to track the latest development, or contribute patches,
    there is also [a Bazaar
    archive](http://burtonini.com/bzr/flickrpc/).


