Title: Asynchronous Flickr Library, version 0.2
Date: 2007-02-07 11:10
Tags: tech
Slug: asynchronous-flickr-library-version-0-2

Flickrpc 0.2 is released. This has several improvements:

-   Re-license to LGPL
-   Don't use an intermediate deferred, instead chain them (thanks
    Andrew Bennetts)
-   Clean up errback handlers (thanks Andrew Bennetts)
-   Try to import xml.etree for Python 2.5

Basically no new features but it now works on Python 2.5 without an
external ElementTree, the code is cleaner, and it's LGPL instead of GPL.
Grab a [tarball
here](http://burtonini.com/computing/flickrpc-0.2.tar.gz) or the [bzr
tree here](http://burtonini.com/bzr/flickrpc).

<small>NP: <cite>The Good, The Bad, And The Queen</cite></small>
