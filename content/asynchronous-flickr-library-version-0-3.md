Title: Asynchronous Flickr Library, version 0.3
Date: 2008-11-11 21:50
Tags: tech
Slug: asynchronous-flickr-library-version-0-3

*Finally*, Flickrpc 0.3 is released. Some nice features that we all know
and love from Postr here:

-   Proxy support
-   Add more upload arguments: safety, privacy, public/friends/private,
    search\_hidden
-   Cache the users full name, username and NSID (jcrosby)
-   Fix UTF-8 encoding problems
-   Verify our cached token before using it

Grab a [tarball here](http://burtonini.com/computing/flickrpc-0.3.tgz)
or the [Bazaar tree here](http://burtonini.com/bzr/flickrpc).
