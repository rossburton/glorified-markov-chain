Title: Audioscrobbler / Last.fm
Date: 2005-09-15 10:35
Tags: tech
Slug: audioscrobbler-last-fm

Like the sad geek I am I've wanted to use
[Audioscrobbler](http://www.last.fm) (now Last.fm) for some time now,
but I don't really use a media player on a computer to listen to music,
but a hifi with real CDs. This poses a bit of a problem as there isn't a
way of uploading playback data without using a media player... until now
that is.

With a little Python and MusicBrainz magic, and an Audioscrobbler upload
implementation by the mighty [Matt Biddulph](http://www.hackdiary.com),
`cdscrobbler` can submit an album as if you just played it from either a
MusicBrainz album ID or by reading the CD from your computer's CD-ROM
drive.

I've been using it for [a few weeks
now](http://www.last.fm/user/rossburton/) and it's been working great,
so I'm releasing it to the world. At the moment it's only available via
Bazaar from `http://burtonini.com/arch/cdscrobbler--MAIN--0/`, but I'll
make tarballs at some point I'm sure.

<small>NP: <cite>Vertigo</cite>, Groove Armada</small>
