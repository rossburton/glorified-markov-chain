Title: Autotools
Date: 2006-08-24 15:30
Tags: tech
Slug: autotools

I don't know what changed, but recently autotools are refusing to let me
run `configure` when the tree is already configured. I have to run
`make distclean`. Of course if I've started `configure` via an
`autogen`...

    [sbox-ARM: ~/source/osso-addressbook] > make distclean
    /bin/sh ./config.status --recheck
    running /bin/sh /home/ross/realhome/Programming/scratchbox/osso-addressbook/configure  --prefix=/usr  --no-create --no-recursion
    checking for a BSD-compatible install... /scratchbox/tools/bin/install -c
    checking whether build environment is sane... yes
    checking for gawk... gawk
    checking whether make sets $(MAKE)... yes
    configure: error: source directory already configured; run "make distclean" there first
    make: *** [config.status] Error 1

Stab stab kill die die!!

<small>NP: <cite>Zwischen zwei und einer Sekunde</cite>,
krill.minima</small>
