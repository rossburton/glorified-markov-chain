Title: Baby Killing Leaders
Date: 2004-09-09 00:00
Tags: life
Slug: baby-killing-leaders

Todd Berman over on Planet GNOME (or here, if you are reading this
there) recently [blogged about his
thoughts](http://codeblogs.ximian.com/blogs/tberman/archives/000329.html)
on the "War On Terror", and [again replying
to](http://codeblogs.ximian.com/blogs/tberman/archives/000330.html) a
readers comments, albeit in a slightly less calm manner. Good reads.

Given the amount of anti-Bush protests, demonstrations, people and
events going on in America I don't understand how opinion polls still
put Bush and Kerry at \~50/50. Something is seriously wrong over there
guys, and it needs to be fixed. Sadly I think Kerry winning this
election won't be enough, for two reasons: the hard-right nutters
politicians will still be around and the Democrats are still pretty
right-wing when compared to some other countries.

Yes, I'm a tree-hugging nut-eating hippy. The highlight of yesterday was
reading the excellent <cite>The Wrap</cite> from <cite>The
Guardian</cite>, in which they slated <cite>The Daily Mail</cite> (a
tabloid with aspirations of intelligence, meaning serif type) for
running horror stories about how "half your income goes on taxes",
assuming you earn £50400 a year. [I
quote](http://www.dailymail.co.uk/pages/live/articles/news/newscomment.html?in_article_id=316805&in_page_id=1787):

> But what is so unfair about the vast majority of New Labour's tax
> rises is the people they punish. The hard-working, home-owning,
> pension-building middle classes are ruthlessly targeted again and
> again, while the feckless and welfare-dependent are allowed to benefit
> from the Government's commitment to 'social inclusion'.

Erm, yes. **That was the point**.

<small>NP: <cite>Blue For You</cite>, Nina Simone</small>
