Title: Back
Date: 2006-06-05 12:55
Tags: life
Slug: back-2

Last week Vicky and myself went to Paris for a brief holiday (Tuesday to
Saturday). The weather wasn't as great as we were hoping for the
beginning of June, but at least it didn't rain too hard. Paris was nicer
than expected, but I'll blog about that more when I've sorted the
photos.

<small>NP: <cite>Motion</cite>, The Cinematic Orchestra</small>
