Title: Back Breaking
Date: 2004-04-19 00:00
Tags: life
Slug: back-breaking

The last few days have been quite fun. On Thursday I worked from home as
we had to go to the Registar Office and plan the wedding ceremony,
deciding what we are going to say, picking music and so on.

Saturday Vicky was away having a hair consultation so I went down to
London to pick up my suit, freshly fitted (I wish my school trousers
were as comfortable as these ones are!) and met Caroline for lunch. Had
a good afternoon with her, walking around London and stuffing ourselves
with toasted ciabattas. On the way back home I started to watch [Bubba
Ho-Tep](http://www.bubbahotep.com/), which is *hilarious*. That night
with a Chinese we watched [Identity](http://imdb.com/title/tt0309698/)
which was good, better than I expected. Shame we had to take it back
really, I'd like to watch it again.

On Sunday our new TV finally turned up -- which is very cool. I won't
bore everyone with pictures of it, it's a Sony widescreen and looks
exactly the same as all other Sony widescreens. After a minor panic --
our old stand is a great size and holds the VCR, DVD, CD and amplifier
but the new one is "trendy" so only old two items so argh where to put
everything -- we're getting there. Amusingly the box it came in has
cartoons showing how to hold it (it's not light and very front-heavy),
but the staples had left marks which look just like perspiration,
accurately portraying us trying to get the damn thing on the stand... We
"tested" it by watching [High
Fidelity](http://imdb.com/title/tt0146882/), which is still a great
film. Possibly one of my Top 5 All Time Music Related Films...

NP: <cite>Lamb</cite>, by Lamb. Still need better headphones with more
bass.
