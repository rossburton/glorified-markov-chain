Title: Back Home
Date: 2005-07-14 15:50
Tags: life
Slug: back-home

As many have noticed, I'm back from Debconf already. It was a flying
visit mostly taken up with meetings, so I didn't get to meet anyone new
really. However, [Scott](http://www.keybuk.com) still holds the record
for flying visits: a single day at GUADEC, and he flew by Learjet.
Obviously, I suck at both being at Debconf and flying visits.

The weather in Finland appears to be a secret they keep from the world,
I'm sure on the weather reports you never hear of Helsinki being in the
30s. They probably tell everyone Finland is always cold and dark to keep
the tourists away from their beaches.

For anyone else out there with a new ThinkPad which throws a hissy fit
when it resumes with `SectorNotFound` errors, the solution is to turn
off the Hard Drive Pre-Desktop Area (or something like that) in the
BIOS, under Security. Good old Matthew Garrett has a patch and I presume
it will reach mainline kernels shortly. Now suspend/resume fails for
other reasons, but Matthew knows about those too.

Da Boss Matthew introduced me to [Iittala](http://www.iittala.com/)
whilst on a wander around Helsinki to find food, a contemporary Finnish
design store selling homeware (cups, vases, etc). Very cool stuff.

Helsink airport is a terrible, terrible place, for two main reasons.
One: there are no power points in the lounge which is not good as I only
had 5% charge left. Two: the Stockman sells a large amount of Iittala
stuff... A surprisingly low number of Euros later I bought two espresso
cups and a large bowl.

![Espresso](http://www.burtonini.com/photos/Misc/espresso.jpg)

Very, very cool.

Now I best get on with work, I'm away in Cornwall all next week and have
a *huge* patch to review.

<small>NP: <cite>Asia Borninch</cite>, DJ Shadow</small>
