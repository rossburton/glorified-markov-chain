Title: Back Pain
Date: 2003-08-29 00:00
Tags: life
Slug: back-pain

Ouch, ouch, and if I may say it again, ouch. For some reason I still
don't know, my back went to spasm last weekend. Because of this I've
been at home, spending my time taking pain killers (aspirin, ibuprofen,
paracetamol and codeine) and trying to relax my back. This is not fun.
On the plus side, I've caught up with my DVD backlog: I've finished
<cite>Babylon 5</cite> series 2, started <cite>Bottom</cite> series 1,
and watched <cite>Minority Report</cite>, <cite>The Usual
Suspects</cite> and <cite>Terminator 2</cite>.

There are other good points to sitting around at home in pain all day:
last night there was a power cut in central London. I'm not sure exactly
when it was but I think it would have caught me just as I was going
through London, which would have been a right pain. There were still
queues of people outside train stations last night at 22:00...

Last week Bastien stayed over Saturday night as his flight from France
didn't land at Stansted until gone 23:00. It was good to see him again,
and we got free wine and some French hip-hop. What more could I ask for.
:)

In other news, our Notice of Marriage has been announced and the
authority is ready. Yay, it's legal for us to get married!
