Title: Back!
Date: 2005-03-24 02:18
Tags: life
Slug: back

It's 2:18 and we've just got back from India. We're knackered, it's
quarter to eight in the morning India time and we've been travelling
since midday yesterday. The \~550 photos can wait until tomorrow, we're
off to bed.
