Title: Bag Man
Date: 2005-02-24 17:01
Tags: life
Slug: bag-man

Just call me The Bag Man. Last weekend we bought some decent [Samsonite
suitcases](http://www.samsonite.com/gb/en/local_product_display.jsp?product=168*066),
and today both my new camera case, a [Lowepro Nova
3](http://www.lowepro.com/Products/Shoulder_Bags/classic/Nova_3_AW.aspx),
and my new general bag from [Timbuk2](http://www.timbuk2.com) (it's [one
of
these](http://www.timbuk2.com/tb2/catalog/productlisting.t2?skuSetIdStr=23&categoryId=6),
but customised slightly) arrived.

Bags rock! Especially bags in cool colours with nice details like nets
and chunky zips and pockets...

<small>NP: <cite>Buena Vista Social Club</cite></small>
