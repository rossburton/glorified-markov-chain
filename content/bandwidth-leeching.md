Title: Bandwidth Leeching
Date: 2004-12-02 00:00
Tags: tech
Slug: bandwidth-leeching

I loaded up [Visitors](http://www.hping.org/visitors/) today on
`burtonini.com` to see what was going on, and discovered that I've got
12000 referrals from a single domain, which I didn't recognise. A few
minutes later, I discovered that several users on `xanga.com` are using
photos I have taken as web page backgrounds, from my site.

A quick google later, and I've got some `mod_rewrite` magic going on.
Get off my land!

<small>NP: <cite>Keep It Solid Steel</cite>, Mr. Scruff</small>
