Title: Ban(e)d Fish Water
Date: 2004-03-21 00:00
Tags: life
Slug: baned-fish-water

Got back from work Friday evening find Griswald, one of our fish, laying
on the bottom of the tank. No idea why he died, he was the youngest of
the fish, so I hope the others (Talula and Sid) are not going to die
soon too.

The band we've hired for our wedding reception also called Friday
afternoon...

> "Hi, this is Alex from Indigo Jazz. We've a small problem... the
> singer and her drummer husband are emigrating to Australia."

And up to now the planning was going so well... and now it starts. They
are auditioning new band members now so we should be okay...

Finally, Coca Cola are [making a right
mess](http://www.guardian.co.uk/business/story/0,3604,1174127,00.html)
out of selling Peckham Springs. Fooling people.
