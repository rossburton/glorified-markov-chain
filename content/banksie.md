Title: Banksie
Date: 2004-01-01 00:00
Tags: life
Slug: banksie

Whilst updating my Amazon ratings, I noticed that Iain \[M.\] Banks has
a new book listed, cunningly titled ["SF
Novel"](http://www.amazon.co.uk/exec/obidos/ASIN/1841491551).

Now that <cite>The Culture</cite> is dead (the fanzine that is), and
`alt.books.iain-banks` isn't what it used to be, I have no idea what
this book is about. Anybody know?
