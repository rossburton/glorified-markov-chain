Title: Bastards
Date: 2003-09-29 00:00
Tags: life
Slug: bastards

The annoying ~~bas~~*people-of-questionable-parentage* signal
controllers at Liverpool Street station have taken to playing Music
Chairs with us, saying a train will arrive on platform 2 until about a
minute before it is due to leave, when it then turns up on platform 5.
Cue a rush-hour train-load of people charging between platforms.

Hell hath no fury as a woman scorn. Or a commuter whose minidisc player
decides to stop playing mid-track.
