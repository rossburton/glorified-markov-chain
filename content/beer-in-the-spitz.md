Title: Beer in The Spitz
Date: 2004-02-18 00:00
Tags: life
Slug: beer-in-the-spitz

Last night I went to [The Spitz](http://www.spitz.co.uk) (on Old
Spitalfields Market) for a pint or two and to meet some fellow GNOMEies.
Mikael Hallendal is in London for a few days before going to FOSDEM,
staying at Matthew Allum's house in east London, and Matthew Garrett
came down from Cambridge. Mikael appeared to find London positively
balmy after Sweden, and was sitting *outside* (well, inside the market,
but it's not exactly heated). I think he is enjoying the lack of frozen
marshes to fall into.

(I am totally blaming the stress caused by double signal failure on the
way there which lead to me mis-hearing "Old Speckled Hen" as "Uzbek 10".
Maybe I should get my ears looked at, or take up steet yoga, or
something; but now I think about it Uzbek 10 does sound like a cool name
for a wine. If I ever open a vineyard, look out for it.)

Had a good night, was nice to see Matthew (Garrett) and Mikael again,
and Matthew (Allum) is cool too. They tried (and failed) to convince me
to drop everything and go to FOSDEM this weekend, but I did promise to
look at getting to GVADEC, despite the problem of having used most of my
holiday already.

NP: The Mirror Conspiracy, by The Thievery Corporation
