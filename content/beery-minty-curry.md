Title: Beery Minty Curry
Date: 2004-09-22 00:00
Tags: life
Slug: beery-minty-curry

I originally intended to write this on Monday, but I'm sure everyone
knows that blogging about having a beer isn't always at the top of the
To Do list.

Last Friday we went down the pub with Allen and Dave for a few pints.
Good fun night, even had cold Newcastle Brown which is quite rare, and
Allen won the award for Most Outrageous Statement that week. Vicky
mis-heard and still was in shock, when she was corrected she nearly
cried laughing. Don't worry mate, it's our secret \[nudge nudge wink
wink\].

On Saturday they came over in the evening for a solid night of drinking,
curry and films. Specifically:

Drinking
:   Red wine, lager (Asahi, Cobra and Honeywaggle), followed
    by Absinthe. Well, Allen and Dave had a small shot each, foul stuff.

Curry
:   Onion bahjees, lamb tikka masala, vege korma, lamb garlic chilla
    tikka, chicken tikka jalfrezi, rice and peshwari naans.

Films
:   We started the evening off on a low by watching <cite>The X
    Factor</cite>. The least said about that the better, to be honest.
    This was followed by <cite>Shaun Of The Dead</cite> which is
    **excellent**, <cite>Donni Darko</cite> which is also great, most of
    <cite>Deuce Bigalo: Male Gigolo</cite> (amusing, especially after
    the above) and <cite>A Fish Called Wanda</cite>.

Allen and Dave slept over in the front room, thankfully they were alive
the next day and had failed to vomit into household appliances, so after
breakfast and showing Allen some <cite>Spaced</cite>, fancying a bit of
change we went... to see <cite>Anchorman</cite> at the cinema! Not a
great film, and some bits dragged on, but the rest of the film is pretty
good. My verdict is to go see it when it's cheap.

<small>NP: <cite>The Godfather: Best of James Brown</cite></small>
