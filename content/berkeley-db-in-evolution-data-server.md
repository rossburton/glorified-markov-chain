Title: Berkeley DB in Evolution Data Server
Date: 2006-04-24 12:26
Tags: tech
Slug: berkeley-db-in-evolution-data-server

    ------- Comment #13 from Devashish Sharma  2006-04-24 11:07 UTC -------
    Patch committed to cvs head.

</p>
Always a good thing to see, and especially so when it's [this
patch](http://bugzilla.gnome.org/show_bug.cgi?id=319393). The patch
allows Evolution Data Server to be built with a system install of
Berkeley DB, instead of always with the copy shipped in EDS. This is
designed for systems where the version of Berkeley DB won't change over
time, such as embedded systems or Debian-derived distributions (which
allows multiple versions of `libdb` to be installed). There are several
reasons for doing this:

-   Security: bug fixes and security updates don't need to replicated.
    If there is a security fix in Berkeley DB it doesn't need be
    back-ported and applied to the copied code in EDS.
-   Performance: EDS ships with Berkeley DB 4.1, but Berkeley DB 4.3
    works, and is faster.
-   Memory Usage: this is the fun one. In a standard EDS build, both the
    "file" addressbook backend and `libedataserver` link to Berkeley DB.
    However, as the library is an embedded copy and isn't installed, it
    is statically linked. Yes, a 600Kb library is statically linked into
    EDS *twice*.

As you can imagine, I'm glad this patch has finally landed upstream!

<small>NP: <cite>Protection</cite>, Massive Attack</small>
