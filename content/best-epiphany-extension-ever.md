Title: Best Epiphany Extension Ever
Date: 2007-11-12 12:19
Tags: tech
Slug: best-epiphany-extension-ever

<cite>[Confirm Window
Close](http://www.sstuhr.dk/epiphany-extensions/#ext-confirmwindowclose)</cite>.
Potentially the best Epiphany extension *ever* (well, second best after
my [Post To
Delicious](http://burtonini.com/blog/computers/ephydeli-2006-11-01-10-47)
extension)

<small>NP: <cite>Trouser Jazz</cite>, Mr Scruff</small>
