Title: Better bash completion?
Date: 2014-02-05 11:55
Slug: better-bash-completion

Bash completion is great and everything, but I spend more time than is
advisable dealing with numerous timestamped files.

    $ mv core-image-sato-qemux86-64-20140204[tab]
    core-image-sato-qemux86-64-20140204194448.rootfs.ext3
    core-image-sato-qemux86-64-20140204202414.rootfs.ext3
    core-image-sato-qemux86-64-20140204203642.rootfs.ext3

This isn't an obvious choice as I now need to remember long sequences of
numbers. Does anyone know if bash can be told to highlight the bit I'm
being asked to pick from, something like this:

    $ mv core-image-sato-qemux86-64-20140204[tab]
    core-image-sato-qemux86-64-20140204194448.rootfs.ext3
    core-image-sato-qemux86-64-20140204202414.rootfs.ext3
    core-image-sato-qemux86-64-20140204203642.rootfs.ext3
