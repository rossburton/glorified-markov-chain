Title: Birthday!
Date: 2003-11-26 12:00
Tags: life
Slug: birthday

Today I'm officially old -- one quarter of a century old.

I took today off and it has been a good day so far -- Vicky got me a
funky black shirt and a very nice DKNY watch, and I also got *Dude,
Where's My County?* and £100 in vouchers. I finally picked up *The Two
Towers* (which has been sitting in the post office), and am currently
watching more of *Babylon 5* series 3. Life isn't bad despite the
weather.
