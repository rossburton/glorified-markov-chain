Title: Blizzards
Date: 2005-01-24 00:00
Tags: life
Slug: blizzards

I was very impressed with the sheer amount of snow in America I could
see on the news and [Nat's
blog](http://nat.org/2005/january/#23-January-2005), but never thought
such a scene would happen in my own back yard. Ladies and gentleman,
brace yourself for a photo of The Great Blizzard of 2005:

![The Great Blizzard Of
2005](http://www.burtonini.com/photos/Misc/blizzard.jpg){}

Brrr. It gives me the shivers just thinking about it. What a nightmare
getting supplies will be...

<small>NP: <cite>Ray Of Light</cite>, Madonna</small>
