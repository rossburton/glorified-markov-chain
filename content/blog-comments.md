Title: Blog Comments
Date: 2006-01-30 10:55
Tags: tech
Slug: blog-comments

This morning before work I hacked up a few Pyblosxom plugins to do word
blacklisting and a basic Turing test on comment posters. The blacklist
currently only contains 'phentermine', 'poker', 'xanax', and 'viagra',
but that should filter out 90% of the comment spam. For the rest, the
Turing test asks the poster to add together two small numbers. The lazy
bit is that these numbers are hard-coded at the moment, I'll add that
later...

Hopefully I haven't broken something and if people pass these tests then
comments actually work, if not the please [email
me](mailto:ross@burtonini.com).

I'll release the plugins later this week when I've added documentation
and ensured they work.

<small>NP: <cite>In Between Dreams</cite>, Jack Johnson</small>
