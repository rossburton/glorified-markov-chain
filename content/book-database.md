Title: Book Database
Date: 2003-12-27 00:00
Tags: life
Slug: book-database

I was about to quickly catalogue the books and DVDs we got for
Christmas, but after a 30-second Google I can't find an online book
database. If I wanted to provide links to movies I'd use the wonderful
[Internet Movie Database](http://www.imdb.com), does anyone know a
database for fiction and non-fiction books?
