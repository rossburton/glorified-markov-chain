Title: Book Meme
Date: 2004-05-03 00:00
Tags: life
Slug: book-meme

The instructions are: Grab the nearest book, open it to page 23, find
the 5^th^ sentence, post the text of the sentence in your journal along
with these instructions.

> <cite> "Security was maintained by a huge standing army consisting,
> according to one account, of 9000 elephants, 30000 calvary and 600000
> infantry." </cite>
