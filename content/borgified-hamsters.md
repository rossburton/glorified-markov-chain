Title: Borgified Hamsters
Date: 2006-03-15 17:35
Tags: life
Slug: borgified-hamsters

Yesterday's news but still pretty cool (and grim at the same time):
injecting peptides into a hamster's damaged brain [caused spontaneous
repair and recovery of
vision](http://www.guardian.co.uk/science/story/0,,1730298,00.html?gusrc=rss)
(from The Guardian).

In other news there are [three new Henry pictures
online](http://www.burtonini.com/photos/Henry). Everyone say
<cite>awww!</cite>

<small>NP: <cite>Lamb Remixed</cite>, Lamb. Thanks Ploum!</small>
