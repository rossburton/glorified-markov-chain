Title: bprobe
Date: 2006-12-01 10:40
Tags: tech
Slug: bprobe

Wow. Behdad makes a casual mention to the [coolest hack in the
world](http://cvs.gnome.org/viewcvs/bprobe/). Yes, there are better
solutions but they are heavy, this is quick and lean. Love it! The only
problem is that typically GTK+ is built with PLT hacks, so the probes
cannot intercept the calls. With a GTK+ built without the PLT
manipulations, this should do exactly what you expect:

    #include <gtk/gtk.h>
    #include <bprobe.h>
    PKG_CONFIG_CFLAGS(gtk+-2.0)

    PROBE
    void
    gtk_widget_show (GtkWidget *w)
    {
            LOG ("gtk_widget_show");
            return SUPER(gtk_widget_show) (w);
    }

<small>NP: <cite>Londinium</cite>, Archive</small>
