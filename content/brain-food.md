Title: Brain Food
Date: 2005-05-19 12:21
Tags: life
Slug: brain-food

As a child I was often told that fish is brain food. Something to do
with it being a good source of protein. Well, I love fish, but it
doesn't seem to help me when I need it, for example when chasing a
bizarre memory corruption bug. However, I did find that this was useful
to me when chasing memory leaks in Evolution:

![Cookie](http://www.burtonini.com/photos/Misc/brain-food.jpg)

*That* is brain food my friends.

Oh, and sorry for spamming the various planets this morning. The
server's software RAID had a bit of a kerfuffle last night and although
everything was recovered (props to Thom and Paul) the mtimes were all
changed...

<small>NP: <cite>Across A Wire</cite>, Counting Crows</small>
