Title: Brain Gym
Date: 2008-04-10 11:00
Tags: life
Slug: brain-gym

> “Man the lifeboats. The idiots are winning. Last week I watched,
> open-mouthed, a Newsnight piece on the spread of "Brain Gym" in
> British schools. I'd read about Brain Gym before - a few years back,
> in Ben Goldacre's excellent Bad Science column for this newspaper -
> but seeing it in action really twisted my rage dial.”

[Charlie Brooker in The
Guardian](http://www.guardian.co.uk/commentisfree/2008/apr/07/education)
gets deservedly angry over Brain Gym, after seeing an article about it
on Newsnight ([1](http://youtube.com/watch?v=M5rH7kDcFpc),
[2](http://youtube.com/watch?v=YjRhYP5faTU) on YouTube). The creator of
Brain Gym was *destroyed* by Paxman, rather too easily to be honest.

<small>NP: <cite>Voices</cite>, Vangelis (via Last.fm)</small>
