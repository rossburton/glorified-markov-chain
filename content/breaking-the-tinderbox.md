Title: Breaking The Tinderbox
Date: 2005-06-13 08:00
Tags: tech
Slug: breaking-the-tinderbox

As annoying as [breaking the tinderbox builds of GNOME
is](http://tieguy.org/blog/index.cgi/383), knowing that Colin's DBus
mega-patch (at the last check the patch was over 280Kb) that I've been
working on is finally committed is far more satisfying. Now to merge
current CVS into my local branch and send Colin another large patch to
review...
