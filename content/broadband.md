Title: Broadband
Date: 2003-09-19 00:00
Tags: tech
Slug: broadband

Last Monday was The Big Day for broadband to finally reach us... but it
didn't. I spent most of Monday swearing at dodgy phone lines, cheap
modems, Microsoft, drivers which don't work, upgrades which break
everything, and talking dogs in adverts.

A BT engineer was called out Thursday, who remarked at what a great line
I have, and how broken the modem is. Oh well, at least I hadn't paid for
it yet. Sitting above me now is a brand-new all-singing all-dancing wifi
ADSL router, which should allow surfing whilst on the toilet. Oh this
will be fun...
