Title: Bugs Fixed
Date: 2005-06-22 16:05
Tags: tech
Slug: bugs-fixed

If you want to tell me that SJ says "Cannot extract CD: file exists",
then I've fixed the bug in CVS. Either build from CVS, or apply [this
patch](http://cvs.gnome.org/viewcvs/sound-juicer/src/sj-extracting.c?r1=1.57&r2=1.59&makepatch=1&diff_format=h)
first.

Though this is a major bug which stopped it ripping more than a single
track, so **you have no excuse**. I've only had two mails so far, there
should be more!

<small>NP: <cite>Buena Vista Social Club</cite></small>
