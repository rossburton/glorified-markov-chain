Title: Building Debian Packages In Scratchbox
Date: 2006-04-11 16:53
Tags: tech
Slug: building-debian-packages-in-scratchbox

Every week I spend half an hour wondering why I can't build Debian
packages inside Scratchbox. The error is "you must be root" which is
solved with `fakeroot`, but I'm already running `debuild` with
`fakeroot`...

That, of course, is the problem. `debuild` will clean the environment
before starting `dpkg-buildpackage`, which wipes all traces of the
Scratchbox magic away, breaking `fakeroot`. Despite `debuild` being an
essential wrapper around `dpkg-buildpackage` in the normal Debian world,
in a Scratchbox it's a big no no. I really must remember this!

<small>NP: <cite>Debut</cite>, Björk</small>
