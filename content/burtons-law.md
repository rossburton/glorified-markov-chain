Title: Burton's Law
Date: 2007-08-13 19:26
Tags: life
Slug: burtons-law

Can I propose Burton's Law, a variation on [Godwin's
Law](http://en.wikipedia.org/wiki/Godwin%27s_Law):

> As a discussion about climate change grows longer, the probability of
> stating that China is building a [new coal power station every
> week](http://www.google.com/search?q=china+building+week+coal+power+station)
> approaches one.

When used as a defense for not attempting to reduce climate change I'd
like to also invoke the sudden death variant, where the discussion is
finished immediately.
