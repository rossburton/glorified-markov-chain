Title: Busy Christmas
Date: 2003-12-21 00:00
Tags: life
Slug: busy-christmas

It's been rather busy the last few days -- too busy to blog at least.

Last Thursday (the 18^th^) I took off ill, with a cold which is coming
and going like the tides. Friday was the work Christmas meal, at the
wonderful [Blue Elephant](http://www.blueelephant.com/london/) on Fulham
Broadway. I arrived a little worn out as I had already walked from
London Bridge to Temple with my first black and white film, snapping en
route. The Thames Path is a nice walk, one day in the summer I shall
have to load up with a pile of film and do it from end to end.

Blue Elephant was excellent as usual (this is our third time there I
think) and I staggered out at 2130, full and rather drunk on Harvey
Wallbanger's, red wine and a few glasses of Laphroaig. Discovered that I
had [Dan's](http://danalderman.co.uk) present still in my bag. Phoned
Rob at 2330 to tell him that he forgot to take the present from my bag,
and discovered he was still in the Blue Elephant... glad to hear [my
boss](http://www.winder.org.uk) was there too, relaxing for what is
probably the first time for quite a while...

Saturday involved nearly-last-minute Christmas shopping and then going
into London to see [Sexie](http://www.eddieizzard.com/). Eddie Izzard
was as glorious as ever, in his leather mini-skirt, black boots, and
breasts. Possibly the best sight of the evening was the touts selling
tickets for "a man in a dress" outside Wembley.

Sunday mainly involved large amounts of sleep, and more
nearly-last-minute shopping. Hopefully I've got everything for Vicky
now, but there is still a few more days left of shopping in case... In a
few minutes friends will start arriving and we'll be off for the now
traditional Christmas Chinese Meal. Hopefully the Thai at the Blue
Elephant won't have accustomed my taste buds too much.

NP: [Out of
Season](http://www.amazon.co.uk/exec/obidos/ASIN/B00006ZSAD), Beth
Gibbons and Rustin' Man. How festive can I get?
