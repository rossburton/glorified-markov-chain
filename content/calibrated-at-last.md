Title: Calibrated At Last
Date: 2007-11-14 17:00
Tags: tech
Slug: calibrated-at-last

Today I finally got a Spyder2 Express monitor calibration kit, and
calibrated my ThinkPad's laptop. I've known it to be shockingly badly
calibrated (although it is an improvement over the X20), but was curious
as to how much it can be corrected.

One word: wow. Thanks to Graeme Gill et al of
[ArgyllCMS](http://www.argyllcms.com/) fame for reverse-engineering how
the Spyder2 works, and making calibrating displays reasonably easy to
do. I'll probably write up what I did at some point, because if you are
like me and don't read the documentation there are a number of stumbling
points.

I also blame Jakub entirely for making me think ArgyllCMS was broken by
picking slightly warm grey tones for Darkilouche. My freshly calibrated
screen looked a little warm instead of the usual very cool, and it took
me a good ten minutes to think about checking that the theme is actually
pure grey.

<small>NP: <cite>Blue Skied An' Clear - A Morr Music
Compilation</cite></small>
