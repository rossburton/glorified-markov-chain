Title: Camera Whore
Date: 2006-06-30 17:14
Tags: tech
Slug: camera-whore

    ross@flashheart ~/Pictures/Photos/Incoming/guadec
    $ du; ls | wc -l
    692M    .
    292

Daniel "Camera Whore" Stone stole my camera and wouldn't give it back.
Two things to note: 1) I predict Daniel will have an EOS 350D within the
month, and 2) when at GUADEC, bring a battery charger and twice as much
CF as you think you'd need, just in case. Daniel was only stopped due to
the battery dying.

I should blog more about GUADEC but in my usual style I'll leave it
until I can't recall anything. Instead I'll just mention my favourite
quote of the week: “GUADEC has been homo-erotic and informative. But
mostly homo-erotic”. Thanks, if I recall correctly, to Fiona.
Personally, I have [no
idea](http://www.flickr.com/photos/87701098@N00/177637207/) what she is
[talking about](http://www.flickr.com/photos/87701098@N00/177637210/).
