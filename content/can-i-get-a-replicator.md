Title: Can I Get A Replicator?
Date: 2008-02-20 10:30
Tags: tech
Slug: can-i-get-a-replicator

There is a Sound Juicer bug which has been reported in 2.20 and 2.21,
where changing the album artist name doesn't change the name that is
used when creating directories ([link to
bug](http://bugzilla.gnome.org/show_bug.cgi?id=515699)). Thinking about
the code this is very strange, and I can't replicate it myself, so I
have to ask the Planets. Has anyone else seen this, or can they
replicate this?

<small>NP: <cite>Workmen digging up my road the bastards</cite>, Thames
Water ([photo](http://flickr.com/photos/rossburton/2278676189/))</small>
