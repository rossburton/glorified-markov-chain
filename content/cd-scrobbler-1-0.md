Title: CD Scrobbler 1.0
Date: 2005-10-24 10:55
Tags: tech
Slug: cd-scrobbler-1-0

I finally got around to cleaning up my Audioscrobbler upload script, so
I am announcing CD Scrobbler 1.0 (a bold version I admit). A tarball is
[here](http://www.burtonini.com/computing/cdscrobbler-1.0.tar.gz),
you'll need a recent Python (I know it works with 2.4, 2.3 should) and
the MusicBrainz module.

<small>NP: <cite>Hold Your Colour</cite>, Pendulum</small>
