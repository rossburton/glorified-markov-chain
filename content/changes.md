Title: Changes
Date: 2005-09-28 14:35
Tags: life
Slug: changes

Ch-ch-ch-changes, in the immortal words of David Bowie.

So [Dan](http://danalderman.co.uk/) pestered me to blog about something
which isn't Sound Juicer or Devil's Pie, so here I go. What's been going
on recently? Quite a bit really, and it can be summarised in three
words: house, dog, chicken.

House. We've finally found a house which isn't too small, or too
expensive, or too damn minging (and we saw plenty of those) to move
into, so we're finally about to step onto the property ladder. Hopefully
we'll be moving in sometime before Christmas, assuming everything goes
to plan with the mortgage, surveys, etc. The house was build around the
50s, has a 40' garden, good size lounge, two bedrooms and a
kitchen/dinner. Oh, and the foulest carpet in the front room imaginable
by man.

Dog. To make a mess of our new house and eat all of our new furniture
we're also trying to find a Cocker Spaniel puppy to buy around New Year.
There are several potentials at the moment and if it's male, he'll be
called Henry. I hope he is male, as we've totally failed to think of a
good name of a female dog...

Chickens. Many years ago when we were living in South Norwood Vicky saw
a documentary about people who rescue dying chickens from battery farms,
and nurse them back to health. After a little treatment and care, they
generally recover and will live for many more years. Vicky really wanted
to do this, but a third floor flat isn't the idea place to keep
chickens. Now that we are moving into a house with a decent sized garden
the dream of rescuing chickens was revived, and co-incidentally a friend
told us about [Omlet](http://www.omlet.co.uk), who make home-friendly
chicken houses amusingly called "eglus". This is also on the wishlist
now, although Vicky won't let me name the chickens Fried and Roast...

<small>NP: <cite>The Essential Billie Holiday</cite></small>
