Title: Cheapo MP3 Player
Date: 2008-02-19 15:16
Tags: tech
Slug: cheapo-mp3-player

Today I ordered what is almost the [cheapest MP3
player](http://www.ebuyer.com/product/132943) I could find, and is
certainly the cheapest I could find which wouldn't fall off when jogging
(as much as I love my black iPod Classic, it's not really suitable for
jogging). I *really* hope that it reads MP3 files from a FAT partition
and uses USB Storage...

<small>NP: <cite>Mishaps Happening</cite>, Quantic</small>
