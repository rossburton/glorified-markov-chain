Title: Christmas (nearly) Over
Date: 2003-12-27 00:00
Tags: life
Slug: christmas-nearly-over

Back home again after a few days at Vicky's mum's house, which was good
fun. Lots of cool presents (funky shirts, DVDs, books) and lovely food
-- a wonderful tuna steak on the 25^th^, and salmon on the 26^th^
(Vicky's family is vegetarian).

Christmas good news included a rebate from overpaying the phone bill,
and my sister getting engaged. Good luck to her and Elliott!

In two hours time I'll be out again for a few days, off to my mum's to
see them for a while. Tomorrow will be my second Christmas, which will
be fun. ;-)

Finally, I just got some colour and B&W photos back from the developers
(Boots do reasonable B&W developing for £8), I'll try and get them
scanned on my Dad's scanner. I have a horrible feelings certain pictures
of me will be the incentive Vicky needs to start creating her own web
site...
