Title: Class
Date: 2005-08-10 10:52
Tags: life
Slug: class

[Banksy](http://www.banksy.co.uk) strikes again, this time [in the West
Bank](http://feed.proteinos.com/item/3216). Sheer class.

<small>NP: <cite>Feed Me Weird Things</cite>, Squarepusher</small>
