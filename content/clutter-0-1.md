Title: Clutter 0.1
Date: 2006-06-22 17:55
Tags: tech
Slug: clutter-0-1

It's getting late, I'm starting to think I'm getting RSI, and I need a
coffee, so instead of saying how great Clutter is I'll just [link to
what Matthew said already](http://o-hand.com/blog/?p=10).

Go Clutter!

<small>NP: <cite>Foolish Games</cite>, Jewel</small>
