Title: Code Dump
Date: 2010-11-13 07:59
Tags: tech
Slug: code-dump

I finally got around to clearing out my \~/Programming and publishing a
number of the silly toy projects I've built up over the years that might
be useful to someone, somewhere. A brief overview of what I've basically
thrown over the wall to GitHub:

[flickrest](https://github.com/rossburton/flickrest)
:   A Python/Twisted library for the Flickr API. This was written for
    use in Postr, although I suspect now that I don't maintain Postr any
    more they have forked. Maybe now this is in Git we can merge
    any changes.

[evo-known-contact](https://github.com/rossburton/evo-known-contact)
:   A small tool I wrote for someone years ago that takes an
    RFC2822-formatted email on stdin, extracts the sender, and sets the
    exit code depending on whether that email address is in the
    address book.

[feednotify](https://github.com/rossburton/feednotify)
:   Display notifications when a RSS feed is updated.

[Zebu](https://github.com/rossburton/zebu)
:   A tool to manage Debian `chroots` using `cowbuilder`.

[Tumblrss](https://github.com/rossburton/tumblrss)
:   Screen-scrape your Tumblr dashboard and generate a RSS feed. This
    has bitrotted but was very useful.

[gupnp-scrobbler](https://github.com/rossburton/gupnp-scrobbler)
:   Listen to announcements over UPnP of music being played and submit
    the tracks to Last.fm.

[ephy-gupnp](https://github.com/rossburton/ephy-gupnp)
:   Dynamically generate bookmarks from UPnP devices that expose a
    web interface. Probably doesn't work with recent Epiphany releases
    because I've switched to Chrome.

[ephydeli](https://github.com/rossburton/ephydeli)
:   An action to add the current page to Delicious.com. Probably doesn't
    work with recent Epiphany releases because I've switched to Chrome
    *and* Pinboard.

[eds-tools](https://github.com/rossburton/eds-tools)
:   Some tools I wrote when working on EDS such as a dummy addressbook
    backend and command-line access to the `libebook` API.

[cdscrobbler](https://github.com/rossburton/cdscrobbler)
:   Submit the current CD (or an arbitrary MusicBrainz album ID) to
    Last.fm as if you'd just finished playing it.

Out of all of these hacks I only actively use flickrest and Zebu now, so
I wouldn't be surprised if there is some serious bitrot in the others.
Hopefully something here is useful to someone, somewhere though!
