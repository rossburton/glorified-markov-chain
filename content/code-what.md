Title: Code What?
Date: 2003-05-14 00:00
Tags: life
Slug: code-what

My employer, [OneEighty Software](http://www.180sw.com/), has a stand at
this years Embedded Systems Show. Another day of standing around, trying
to attract the attention of people walking past and answering difficult
questions... for my bosses. I have 0% interest in the
sales/marketing/bullshit side.

Of course we all have exhibitor passes so we can get in and walk around
if we want, affectionately labelled "Code Monkey" instead of "Software
Engineer". At least that's what I thought, until I saw someone else's...
*mine* is labelled "Code Monkey" and everyone else is "Software
Engineer".

I'm not entirely sure how to take this... `:)` In a good way I think,
but... am I just a monkey who can code?
