Title: Coffee Help Needed
Date: 2005-02-28 16:47
Tags: life
Slug: coffee-help-needed

I'm currently addicted to Monsoon Malabar coffee, be it from
[Wittards](http://www.whittard.co.uk/cgi-bin/whittard.filereader?42234cff01b6412c273fd43a3b830656+EN/products/1330B)
or
[Taylors](http://www.taylorscoffee.co.uk/con_Coffee.asp?storyid=%7B44751786%2DF520%2D4168%2DA03B%2D30F14920646B%7D).
It's a wonderfully rich, earthy coffee which tackles waking me up in the
morning as well as it keeps me going in the afternoon. However, it's not
Fair Trade: although both Wittards and Taylors claim their coffee is
“ethically sourced” they do separate Fair Trade branded products.

So, the help. Can anyone name a Fair Trade source of Monsoon Malabar, or
something similar?

<small>NP: <cite>Creating Patterns</cite>, 4 Hero</small>
