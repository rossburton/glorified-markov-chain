Title: Colour Calibrated EoG Patch
Date: 2005-06-12 12:58
Tags: tech
Slug: colour-calibrated-eog-patch

I've now fixed the few small problems with my Little CMS patch for Eye
Of Gnome, and it is now a lot faster and actually correct (previously it
was adjusting most of the pixels hundreds of times...) If you want to
give it a go, the patch is [available
here](http://www.burtonini.com/computing/eog-cms-20050612.diff). It
currently uses sRGB as the display profile and only creates profiles on
the fly from embedded whitepoint and primary chomatic information, but
will not read embeded ICC profiles. This is due to me scratching an
itch, Canon EOS cameras don't embed a profile when they are using Adobe
RGB.

If anyone out there has some feedback on this issue, feel free to edit
the [Colour Management](http://live.gnome.org/ColourManagement) page on
the GNOME wiki.
