Title: Colour-Calibrated Eye of Gnome
Date: 2005-06-10 10:42
Tags: tech
Slug: colour-calibrated-eye-of-gnome-2

Last night I got fed up of manually converting photos from my camera
from Adobe RGB to sRGB for display (I had a long blog entry about this
but it got lost...), that I decided to try and hack colour space support
into EOG.

Working on the scratching an itch principle, my EOG will now check a
JPEG image to see if it contains any embedded white point and primary
colour coefficients. If it does, a ICC profile is generated from the
data (using a gamma of 2.2) and the image transformed to sRGB. It's a
little slow at the moment as I'm doing it row at a time, and it has a
habit of crashing on the last row but one (no idea why), but if I tell
it to transform only half of the image the results are impressive:

![Eye of Gnome colour-correcting a
photo](http://www.burtonini.com/computing/screenshots/eog-icc.png)

The next stage once the bugs are fixed is to make the code optional, and
to handle embedded ICC profiles.

<small>NP: <cite>F♯A♯ ∞</cite>, Godspeed! You Black Emporer</small>
