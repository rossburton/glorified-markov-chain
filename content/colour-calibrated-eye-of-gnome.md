Title: Colour-Calibrated Eye Of Gnome
Date: 2005-06-16 09:57
Tags: tech
Slug: colour-calibrated-eye-of-gnome

A few days of early-morning hacking later, and I've got a
proof-of-concept ICC system working. It's very simple but works well for
me.

The first step is to associate an ICC profile to each screen. This can
be done using `xicc`, from
[`xicc-0.1.tar.gz`](http://www.burtonini.com/computing/xicc-0.1.tar.gz).
Simply run this specifiying the path to the relevant ICC profile and it
will set the atom on the root window of the default screen (multi-screen
support will be implemented next). For example:

>     $ xicc ~/tmp/ICM/ibmtplcd.icm

`ibmtplcd.icm` is the profile for the IBM ThinkPad LCDs.

Then patch Eye Of Gnome with [this
patch](http://www.burtonini.com/computing/eog-cms-20050616.diff). Now
when EoG opens a JPEG image which describes the colour space using the
standard EXIF properties `White Point` and `Primary Chromaticities` it
will create a profile on the fly, and when the image is displayed the
profile for the screen is obtained and the image corrected.

There is a lot to do yet:

-   Read embedded ICC profiles from JPEG and PNG files
-   Read the whitepoint/chromatic information from PNG files
-   Cache the profile from the screen
-   Make `xicc` multi-screen aware

But it does work! I'll write a small specification for the atom I'm
using shortly.

<small>NP: <cite>Mrs. Cruff</cite>, Mr. Scruff</small>
