Title: Come And Get It!
Date: 2006-08-20 16:50
Tags: life
Slug: come-and-get-it

In an attempt to clear out some of the stuff I never use, I've put a
Thinkpad charger and a Lowepro camera bag on eBay.

!(http://burtonini.com/photos/Misc/ebay-charger.jpg)

[IBM ThinkPad charger](http://cgi.ebay.co.uk/ws/eBayISAPI.dll?ViewItem&item=160020206649).
This will charge most of the ThinkPads in existence, I don't know when
it was introduced but I got it for my ThinkPad X22 (bought in 2001), and
has since been used with an X32, a T40p, and a T43. Basically if you
have a ThinkPad and the charger is not a brand new 20V Lenovo model (?60
range only as far as I know), this is what you want. This retailed at
£50.

!(http://burtonini.com/photos/Misc/ebay-bag.jpg)

[Lowepro Nova 3 AW camera bag](http://cgi.ebay.co.uk/ws/eBayISAPI.dll?ViewItem&item=160020205655).
Good condition, excellent quality camera bag from Lowepro, capable of
storing an SLR, 3-4 lenses, flash, filters, charger, and more. The bag
is is dark grey and black. I've been using it to hold a Canon EOS-300D,
18-55mm, 50mm, 28-135mm, a Speedlite 380EX, cables and a number of
filters. This retailed at £45.

<small>NP: <cite>Boys for Pele</cite>, Tori Amos</small>
