Title: Company Meal 2003 Walkabout
Date: 2003-12-29 00:00
Tags: life
Slug: company-meal-2003-walkabout

On the 19^th^ December, it was [our company
meal](http://www.burtonini.com/blog/life/busy-20031221.xhtml), I took
the scenic route with my camera and took a few photos en route. It was
not a lovely sunny day as you will see...

Liverpool Street train station, nice and quiet.

[![Liverpool Street](http://www.burtonini.com/photos/Xmas2003Walkabout/thumb-01-liverpool-street.png "Liverpool Street")](http://www.burtonini.com/photos/Xmas2003Walkabout/01-liverpool-street.jpg)

Just outside Liverpool St. station, and possibly the strangest location
for a Christmas tree I know of.

[![Savoy Taylors](http://www.burtonini.com/photos/Xmas2003Walkabout/thumb-02-savoy-tailors.png)](http://www.burtonini.com/photos/Xmas2003Walkabout/02-savoy-tailors.jpg)
[![Moorgate](http://www.burtonini.com/photos/Xmas2003Walkabout/thumb-03-moorgate.png)](http://www.burtonini.com/photos/Xmas2003Walkabout/03-moorgate.jpg)

This is just off Moorgate, there is a large old building with its roof
*covered* in satellite dishes. Here they are reflected in the
neighbouring office block. Possibly would have worked better with B&W
film.

[![Dish reflection](http://www.burtonini.com/photos/Xmas2003Walkabout/thumb-04-dish-reflection.png)](http://www.burtonini.com/photos/Xmas2003Walkabout/04-dish-reflection.jpg)

Me, reflected in a mirror inside Moorgate tube station. Sadly I was
actually wearing a black coat, there is a rather blue cast on the
photos.

[![Moorgate mirror](http://www.burtonini.com/photos/Xmas2003Walkabout/thumb-05-moorgate-mirror.png)](http://www.burtonini.com/photos/Xmas2003Walkabout/05-moorgate-mirror.jpg)

Around London Bridge. Old school and 60's architecture crammed into a
small space.

[![Denmark House](http://www.burtonini.com/photos/Xmas2003Walkabout/thumb-06-denmark-house.png)](http://www.burtonini.com/photos/Xmas2003Walkabout/06-denmark-house.jpg)
[![Colechurch](http://www.burtonini.com/photos/Xmas2003Walkabout/thumb-07-colechurch.png)](http://www.burtonini.com/photos/Xmas2003Walkabout/07-colechurch.jpg)

Tower Bridge from London Bridge, the [Millennium
Bridge](http://www.arup.com/MillenniumBridge/) and the [Tate
Modern](http://www.tate.org.uk/modern/), and a underground section of
the [Thames Path](http://www.thames-path.co.uk/). Spot when I swapped to
black and white film.

[![Thames](http://www.burtonini.com/photos/Xmas2003Walkabout/thumb-08-thames.png)](http://www.burtonini.com/photos/Xmas2003Walkabout/08-thames.jpg)
[![Tate](http://www.burtonini.com/photos/Xmas2003Walkabout/thumb-11-bridge-tate.png)](http://www.burtonini.com/photos/Xmas2003Walkabout/11-bridge-tate.jpg)
[![Thames Path](http://www.burtonini.com/photos/Xmas2003Walkabout/thumb-09-thames-path.png)](http://www.burtonini.com/photos/Xmas2003Walkabout/09-thames-path.jpg)

A funky piece of bark from along the Embankment, the [London
Eye](http://www.londoneye.com/) across the river, and Temple tube
station.

[![Bark](http://www.burtonini.com/photos/Xmas2003Walkabout/thumb-12-bark.png)](http://www.burtonini.com/photos/Xmas2003Walkabout/12-bark.jpg)
[![Thames Eye](http://www.burtonini.com/photos/Xmas2003Walkabout/thumb-10-thames-eye.png)](http://www.burtonini.com/photos/Xmas2003Walkabout/10-thames-eye.jpg)
[![Temple](http://www.burtonini.com/photos/Xmas2003Walkabout/thumb-13-temple.png)](http://www.burtonini.com/photos/Xmas2003Walkabout/13-temple.jpg)

Now some photos of my colleagues at work.

Aradna, Alvin, John, Russel.

[![Aradna](http://www.burtonini.com/photos/Xmas2003Walkabout/thumb-aradna.png)](http://www.burtonini.com/photos/Xmas2003Walkabout/aradna.jpg)
[![Alvin](http://www.burtonini.com/photos/Xmas2003Walkabout/thumb-alvin.png)](http://www.burtonini.com/photos/Xmas2003Walkabout/alvin.jpg)
[![John](http://www.burtonini.com/photos/Xmas2003Walkabout/thumb-john.png)](http://www.burtonini.com/photos/Xmas2003Walkabout/john.jpg)
[![Russel](http://www.burtonini.com/photos/Xmas2003Walkabout/thumb-russel.png)](http://www.burtonini.com/photos/Xmas2003Walkabout/russel.jpg)

Ric, Peter, Rob.

[![Ric](http://www.burtonini.com/photos/Xmas2003Walkabout/thumb-ric.png)](http://www.burtonini.com/photos/Xmas2003Walkabout/ric.jpg)
[![Peter](http://www.burtonini.com/photos/Xmas2003Walkabout/thumb-peter.png)](http://www.burtonini.com/photos/Xmas2003Walkabout/peter.jpg)
[![Rob](http://www.burtonini.com/photos/Xmas2003Walkabout/thumb-rob.png)](http://www.burtonini.com/photos/Xmas2003Walkabout/rob.jpg)

Helen and John; Ric, Helen and Russel; Helen and Russel.

[![Helen and John](http://www.burtonini.com/photos/Xmas2003Walkabout/thumb-helen-john.png)](http://www.burtonini.com/photos/Xmas2003Walkabout/helen-john.jpg)
[![Ric, Helen, Russel](http://www.burtonini.com/photos/Xmas2003Walkabout/thumb-ric-helen-russel.png)](http://www.burtonini.com/photos/Xmas2003Walkabout/ric-helen-russel.jpg)
[![Helen and Russel](http://www.burtonini.com/photos/Xmas2003Walkabout/thumb-helen-russel.png)](http://www.burtonini.com/photos/Xmas2003Walkabout/helen-russel.jpg)

And finally, a cuddly pig.

[![Pig](http://www.burtonini.com/photos/Xmas2003Walkabout/thumb-pig.png)](http://www.burtonini.com/photos/Xmas2003Walkabout/pig.jpg)
