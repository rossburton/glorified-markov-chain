Title: Completed Plans
Date: 2005-05-20 12:19
Tags: tech
Slug: completed-plans

Not only does Bonobo now use approximately 130Kb less memory (as
measured by pmap) thanks to the work of [Alex and
co](http://bugzilla.gnome.org/show_bug.cgi?id=168948), but my [EDS leak
patch](http://www.burtonini.com/blog/computers/eds-2005-05-16-16-55)
finally got approved. Hooray!

<small>NP: <cite>Keep it Solid Steel</cite>, Mr Scruff</small>
