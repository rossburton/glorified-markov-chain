Title: Congratulations
Date: 2005-07-04 09:05
Tags: life
Slug: congratulations

Many congratulations to Mickael and Carina, [little
Emma](http://micke.hallendal.net/archives/2005/07/life_as_a_fathe.html)
was born last Sunday. I'm sure she'll bring you many years of joy!

<small>NP: <cite>The Litmus Test</cite>, Cut Chemist</small>
