Title: Contact Lookup Applet 0.10
Date: 2004-10-27 17:21
Tags: tech
Slug: contact-lookup-applet-0-10

Version 0.10 of the Contact Lookup Applet is available [from
here](http://www.burtonini.com/computing/contact-lookup-applet-0.10.tar.gz),
Debian packages are available from [my
repository](http://www.burtonini.com/debian/) and will be hitting
`unstable` shortly.

-   Show a dialog if there are no completing sources set
-   Hide unused buttons
-   Fix setting sources in `EContactEntry`
-   I really should start crediting translators: Iñaki Larrañaga (eu),
    Amanpreet Singh Alam (pa), Hendrik Brandt (de), Marcel Telka(sk),
    and Raphael Higino (pt\_BR)

