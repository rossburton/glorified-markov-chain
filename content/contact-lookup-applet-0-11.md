Title: Contact Lookup Applet 0.11
Date: 2005-03-08 02:27
Tags: tech
Slug: contact-lookup-applet-0-11

Version 0.11 of the Contact Lookup Applet is available [from
here](http://www.burtonini.com/computing/contact-lookup-applet-0.11.tar.gz).
It requires GNOME 2.10, but don't worry if you don't have that as there
are very little actual changes. Because of this I don't have any Debian
packages yet, but Ubuntu Hoary already has this version.

-   Request the focus when pressed so that it works with GNOME 2.10
-   Follow the panel background (Vincent Noel)
-   Handle the sources gconf key not being set (Sylvain Pasche)

