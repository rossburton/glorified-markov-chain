Title: Contact Lookup Applet 0.12
Date: 2005-04-11 01:26
Tags: tech
Slug: contact-lookup-applet-0-12

Contact Lookup Applet 0.12 is released. I didn't do a lot of work on
this, but luckily Bastien Nocera did!

-   Change EContactEntry to allow apps to set the fields to search, and
    take an ESourceList rather than an ESourceGroup (Bastien Nocera)
-   Build with libpanel-applet 2.8 and 2.10 (BN)
-   Build with libebook 1.0 and 1.2 (BN)
-   Use automake 1.9 (BN)
-   Depend on GTK+ 2.6, and use GtkAboutDialog (Pedro
    Villavicencio Garrido)

You can grab it from [the usual
place](http://www.burtonini.com/computing/contact-lookup-applet-0.12.tar.gz).
Debian packages should be heading towards Sid shortly.

<small>NP: <cite>Mezzanine</cite>, Massive Attack</small>
