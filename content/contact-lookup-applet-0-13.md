Title: Contact Lookup Applet 0.13
Date: 2005-07-25 02:07
Tags: tech
Slug: contact-lookup-applet-0-13

Contact Lookup Applet 0.13 is released. No major changes, but some nice
features for developers (like `gnome-phone-manager`) and if you've a new
GTK+ then the completion isn't constrained by the size of the entry.

-   Allow EContactEntry to be subclassed (Bastien Nocera)
-   If GTK+ 2.7.3 is being used, allow the completion to be as wide as
    required
-   Handle source lists which don't contain any groups
-   Cleanups (BN)

You can grab it from [the usual
place](http://www.burtonini.com/computing/contact-lookup-applet-0.13.tar.gz).
Debian packages heading towards Sid once I update my `pbuilder`...

<small>NP: <cite>Sheryl Crow</cite>, Sheryl Crow</small>
