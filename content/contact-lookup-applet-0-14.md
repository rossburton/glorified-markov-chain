Title: Contact Lookup Applet 0.14
Date: 2006-05-17 01:22
Tags: tech
Slug: contact-lookup-applet-0-14

Contact Lookup Applet 0.14 is released, especially done for sebuild.

-   Translate the .server files (Frederic Crozat)
-   Don't set sensitivity ourself (Bastien Nocera)
-   Correctly handle contacts with no email (Adam Petaccia)

Translators: Adam Weinberger (en\_CA), Clytie Siddall (vi), Daniel
Nylander (sv), Francisco Javier F. Serrador (es), Frank Arnold (de),
Funda Wang (zh\_CN), Gabor Kelemen (hu), Ilkka Tuohela (fi), Iñaki
Larrañaga Murgoitio (eu), Jean-Michel Ardantz (fr), Kjartan Maraas (nb),
Marcel Telka (sk), Maria Soler (ca), Maxim Dziumanenko (uk), Michał
Kastelik (pl), Miloslav Trmac (cs), Takeshi AIHANA (ja), Yavor Doganov
(bg), Žygimantas Beručka (lt).

You can grab it from [the usual
place](http://www.burtonini.com/computing/contact-lookup-applet-0.14.tar.gz).
Debian packages heading towards Sid once I update my `pbuilder`...
