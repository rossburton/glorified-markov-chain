Title: Contact Lookup Applet 0.15
Date: 2006-10-17 03:40
Tags: tech
Slug: contact-lookup-applet-0-15

Contact Lookup Applet 0.15 is finally released. Forgetting to release
the fix to let it build with recent Evolution tells me quite nicely how
many people actually use this: None. If there are any real users out
there, please say so!

-   Build with recent EDS (Ed Catmur)

Translators: Christophe Merlet (fr), David Lodge (en\_GB), Francisco
Javier F. Serrador (es), Gabor Kelemen (hu), Iñaki Larrañaga Murgoitio
(eu), Jovan Naumovski (mk), Luis Matos (pt), Mahesh subedi (ne), Valek
Filippov (ru).

You can grab it from [the usual
place](http://www.burtonini.com/computing/contact-lookup-applet-0.15.tar.gz).
Debian packages heading towards Sid now.
