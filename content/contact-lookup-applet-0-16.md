Title: Contact Lookup Applet 0.16
Date: 2007-05-24 15:00
Tags: tech
Slug: contact-lookup-applet-0-16

This is a very overdue and probably the last release of
contact-lookup-applet. Tarballs are [available in the usual
place](http://burtonini.com/computing/contact-lookup-applet-0.16.tar.gz).
A few bug fixes in this release:

-   Display more than one street address in a combo (\#375433,
    Travis Reitter).
-   Support secondary phone numbers (\#359816, Timo Aaltonen).
-   Remove ellipsis from the About menu item (\#340450, Brian Pepple).

I'm also looking for a co-maintainer, or even better someone to take
over maintainership. I don't use this anymore, but many distributions
still ship it (and Ubuntu even has it in the stock install). The
codebase is pretty small and simple, so is anyone interested?

<small>NP: <cite>Peel Session</cite>, cLOUDDEAD</small>
