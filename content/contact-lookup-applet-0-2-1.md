Title: Contact Lookup Applet 0.2.1
Date: 2003-12-08 22:31
Tags: tech
Slug: contact-lookup-applet-0-2-1

Right, I removed the Search button from the applet. I added in the past
as it was the only way to reach the applet context menu, but now that I
have a little icon that works well for the applet context menu. That is
the only change here apart from a file rename, so don't get too excited.

Download it
[here](http://www.burtonini.com/computing/contact-lookup-applet-0.2.1.tar.gz).
