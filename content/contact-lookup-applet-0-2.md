Title: Contact Lookup Applet 0.2
Date: 2003-12-05 00:00
Tags: tech
Slug: contact-lookup-applet-0-2

Sweet news in the form of Contact Lookup Applet 0.2, which I believe
will work for more than 1% of people so I'm announcing it to the hordes.
Download it from
[here](http://www.burtonini.com/computing/contact-lookup-applet-0.2.tar.gz).

In case you are a little slow today, this is an applet for your panel
which will lookup contacts... specifically in an `evolution-data-server`
addressbook you specify. Yes folks, you'll have to build Evolution from
CVS to make this baby work. But it's worth it, trust me. `;-)`

There is still a substantial To Do list, but it works well enough --
assuming you type enough to get a single match from the address book, as
only the first result is displayed. If you try it and want to make
feature requests, *please* check `TODO` first. I also recommend running
the factory in a terminal the first time you use it, several (read as
"all") error conditions are handled with `g_warning()` and they will
disappear to the bit bucket otherwise.

Every time someone asks why I haven't yet put a quality setting in Sound
Juicer, God kills a kitten. I've asked him to extend the same practise
to feature requests for Contact Lookup Applet which are already in the
To Do. Please, think of the kittens.

Some people seem to like screenshots:

![](http://www.burtonini.com/computing/screenshots/contact-applet-1.png)  
![](http://www.burtonini.com/computing/screenshots/contact-applet-2.png)

<small> NP: Distractions - Zero 7. I forgot to record Mr. Scruff onto a
minidisc last night, dammit. </small>
