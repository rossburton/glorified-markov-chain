Title: Contact Lookup Applet 0.3
Date: 2003-12-11 18:13
Tags: tech
Slug: contact-lookup-applet-0-3

Autocompletion in the entry! There are still some niggly bugs to fix,
but this is looking fabtastic now. This release requires GTK+ 2.3.

Download it
[here](http://www.burtonini.com/computing/contact-lookup-applet-0.3.tar.gz).
