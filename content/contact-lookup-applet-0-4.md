Title: Contact Lookup Applet 0.4
Date: 2004-01-12 14:49
Tags: tech
Slug: contact-lookup-applet-0-4

I've now committed the API changes to evolution-data-server, so can
finally do a new release of this applet. Featuring display of video
conferencing URL, complete asyncronous lookup and demonstrates a rather
annoying bug in gnome-panel 2.5.

If you run GNOME 2.5, you will not be able to focus the text area by
simply clicking. Alt-click instead to focus the applet, and then click
to focus the text area. Hopefully Mark McLoughlin can figure out a way
to fix this before 2.6 is released...

Download it
[here](http://www.burtonini.com/computing/contact-lookup-applet-0.4.tar.gz).
