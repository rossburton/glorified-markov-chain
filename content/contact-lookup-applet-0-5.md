Title: Contact Lookup Applet 0.5
Date: 2004-01-22 19:36
Tags: tech
Slug: contact-lookup-applet-0-5

A new release of the applet is available.

-   Change the completion search to match in full name, nick name and
    email address
-   Add a tooltip to the applet explaining what it is
-   Use the "contact card" stock icon instead of the "person" icon
-   Use the word "search" instead of "lookup"

I'm currently tackling showing the IM fields, this is leading to a
redesign of the interface... The next release will either be next week,
or not for a long time, depending on how many Inspirons^\[1\]^ I absorb
over the next few days.

NP: [Six Million Ways To
Live](http://www.amazon.co.uk/exec/obidos/ASIN/B0000C3I8H), by Dub
Pistols

<small>\[1\] The particle of inspiration</small>
