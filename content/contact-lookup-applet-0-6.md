Title: Contact Lookup Applet 0.6
Date: 2004-04-14 03:37
Tags: tech
Slug: contact-lookup-applet-0-6

Version 0.6 of the Contact Lookup Applet is available [from
here](http://www.burtonini.com/computing/contact-lookup-applet-0.6.tar.gz),
and a Debian package for `experimental` is currently uploading.

I never did finish the new and improved IM field display, but this
release at least builds with the current evolution-data-server API.
