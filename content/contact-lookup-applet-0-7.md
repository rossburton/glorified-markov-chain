Title: Contact Lookup Applet 0.7
Date: 2004-08-16 17:46
Tags: tech
Slug: contact-lookup-applet-0-7

Version 0.7 of the Contact Lookup Applet is available [from
here](http://www.burtonini.com/computing/contact-lookup-applet-0.7.tar.gz),
and a Debian package for `experimental` is in the queue.

I'd love to take credit for this release, but all I did was change a
pkg-config line and do the dist. Michael Henson was a wonderful man and
send a massive patch:

-   Contact pictures are now displayed in auto-complete list
-   Contact dialog scales pictures to a reasonable height
-   Contact dialog bolds and enlarges the size of contact name
-   Fixed a memory leak when displaying the photo in the contact dialog
-   Don't display fields if the returning string is empty
-   Completion logic fixed
-   Tooltips to notify the user of any errors
-   Glade love
-   Query strings are now split on spaces when passed into the query

Go Michael!

A little screenshot to tempt people into upgrading:

![](http://www.burtonini.com/computing/screenshots/contact-applet-photos.png){}

<small>NP: <cite>Riding High</cite>, Bob Marley</small>
