Title: Contact Lookup Applet 0.8
Date: 2004-08-25 16:28
Tags: tech
Slug: contact-lookup-applet-0-8

Version 0.8 of the Contact Lookup Applet is available [from
here](http://www.burtonini.com/computing/contact-lookup-applet-0.8.tar.gz),
and a Debian package for `experimental` is in the queue. This release is
dedicated to Jeff Waugh, as his pimping of contact-lookup-applet at the
Canonical conference inspired me to work on it a bit more.

-   Show email and IM addresses in combo boxes, not a label (somewhat
    based on a patch by Olivier Dubroca)
-   Add window icons
-   Make the photo display better in the contact dialog
-   Display the first contact if many were found when user activates the
    entry
-   Loads of small bug fixes (see ChangeLog)

![](http://www.burtonini.com/computing/screenshots/contact-applet-0.8.png){}
