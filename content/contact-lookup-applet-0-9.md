Title: Contact Lookup Applet 0.9
Date: 2004-10-07 01:33
Tags: tech
Slug: contact-lookup-applet-0-9

Version 0.9 of the Contact Lookup Applet is available [from
here](http://www.burtonini.com/computing/contact-lookup-applet-0.9.tar.gz),
and a Debian package is in `experimental`. This release is dedicated to
everyone who wants a completing address book entry widget -- it's a
separate widget now.

-   Split the completing entry into a separate widget
-   Search all address books marked as completion sources in Evolution
-   Support i18n, with prompt translations from Christian Rose and Adam
    Weinberger

