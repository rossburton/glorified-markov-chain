Title: Contact Lookup Applet (Fame And Glory)
Date: 2004-09-17 00:00
Tags: tech
Slug: contact-lookup-applet-fame-and-glory

The totally wonderful (some would even say "rad", but not me!) [Ubuntu
Linux](http://www.ubuntulinux.org) became the first distribution I know
of to ship contact-lookup-applet this week, when
<cite>WartyWarthog</cite> was released in preview form. This did tickle
my funny bone, as Ximian/Novell paid for its development via a Bounty.
I've been informed \[updated, thanks Luis\] it will be in Novell Linux
Desktop (due "soon") and SuSe 9.2, and there are rumours it will be in
FC3 too.

What more proof do you need that RPM-based distributions are inferior?

<small>NP: <cite>Like Water For Chocolate</cite>, Common</small>
