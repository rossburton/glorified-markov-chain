Title: Contacts 0.2 for Ubuntu Edgy Eft
Date: 2006-12-01 20:20
Tags: tech
Slug: contacts-0-2-for-ubuntu-edgy-eft

I've build a package for all the Contacts users running Edgy who don't
fancy building Contacts 0.2 from source. Simply add the following source
to `sources.list`:

    deb http://debian.o-hand.com edgy/

Remember to report any bugs to [our
Bugzilla](http://bugzilla.openedhand.com/enter_bug.cgi?product=Contacts)!
