Title: Contacts 0.3
Date: 2007-02-02 10:40
Tags: tech
Slug: contacts-0-3

I've just released [Contacts 0.3](http://projects.o-hand.com/contacts).
This release doesn't have a lot of visible features, but the use of
Glade was removed and we have more translations.

More importantly, it's also available for the Nokia N800! Grab your
Application Catalogue installation lines from
[maemo.o-hand.com](http://maemo.o-hand.com/) and install Dates and
Contacts on your N800 today! Contacts doesn't yet have a Maemo port so
it isn't totally integrated, but as both Contacts and the N800
addressbook use evolution-data-server, your contacts are shared.

<small>NP: <cite>Skalpel</cite>, Skalpel</small>
