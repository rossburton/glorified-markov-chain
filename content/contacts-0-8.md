Title: Contacts 0.8
Date: 2007-12-19 09:57
Tags: tech
Slug: contacts-0-8

I did the quarterly patch review and translation update of Contacts last
night, and rolled [Contacts
0.8](http://pimlico-project.org/contacts.html). Nothing amazing in this
release, sorry, all of the hard work by Thomas has gone onto the all-new
blinging rewrite.

<small>NP: <cite>Out Of Season</cite>, Beth Gibbons and Rustin
Man</small>
