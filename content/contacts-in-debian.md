Title: Contacts in Debian
Date: 2006-02-23 10:11
Tags: tech
Slug: contacts-in-debian

I've just uploaded into Debian (and [my local
repository](http://www.burtonini.com/debian/)) a first-stab at packaging
[Contacts](http://chrislord.net/blog/Software/Contacts/), the ulta-cool
lightweight address book that uses Evolution Data Server.

Hopefully Chris will get around to making a 0.2 release soon, I've
packaged 0.1 as it's known to work, but [the
repository](http://svn.o-hand.com/view/contacts/) contains some
kick-arse improvements.
