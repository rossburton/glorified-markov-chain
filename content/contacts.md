Title: Contacts
Date: 2005-12-15 10:20
Tags: tech
Slug: contacts

We've just [open-sourced
<cite>Contacts</cite>](http://www.soton.ac.uk/~cil103/2005/12/contacts.html),
our light addressbook that uses Evolution Data Server as a backend.
Although it was designed for handheld devices, its very usable on
desktops too. Chris Lord, who wrote it in the summer whilst interning at
[Opened Hand](http://www.o-hand.com/), has [more details and
screenshots](http://www.soton.ac.uk/~cil103/2005/12/contacts.html).
