Title: Contributions to libsocialweb
Date: 2011-02-22 16:45
Tags: tech
Slug: contributions-to-libsocialweb

Thanks to those nice people at Novell and Collabora,
[libsocialweb](http://git.gnome.org/browse/libsocialweb) now supports
Facebook, Flickr, Last.fm, Plurk, Sina, SmugMug, Twitter, Vimeo and
YouTube. As if that isn't enough, there are patches queued to bring back
MySpace. Thanks Novell and Collabora!
