Title: Coprophagia
Date: 2005-05-20 12:50
Tags: life
Slug: coprophagia

(I hope I didn't upset anyone eating whilst reading there)

So Micke, Richard, Glynn, and Jeff (!) all think that my [sweet and
all-so-good
cookie](http://www.burtonini.com/blog/life/brain-food-2005-05-19-12-21)
looks like, well, poo.

I dread to think what they'll make of my [lunch
today](http://www.burtonini.com/photos/Misc/soup.jpg)...

<small>NP: <cite>Mezzanine</cite>, Massive Attack</small>
