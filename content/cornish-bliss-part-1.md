Title: Cornish Bliss (part 1)
Date: 2006-08-06 21:10
Tags: life
Slug: cornish-bliss-part-1

Let's start this with a cliché:

<cite>A Short History of Tractors in Ukrainian</cite>: £8  
<cite>Best Of</cite> by Nina Simone: £14  
One week in a rather swish house just outside St. Ives: about £200  
No email. No Jabber. No Subversion. No Bugzilla: *priceless*

For the last week Vicky and myself were in Cornwall, staying in [a house
just outside St.
Ives](http://www.chycor.co.uk/cottages/carbisbay_gwelmar/index.htm) with
her family. The house was much nicer than we expected, very tastefully
decorated and well equipped, with a DVD player, hi-fi, coffee machine,
and so on.

[![Settling
In](http://static.flickr.com/82/208276098_f12e0824e2_m.jpg){
}](http://www.flickr.com/photos/rossburton/208276098/)

The house was in Carbis Bay, about a twenty minute walk from St. Ives
along the costal path that was directly off the bottom of the garden.
The path towards St. Ives is also the nicest stretch of costal path I've
been on: it is surfaced and wide enougth to drive on, unlike the paths
we've been on previously which are often no more than a foot wide
cutting in the earth, next to a sheer cliff.

[![St.
Ives](http://static.flickr.com/69/208283034_d8c5f1d948_m.jpg){
}](http://www.flickr.com/photos/rossburton/208283034/)
[![The
Sea](http://static.flickr.com/58/208282050_cdccff8e62_m.jpg){
}](http://www.flickr.com/photos/rossburton/208282050/)

Our first impression of Cornwall was one of chill: when the taxi dropped
me off at the train station to start the journey down the thermometer
said 38°C, but at the same time the next day it was “only” 23°C. Brrrr.
The traditional Cornish summer proceeded to roll in a few days later,
with heavy rain, gusty wind and general grimness for a day. Yay for DVD
players!

[![Typical Cornish
Summer](http://static.flickr.com/59/208291797_91e6890f95_m.jpg){
}](http://www.flickr.com/photos/rossburton/208291797/)

St. Ives is a lovely town. Unlike Newquay which was got *far* too
popular for it's own good (the two-carriage train had both a Stag and
Hen party on board en route to Newquay, trying not that subtly to pull
each other), St. Ives is busy but not crammed. There is no demographic
that dominates the tourists, a mix of families young and old, surfers,
twenty-somethings and pensioners means it doesn't feel like a tourist
hotspot, and it manages to cater for everyone. The habour front has
lounge bars, traditional pubs, restaurants and [cornish
pasty](http://en.wikipedia.org/wiki/Cornish_pastie) shops, catering for
everyone. The town has become quite a focal point for artists recently
(since 1928, Wikipedia tells me), and there seems to be more independent
art galleries than pasty shops (!), including the Cornwish outpost of
the [Tate](http://www.tate.org.uk/stives/). Tate St. Ives is pretty
small for anyone who has been to Tate Modern, but it's damn good: due to
the size it is very focused (there are just two galleries) and the
building itself is a wonderful modern piece of art deco architecture. We
went with the intention of getting some more pictures for the house and
did quite well: a print of [Horizontal
Stripes](http://tate.artgroup.com/mall/productpage.cfm/Tate/PD-TATE1210-GE/70253)
by Patrick Heron, and a limited run print (427/600) by a local artist.
I'm too lazy to remember the name of the picture or the artist, but I'll
take a photo of it later. Our Grand Plan of having more individual art
in the house is going well, we've an original oil-on-canvas abstract to
collect from the framers that we bought in Paris too.

To be continued...
