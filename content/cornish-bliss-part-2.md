Title: Cornish Bliss (part 2)
Date: 2006-08-08 10:30
Tags: life
Slug: cornish-bliss-part-2

Now, where was I...

[![Carbis
Bay](http://static.flickr.com/72/208944140_b22c45ce76_m.jpg){
}](http://www.flickr.com/photos/rossburton/208944140/)
[![Carbis
Bay](http://static.flickr.com/89/208904221_83862621cd_m.jpg){
}](http://www.flickr.com/photos/rossburton/208904221/)

Ah yes. After the horrendously grim weather had passed, the weather
improved and we headed for the beach. On the way down we commented on
how this was the classic British burning weather: bright sunshine, a
strong breeze, and occassional clouds combine to burn skin without even
feeling that hot. Of course knowing this meant nothing, we were too
distracted with purchasing pasties and drink to think about putting a
decent amount of sunblock on.

[![Steve](http://static.flickr.com/75/208890157_47276279a5_m.jpg){
}](http://www.flickr.com/photos/rossburton/208890157/)
[![Relaxing](http://static.flickr.com/77/208921493_9610c1a8be_m.jpg){
}](http://www.flickr.com/photos/rossburton/208921493/)

Obviously the main thing to do at the beach, after we'd sat down, not
applied sunblock, and scoffed a pasty, was to dig a hole. A huge hole.
Spades were purchased and we took turns to help Pete dig The Hole.

[![Digging](http://static.flickr.com/81/208904554_f7d5927d4e_m.jpg){
}](http://www.flickr.com/photos/rossburton/208904554/)
[![Diggers](http://static.flickr.com/73/208922188_bc025cd471_m.jpg){
}](http://www.flickr.com/photos/rossburton/208922188/)

Astute readers will notice the inevitable outcome of saying “this is
burning weather”, not putting enough sunblock on, and digging a hole (an
activity that results in the back being exposed to the sun). Ouch.

After the hole had been dug we had to full it in again to avoid trapping
small childen in it. Obviously this led to a series of hilarious scenes
involving burying Pete up to his chest, modelling breasts and a penis,
and so on. Finally the hole was flat again, at which point an impromptu
long jump sand pit was arranged. I came first in the long jump, and
although failing miserably at triple jump although I swear my technique
was best (it's all in the wrists).

[![Pete](http://static.flickr.com/83/210407792_b1fb087081_m.jpg){
}](http://www.flickr.com/photos/rossburton/210407792/)

Next was to explore the costal path in the opposite direction towards
Porthkidney beach. The beach is pretty huge by my standards, and due to
the lack of facilities (no close car park, shops, toilets, and so on)
it's almost deserted: there were a few other people there with dogs (the
other local beaches are dog-free in summer) and that's about it.
Googling to confirm the name of the beach reveals that there is a
history of naturism and "inappropriate gay activity", but we didn't
encounter any of that. ;)

[![Progression](http://static.flickr.com/96/208945467_b1b5a4d422_m.jpg){
}](http://www.flickr.com/photos/rossburton/208945467/)
[![Ross](http://static.flickr.com/89/208946702_ede748e7ca_m.jpg){
}](http://www.flickr.com/photos/rossburton/208946702/)
[![Footprints](http://static.flickr.com/79/208945864_c37e24bf40_m.jpg){
}](http://www.flickr.com/photos/rossburton/208945864/)

The costal path was great, far more rough than the walk to St Ives
(often just a foot wide cutting in the ground), steep in places, and
generally running very close to the cliff edge. The views were great,
but I always think what a horror paths like these would be in winter,
with the full force of the Atlantic winds pounding against the cliffs.
As a finale it turns out that the costal path follows the cliff all the
way along the back of the beach, which would easily be another twenty
minutes of walking to reach sand. There is a shortcut down some stone
stairs to the beach, but we arrived at high tide and the bottom of the
stairs (well, rocks) were a foot deep in water. Wading up to the beach
was a fitting end to the walk, and made the beach feel like our own
little desert island!

[![Cliff
Steps](http://static.flickr.com/88/210376971_effaf25f55_m.jpg){
}](http://www.flickr.com/photos/rossburton/210376971/)
[![Wading To
Land](http://static.flickr.com/93/210375696_f921919d0b_m.jpg){
}](http://www.flickr.com/photos/rossburton/210375696/)
[![Limpets!](http://static.flickr.com/77/210373199_0d82993fc2_m.jpg){
}](http://www.flickr.com/photos/rossburton/210373199/)
[![Ross](http://static.flickr.com/63/210374075_08be86a4eb_m.jpg){
}](http://www.flickr.com/photos/rossburton/210374075/)

I'll have to explain the expression on Vicky in the above photo. As a
child when Vicky went to visit her father in Devon they used to go to
the beach and spend the day annoying the wildlife: chasing crabs,
kicking limpets off rocks and so on. When Vicky noticed that the rocks
at the bottom of the cliff were covered in limpets, she shouted
"limpets!" with a manic expression and preceeded to prod them
frantically.

After lots of sitting around and digging tunnels, we headed for the
dunes for a spot of dune diving. This involves running at top spead down
the dunes and throwing yourself into the sand at the bottom. Ah, the
simple pleasures in life!

[![Porthkidney
Beach](http://static.flickr.com/88/210382315_1a104fa846_m.jpg){
}](http://www.flickr.com/photos/rossburton/210382315/)
[![Dune
Diving](http://static.flickr.com/84/210385996_eed6d636cd_m.jpg){
}](http://www.flickr.com/photos/rossburton/210385996/)
[![Dune
Diving](http://static.flickr.com/60/210386539_9875a88ddf_m.jpg){
}](http://www.flickr.com/photos/rossburton/210386539/)
[![Dune
Diving](http://static.flickr.com/68/210388936_07e7af0dcc_m.jpg){
}](http://www.flickr.com/photos/rossburton/210388936/)
[![Dune
Diving](http://static.flickr.com/91/210390543_39ebb43312_m.jpg){
}](http://www.flickr.com/photos/rossburton/210390543/)

After my dive I ran back up the hill in the manner of a mad man, arms
out-stretched to Vicky as I collapsed in front of her, gasping "It's".
"What?", was the confused reply. This is terrible, I really need to get
Vicky to watch the <cite>Best Of Monty Python</cite> DVD we have
somewhere...

[![It's!](http://static.flickr.com/90/210408596_9e79d7e956_m.jpg){
}](http://www.flickr.com/photos/rossburton/210408596/)

Possibly more to write, but <cite>Lost</cite> is on, so I'm off for now.
