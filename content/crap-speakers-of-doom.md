Title: Crap Speakers Of Doom
Date: 2006-07-20 09:15
Tags: life
Slug: crap-speakers-of-doom

I just discovered a use for the Treble Booster EQ on my iPod... Now that
I'm working in the ~~spare room~~ study pretty much all the time I
really miss not sitting in the sweet spot for the hifi downstairs.
Hanging around I had some cheap Logitech computer speakers, so I've dug
those out and plugged my iPod in. This morning I fancied a bit of Sigur
Rós so I slapped on <cite>Ágætis Bryjun</cite> and after two minutes was
enjoying the bass (the speakers are sub/sat) but was left wondering what
happened to the drums. If I listened very carefully I could just make
out a snare, but the cymbals... totally inaudible. Thus Treble Booster,
which has at made this album listenable too at least. It's a pain that
these speakers are so bass-heavy as my laptop doesn't have bass/treble
controls. The irony is that the sub doesn't even go that low, on
<cite>Flugufrelsarinn</cite> the bass drum is totally drowned out by the
organ. The mid-bass is just very, very loud.

Oh well, I'm planning to get a pair of small speakers (MS Avant 902i are
looking good) and one of those super-cheap Super-T amps for this room at
some point.

<small>NP: <cite>Ágætis Bryjun</cite>, Sigur Rós</small>
