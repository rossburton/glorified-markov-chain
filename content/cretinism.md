Title: Cretinism
Date: 2006-03-10 14:04
Tags: life
Slug: cretinism

Oh sorry, I meant “creationism” (via [The
Guardian](http://education.guardian.co.uk/schools/story/0,,1728235,00.html?gusrc=rss)):

> Pupils in England will be required to discuss creationist theories as
> part of a new GCSE biology course being introduced in September.

Why can't we keep the scientific theories in the science classrooms, and
the religious counter-arguments in the religious education classrooms?

<small>NP: <cite>He Has Left Us Alone, but Shafts of Light Sometimes
Grace The Corners Of Our Rooms</cite>, A Silver Mt Zion</small>
