Title: CSS Nightmare Garden
Date: 2005-01-20 00:00
Tags: tech
Slug: css-nightmare-garden

[This](http://zengarden.20megsfree.com/) is probably the most amusing
and authentic entry to the [CSS Zen
Garden](http://www.csszengarden.com/) I've ever seen. Ah, those were the
days...

<small>NP: <cite>Best of Ella Fitzgerald and Louis
Armstrong</cite></small>
