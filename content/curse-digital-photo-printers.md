Title: Curse Digital Photo Printers
Date: 2005-04-26 16:55
Tags: life
Slug: curse-digital-photo-printers

(not home printers, but places which develop film and normally will do
prints from JPEG too)

CURSE YOU ALL.

I've a Canon EOS-300D, and that by default writes JPEG files using the
"Adobe RGB" colour space. I don't know what the differences are, but
Canon recommend Adobe RGB so Adobe RGB I use. However, when I take the
files down to Jessops or Boots the prints are a little darker than I'd
expect. On the basis that the developing staff in Jessops generally know
something, I tried asking if their equipment handles the colour space
specified in the JPEGs correct, and they don't know. They also don't
know what colour space the equipment is working in. I'm not even going
to bother asking in Boots, and I tried asking `dlab7.co.uk` to get this
wonderful response:

> Rgb images are best suited to our printers. We do not offer any colour
> management of your images. However I can suggest sending us some un
> 'touched' files for printing and colour match your computer to the
> prints produced by us.

So I've got to send RGB images, which is fair enough. I can't think of
any digital cameras which take in CMYK, but I could have produced a CMYK
file in Photoshop I guess. Tthey don't do colour matching, so I should
send them a photo and then calibrate *my* monitor to match it, and then
edit every image I want to get printed so that it comes out correct.
Sigh.

So, does anyone know of a decent (and not expensive) British photo
company which actually knows what a colour space is, and will convert
the images if required? Of course if someone who actually knows these
things can tell me that the machines all use sRGB, I'd switch straight
away.

*Update:* I've done some research, and am not switching to sRGB. Ever.

<small>NP: <cite>Best Of</cite>, Otis Redding</small>
