Title: Cycling Dad Nirvana Approaches
Date: 2015-04-19 21:44
Tags: cycle, life
Slug: cycling-dad-nirvana-approaches

Last week Alex wanted to go for a bike ride so we had a play on the
local pump track and some of the cheeky trails hidden away nearby. This
was his first ride off the pavements so I was cautious but [much fun was
had](https://vimeo.com/124923068) by Alex and he's spent most of the
last week talking about the ride and in particular the pump track.
There's a pretty good one at Thetford Forest now so now that Spring has
(mostly) sprung we decided to have a family day out and give Isla some
proper practise at riding her new bike.

[flickr:id=16995308217]

For the start of the ride it was me and Alex riding ahead with Vicky
riding alongside Isla whilst she practised the hard bit of stopping and
starting. It didn't take long before we heard a loud "COMING THROUGH!"
and Isla flew by. A few kilometres down the Shepherd trail we decided to
head (badly, I can't recall the Shepherd route at all) before little
legs tired and find the pump track. There were a few tumbles, Alex was
getting tired and there's a tight berm with a very loose straight line.
Isla of course had the confidence of a bold little sister and wanted a
go, which led to a great double face-plant when I was running alongside
guiding her around. Nothing a bit of Savlon won't solve though, and ice
cream made it all better!

All in all, a good day: Alex had a good ride and fun on the pump track,
and Isla is massively more confident on her bike, especially when the
path isn't new-pavement-smooth. Not panicking when the path is a bit
"bumpy lumpy" is an important skill when riding alongside traffic!
