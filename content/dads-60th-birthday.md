Title: Dad's 60th Birthday
Date: 2003-06-11 23:19
Tags: life
Slug: dads-60th-birthday

My Dad finally reached the ripe old age of 60 yesterday, so we went out
to the Red White and Blue pub in Bishop's Stortford. They advertise
"large portions" outside, and they are not joking! After a large potato
wedges with sour cream and chive dip starter, I had cajun chicken with
chips, onion rings, salad, fried tomatoes and mushrooms. Good
old-fashioned onion rings made with real rings of onion, well fried
large but crispy chips, it was all good. And very large. A good evening
was had by all present, which didn't include my sister as she is
swanning around in Morocco at the moment. Missing our father's 60th,
disgusting `:)`

Present wise, Dad is buying himself a new camera (Canon EOS-300V with
28-105mm and something-300mm lenses), and we (my sister and myself) got
him a set of power tools -- a rechargable drill and a huge manly power
drill.
