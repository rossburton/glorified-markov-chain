Title: Daily Mail Science
Date: 2005-02-28 15:25
Tags: life
Slug: daily-mail-science

I just received my weekly <cite>A Worm's Eye View</cite>, the editorial
which comes with [The Guardian's](http://www.guardian.co.uk) news
round-up service. I may disagree with the author, [Andrew
Brown](http://www.thewormbook.com/helmintholog/), on several issues
(specifically he doesn't think Linux is any good, but at least he uses
OO.o), but he is often right on the mark.

> Guardian readers can have little idea how dreadful the general
> coverage of science stories is in the British press. All distinction
> vanishes between chemists, social scientists, particle physicists,
> snail taxonomists, people who chop the heads of little chickens with
> giant scissors - they're all "scientists".
>
> All 'scientists' have one task. The Daily Mail subscribes to a
> radically simplified version of the atomic theory and everyone else
> tries to follow. According to this theory everything in the world is
> made from two kinds of basic substance: those that cure cancer, and
> those that cause it. The job of a scientist is to go through the world
> classifying everything into one category or the other. Every time
> something is identified as made of one sort of atom or the other, we
> have a story, preferably a scandal.

<small>NP: <cite>Cafe Del Mar Volume 1</cite></small>
