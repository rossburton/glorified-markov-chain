Title: Daily Mail Smack Down
Date: 2007-11-11 14:45
Tags: life
Slug: daily-mail-smack-down

There are many reasons to love the blog [Five Chinese
Crackers](http://5cc.blogspot.com/), and [printing the
letter](http://5cc.blogspot.com/2007/10/m-garapich-to-j-slack.html) sent
to <cite>The Daily Mail</cite> from the author of an immigration report
which was drastically misrepresented certainly is one of them:

> “Unfortunately, your piece is a mixture of ignorance,
> misinterpretation and speculation. I couldn't care less about your
> intellectual capacity to absorb the data, but you have included my
> name in an article that conveys a false impression of what the study
> was about.”

In other news, today I discovered that the epitome of British retailing,
[Marks and Spencer](http://en.wikipedia.org/wiki/Marks_and_Spencer), was
co-founded by **a foreigner!** [Michael
Marks](http://en.wikipedia.org/wiki/Michael_Marks) was born in Belarus,
and came to England to avoid the persecution of Jews. The warehouse in
Leeds where they started up has a [blue
plaque](http://www.flickr.com/photos/44067831@N00/157425150/)
commemorating the event. What would the Express or Mail say to this?
