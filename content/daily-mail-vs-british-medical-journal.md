Title: Daily Mail vs British Medical Journal
Date: 2007-08-01 10:40
Tags: life
Slug: daily-mail-vs-british-medical-journal

From [an article in the Daily
Mail](http://www.dailymail.co.uk/pages/live/articles/health/thehealthnews.html?in_article_id=471911)
this week:

> “Previous studies have found that cannabis claims 30,000 lives a year
> by causing cancer, heart disease and bronchitis, and that it can
> double the risk of mental illnesses such as schizophrenia.”

Now, I wonder where that data is from. Oh look, the [British Medical
Journal](http://www.bmj.com/cgi/content/full/326/7396/942):

> “It may be argued that the extrapolation from small numbers of
> individual studies to potential large scale effects amounts to
> scaremongering. For example, one could calculate that if cigarettes
> cause an annual excess of 120 000 deaths among 13 million smokers, the
> corresponding figure for deaths among 3.2 million cannabis smokers
> would be 30 000, assuming equality of effect.”

Genius. Thanks to [Dr Ben Goldache](http://badscience.net) for spotting
this.

<small>NP: <cite>The Good, The Bad & The Queen</cite>, The Good, The Bad
& The Queen</small>
