Title: Daniel Elstner, Rediscovered
Date: 2004-01-07 00:00
Tags: tech
Slug: daniel-elstner-rediscovered

Murray Cumming
[reports](http://www.advogato.org/person/murrayc/diary.html?start=150)
that [Daniel Elstner](http://www.advogato.org/person/danielk/) is alive
and well, which is excellent news. He really helped me when I was
starting to learn gtkmm, and
[Regexxer](http://regexxer.sourceforge.net/) is a truly wonderful piece
of software.

Hopefully he will come back to the GNOME/Debian world one day, and I'm
sure he will be surprised at the welcome he'll get...
