Title: Dates 0.3
Date: 2007-02-02 14:26
Tags: tech
Slug: dates-0-3

Following up the Contacts release earlier, I just rolled [Dates
0.3](http://projects.o-hand.com/dates). This mainly has a simple fix
that means on the first start it enables a calendar, so you can do
something useful with it.

Again, N800 packages are already online, and I'm rolling packages for
Debian now. 770 packages will be available when I've found a
770-compatible scratchbox!
