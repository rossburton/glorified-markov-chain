Title: Dates Screencast
Date: 2006-03-13 11:50
Tags: tech
Slug: dates-screencast

[Chris](http://chrislord.net/) has been a busy man for the last few
days, improving the performance of
[Dates](http://projects.o-hand.com/dates). It's now a really usable
program: previously startup took several minutes (I've four calendars,
two of which are large webcals) but now it is just a second.

It's so cool I even [made my first
screencast](http://www.burtonini.com/computing/screenshots/dates.gif). I
used Byzanz so the dithering isn't part of the application, and the mode
changes should be smooth animations but happened too fast for Byzanz to
capture. But you can get a feel for the application, and I hope you
agree it kicks arse.

<small>NP: <cite>Motion</cite>, Cinematic Orchestra</small>
