Title: Day 2, Part II
Date: 2003-06-18 02:07
Tags: tech
Slug: day-2-part-ii

Just about to dive out to the Ximian PARRRTAY after a quick change and
wash. I'm tired, I've gone to a talk in every slot, and for some reason
listening to someone turns out to be quite tiring.

Some quick observations:

-   Miguel de Icaza types as fast as he speaks, and he
    gesticulates heavily. This man must be scary drunk. He is also a
    good public speaker.
-   Glynn Foster needs sleep. I hope he manages to get some time off
    from work for arranging GU4DEC, he deserves it.
-   Jeff Waugh is a fun man to be around -- he is as loud as his IRC
    persona, which is a good trick.
-   The flashing LED pens from Sun Microsystems (Desktop Group) are
    possibly the coolest conference giveaway, tying with the ASCII table
    mug from the Embedded Systems Show.
-   Wifi cards eat battery power. A 4 hour charge is now lasting
    2 hours.
-   Wifi truly rocks.

People still to meet: `telsa`, `fcrozat`, `havoc`.
