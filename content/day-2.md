Title: Day 2
Date: 2003-06-17 19:09
Tags: tech
Slug: day-2

Now it's the first break on Day Two. I've just come out of Anders' D-BUS
talk, which was good. D-BUS really does look like fun. Met Mikael
Hallendal, who has kindly made me a [small
sign](http://micke.hallendal.net/pics/ross-maintainer.jpg)... :)
Annoyingly just missed Ross Golder, he was in the hacking room, I went
to the hacking room, and...

    <ross_> Burrito: seen ninja
    -Burrito/#gnome- I last saw ninja (~guadec6@193.1.27.184)
    0d 0h 3m ago [quit: (Client exiting)]
      

Dammit.

*\[time passes\]*

Just went to James's talk on PyORBit, which looks very elegant. After
looking at the Java ORBit APIs I was scared by CORBA and stayed away,
but this looks very nice. Hopefully I'll be able to play with Evolution
1.4 via CORBA now... Now I'm in Alex's Nautilus talk, which promises to
be interesting. I've tried to hack Nautilus in the past but ended up
running out of disk space... its not a small module. Maybe if I know
something about the code I'll have another attempt.
