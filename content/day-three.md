Title: Day Three
Date: 2003-06-18 22:07
Tags: tech
Slug: day-three

Damn my head hurt this morning. I managed to crawl out of bed, avoiding
Helen's knocking on the door and ringing the bell. Had a loong shower
and battled with Natwest Card Services (international call on a mobile,
lots of pressing \# and 1 and 4) to find out that my credit card has
been locked. Arse. I can't get money out of the machines, but at least I
can get a cash advance in person. Going two days with 30 euros is
hard...

I met up with Thom in the hacker room, and we read email and tried to
hack. Tried being the operative word. Bastien joined us and around
mid-day we just had to eat -- the copious Guinness the night before was
making itself known far too much. Bastien claimed to know a place which
does an all-day breakfast, which was the best idea. Temple Bar is cool,
its Dublin's version of Leicester Square or Soho, depending on where you
are. In a way, its better than London, as its more compact, and appears
to be of a higher quality -- plenty of bars, restaurants and small music
shops. Oh, and the best all day Irish breakfast I've ever had.

Suitably recharged, we wandered around the corporate exhibits. The HP
pens are sucky, they are nice looking but feel cheap, and just nothing
can come close to the joy of the Sun pens, which rule. HP however do
have mints in little Tux cases, which is better. However, Sun totally
kicked arse with the Sun Ray demo, two thin clients (one [embedded into
the display](http://wwws.sun.com/hw/sunray/sunray100/), á la the old
iMac, and one [seperate box](http://www.sun.com/hw/sunray/sunray1/) and
a TFT). You "login" by inserting a smartcard (JavaCard of course), which
actually just connects the display to your session on the server.
Removing the card instantly locks your session and the display goes back
to the login prompt. The magic trick is when you put your smartcard in
another thin client, and get your session back instantly. All of this is
continually running on the server, so nothing has to load -- the Rays
don't even have hard disks -- and its very fast. Apparently they are
even working on server switching, so a user in the States can connect
from a Europe office, and get all of their applications transfered to
the local server to avoid X across the Atlantic. The only problem is the
theme... it's better than Metal, but still a little clunky for my
liking.

Back to the hacking room to try and work on Sound Juicer for a bit.
Bastien is playing with the new Helix (aka Real) SDK, there are plans to
get Totem as the front end for the new Real player. And I finally met
Murray Cumming of GTKMM fame, who is very softly spoken. Doesn't seem to
fit with his irc/email image, which can be... forward.
