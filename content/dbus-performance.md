Title: DBus Performance
Date: 2006-07-22 12:20
Tags: tech
Slug: dbus-performance

Ah, the classic DBus performance issue has reared it's head on Planet
Gnome again. I'll chip in my thoughts on this, as I've done a fair bit
of profiling on a [large user of DBus](http://projects.o-hand.com/eds).

When I first ported of EDS to DBus, it didn't perform as fast as the
original ORBit implementation. However when I dug out
[OProfile](http://projects.o-hand.com/oprofile) I discovered that the
main bottle necks were deep in EDS itself, and DBus although in the
profiles, wasn't anywhere near the top. We spent a few months optimising
EDS-DBus (there was some *serious* low-hanging fruit) and by the time
we'd done the obvious optimisations, EDS-DBus was faster than upstream
EDS for the typical usage (running a book view, which happens every time
you search in Evolution).

For some methods EDS-DBus was still slower than EDS, and `getContacts()`
is a perfect example. This method takes a query, builds the set of
contacts that match, and sends it back to the client in a single list
(compared to book views that do the same thing incrementally, and
provide future updates). When profiled you discover that `libdbus` and
`libc` are the top offenders, taking almost 70% of the runtime. The
benchmark was asking for all contacts, so EDS-DBus was sending a 150
contacts as vCards over the bus in a single list, which amounts to about
100Kb of data. Now, this gets copied when it enters the library, copied
again as it enters the sockets to the bus, which then sends it into
another socket to the client, which then makes a copy. These copies
really hurt, and are one of the things that ORBit does better than DBus:
it copies less frequently and has generally had more optimisation time.
There are several ways of making it better.

-   Those nice people at [Collabora](http://collabora.co.uk/) have
    ~~batshit insane~~ cunning plans for a SHM-based transport, instead
    of sockets. This would reduce the number of copies, but does
    introduce some, well, *interesting* locking fun.
-   Almost half of the copies could be removed by creating a private
    connection between the client and the server, missing out the
    bus entirely. I plan on doing this for the book views as they are
    heavily used and the bottleneck is, again, the copies.
-   Obviously, heavily profiling and optimising `libdbus` to remove any
    redundant copies would help. There may be cunning ways to avoid some
    of the copies for certain situations, and allowing those to happen
    in the GLib bindings would be useful.

In conclusion, the DBus vs ORBit performance issue isn't a real one
generally. It's a rare case where the cost of the transport is the
bottleneck (generally only in fake benchmarks), and when the transport
is the bottleneck, there are generally other ways of optimising the
code: as Havoc says DBus makes doing true asynchronous method calls
trivial, and if you are sending huge amounts of data down the bus a
private connection should be used (which will also make you a nicer
citizen on the bus, as sending huge messages will clog up the tubes).

<small>NP: <cite>Kind Of Blue</cite>, Miles Davis </small>
