Title: DBus-using Library Design
Date: 2008-02-18 15:22
Tags: tech
Slug: dbus-using-library-design

As a rule of thumb, when writing a library which uses DBus please
*always* make asynchronous DBus method calls. You never know when
someone will write a system using your library which, via a series of
plugins, ends up calling itself. A single main loop and blocking DBus
calls are not a great combination, and leads to lots of frustration.

Yes, `libnotify`, I'm looking at you.

<small>NP: <cite>Out of Season</cite>, Beth Gibbons</small>
