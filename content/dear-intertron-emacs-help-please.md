Title: Dear Intertron: Emacs Help Please?
Date: 2008-03-19 10:40
Tags: tech
Slug: dear-intertron-emacs-help-please

I recently switched to Emacs from XEmacs, and have pretty much got it
working how I like. There are just two problems remaining.

1.  I'm using emacsclient, and when I close the last frame Emacs quits.
    With XEmacs when in server mode the process continues when the last
    frame is closed, anyone know how I can get Emacs to do this too?
2.  Emacs appears to be moving the mouse pointer when I open a
    new frame. This is totally frustrating not only because I use sloppy
    focus, but also because its moving the point to *the wrong frame*.
    How can I turn this off?

Help greatly appreciated!
