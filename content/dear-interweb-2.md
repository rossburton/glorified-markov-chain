Title: Dear Interweb...
Date: 2007-01-18 10:30
Tags: tech
Slug: dear-interweb-2

Quick question for the Interweb regarding bash completion. If I do
`cat ~/[tab]`, I get to pick from 182 completions, as it lists all of
the dotfiles. Is there a way of telling bash to only complete dot files
if I put a dot, otherwise only complete non-hidden files? Answers on a
postcard, or if you must, a comment on this blog. Thanks!

<small>NP: <cite>IBM 1401, a User's Manual</cite>, Jóhann
Jóhannsson</small>
