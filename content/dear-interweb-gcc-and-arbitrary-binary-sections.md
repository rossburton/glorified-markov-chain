Title: Dear Interweb: GCC and Arbitrary Binary Sections
Date: 2007-07-13 12:30
Tags: tech
Slug: dear-interweb-gcc-and-arbitrary-binary-sections

Mono/C♯ has this nice feature where arbitrary files can be linked into
the final binary, and you can programmatically access them. I'd like to
be able to do that in C too, I'm sure it is possible, I just don't know
an easy way. I know that if you have a section `foo`, then ld will
create `__start_foo` and `__stop_foo` symbols which point to the start
and end of the section, so all I really want is an easy way to get ld to
use the contents of an arbitrary file (say, `ui.xml`) as a section.

Anybody know how to do this? **Update:** thanks to Daniel Jacobowitz for
giving enough clues to a working, and clean, solution. I'll blog this
shortly.

<small>NP: <cite>The Sound Of A Handshake</cite>, cLOUDDEAD</small>
