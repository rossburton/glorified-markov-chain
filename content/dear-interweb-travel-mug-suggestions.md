Title: Dear Interweb: Travel Mug Suggestions
Date: 2009-08-10 21:37
Tags: life
Slug: dear-interweb-travel-mug-suggestions

I'm looking for a travel mug for when I go to the office and need some
recommendations. So far every one I've found is sealable enough so that
it won't splash around when it is upright (say in a car mug holder), but
as this has to survive a cycle ride across town in my bag it has to have
a perfect seal. Does anyone know of a mug like this, or should I resign
myself to being forced to grab a coffee from [Taylor Street
Baristas](http://www.taylor-st.com/) when I get to London?
