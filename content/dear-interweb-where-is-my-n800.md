Title: Dear Interweb: Where Is My N800
Date: 2007-07-17 10:30
Tags: tech
Slug: dear-interweb-where-is-my-n800

This is unusual for <cite>Dear Interweb</cite>... my personal N800 was
on our stand at GUADEC being used to demo Contacts, Dates and Tasks.
Then by the end of the day when we packed up... it was gone. I'm hoping
it is buried in someone else's bag from OpenedHand, but I'm also
thinking that someone picked it up from the stand thinking it was their
own.

Can everyone please check that any N800s they have are actually their
own? Mine is quite distinctive, it has the full Pimlico suite installed
(Contacts, Dates, Tasks) and my addressbook, calendar, and task list on.
Thanks!
