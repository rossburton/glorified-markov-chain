Title: Dear InterWeb...
Date: 2008-02-19 21:55
Tags: tasks, tech
Slug: dear-interweb

Mock The Week is about to start, so I'll be quick. Can I ellipsize a
GtkMenuItem? It's easy to make Tasks have very long menu items, which
end up being too big for the screen. I'd like to get the menu items
truncated so that the menu will fit on the screen, or something. I guess
I'll have to truncate the string manually if there is no magic way of
doing it directly in the menu, but that really is a fallback plan.
