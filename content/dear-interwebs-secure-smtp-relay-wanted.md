Title: Dear Interwebs: Secure SMTP Relay Wanted
Date: 2008-03-30 14:22
Tags: tech
Slug: dear-interwebs-secure-smtp-relay-wanted

I'm looking for a basic SMTP relay which supports SMTP AUTH, TLS,
supports the sendmail interface, and has a local mail queue, so that I
can send mail from my laptop in Evolution (to localhost, or calls
sendmail) and the shell (calling sendmail) when online or offline.

I need SMTP AUTH and TLS, which means nbsmtp, masqmail, and nullmailer
are out. I want a local queue for when I'm not online which means esmtp,
ssmtp, msmtp, and nullmailer are out (I'm not convinced that msmtp's
queue scripts are reliable enough). Surely there must be a simple SMTP
relay which will reliably manage a queue if the mail cannot be sent! If
not, does anyone know of a good guide to configuring Postfix or Exim to
do this?
