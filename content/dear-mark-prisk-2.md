Title: Dear Mark Prisk
Date: 2007-11-29 12:15
Tags: life
Slug: dear-mark-prisk-2

I sent this last night to my MP as a followup to [his
reply](http://www.burtonini.com/blog/life/prisk-2007-11-28-15-00) to my
original letter.

> Dear Mark Prisk,
>
> Recently I wrote to you regarding the "homoeopathic hospitals" EDM,
> and your response included the following paragraph:
>
> "All therapies should be considered equally, and decisions on whether
> or not to provide them on the NHS should be evidence-based, as is the
> case with all other conventional medicines and treatments."
>
> I wholeheartedly agree that all therapies provided by the NHS should
> be judged on openly peer-reviewed evidence of their effectiveness,
> because otherwise we'd still be using leeches, performing exorcisms or
> practising blood letting. However, as far as I am aware there is no
> scientific evidence that homoeopathy is any better than placebo, so
> could you tell me where you saw the evidence for homoeopathy that you
> are using to justify homoeopathic hospitals because I'd like to see it
> myself. Indeed, evidence that it was in fact better than placebo would
> be welcomed with open arms by the scientific community, because this
> would open entirely new realms of both medicine and physics.
>
> Yours sincerely,
>
> Ross Burton
