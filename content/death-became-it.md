Title: Death Became It
Date: 2004-05-18 00:00
Tags: tech
Slug: death-became-it

For the last few weeks I've been seeing weird DMA errors from the kernel
when I start using my laptop, and the wifi card has been a pain when I
insert it. I put this down to the new kernel or some package updates...
until tonight when I tried to use the laptop to be greeted with a number
of decidedly non-trivial messages...

A `fsck.ext3` later and the disk is worse, some `dentry`s are invalid
and I've a number of bad sectors. Arse. Luckily I had enough time before
the disk fell apart to boot my desktop and run `unison`, so I have a
backup of 90% of my personal files, but reinstalling the laptop is not a
task I'm looking forward to. My magical `interfaces` file took me ages
to get right!

Sigh, the return is filed with Hitachi so I'll be sending the disk off
tomorrow and will receive a replacement, well, in the future.

<small><cite>Rock On Time Dub</cite>, King Tubby. Must get more dub to
go with the bassier headphones I'll be getting.</small>
