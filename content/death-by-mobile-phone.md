Title: Death By Mobile Phone
Date: 2004-01-28 00:00
Tags: life
Slug: death-by-mobile-phone

In my pocket right now I have what is possibly the most annoying mobile
phone in existence -- the Siemens A55. Why do I have this instead of my
ever-faithful T68i? Well, it turns out my T68i (ever-faithful, remember)
decided that instead of sending noise into my ear when I use it, it
would send electricity instead.

Imagine the scene: I want to make a quick call to see where a friend is,
so I put the phone up to my ear and it feels like someone has stuck a
pin on my phone. I look at the phone, it seems okay. I put it by my ear,
and Slow Death By Invisible Mobile Phone Needles is back. Twenty seconds
of scientific experimenting later, and I discover that the needles
appear to retract when I remove the phone from the charger -- it appears
that my computer science degree was useful after all. Vicky wonders what
I am doing (did I forgot to mention I've been shouting curses at the
phone?) and as I explain she pokes at the phone, and also gets a shock
in her finger. The next day I was down the Vodafone shop explaining
about the Death By Tiny Needles the phone was trying to inflict on me,
hoping that they would upgrade me to, say, a T610, but no. Instead I get
this crappy red A55 whilst they repair my phone, which takes a
fortnight.

One interesting thing came out of all this -- Vodafone obviously have a
stash of phones they give to people who break their phones. It is
logical to assume that when they get the phones back again they don't
have any SIM cards in, so they can't wipe the phones memory easily...
(well, they could use another SIM to boot the phone and wipe it, but
they don't). Turns out the lady (she has a boyfriend) who last had this
phone didn't delete the SMS's she had received. She is "a ploncker"
(sic) says one. Another gives me someones bank details. One more
suggests they engage in "phone nookie". One of the others is a little
more risqu&eactute; and not suitable for this forum...
