Title: Debian Documentation in Yelp Love Shocker
Date: 2005-02-18 10:41
Tags: tech
Slug: debian-documentation-in-yelp-love-shocker

Today I started asking [Shaun](http://www.gnome.org/~shaunm/blog/) about
hacking on Yelp again, with an aim of adding support for reading the
Debian `doc-base` index files. Just as I started to understand what I
needed to do, [Thom](http://blog.clearairturbulence.org/) pointed out
that if I installed the [Ubuntu `doc-base`
package](http://archive.ubuntu.com/ubuntu/pool/main/d/doc-base/) it
would Just Work, as they generate OMF at install-time.

This is totally excellent news and I hope that Thom finds the time to
take this upstream into Sid so we can have it in Sarge. Being able to
use Yelp to read all of the documentation I've got installed on my
machine is wonderful, and far more enjoyable than using a web browser
and searching around `/usr/share/doc`. Requisite screenie:

![Debian documentation in Yelp](http://www.burtonini.com/computing/screenshots/yelp-doc-base.png)

Thom rocks my world!

<small>NP: <cite>Greatest Hits</cite>, Toots and the Maytals</small>
