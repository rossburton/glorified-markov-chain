Title: Debian GNOME Policy
Date: 2003-07-31 00:00
Tags: tech
Slug: debian-gnome-policy-2

After a brief discussion about the naming scheme for GNOME applets on
`debian-gtk-gnome`, I realised that the GNOME Policy I have online is
out of date.

This is the latest version, 20030502-1, in
[Docbook](/computing/gnome-policy-20030502-1.xml) and
[HTML](/computing/gnome-policy-20030502-1.html). Expect a new revision
today or tomorrow as I add changes from Gustavo about DevHelp.
