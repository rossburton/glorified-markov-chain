Title: Debian GNOME Policy
Date: 2005-01-23 00:00
Tags: tech
Slug: debian-gnome-policy

I've finally made some changes to the Debian GNOME Policy and commited
them into Subversion. A HTML version of the document is [available
here](http://www.burtonini.com/computing/gnome-policy-20050123.html).
This release has some fairly large changes, such as recommending the use
of `dh_gconf` and `dh_scrollkeeper`, removal of the Nautilus view's
section, a final rewrite of the `gtk-doc` section, and probably several
other changes I've forgotten about. Everyone involved please read it and
mail comments to the [mailing
list](mailto:debian-gtk-gnome@lists.debian.org).
