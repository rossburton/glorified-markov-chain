Title: Debian Gubbins
Date: 2003-02-04 00:00
Tags: tech
Slug: debian-gubbins

Many things have been going on...

I have finally officially passed all of the Debian tests. All that is
left now is for my AM to write the report, and recommend me. Excellent!

I've been on a packaging mission recently too. Glynn Foster's `gdialog`
replacement, <cite>Zenity</cite>, is packaged but awaiting a manpage.
This morning I stole the RH patches and packaged up `printman`, a GNOME
2 printer manager written by Sun/Ximian/Wipro. These packages are
currently sitting in my repository but I hope to get them uploaded asap.

I've also written a [Debian GNOME Policy](/computing/gnome-policy.html)
document. It's only a draft at the moment, but it is getting there.
Comments are very welcome!

Just for good measure, I also created `dh_gconf` and `dh_scrollkeeper`.
Joey Hess has said he would put them into `debhelper`, so hopefully they
will be in sid soon. Doing Scrollkeeper and GConf correctly is quite
hard, and these tools make it trivial.
