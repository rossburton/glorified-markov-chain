Title: Debian Maintainership
Date: 2002-10-10 00:00
Tags: tech
Slug: debian-maintainership

Sweet - last night Colin Walters advocated me for New Maintainership in
Debian. This is an important first step down the long (and slow) process
of becoming a Debian Developer.
