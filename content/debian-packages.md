Title: Debian Packages
Date: 2003-06-05 00:00
Tags: tech
Slug: debian-packages

I'm a little busy, but managed to upload Zenity 1.3 and EggCups into
Sid. EggCups is the cute print job notification tool from Red Hat 9,
which shows an icon when there are active print jobs.
