Title: Debian Packaging
Date: 2002-10-04 00:00
Tags: tech
Slug: debian-packaging

My local APT tree is getting quite full now. The
[Meld](http://meld.sf.net) and [Straw](http://www.nongnu.org/straw)
packages are just waiting for `python2.2-gnome2` to be packaged (if you
have installed it yourself, please try out the packages) before they can
be uploaded by a kind sponsor (who has mainly been robster, of
DebianPlanet fame, recently. Thanks!).

I've also packaged the Mist GTK+ theme engine (for both GTK+ 1.2 and
GTK+ 2.0) and the Python wrapper around
[MusicBrainz](http://www.musicbrainz.com). I only did it as an
experiment to see how Python can be packaged (very easily is the
answer), but I might use it in my CD ripping tool when I eventually get
that off the ground.
