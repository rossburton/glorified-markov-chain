Title: Debian's Centre of Gravity
Date: 2004-01-05 00:00
Tags: tech
Slug: debians-centre-of-gravity

The location of Debian's centre of gravity in England was confirmed
again yesterday, when [Martin Michlmayr](http://www.cyrius.com/) moved
there to study. At the same time I found this out, I also discovered
that he fixed the [Developer's Packages
Overview](http://qa.debian.org/developer.php?login=ross). When I
eventually get my sorry arse up to Cambridge (a whole £6.80 on the
train) I shall have to buy him a drink.

NP: Keep It Unreal, by Mr. Scruff.
