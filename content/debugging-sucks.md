Title: Debugging Sucks
Date: 2003-10-14 00:00
Tags: tech
Slug: debugging-sucks

I hate debugging. I hate debugging memory leaks more. I *really* hate
debugging memory leaks when
[Valgrind](http://developer.kde.org/~sewardj/) won't work.

Today I finally had a poke at the memory leak in `gnome-cups-icon` in
Debian. This isn't a little memory leak, its 20 bytes or so every few
seconds, which soon adds up to a 600M process when left running
overnight. As Valgrind just refused to play along with Bonobo, I had to
resort to more hacky measures:

    #include <stdlib.h>
    static void
    print_usage(void)
    {
            char buf[256];
            sprintf(buf, "grep VmRSS /proc/%d/status", getpid());
            system(buf);
    }

    static char *
    get_default (void)
    {
            ...
            print_usage();
            default_dest = cupsGetDest (NULL, NULL, num_dests, dests);
            print_usage ();
            ...
      

Urrrgh. In the end I traced the leak to `cupsFreeDests()` not actually
freeing any memory, which sucks.
