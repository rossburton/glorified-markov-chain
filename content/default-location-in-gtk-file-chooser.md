Title: Default Location in GTK+ File Chooser
Date: 2006-02-20 12:30
Tags: tech
Slug: default-location-in-gtk-file-chooser

Today I finally got annoyed enough with Ubuntu's GTK+ defaulting my file
choosers to `$HOME/Documents` rather than `$HOME`. I can see how this
would be useful, but Documents contains a few files, whereas I generally
want to get into \~/Pictures, or \~/Programming. Luckily sebuild saves
the day, by telling me about `GTK_DEFAULT_FILECHOOSER_DIR`. One quick
export later:

> `export GTK_DEFAULT_FILECHOOSER_DIR=$HOME`

And voilà, it works. Now please can the comments section of this post
not turn into a “new GTK file chooser sucks arse d000dz old one r0xkkz”.
You try explaining to a non-geek why “parent folder” is `..`, and why
`.` is in the folder list at all.

<small>NP: <cite>Songs In The Key Of Life</cite>, Stevie Wonder</small>
