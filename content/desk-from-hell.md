Title: Desk From Hell
Date: 2005-02-06 00:00
Tags: life
Slug: desk-from-hell

Now that I'm working for [Opened Hand](http://www.openedhand.com) from
home I mostly work with my laptop sitting on the sofa, with a few
cushions to support my back. This isn't a bad working position as I'm
comfortable and it means I'm in the right place for my speakers, so get
to listen to loud music. Fast-forward a month or so: Vicky left Peason
and is looking for another job, whicih involves lots of time on the
other computer searching for jobs, writing letters, filling in forms,
etc. This is a good thing as it justifies Eddie's existance: previously
his entire reason to exist was as a backup for my data and to play
Half-Life 2 on. This is also a bad thing: Vicky is <emph>tidying</emph>
my desk.

This is some strange use of the word "tidy" I'd never heard before, it
started with a [fluffy pen monster
thing](http://www.burtonini.com/photos/Random/thumb-img_0181.jpg)
appearing, followed by voiles (thin curtains apparently) on the window,
and finally **flowers** by the monitor. She appeared to notice my
expression of shock and horror at this point and finished it off my
putting a pink, furry, cushion on the chair, which was hastily removed.
I note that the rest is still there, and doesn't show any sign of
moving.

Maybe it's time I "tidy" the front room with chunkier speaker wire, a
new subwoofer, and a pile of trashy men magazines?

<small>NP: <cite>O</cite>, Damien Rice</small>
