Title: Despair
Date: 2003-09-24 00:00
Tags: life
Slug: despair

Sometimes I give up with humanity, I really do.

I've been seeing too many stories like [this
one](http://www.mirror.co.uk/news/allnews/content_objectid=13441530_method=full_siteid=50143_headline=-ATKINS%2DAGONY-name_page.html)
recently. The executive summary is some fool goes on an extreme diet
(Atkins this time), and they start feeling weak, tired, dehydrated,
whatever. Whereas normal people would consider, say, eating or drinking
something, these people carry on starving themself until they can't even
get out of a bath.

JESUS PEOPLE. You'd think the Atkin's diet was destroying brain cells
instead of fat cells. GET A GRIP.
