Title: Detox
Date: 2006-03-10 16:45
Tags: life
Slug: detox

Nice to see that [Cecil considers detox diets to be
bunk](http://www.straightdope.com/columns/060310.html). People, we have
two kidneys and a liver for a reason.

<small>NP: <cite>Return Of The Boom Bap</cite>, KRS-One</small>
