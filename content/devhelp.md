Title: DevHelp
Date: 2003-03-11 00:00
Tags: tech
Slug: devhelp

I've been hacking now and again on DevHelp recently, and its looking
really good. Micke re-wrote the parser so that instead of taking 2
minutes to start (DevHelp 0.4, with my 4M Java API book) it now takes
about 10 seconds.

However, there are some changes from DevHelp 0.4 which will bite you in
the arse:

Book Format

:   The book format has slightly changed. Summary: remove all
    `<chapter>` tags and replace them with `<sub>`.

Book Location

:   Devhelp searches in `$PREFIX/share/gtk-doc`,
    `$PREFIX/share/devhelp/books` and `$HOME/.devhelp/books` for books.
    A book is comprised of a directory (say `foo`), with a `.devhelp`
    file inside (say `foo.devhelp`). This is different to DevHelp 0.4
    which seperated the specs and books into seperate folders.

    Note that DevHelp 0.5 is a strange beast and searches
    `$HOME/.devhelp2/books` instead. This has been changed in CVS.

    DevHelp can read gzip-compressed `.devhelp` files, if you have
    large books.

Base URL

:   The location of the content is set by the optional `base` attribute
    to the `book` element. If this element does not exist, the base URL
    for the content is the location of the `devhelp` file.

    This allows books with content in read-only locations; simply set
    the base URL to point at the directory where the content is.

Recent gtk-doc installs generate DevHelp files when they generate the
API documentation, so in theory every GNOME library you install will
have documentation in DevHelp. It's a shame that this doesn't always
work, many packages need to be updated.
