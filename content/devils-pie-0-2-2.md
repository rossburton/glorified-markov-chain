Title: Devil's Pie 0.2.2
Date: 2002-10-06 16:45
Tags: tech
Slug: devils-pie-0-2-2

Devil's Pie 0.2.2 is out, available at the [usual
location](/computing/devilspie-0.2.2.tar.gz). This release fixes a few
bugs, adds a default configuration file, and adds actions to set the
WM\_STATE\_BELOW and WM\_STATE\_ABOVE hints.

Debian packages are also available from my `apt-get`-able repository, at
[http://www.burtonini.com/debian/](/debian/).
