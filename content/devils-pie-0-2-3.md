Title: Devil's Pie 0.2.3
Date: 2002-11-07 19:19
Tags: tech
Slug: devils-pie-0-2-3

Devil's Pie 0.2.3 is out, available at the [usual
location](/computing/devilspie-0.2.3.tar.gz). This release is nothing
more than a rebuild of the GOB files, with GOB 2.0.3. This will cure any
problems along the lines of "FooMatcher cannot be found" when Devil's
Pie was built using GCC 3.
