Title: Devil's Pie 0.2.4
Date: 2003-05-02 14:49
Tags: tech
Slug: devils-pie-0-2-4

Devil's Pie (everyone favourite window manipulation tool) 0.2.4 is out.
It appears that I intended to do a 0.2.4 release many moons ago, since
it has regular expression matching for window titles and application
names, but I never got around to it...

In this last week I have received not one but two different RPM spec
files for it, so I thought I best get a release with them in out before
I get more!

Thanks to Patrick Aussems for the regexp code, and Michael Raab/Lars R.
Damerow for the RPM spec files.

Downloads are in the usual place, a [tarball is
here](/computing/devilspie-0.2.4.tar.gz) and [Debian packages](/debian/)
are available. The tarball has a spec file, so `rpmbuild -tb` should
work.
