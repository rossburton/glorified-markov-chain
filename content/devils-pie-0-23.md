Title: Devil's Pie 0.23
Date: 2012-10-01 10:55
Tags: devilspie, tech
Slug: devils-pie-0-23

This may come as a shock to some, but I've just tagged Devil's Pie 0.23
([tarball
here](http://www.burtonini.com/computing/devilspie-0.23.tar.xz)).

**tl;dr: don't use this, but if you insist it now works with
`libwnck3`**

The abridged changelog:

-   Port to libwnck3 (Christian Persch)
-   Add unfullscreen action (Mathias Dalh)
-   Remove exec action (deprecated by spawn)

I probably wouldn't have ever released this as I'm generally not
maintaining it and tend to push people towards [Devil's Pie
2](http://www.gusnan.se/devilspie2/) which funnily enough had a 0.23
release two days ago, but Christian asked nicely and I was waiting on a
Yocto build to finish.
