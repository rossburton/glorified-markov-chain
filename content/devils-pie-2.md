Title: Devil's Pie
Date: 2007-11-24 20:54
Tags: tech
Slug: devils-pie-2

A window-matching utility, inspired by Sawfish's "Matched Windows"
option and the lack of the functionality in Metacity. Metacity lacking
window matching is not a bad thing — Metacity is a lean window manager,
and window matching does not have to be a window manager task.

Devil's Pie can be configured to detect windows as they are created, and
match the window to a set of rules. If the window matches the rules, it
can perform a series of actions on that window. For example, I can make
all windows created by X-Chat appear on all workspaces, and the main
Gkrellm1 window does not appear in the pager or task list.

### Download

Latest download is
[devilspie-0.22.tar.gz](/computing/devilspie-0.22.tar.gz).

### Bug Reporting

You can report bugs in Devil's Pie in the [GNOME
Bugzilla](http://bugzilla.gnome.org/browse.cgi?product=devilspie).
