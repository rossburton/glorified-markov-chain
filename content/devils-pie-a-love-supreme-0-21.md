Title: Devil's Pie "A Love Supreme" 0.21
Date: 2007-09-24 04:29
Tags: tech
Slug: devils-pie-a-love-supreme-0-21

Devil's Pie (someones favourite window manipulation tool) 0.21 is out.
Now with even more actions!

-   Add change\_workspace action (\#453464, thanks Adam Więckowski)
-   Add decorate action (\#449634, thanks Galkin Vasily)
-   Add quit action (thanks Leon Zhang)
-   Handle wnck\_screen\_get\_active\_workspace() failing (\#343546)

Downloads are in the [usual
place](http://www.burtonini.com/computing/devilspie-0.21.tar.gz).
