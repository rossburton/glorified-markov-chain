Title: Devil's Pie Bugzilla
Date: 2006-01-26 10:40
Tags: tech
Slug: devils-pie-bugzilla

There is now a Devil's Pie product in the GNOME Bugzilla, so any bugs
can be filed there instead of sending me a mail (which I'll then forget
about). Thanks to the super-rad Bugzilla upgrade, the [summary
page](http://bugzilla.gnome.org/browse.cgi?product=devilspie) is pretty
useful.

<small>NP: <cite>Dial 'M' For Monkey</cite>, Bonobo</small>
