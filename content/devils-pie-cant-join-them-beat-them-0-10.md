Title: Devil's Pie "Can't Join Them? Beat Them" 0.10
Date: 2005-05-05 04:23
Tags: tech
Slug: devils-pie-cant-join-them-beat-them-0-10

Devil's Pie (everyone favourite window manipulation tool) 0.10 is
finally out. This release is very special as it actually compiles with
GNOME 2.10, many thanks to Crispin Flowerday for the final patch to
complete this.

-   Re-implement the required private wnck functions (Crispin Flowerday)
-   Add min/max hints to the opacity action property
-   Display the min/max hints in the generated documentation

Downloads are in the [usual
place](http://www.burtonini.com/computing/devilspie-0.10.tar.gz). I'll
have Debian Sid and/or Ubuntu Hoary packages shortly.
