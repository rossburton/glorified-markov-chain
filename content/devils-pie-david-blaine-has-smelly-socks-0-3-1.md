Title: Devil's Pie "David Blaine has smelly socks" 0.3.1
Date: 2003-09-18 01:45
Tags: tech
Slug: devils-pie-david-blaine-has-smelly-socks-0-3-1

Devil's Pie (everyone favourite window manipulation tool) 0.3.1 is out.
Finally. Again. Changes are:

-   Don't crash when a window role is null in various places
    (Rob Mayoff)
-   Don't crash on unknown properties (RM)
-   Being very slow at applying patches (me)

Downloads are in the usual place, a [tarball is
here](http://www.burtonini.com/computing/devilspie-0.3.1.tar.gz).
