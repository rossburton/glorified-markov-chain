Title: Devil's Pie "Derren Brown Is A Witch" 0.4
Date: 2004-06-07 07:02
Tags: tech
Slug: devils-pie-derren-brown-is-a-witch-0-4

Devil's Pie (everyone favourite window manipulation tool) 0.4 is out.
Not bad since the last release was in September... Changes are:

-   Add an action to set the window type atom (Olivier Andrieu)
-   Add an action to set the decorate window hint in OpenBox
    (Vaclav Lorenc)
-   Watch all screens for events, instead of just the default

Downloads are in the usual place, a [tarball is
here](http://www.burtonini.com/computing/devilspie-0.4.tar.gz). Debian
packages will be prepared tomorrow, I'm going to bed now. The tarball
has a spec file, so `rpmbuild` should work.
