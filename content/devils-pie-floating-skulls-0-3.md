Title: Devil's Pie "Floating Skulls" 0.3
Date: 2003-08-14 17:16
Tags: tech
Slug: devils-pie-floating-skulls-0-3

Devil's Pie (everyone favourite window manipulation tool) 0.3 is out.
Finally. Changes are:

-   Vastly improved documentation (Gaetan de Menten)
-   Add a "fullscreen" property (requires libwnck 2.3.something)
-   Many bug fixes (Patrick Aussems)

Downloads are in the usual place, a [tarball is
here](http://www.burtonini.com/computing/devilspie-0.3.tar.gz). A Debian
package has been uploaded into Sid, hopefully it will be accepted soon.
The tarball has a spec file, so `rpmbuild -tb` should work.
