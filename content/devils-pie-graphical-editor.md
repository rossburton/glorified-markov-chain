Title: Devil's Pie Graphical Editor
Date: 2008-03-18 10:30
Tags: tech
Slug: devils-pie-graphical-editor

Thanks to [Chris](http://chrislord.net) for pointing out
[gdevilspie](http://code.google.com/p/gdevilspie/) to me, a graphical
interface to writing Devil's Pie rule files. I've never used it so I
can't comment on how well it works, but I'm very glad that someone
finally wrote it!
