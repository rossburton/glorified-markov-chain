Title: Devil's Pie in Fedora
Date: 2006-12-10 17:00
Tags: tech
Slug: devils-pie-in-fedora

Sebastian Vahl just mailed me to say that he has packaged [Devil's Pie
for Fedora
Core](http://download.fedora.redhat.com/pub/fedora/linux/extras/6/i386/repodata/repoview/devilspie-0-0.19-2.fc6.html)
(5, 6, and development) in Extras. Thanks Sebastian!
