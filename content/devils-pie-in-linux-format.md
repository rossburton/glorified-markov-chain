Title: Devil's Pie in Linux Format
Date: 2007-02-08 16:50
Tags: tech
Slug: devils-pie-in-linux-format

Jeff Waugh talks about Devil's Pie in his [interview with Linux Format
(issue 87)](http://linuxformat.co.uk/waugh.html).

> However, there's this really cool thing called Devil's Pie, which is
> an extra little thing you run. It just plugs in and you can completely
> script the way Metacity works, using Lisp. Much in the same way you
> could with Sawfish, except for being a plugin and being directly
> focused on scripting the window manager. You can do amazing stuff. The
> work on that has actually been sponsored by Pixar.

Thanks Jeff! Any more of this and people will start thinking I'm paying
you...

<small>NP: <cite>Flight 602</cite>, Aim</small>
