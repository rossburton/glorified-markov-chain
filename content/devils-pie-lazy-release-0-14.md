Title: Devil's Pie "Lazy Release" 0.14
Date: 2005-10-17 04:05
Tags: tech
Slug: devils-pie-lazy-release-0-14

Devil's Pie (someones favourite window manipulation tool) 0.14 is out. A
nice and lazy release here, I just merged patches from other people.

-   Add Set Geometry (Guido Boehm)
-   Fix Set Window Type (Stefan van der Haven)
-   Make Pin work as expected (Jean-Yves Lefort)
-   Append new expressions, not prepend (Lars Damerow)
-   Fix typos in README (Larry Virden)

Downloads are in the [usual
place](http://www.burtonini.com/computing/devilspie-0.14.tar.gz).
