Title: Devil's Pie "Mystery Boxes" 0.8
Date: 2005-01-24 02:17
Tags: tech
Slug: devils-pie-mystery-boxes-0-8

Devil's Pie (everyone favourite window manipulation tool) 0.8 is out.
This release is full of new features thanks to many contributed patches,
so grab this now and have fun.

-   Bring back --apply-to-existing, so you can work on existing windows
-   Add maximized\_horizontally and \_vertically to the resize action
    (John Russell)
-   Add an execute action (John Russell)
-   Add an opacity setting action (Guido Boehm)
-   Generate decent matcher/action documentation at build-time from the
    source
-   Check that regular expressions parsed correctly, fixing a common bug
-   Less compile warnings

Downloads are in the usual place, a [tarball is
here](http://www.burtonini.com/computing/devilspie-0.8.tar.gz), Debian
packages [here](http://www.burtonini.com/debian), and will be in
unstable tomorrow.
