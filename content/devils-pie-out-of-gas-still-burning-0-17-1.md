Title: Devil's Pie "Out Of Gas, Still Burning" 0.17.1
Date: 2006-03-07 16:53
Tags: tech
Slug: devils-pie-out-of-gas-still-burning-0-17-1

Devil's Pie (someones favourite window manipulation tool) 0.17.1 is out.
This time I actually declare the dependency on Glib 2.10...

-   Depend on GLib 2.9.1 onwards

Downloads are in the [usual
place](http://www.burtonini.com/computing/devilspie-0.17.1.tar.gz).
