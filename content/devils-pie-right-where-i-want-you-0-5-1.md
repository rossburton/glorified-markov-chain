Title: Devil's Pie "Right Where I Want You" 0.5.1
Date: 2004-08-17 17:44
Tags: tech
Slug: devils-pie-right-where-i-want-you-0-5-1

Devil's Pie (everyone favourite window manipulation tool) 0.5.1 is out.
Brown paper bag release to fix a typo.

Downloads are in the usual place, a [tarball is
here](http://www.burtonini.com/computing/devilspie-0.5.1.tar.gz).
