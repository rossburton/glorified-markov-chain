Title: Devil's Pie "Right Where I Want You" 0.5
Date: 2004-08-17 15:48
Tags: tech
Slug: devils-pie-right-where-i-want-you-0-5

Devil's Pie (everyone favourite window manipulation tool) 0.5 is out.

-   Add an action to set the geometry of a window, by Henrik
    Brix Andersen.

Downloads are in the usual place, a [tarball is
here](http://www.burtonini.com/computing/devilspie-0.5.tar.gz). Debian
packages being uploaded shortly.

<small>NP: <cite>Keep It Unreal</cite>, Mr Scruff</small>
