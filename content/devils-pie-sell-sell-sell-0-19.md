Title: Devil's Pie "Sell Sell Sell" 0.19
Date: 2006-12-01 16:56
Tags: tech
Slug: devils-pie-sell-sell-sell-0-19

Devil's Pie (someones favourite window manipulation tool) 0.19 is out.
Just a fix for older systems here.

-   Don't use `wnck_window_set_geometry` as it was introduced in GNOME
    2.16 (\#381233).

Downloads are in the [usual
place](http://www.burtonini.com/computing/devilspie-0.19.tar.gz).
