Title: Devil's Pie "Simmer To A Sizzle Like The Days Of Old" 0.18
Date: 2006-10-20 17:22
Tags: tech
Slug: devils-pie-simmer-to-a-sizzle-like-the-days-of-old-0-18

Devil's Pie (someones favourite window manipulation tool) 0.18 is out.
Features galore in this release!

-   Add window\_property matcher (Nigel Tao)
-   Add set\_viewport action (James Willcox)
-   Add opacity action (Pavel Palat)
-   Add sticky action (Steve Leung)
-   Add unmaximise action (Alex Menk)
-   Add unminimise action (Richard Neill)
-   Fix negative offsets in geometry action (Vincent Ho, Midred)

Downloads are in the [usual
place](http://www.burtonini.com/computing/devilspie-0.18.tar.gz).
