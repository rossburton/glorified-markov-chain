Title: Devil's Pie "Stay With Me While We Grow Old" 0.17
Date: 2006-03-07 01:18
Tags: tech
Slug: devils-pie-stay-with-me-while-we-grow-old-0-17

Devil's Pie (someones favourite window manipulation tool) 0.17 is out.
Nothing interesting, just a fix so that it builds with GLib 2.10.

-   Fix compile with GLib 2.10

Downloads are in the [usual
place](http://www.burtonini.com/computing/devilspie-0.17.tar.gz).
