Title: Devil's Pie "Stumpy" 0.7
Date: 2004-09-21 05:21
Tags: tech
Slug: devils-pie-stumpy-0-7

Devil's Pie (everyone favourite window manipulation tool) 0.7 is out. It
appears I forgot to tell the world about 0.6 ("Salmon Sunset"), so I'll
put all the changes here.

-   Fix the fullscreen action
-   Set the Motif hint when setting the decoration hint (Vaclav Lorenc)

Downloads are in the usual place, a [tarball is
here](http://www.burtonini.com/computing/devilspie-0.7.tar.gz). Debian
packages being uploaded to
[`burtonini.com/debian`](http://www.burtonini.com/debian) now and will
be in Debian... shortly. The <cite>Sarge</cite> freeze will affect this,
of course.
