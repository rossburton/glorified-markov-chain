Title: Devil's Pie "Sweet Music, Soul Music?" 0.22
Date: 2007-11-24 20:55
Tags: tech
Slug: devils-pie-sweet-music-soul-music-0-22

Devil's Pie (someones favourite window manipulation tool) 0.22 is out.
Just one bug fix, which shows that my users all use OpenBox.

-   Fix decorate/undecorate with OpenBox (thanks Rafał Mużyło)
-   Fix typos in man page (Loïc Minier)

Downloads are in the [usual
place](http://www.burtonini.com/computing/devilspie-0.22.tar.gz).
