Title: Devil's Pie "Trying To Make Their Paper" 0.12
Date: 2005-09-27 00:44
Tags: tech
Slug: devils-pie-trying-to-make-their-paper-0-12

Devil's Pie (someones favourite window manipulation tool) 0.12 is out.
This release is very boring and is mainly a cleanup release:

-   Quit if no flurbs were loaded
-   Use GOption instead of popt
-   Major code cleanup

Downloads are in the [usual
place](http://www.burtonini.com/computing/devilspie-0.12.tar.gz).

Thanks to a little help from a very nice Devil's Pie user, expect
another release tomorrow. Brace yourself, it's going to be surprising!
