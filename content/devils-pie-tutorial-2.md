Title: Devil's Pie Tutorial
Date: 2007-07-26 11:17
Tags: tech
Slug: devils-pie-tutorial-2

Christer Edwards over at [Ubuntu Tutorials](http://ubuntu-tutorials.com)
has written [a short tutorial on Devil's
Pie](http://ubuntu-tutorials.com/2007/07/25/how-to-set-default-workspace-size-and-window-effects-in-gnome/).
Thanks Christer!

Now if only people would stop using a hack I wrote several years ago and
fix the real problems...

<small>NP: <cite>The Last Flowers from the Darkness</cite>, Mark Van
Hoen</small>
