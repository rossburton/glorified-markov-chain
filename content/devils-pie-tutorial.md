Title: Devil's Pie Tutorial
Date: 2006-01-24 18:00
Tags: tech
Slug: devils-pie-tutorial

On Xlife there is a [brief tutorial on Devil's
Pie](http://x2.zuavra.net/index.php/48/). I should merge it into the
README and add a reference for the actions at some point, but it's a
good start.

<small>NP: <cite>A Rush Of Blood The The Head</cite>, Coldplay</small>
