Title: Devil's Pie "Used To Love you" 0.11
Date: 2005-09-16 17:48
Tags: tech
Slug: devils-pie-used-to-love-you-0-11

Devil's Pie (everyone favourite window manipulation tool) 0.11 is out.
This release fixes a few bugs which meant that the window type and
execute actions were not usable, and adds a shading action.

-   Add an action to shade windows by Anonymous. I didn't write it but
    I've lost the mail the patch came in...
-   Fix the window type action
-   Fix the execute action
-   Check for xsltproc in configure, as it's required

Downloads are in the [usual
place](http://www.burtonini.com/computing/devilspie-0.11.tar.gz). I'll
have Debian Sid packages uploading shortly.
