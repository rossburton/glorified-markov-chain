Title: Devil's Pie "Wearing That Dress" 0.20.1
Date: 2007-01-29 15:26
Tags: tech
Slug: devils-pie-wearing-that-dress-0-20-1

Devil's Pie (someones favourite window manipulation tool) 0.20.1 is out.
Brown paper bag release, whoops!

-   Fix parsing (Lars Damerow)
-   Fix test suite

Downloads are in the [usual
place](http://www.burtonini.com/computing/devilspie-0.20.1.tar.gz).
