Title: Devil's Pie "Wearing That Dress" 0.20.2
Date: 2007-01-29 15:26
Tags: tech
Slug: devils-pie-wearing-that-dress-0-20-2

Devil's Pie (someones favourite window manipulation tool) 0.20.2 is out.
Brown paper bag release, whoops!

-   Fix window\_workspace (Andrew Yates)

Downloads are in the [usual
place](http://www.burtonini.com/computing/devilspie-0.20.2.tar.gz).
