Title: Devil's Pie "Wearing That Dress" 0.20
Date: 2007-01-12 21:45
Tags: tech
Slug: devils-pie-wearing-that-dress-0-20

Devil's Pie (someones favourite window manipulation tool) 0.20 is out.
New features galore!

-   Handle multiple expressions in a file (Lars Damerow)
-   Add spawn\_sync and spawn\_async actions to start processes
    (David Decotigny)
-   Add println, str, hex, and expand print (DD)
-   Add window\_xid matcher (DD)

Downloads are in the [usual
place](http://www.burtonini.com/computing/devilspie-0.20.tar.gz).
