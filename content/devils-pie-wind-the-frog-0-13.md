Title: Devil's Pie "Wind The Frog" 0.13
Date: 2005-09-28 02:31
Tags: tech
Slug: devils-pie-wind-the-frog-0-13

Devil's Pie (someones favourite window manipulation tool) 0.13 is out.
This release is far more exciting than 0.12!

-   Total rewrite.
-   No, really. A complete rewrite, no file left untouched. Many thanks
    to [Pixar Animation Studios](http://www.pixar.com) for sponsoring
    this work.
-   Use s-expressions instead of XML as the configuration file format,
    allowing matches to be combined in new and exciting ways with
    `(and)` `(or)` and `(not)` operators.
-   Read configuration from both the users home directory and /etc,
    allowing centralised configuration.
-   Add the beginnings of a test suite.

Downloads are in the [usual
place](http://www.burtonini.com/computing/devilspie-0.13.tar.gz). I'll
have Debian packages uploaded shortly I expect.

Thanks again to Pixar, for without their kindness this release would
never have happened.
