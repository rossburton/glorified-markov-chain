Title: Devil's Pie
Date: 2002-09-08 07:00
Tags: tech
Slug: devils-pie

Devil's Pie 0.2.1 is released. I'm working on Debian packages, and maybe
a RH7x RPM too. For now however, tarballs are [available
here](http://www.burtonini.com/computing/devilspie-0.2.1.tar.gz).
