Title: Die Evolution Die
Date: 2006-04-26 15:54
Tags: tech
Slug: die-evolution-die

This may well be `en_GB` sucking really badly (until recently in Ubuntu
it didn't have 24-hour time either), but this is really bad form for a
birthday field:

![Birthday field in
Evolution](http://burtonini.com/computing/screenshots/evo-y2k.png)

Someone will be first up against the wall, mark my words. A [bug is
filed](http://bugzilla.gnome.org/show_bug.cgi?id=339813), fingers
crossed it gets fixed soon.

**Update:** Richard "bork bork" Hult, running in `sv_SE`, has a four
digit year field. `en_GB` sucks.

<small>NP: <cite>Gonzo Jazz</cite>, Margin of Safety</small>
