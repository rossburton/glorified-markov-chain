Title: Die Libgnome Die
Date: 2007-05-04 16:20
Tags: tech
Slug: die-libgnome-die

I just committed to Evince [a
patch](http://bugzilla.gnome.org/show_bug.cgi?id=328842) which makes the
use of `libgnome` optional, meaning that Evince is now buildable on a
pure [GMAE](http://gnome.org/mobile) system ([obligatory
screenshot](http://burtonini.com/computing/screenshots/evince.png)). As
GMAE has GConf and gnome-vfs, and gnome-keyring is trivial enough to
leave in, the patch turned out to be quite simple:

-   Don't use libgnome to start the help browser, but call Yelp directly
-   Don't use libgnome to get an icon for a MIME type, instead copy ten
    lines of code from GTK+
-   Disable use of GnomeProgram and session management

So the only regressions are that the keyboard accelerators are not
saved, there is no session management (until session management lands in
GTK+), and if gnome-vfs wants a password you don't get a password
dialog. In my opinion only the last regression is actually a problem,
and I want to fix that at some point.

Thanks to the Evince maintainers for their rapid review!

<small>NP: <cite>Position Correction</cite>, TZU</small>
