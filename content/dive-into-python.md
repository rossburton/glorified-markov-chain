Title: Dive Into Python
Date: 2003-10-10 00:00
Tags: tech
Slug: dive-into-python

Before I forget again, last night I made Debian packages for [Dive Into
Python](http://diveintopython.org). They won't go into Sid just yet, as
it is under the GFDL and there are some, well, issues there. For now,
packages are available in [my
repository](http://www.burtonini.com/debian/).
