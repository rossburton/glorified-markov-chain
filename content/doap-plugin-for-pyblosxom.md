Title: DOAP Plugin for Pyblosxom
Date: 2004-10-25 00:00
Tags: tech
Slug: doap-plugin-for-pyblosxom

People who are in the know are expecting big things from
[<abbr title="Description Of A Project">DOAP</abbr>](http://usefulinc.com/doap),
the XML/RDF schema for describing software projects. As a standard
location for DOAP storage is on the project's home page, it made sense
to link to DOAP files inside my blog.

The Pyblosxom plugin I created works quite well. If there is a file
called `doap.rdf` in a category the plugin will create replacement
variables so that my flavour templates can insert a `<link>` element in
the header and show a button in the sidebar. These links point to the
file, so of course at some point a client will try to load it. However
Pyblosxom treats this as a request for the article with the ID "doap"
using the `xml` flavour. Thankfully plugins can catch requests for files
and respond to them there and then, so the plugin can handle these
requests manually.

End result: the [Sound
Juicer](http://www.burtonini.com/blog/computers/sound-juicer) blog
category has a DOAP file, so the category has a `<link>` element and a
link to the DOAP in the sidebar. This is more than enough for the [DOAP
Viewer](http://doapy.bonjourlesmouettes.org/doap-viewer) to find the
DOAP, as can be [seen
here](http://doapy.bonjourlesmouettes.org/doap-viewer?url=http://www.burtonini.com/blog/computers/sound-juicer/doap.rdf).
At some point this week I'll clean the source up and submit it to the
Pyblosxom plugin registry, but if anyone wants it now just mail me.
