Title: DOAP World Domination
Date: 2006-03-14 21:00
Tags: tech
Slug: doap-world-domination

[Edd
chumped](http://dailychump.org/2006/03/14/2006-03-14.html#1142364107.195099)
that [Apache are using DOAP](http://projects.apache.org/doap.html) on
their projects site. This is very cool, and I really think that GNOME
should follow their example.

I've been thinking for a while about building a DOAP-based software map
for GNOME, and the [Apache Projects](http://projects.apache.org/) site
provides a very useful starting point... Time to find the source code
and try a GNOME-branded version!
