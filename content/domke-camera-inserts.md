Title: Domke Camera Inserts
Date: 2006-06-09 11:40
Tags: life
Slug: domke-camera-inserts

Quick question to the Interweb: does anyone out there have a Domke
camera bag or inserts, specifically the <cite>Adjustable Insert</cite>?
I have some questions about it and other Domke inserts. If you do,
please [email me](mailto:ross@burtonini.com).

<small>NP: <cite>Worldwide Underground</cite>, Erykah Badu</small>
