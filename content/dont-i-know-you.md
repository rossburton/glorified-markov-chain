Title: Don't I Know You?
Date: 2004-01-16 00:00
Tags: life
Slug: dont-i-know-you

So there I was, standing on the train home reading [Carter Beats the
Devil](http://www.amazon.co.uk/exec/obidos/ASIN/0340794992) (note to
self: create a reading list page) performing my life-time dream of
door-open-button-pusher (with a bonus of having to pull the doors apart
sometimes too) when I heard "Don't I know you?" in a rather familar
voice...

Enter long-time-no-see friend Cole, who I haven't seen for many years.
Good to see him again, albeit for about 15 seconds. At least I got a
book recommendation out of him -- the latest Jeff Noon novel.
