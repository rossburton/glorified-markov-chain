Title: Double Plus Ungood
Date: 2006-03-07 17:25
Tags: life
Slug: double-plus-ungood

I'm really not liking the way things are going at the moment.

-   “[the Legislative and Regulatory Reform Bill ... gives ministers
    power to alter any law passed by
    Parliament](http://www.timesonline.co.uk/article/0,,1072-2049791,00.html)”
-   “[\[South Dakota\] has signed into law a bill banning most
    abortions](http://news.bbc.co.uk/1/hi/world/americas/4780522.stm)”
-   “[Vice President Dick Cheney said Tuesday that Iran will not be
    allowed to have a nuclear weapon and warned ‘the United States is
    keeping all options on the table in addressing the irresponsible
    conduct of
    the regime.’](http://www.guardian.co.uk/worldlatest/story/0,,-5668881,00.html)”

<small>NP: <cite>Ambient Mix</cite>, DJ krill.minima</small>
