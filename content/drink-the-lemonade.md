Title: Drink The Lemonade
Date: 2007-01-23 13:10
Tags: tech
Slug: drink-the-lemonade

[Dave Cridland]() posted to maemo-developers again today, spuring me to
spend my lunch time checking out his email client instead of doing
something productive like eating. It's *very* interesting.

Basically he edits the RFCs for
[<cite>Lemonade</cite>](http://www.lemonadeformobiles.com/), which is a
IETF standard for email on mobile devices. Basically it is a set of
extensions for IMAP and SMTP that improve their performance over
high-latancy low-bandwidth connections (like bad wi-fi or GPRS), such as
forward-without-download and efficient re-synchronisation commands.
Existing extensions such as a useful IDLE implementation are also used
to push new mail notifications to the client.

To test the ideas in the real world there is
[Polymer](http://trac.dave.cridland.net/cgi-bin/trac.cgi/wiki/Polymer).
Polymer is a simple IMAP client that uses the Lemonade extensions as
much as possible, so is ideal for mobile use. There is more: it uses
ACAP to store the settings on the Internet (Dave also provides free ACAP
accounts) so it's configure once, run anywhere. I've just tried it on my
laptop, and it works nicely.

But it gets better. There is also
[Telomer](http://trac.dave.cridland.net/cgi-bin/trac.cgi/wiki/Polymer770),
an email client using Lemonade and designed for Maemo. I've just tried
it out, it may be seriously lacking in features at the moment, but it
works. Features can be written over time, but it takes serious thought
to get something as fast and usable as Polymer on mobile devices. I'll
quote Dave:

> That all said, once it's started, it connects to the mailserver over
> GPRS in about 10 seconds and pulls up the summary listing in 15, with
> a mailbox size of 2,882 messages. With a mailbox with 33,732 messages
> in, it's a little slower at around 25 seconds. That's still way faster
> than a desktop client on a LAN, of course, unless that desktop client
> is Polymer - in which case it'll seem remarkably slow.

*This* is what we need for a mobile email client. I know I've found my
email client of choice on my new N800.

<small>NP: <cite>Wish You Were Here</cite>, Pink Floyd</small>
