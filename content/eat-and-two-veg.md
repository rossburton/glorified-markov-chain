Title: Eat And Two Veg
Date: 2005-11-20 14:35
Tags: life
Slug: eat-and-two-veg

Last night Vicky and myself went to [Eat And Two
Veg](http://www.eatandtwoveg.co.uk), a vegetarian restaurant on
Marylebone High Street, with Eva and Saadia. It's an excellent
restaurant with a 100% vege menu, and along with the "pure" vegeterian
dishes they also had some mock-meat dishes, such as Crispy Fried Luck
for starters, burgers, and sausage and mash.

The restaurant is in the style of a 60's diner, and we even got a booth
which was great, as we had a bit more privacy.

![Saadia and Eva](http://www.burtonini.com/photos/Misc/img_3801-sml.jpg)

It was great to see Eva and Saadia again, it's been months since we've
seen Saadia and the last time we saw Eva was in February...

<small>NP: <cite>Respect</cite>, Aretha Franklin</small>
