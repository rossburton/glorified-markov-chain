Title: Edgy Eft packages for Contacts and Dates
Date: 2007-02-02 16:50
Tags: tech
Slug: edgy-eft-packages-for-contacts-and-dates

After ~~gentle pestering~~ asking from Bryan Forbes, I build packages
for Contacts and Dates for Ubunty Edgy Eft. They are available in the
usual place, [debian.o-hand.com](http://debian.o-hand.com).

It also looks like Chris still has a 770 scratcbox to hand, so we might
even have 770 packages soon too!

<small>NP: <cite>9</cite>, Damien Rice</small>
