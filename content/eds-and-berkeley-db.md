Title: EDS and Berkeley DB
Date: 2006-07-18 10:40
Tags: tech
Slug: eds-and-berkeley-db

Back in the good old days of Evolution 0.9, Evolution depended on
Berkeley DB being installed. At the time Berkerley DB ("BDB") was
undergoing a some major changes so it wasn't uncommon to upgrade your
distribution and find that the new BDB can't read your addressbook. This
was solved by Evolution embedding a copy of BDB into the source code.
and statically linking it into the relevant addressbook backend. Problem
solved.

Over time Evolution was refactored, split into Evolution and Evolution
Data Server, and so on. Then BDB was statically linked into not only the
addressbook backend but also libedataserver, which contains some generic
caches based on BDB. Now BDB isn't exactly a small library (it adds
\~600K to a .so), so the second static linking started to irritate
people. Not only does EDS have a private copy of a library, meaning
extra work when BDB has security bugs, but there is 1.2M of code that
could be shared. This led to various distributions patching EDS to use a
system copy of BDB, a patch that has finally made it upstream, but not
everyone is using it: by default EDS uses it's private copy.

Now fast-forward to today. I did a full build of EDS from CVS using the
system BDB (it's also a lot faster to build that way) and hit build
failures from code demanding the static BDB to be built. Fixing these
was trivial, but when I checked in the patch it worked fine... I
investigated and discovered that BDB is now used in four places inside
EDS:

-   The addressbook "file" backend. This is the original user
-   libedataserver
-   The Groupwise backend, as a cache
-   libedata-book, the server-side addressbook library

Now, BDB is approximately 600K when statically linked, and a typical EDS
build statically links it *four* times. The server process will load
both backends and both libraries, so it contains all four copies of BDB
at once.

This is getting silly. Please, all packagers, if you support multiple
versions of BDB in your distribution, use `--with-libdb=/usr` to
dynamically link to BDB. As a fallback I'm about to create a patch that
installed the private BDB into a private library directory, to avoid all
of this static linking.

<small>NP: <cite>Neighbour Radio</cite>, Last.fm (via Last Exit)</small>
