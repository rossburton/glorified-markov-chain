Title: EDS and Memory
Date: 2008-03-19 21:20
Tags: tech
Slug: eds-and-memory

I was going to reply to Philip's post, but [Federico did a wonderful
job](http://www.gnome.org/~federico/news-2008-03.html#19) before I could
start.

That said, I still haven't forgiven you for some of the finer details of
`libecal` Federico. :)
