Title: EDS DBus vs CORBA
Date: 2005-12-15 14:54
Tags: tech
Slug: eds-dbus-vs-corba

Ah, Novell have released an initial report comparing the [DBus and CORBA
ports of EDS](http://go-evolution.org/DBus_Port_of_EDS). As the author
of the DBus port, I should make some observations.

The incorrect reporting of large numbers of contacts in a book view is
very odd, and I've not seen that before. I'll try and replicate that
locally, I did change some message limits so maybe they are being hit.

Occassional `dbus_g_proxy_cancel_call` assertions are known to happen.
I've a potential fix locally, but it requires a patch to DBus, which
hasn't been committed yet.

Now to the benchmarks. The primary optimisation target for my work has
been in the book views, as these are the primary bottleneck in a
real-world application. This is visible in Test 1, where the DBus port
takes 17 seconds compared to 72 seconds for CORBA. A number of the
optimisations I've made can be applied directly to the CORBA code too,
and are currently sitting in Bugzilla.

Test 2, calling `e_book_get_contacts()` is interesting. It may be that
verbose mode is enabled in DBus, which has a serious performance impact
on large messages (and this is a very large message). Alternatively this
difference may simply be due the message bus, which adds a layer of IPC
(and thus string copies) which CORBA doesn't have. As very few
real-world applications (I'd say none) use this function (it blocks the
interface unlike book views which are asynchronous), this isn't a very
meaningful benchmark in the scheme of things.

For the calendar, there has been no performance turning done at all.
It's known that the calendar port to DBus isn't quite complete, and
needs cleaning up and performance work.
