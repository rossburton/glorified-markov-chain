Title: Ely
Date: 2008-04-02 22:45
Tags: life
Slug: ely

Today, we went to Ely. Nice (very small) city, with a rocking cathedral.

[![Ely
Cathedral](http://farm3.static.flickr.com/2379/2383737948_2031d1f550.jpg){
}](http://www.flickr.com/photos/rossburton/2383737948/ "Ely Cathedral by Ross Burton, on Flickr")  
[![Ely
Cathedral](http://farm4.static.flickr.com/3217/2382903929_437777164b.jpg){
}](http://www.flickr.com/photos/rossburton/2382903929/ "Ely Cathedral by Ross Burton, on Flickr")

In other news, the new Lightroom 2 beta is *very* nice.
