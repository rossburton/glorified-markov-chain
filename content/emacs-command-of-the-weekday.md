Title: Emacs Command of the Weekday
Date: 2009-04-23 16:00
Tags: tech
Slug: emacs-command-of-the-weekday

When
[Thomas](http://blogs.gnome.org/thos/2009/04/23/vim-command-of-the-day/)
talks about "us all" learning a new Vim command, he meant "us heretics".
We pure and just people on the path of truth are far more interested in
[ecotd](https://twitter.com/ecotd), Emacs Command of the Day, by our
very own [Neil](http://www.busydoingnothing.co.uk/blog).

Okay, I admit at times it looks like a parody, but honestly it isn't!
