Title: Employment!
Date: 2004-11-03 00:00
Tags: life
Slug: employment

Yes, I am gainfully employed again. Thank-you everyone for the nice
comments when OneEighty died, and even more thanks for the various
potential job offers. On Monday I started working for [Opened
Hand](http://www.openedhand.com), with Matthew Allum of
[Matchbox](http://matchbox.handhelds.org) fame. The work is fun and
interesting, and I get to work from home. At the moment I'm mainly
reading large amounts of documentation for a meeting at the end of the
week, and preparing to port some software to GTK+. All good fun!

<small>NP: <cite>Beautiful Freak</cite>, Eels</small>
