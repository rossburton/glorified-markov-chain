Title: End Of An Era
Date: 2004-10-18 00:00
Tags: life
Slug: end-of-an-era

Today is indeed the end of an era. It all started this morning, when one
of our management guys turned up *in a suit*. He normally sticks to
"smart casual" dress, and when asked if he had a meeting he seemed quite
uncomfortable. Shortly afterwards one of the investors arrived, and then
the top man from our parent company. Blimey, we thought, everyone is
here today. Finally Russel (our boss) arrived, which confused us.
"Aren't you supposed to be in America?", we asked (we had a stand at the
ARM conference). "Yes, well, on a plane", he replied. Body language
alone told us this wasn't going to be a normal day.

After much speculation among us lowly programmers, and a long meeting by
management, the top man from the parent company shot off out of the
office like he was on fire. Then the remaining members of the meeting
came out and informed us that as of 12:30 that day, OneEighty Software
Ltd was no longer being funded. No funding means no money, so OneEighty
was being liquidated.

We talked briefly about how this happened, which was especially
confusing as we were very close to finishing a port to a faster, better
chip. I spend an hour squeezing the contents of my desk and drawers into
my bag, and we all went down to the <cite>Porter and Sorter</cite> for a
pint. Not much more to say really, I'm currently doing my last commute
from London for a while and probably my last ever from sunny Croydon.

I think this qualifies for the most upsetting command I've ever entered
into a shell:

    ross@hactar ~/Programming
    $ du -hs OneEighty/
    70M     OneEighty/
    ross@hactar ~/Programming
    $ rm -rf OneEighty/
    ross@hactar ~/Programming
    $

Many people will point out that at the end of the day this is just a job
and there are other jobs, and that is indeed true. What is also true
that Dan and myself worked with Russel on what is now called ORIGIN-J,
for what is now called OneEighty Software Ltd, back when we were still
at university in our spare time over four years ago. We've seen the
architecture evolve, processors come and go, and the company grow from
three people working at home, to seven people in a Regus office, to
thirteen people in another office. We've been there from the beginning
to the end, and it's sad to see us shut down just as things, in a real
physical product sense, were starting to take off.

Personally, I'm going to take a few days out to sort out the chaos left
behind by <cite>The Gift Registry</cite> going into administration, and
to check my Debian packages are ready for Sarge. If anyone out there in
the London/Cambridge area wants to hire a C/Java/Python/Debian coder,
[mail me](mailto:ross@burtonini.com). If anyone wants to hire a
kick-arse Linux system administrator in the Croydon area who battled
Samba in a domain environment and came out with his sanity intact, mail
me and I'll forward the message to Dan.

Finally, thank you Russel, you were a great boss.

<small>NP: <cite>Lamb</cite>, Lamb. Loudly.</small>
