Title: End of Day 2
Date: 2003-06-18 10:19
Tags: tech
Slug: end-of-day-2

Really the end. As I start writing this its five past three in the
morning, after the Ximian Party.

It started innocently enough, Thom and myself wandering to the venue, to
meet Bastien walking away from the pub. He confirmed where the pub was,
but said he was off to get food. Turns out the group of people he was
with included The Alan Cox and The Jim Gettys.

After a good, reasonably priced, and very large meal, we went back to
the pub for the Ximian Party. We were supposed to get vouchers for a
free beer, but we only got one of those, and then decided that the
reserved bar upstairs was far too crowded and busy for us, there being
nowhere to sit, so we trekked back downstairs to the "normal" bar, which
was far more civilised and had seats. As we chatted about the usuals:
GStreamer, Totem, licensing, Real, GNOME, work, <cite>Coupling</cite>,
<cite>Spaced</cite>, and rugby; random hackers from upstairs appeared
and joined us, left us, and on the whole contributed to a good evening.

That night started at eight thirtyish, and its now ten past three. I
need to drink something which isn't a stout.

Later that night (probably around half eleven) we moved upstairs as we
thought that sufficient people had left so we might be able to get a
seat. En route I finally met Telsa, and chatted with Glynn Foster, Jeff
Waugh, Nat Friedman, Johan Dahlin, Thomas Vander Stichele and Dave
"Printman" Camp. Glynn got a cheer for arranging the conference, and was
forced to make a speech, which consided solely of: <cite>In the importal
words of Jeff Waugh: Pants Off!</cite>

What a great conference. I'm off to bed. Sod breakfast. I can find a
sandwich.
