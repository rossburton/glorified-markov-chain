Title: End Of Love Soup
Date: 2005-11-02 14:55
Tags: life
Slug: end-of-love-soup

So <cite>[Love Soup](http://www.bbc.co.uk/drama/lovesoup/)</cite> ended
last night, in the traditional British 6-part style. Vicky, as ever, had
a positive happy-ending outlook and expected Alice and Gil to finally
meet. I'm obviously far more jaded and expected the final scene to,
well, be what it was. An excellent show, more of the same please BBC!

Alternatively a third series of <cite>Green Wing</cite> would be great,
if you are listening Channel 4.

<small>NP: <cite>The Ultimate Best Ibiza Summer Chill-out Album Ever
(Disc 1)</cite>, or something similar.</small>
