Title: Enid Blyton For Sale
Date: 2005-04-14 18:47
Tags: life
Slug: enid-blyton-for-sale

If anyone out there collects Enid Blyton books, knows someone who is
collecting Enid Blyton books, or wants to start collecting Enid Blyton
books, the lovely Mrs. Burton has put a set of [eleven books from the
1950s and 60s on
eBay](http://cgi.ebay.co.uk/ws/eBayISAPI.dll?ViewItem&category=72293&item=6956240779).

Bid my pretties, bid!
