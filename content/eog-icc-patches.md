Title: EoG ICC Patches
Date: 2005-06-24 17:27
Tags: tech
Slug: eog-icc-patches

Quick announcement: The Eye Of Gnome patches to use the ICC Profiles In
X specification are now available via Arch, at
[`http://www.burtonini.com/arch/eog--xicc--0`](http://www.burtonini.com/arch/eog--xicc--0).
Now that EoG has a maintainer (hi Tim!), I hope to get these suitable
for upstream soon.

<small>NP: Groove Salad, Soma.fm</small>
