Title: EphyDeli 0.2
Date: 2006-11-01 10:47
Tags: tech
Slug: ephydeli-0-2

EphyDeli is a Python extension for Epiphany that adds Post To Delicious
menu and toolbar items, for posting the current page to Del.icio.us.
It's lean, mean, and very simple, but does the job.

I've just released EphyDeli 0.2. Johan Dahlin was disgusted enough by my
quick hack to refactor it for me, and Gareth Murphy drew some cool new
icons. Thanks guys!

To download it you can either [the
tarball](http://burtonini.com/computing/ephydeli-0.2.tar.gz) or fetch
the [bzr tree](http://burtonini.com/bzr/ephydeli/).
