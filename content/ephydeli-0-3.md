Title: EphyDeli 0.3
Date: 2008-04-29 20:12
Tags: tech
Slug: ephydeli-0-3

EphyDeli is a Python extension for Epiphany that adds <cite>Post To
Delicious</cite> menu and toolbar items for posting the current page to
Del.icio.us. I know of several people who use it frequently and the last
release was in 2006, so I've obviously mastered the Unix philosophy well
here! This release was caused by those mean old Epiphany developers
changing the API, many thanks to Thibauld Nion for noticing this and
sending a patch.

To download it you can either grab [the
tarball](http://burtonini.com/computing/ephydeli-0.3.tar.gz) or fetch
the [bzr tree](http://burtonini.com/bzr/ephydeli/).
