Title: EphyDeli
Date: 2006-08-29 21:40
Tags: tech
Slug: ephydeli

Woohoo, I solved my Epiphany problem, so have just done the first
release of EphyDeli.

EphyDeli is a Python extension for Epiphany that adds Post To Delicious
menu and toolbar items, for posting the current page to Del.icio.us.
It's lean, mean, and very simple, but does the job.

To download it you can either [the
tarball](http://burtonini.com/computing/ephydeli-0.1.tar.gz) or fetch
the [bzr tree](http://burtonini.com/bzr/ephydeli/). Don't hesitate to
mail me with any problems, and don't hesitate to drop a quick mail if
you actually use it so I can see if it's popular.
