Title: Epiphany Requested
Date: 2006-08-29 18:00
Tags: tech
Slug: epiphany-requested

I'm trying to write an extension in Python for Epiphany that has a
toolbar button, and let the user add/remove it at will. Now, I can add a
button, but I can't get it to appear in the editor. This is probably due
to me being silly and not understanding how the editable toolbar
works...

I have already created an action for my extension, which is called
"Post". If I do this:

    shell = epiphany.ephy_shell_get_default()
    shell.get_toolbars_model().add_item(0, 0, "Post")

Then the button appears on the toolbar, and works. I can even remove it
from the toolbar with the editor, but if I do this I can't add it back
again. Does anyone understand how to add a toolbar and menu item (for
the same action) to Epiphany from a Python extension?
