Title: Ettore
Date: 2003-12-12 00:00
Tags: tech
Slug: ettore

I've written and deleted this first paragraph five times so far.

I never met Ettore. However he was always willing to help with my stupid
questions about the code in \#evolution, and had a fabulous blog from
which I stole his photos and Italian recipes.

I can't write any more, the right words refuse to form in my head.
Ximian, my thoughts are with you. This had been a hard month.
