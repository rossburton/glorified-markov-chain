Title: Eventful Weekend
Date: 2004-01-12 00:00
Tags: tech
Slug: eventful-weekend

Quite an eventful weekend. Terrible news about Mark Finlay, who has
passed away. Obviously battling (and hacking) to the very end, his
illness never showed on IRC and the mailing lists. My promise to him to
implement gnome-scan has been renewed -- the mockups hashed out on his
blog were looking very promising.

My grandmother went to hospital last week with pluracy and an irregular
heartbeat. Outlook is good, but having a nasty cough I thought it would
be best not to visit her just yet.

Saturday I played with Thomas's audio profiles for a while, creating a
combobox listing all available profiles. Thomas rewrote it on Sunday, so
hopefully I'll be able to put it into Sound Juicer shortly. This will
entail a rewrite of `SjExtractor`, but it has been needed for a long
time. The same rewrite will allow the gnome-vfs method to be a far
cleaner patch, and satisfy my problems with the user interface.

Finally, Mr. Kilroy-Silk, apart from having a silly surname, is also [a
fool](http://news.bbc.co.uk/1/hi/uk/3383589.stm).

NP: Run Come Save Me, by Roots Manuva
