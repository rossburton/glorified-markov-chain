Title: Evil Plant Sex Goo
Date: 2004-06-16 00:00
Tags: life
Slug: evil-plant-sex-goo

The Evil Plant Sex Goo has been strong recently, as many people in
England probably know. So it was no surprise yesterday when I developed
allergic conjunctivitis, again (forth time in three years), even after
daily antihistamines and frequent eye washes. I hate hay fever, I hate
plant sex goo, and I hate the antibiotic eye drops. At least I'm not
alone: the man opposite me on the train is suffering from The Fever too.
Maybe we should form a nose-blowing and sniffing double act...

Once I'm in Croydon I think I'll do my lunch shopping before I go to
work, the local supermarket has a wonderfully air-conditioned cheese and
meat section. Aaah the relief.

In totally unrelated news, this week I did a
[Matt](http://www.hackdiary.com) and bought two albums from hearing them
on the Beeb. [Bebel Gilberto](http://www.bebelgilberto.com/) was on
Jools last week with some funky modern bosso nova, and
[Murcof](http://www.murcof.com/) (minimalist classical with house
influence and sound textures, apparently) were mentioned by Matt
yesterday. They have a funky Flash based site and three streams to
download, which are great mellow pieces. Perfect for deep-hack-mode
background music.
