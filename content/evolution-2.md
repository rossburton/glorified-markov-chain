Title: Evolution 2
Date: 2003-12-03 00:00
Tags: tech
Slug: evolution-2

I've been having a play with Evolution 1.5 recently. It's looking really
nice, the clear evolution and evolution-data-server split should lead to
more programs using the backends.

Some screenshots: [the
mailer](http://www.burtonini.com/computing/screenshots/evo15-mail.png),
[the address
book](http://www.burtonini.com/computing/screenshots/evo15-contacts.png),
[the
calendar](http://www.burtonini.com/computing/screenshots/evo15-calendar.png).

My address book search applet is rocking nicely now, its nearly complete
apart from the auto-completing magic.

PS: I promise a decent diary entry soon. Honest.
