Title: Evolution Broken
Date: 2006-07-17 13:15
Tags: tech
Slug: evolution-broken

That was a nice inflammatory title to get everyone's attention. I've
just committed a change to Evolution Data Server
([\#313533](http://bugzilla.gnome.org/show_bug.cgi?id=313533), extending
`EContactPhoto`) that not only breaks ABI but also API. Rock on!

Now I've yet to update Evolution for the new API, so people doing builds
from CVS will find they break. I'll try and get it fixed today, but I
can't promise it. The fixes will be fairly simple if you ignore the new
features, so someone else should be able to easily do it.

<small>NP: <cite>Cool Groove</cite></small>
