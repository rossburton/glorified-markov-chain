Title: Evolution in Maemo
Date: 2005-09-27 09:26
Tags: tech
Slug: evolution-in-maemo

So Philip Van Hoof has started to [build our D-BUS port of Evolution
Data Server in
Maemo](http://mail.gnome.org/archives/evolution-hackers/2005-September/msg00136.html),
with the aim of building Evolution itself. I've not actually tried
running Evolution on a 5 inch screen, but I've a feeling it might be a
little cramped. How about trying something [designed for smaller
devices](http://www.soton.ac.uk/~cil103/2005/09/overlapping-events-in-datesview.html)?
Dates is being written by [Chris Lord](http://www.chrislord.net), who is
working for OpenedHand during the summer holiday, and has been kicking
arse.

<small>NP: <cite>Black Sunday</cite>, Cypress Hill</small>
