Title: Evolution Love
Date: 2007-08-02 16:15
Tags: tech
Slug: evolution-love

If anyone fancies getting their hands dirty with some Evolution Data
Server code, I just marked
[\#314709](http://bugzilla.gnome.org/show_bug.cgi?id=314709) with the
`gnome-love` keyword. This patch involves adding support for per-contact
geopositioning to EDS. Half of the patch is trivial: adding a \#define.
The other half involves creating a struct to represent the `GEO` field
(two floats), and hooking it up to the `EContact` accessors. I'm willing
to mentor anyone who seriously wants to implement this.

<small>NP: <cite>()</cite>, Sigur Rós</small>
