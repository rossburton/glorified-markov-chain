Title: Evolution Lovers and Haters
Date: 2007-08-31 16:11
Tags: tech
Slug: evolution-lovers-and-haters

<!-- -*- Mode: html -*- -->

Evolution (the email client, not the theory) really seems to bring out
extreme emotions in its users.

    RP: pgc: I'm with you, I hate evolution
    dodji: yay
    dodji: evo haters fanclub
    chris: I also hate Evo, but everyone here loves it and wants to have its babies
    ross: I've had evo's babies
    chris: ross: I bet there were random duplicates

Poor Evolution.

<small>NP: <cite>Directions LP</cite>, Acroplane</small>
