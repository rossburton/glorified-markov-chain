Title: Evolution Patches
Date: 2004-01-02 00:00
Tags: tech
Slug: evolution-patches

Last night while watching [What Women
Want](http://www.imdb.com/title/tt0207201/) I pulled apart my Evolution
patches and sent a series of mails off to `evolution-patches` and Chris
Toshok. Hopefully they will be accepted soon, and my plans for World
Domination via Evolution 2 can continue. MWHAAHA! For the interested,
the patches fix the asynchronous EBook API, and adds a video
conferencing URL field to contacts.

NP: [The Invisible
Band](http://www.amazon.co.uk/exec/obidos/ASIN/B00005K9N7), by Travis.
