Title: Ewan and Charley Hoaxed
Date: 2007-11-26 10:30
Tags: life
Slug: ewan-and-charley-hoaxed

Last night in the excellent [Long Way
Down](http://en.wikipedia.org/wiki/Long_Way_Down), Ewan and Charley
crossed the equator and were shown how water spins in different
directions as it pours out of a bowl either side of the equator.
Remember everyone, [this is an urban
legend](http://en.wikipedia.org/wiki/Coriolis_effect#Draining_bathtubs_and_toilets),
and the demonstration was faked.

<small>NP: <cite>Oneiric</cite>, Boxcutter</small>
