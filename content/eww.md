Title: Eww
Date: 2004-04-14 00:00
Tags: tech
Slug: eww

[Glynn](http://www.gnome.org/~gman/), dude, you are no longer qualified
to comment on [my colour
scheme](http://www.burtonini.com/computing/screenshots/desktop-20040414.png)
when you think the [purple
barf](http://www.gnome.org/~gman/blog/14042004) you ship is actually
“sweet”. I can feel my photoreceptors burning away as I look at that
screenshot.
