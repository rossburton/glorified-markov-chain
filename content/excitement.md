Title: Excitement
Date: 2003-07-31 00:00
Tags: life
Slug: excitement

And I though receiving a free stuffed Ximian monkey would be the most
exciting thing to happen to me this week...

Wednesday afternoon my eyes were playing around again, being sore and
itchy. As I suffer from occasional hay fever and had continually
disabled DrWright today, I thought it was just obvious why. I spent a
few minutes looking out of the window, and noticed a familiar but
unwelcome sensation in the corner of my left eye. A quick trip to the
toilets confirmed the presence of allergic conjunctivitis, yet again.

I was totally un-decisive for 10 minutes whilst I weighted up the
options: go to the local hospital in Croydon and possibly come out
several hours later, a long way from home; go to the walk-in clinic at
Victoria station and be told where the nearest eye clinic/A&E is; or
hope it doesn't get too bad and go to the A&E near home. In the end the
itching got worse and I didn't want to travel looking like a freak, so I
jumped in the cab to Mayday University Hospital, Croydon. Nice place.

That was sarcasm.

I was pleasantly surprised by the speed at which I was seen, around five
minutes -- I'm always seen within 10 minutes at my local hospital but I
had assumed that was due to the rather posh eye unit in A&E. Mayday's
(great name for a hospital) wasn't as posh: the "eye unit" was a desk in
a corner, with a piece of paper taped to the side. It was the eye unit
as opposed to, say, the ear unit, as the desk had one of those small eye
examination widgets on. Come to think of it, it looked just like the ear
examination widget... Anyway, he had a look at my eyes, confirmed
exactly what I thought (yes I have hay fever, probably an allergic
reaction, yadda yadda) and gave me some eye ointment as they had "run
out of drops". I wondered if they had "run out" of anything else, such
as bandages or pain killers.

Anyway, I'm not a fan of things touching my eye so not having to use
drops seemed like a good thing, but now I think I prefer drops. I'm
supposed to pull my eye lid down and run a line of this sticky yellow
cream along the inside of the eye lid, then shut my eye for thirty
seconds. What the doctor didn't mention is that when I open my eye again
there will be a noticeable sticky feeling and the cream is so thick it's
like viewing the world through a soft-focus lens for the next 10
minutes.

So I stumbled out of the hospital, called a taxi and found out the next
cab wouldn't be available for 45 minutes. Great. I have no idea where I
am apart from "north(ish) of Croydon (possibly)". After a quick call to
[Dan](http://www.danalderman.co.uk) who lives locally, we decide that
the best thing would be to walk down the road and find a bus stop. Thank
God for Dan's total recall of the area (maybe he should become a taxi
driver), as within a minute I could see a bus stop going in the right
direction, exactly where he said it was. The right numbered bus was even
coming around the corner.

At that moment a young bloke (early 20's I'd guess) ran past from an
alley and ran across two lanes of traffic. I thought he must be in a
rush. Then I found out why.

Two police men ran out of the same alley, one shouting into his radio,
"He's crossed the road". The man was running towards a car waiting on
the other side of the road, his get away car I assumed. Apparently not,
from the shout of "Drive you \$\*!\*" and the ensuing fight for the car.
The woman driver gave a good fight before the man undid her seat belt,
opened her door and just pushed her out onto the pavement. As she fell
he started to try and drive away, but by now the police men had crossed
the road and were all over the car, trying to stop him drive away. It
was close, but the man managed to escape the police man's attempts at
stopping him and threw the policeman into to road, who ended up about a
foot away from being ran over by a large van which luckily wasn't going
too fast (this was 18:00 in Croydon, 20mph *is* fast). He stormed down
the road as the bus turned up, and as I was getting my ticket a
description of the man and the car he just stole was coming over the bus
intercom. Cunning, I thought: nobody notices a London bus, but they are
all on the lookout for people the police are looking for. As the bus
made its way back to the town centre three more police cars and a police
van screamed past, sirens wailing. I don't know what this man did, but
it must have been quite serious.

All of this happened over 30 seconds whilst I was still on the phone to
Dan. He the admitted that this area of Croydon is probably the
scummiest, seediest more dangerous area of Croydon. Croydon isn't a
great place: walking down the busy high street flashing a posh mobile
phone is an invite to have it taken from you, but around the hospital it
felt like wearing posh socks was going too far.

Anyway, I made it home, ate a large pizza (Pizza Hut "Edge" pizza's are
not worth it, the base is too thin) and watched <cite>The Whole Nine
Yards</cite>. Not a fantastic film, but perfectly watchable.
