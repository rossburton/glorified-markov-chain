Title: EXIF Tags In Nautilus
Date: 2005-07-28 14:50
Tags: tech
Slug: exif-tags-in-nautilus

After seeing one of the new Windows Vista screenshots showing Explorer
listing image titles in the list view, I decided to have a quick hack on
this in Nautilus. Hey presto:

![](http://www.burtonini.com/computing/screenshots/nautilus-exif.png)

ROCK ON. At the moment it only lets you see the image title on JPEG
files (from the Image Description EXIF tag), but I'll extend that later.
The good thing is that it's nicely behaved, using asynchronous I/O and
only reading as much of the file as it needs.

<small>NP: <cite>Worldwide Underground</cite>, Erykah Badu</small>
