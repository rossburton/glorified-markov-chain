Title: Express Tosh
Date: 2005-11-17 11:30
Tags: life
Slug: express-tosh

If I had known that the Daily Express front page yesterday was just so
damn good I'd have bought a copy myself. With a paper bag on my head,
obviously. Instead I have to make do with a small image.

![Diana Fund Pays Out To Gypsies And Asylum
Seekers](http://www.burtonini.com/images/express.png)

I'm still not sure that this isn't some great spoof or prank. Maybe
someone broke in and changed the front page just as it went to press?
What do we have here:

-   Diana
-   Gypsies
-   Asylum Seekers
-   Banned Sacred Items Of Our Wonderful Culture (in this instance,
    Santa and Christmas lights). Hard to believe as it's probably [not
    true](http://www.bigdaddymerk.co.uk/mailwatch/?p=482).
-   Girls in Short Skirts. For once this isn't a token shot of some
    B-list celebrity, but a genuine story about how Clare Bernal's
    murderer had taken cocaine.

Marvellous stuff. I can only imagine the Express has taken to being a
parody of itself...

<small>NP: <cite>Individuality</cite>, Rachelle Ferrell</small>
