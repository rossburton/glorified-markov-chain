Title: Eye of Gnome Bling
Date: 2007-11-23 11:50
Tags: tech
Slug: eye-of-gnome-bling

Last night I finally got around to having a play with Clutter.
Considering I'd never used it before, I'm really impressed with just how
easy the API is to use. Within half an hour I had images fading in and
out, and this morning I built Eye of Gnome and rewrote my hack as a
slideshow plugin. There is no point releasing the code just (patches
required, and a nasty bug remaining), but I did make a screencast.

[![](http://burtonini.com/computing/screenshots/thumb-eog-bling.ogg.jpg)](http://burtonini.com/computing/screenshots/eog-bling.ogg)

<small>NP: <cite>Maxinquaye</cite>, Tricky</small>
