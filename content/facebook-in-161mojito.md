Title: Facebook in ¡Mojito!
Date: 2009-09-14 10:04
Tags: tech
Slug: facebook-in-161mojito

Thanks to those nice people at Novell,
[Mojito](http://moblin.org/projects/mojito) (everyone's favourite social
aggregator, as used in [Moblin](http://moblin.org)) now has Facebook
support. We now support Facebook, Flickr, Last.fm, MySpace and Twitter —
any requests for the next service?

<small>NP: <cite>Cold Water Music</cite>, AiM</small>
