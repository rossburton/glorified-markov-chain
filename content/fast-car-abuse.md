Title: Fast Car Abuse
Date: 2004-05-09 00:00
Tags: life
Slug: fast-car-abuse

> To: vernon.kay@bbc.co.uk
>
> Vernon,
>
> I just turned on my radio and had the misfortune to hear a remix of
> Fast Car -- is nothing sacred these days!? Could you tell me who is
> responsible for this atrocity...
>
> Cheers,  
> Ross
>
> </tt>

What's next? A happy hardcore mix of <cite>Imagine</cite>?
