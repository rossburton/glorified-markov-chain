Title: Fat Celebrity Homes From Hell
Date: 2004-02-03 00:00
Tags: life
Slug: fat-celebrity-homes-from-hell

Sometimes TV makes me scream "NO MORE!!", when we are treated to another
program about the housing market or fat people dieting or fashion, or
more normally two of the above combined with "celebrities" or "holiday".
<cite>Grand Designs</cite> was a good program, but the relentless push
towards Yet Another Program about moving home is driving me up the wall.
Thank God we're finally past the garden fixation.

However, last night <cite>No Angels</cite> started, and it's not at all
bad. A slightly more adult (and British) version of <cite>Scrubs</cite>,
which will hopefully develop into a series as good as the seminal
<cite>Teachers</cite>. Fingers are crossed that they don't have a themed
episode about fat people buying houses and doing garden design.

NP: Dub Come Save Me, Roots Manuva.
