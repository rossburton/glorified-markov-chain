Title: Favoured Photography
Date: 2005-08-18 17:47
Tags: photography
Slug: favoured-photography

When in doubt, alliterate.

From the critique queue of photo.net I'm developing quite a taste for
black and white photography. I like the simplicity that the lack of
colour gives a scene, and the way the use of light for increased
contrast gives more impact than colour often does. I've only tried B&W a
few times before and my attempts generally were quite washed out and
lacking in contrast, I probably need to be more aggressive with the
Levels control.

Anyway, the pictures I've liked on photo.net today are: [Winter's
Evening](http://www.photo.net/photodb/photo?photo_id=3643238&size=lg),
[The
Wharf](http://www.photo.net/photodb/photo?photo_id=3644149&size=lg),
[B/W
Water](http://www.photo.net/photodb/photo?photo_id=3642141&size=lg),
[Wind In
Grass](http://www.photo.net/photodb/photo?photo_id=3644537&size=lg), and
[Drop 1](http://www.photo.net/photodb/photo?photo_id=3642585&size=lg).
Plenty of inspiration available there!

I also liked [Coming
Back](http://www.photo.net/photodb/photo?photo_id=3645151&size=lg),
partly because it's a great photo with great tones with the sea fading
into the distance making the boat really stand out, and party because it
reminds me of our [holiday in
India](http://www.burtonini.com/photos/200503-India/img_1774.jpg).
[Jeremy Parker](http://jeremyparkerphotography.com/) has some great
photos on his site, with a clever site design which changes the
background colour of the gallery pages to complement each photo.

<small>NP: <cite>Best Of</cite>, Ella Fitzgerald and Louis
Armstrong</small>
