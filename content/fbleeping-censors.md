Title: F[bleep]ing Censors
Date: 2005-02-01 00:00
Tags: life
Slug: fbleeping-censors

My weekly Cineworld email just arrived, with the cinema times for the
upcoming week. This entry caught my eye:

> Ocean's Twelve (12A)  
> (contains one audible and some bleeped uses of strong language)

<cite>some bleeped uses of strong language</cite>? Pardon? Surely if the
language is too strong for a 12A, the solution is to make it a 15?
Apparently not, so we get the film ruined with inappropriate bleeps.

Update: many people have informed me that this is intentional bleeping
for comedic effect in a single scene, so that's okay then.

<small>NP: <cite>Do You Want More?!!!??!</cite>, The Roots</small>
