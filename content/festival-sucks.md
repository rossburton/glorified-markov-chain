Title: Festival Sucks
Date: 2004-12-14 00:00
Tags: tech
Slug: festival-sucks

Festival sucks. That is all.

Okay, I'll elaborate on that. When using the command-line client
`festival_client`, if you tell it to write the generated wave to a file
it also outputs some textual header *and the the wave again* to standard
error. Gar. And I can't type `fe[tab][ret]` any more to get my email.
This sucks.

<small>NP: <cite>Endtroducing...</cite>, DJ Shadow</small>
