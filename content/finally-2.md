Title: Finally!
Date: 2007-04-03 20:30
Tags: tech
Slug: finally-2

At OpenedHand we've been working on confidential projects for a while,
and it's nice that we're [finally able to talk about
them](http://o-hand.com/blog/?p=14). The first project we can talk about
is the [Vernier Labquest](http://vernier.com/labquest/): a really
interesting handheld device for science education. For the geeks out
there, it runs Linux, [Poky](http://projects.o-hand.com/poky), X11,
[Matchbox](http://projects.o-hand.com/matchbox/) and GTK+. We're really
excited to see this being released soon, and are very proud to have been
a part of it.

The second project is even more interesting for the geeks out there, but
you'll have to wait a few more days for that...
