Title: Finally
Date: 2005-06-16 18:03
Tags: tech
Slug: finally

The problem with large projects is that for a long time there is often
no real progress, then all of a sudden [stuff just starts to
work](http://www.burtonini.com/computing/screenshots/evo-dbus.png).

For the curious, on the right is the infamous Evolution showing the
address book, and on the left is a terminal running the address book
backend. The two normally communicate via Bonobo, but these are
communicating via DBus...
