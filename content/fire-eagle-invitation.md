Title: Fire Eagle Invitation?
Date: 2008-05-12 21:50
Tags: tech
Slug: fire-eagle-invitation

Does anyone out there on the Intarwebs work for Yahoo, or have a friend
who works at Yahoo? I'd really like to give this [Fire
Eagle](http://fireeagle.yahoo.net/) thing a go, specifically integrating
[Gypsy](http://gypsy.freedesktop.org/) and
[GeoClue](http://geoclue.freedesktop.org/) with Fire Eagle, but it's
invitation only at the moment...

**Update:** I now have an account!
