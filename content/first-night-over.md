Title: First Night Over
Date: 2003-06-17 07:51
Tags: tech
Slug: first-night-over

Well, I'm in bed writing this before I got to sleep. After the afternoon
talks were over I went down to the machine room with Thom, meeting
Matthew Garret (of Dasher fame) en route, and hung around there for a
while, finding Edd Dumbill and heckling Jeff and Glynn as they totally
failed to make a "It Just Works" MacOS X laptop talk to a projector and
play a QuickTime movie.

After a quick change, we tried to go Out On The Town. Unfortunately my
current account is seriously overdrawn (oh bugger) and none of the
machines would accept my credit card. This is **not** funny. I have to
survive a week until I get paid on no money for some reason (probably
too much shopping), but I can't even use my credit cards here!

In the end we found a really good pizzeria, where I had the best pizza
I've had since Rome (<cite>Le Chef</cite>, peppers, pepperoni, onion and
cheese I believe). Afterwards a quick call to my better half as they
say, and a pint (or three) of Guiness in a local pub. Now this is
service -- I go to the bar and ask for three pints and a whiskey, he
tells me to sit back down and he will bring them too me. Excellent. I
must educate my local pub in this habit.

Tomorrow's talks look on the whole better than todays -- Thomas's
GStreamer talk was excellent, but Soren's Performance talk needed more
practise and over-ran quite a lot. At the very least, tomorrow Thom and
myself can heckle Edd as he gives his Bluetooth talk...

People still to see: `andersca`, `alex`, `ninja`, `telsa` `walters`,
`oskuro` and `docpi`. I'm sure you are out there, find me! `:)`

Last not least, congratulations to Glynn Foster for arranging a great
conference. It may have been absolute hell to arrange, but it appears to
be going well. Nice work. Now can I have one of those flashing Sun pens?

Things I Miss (in accending order):

1.  A book to read in bed/on the toilet/anywhere else I fancy reading.
2.  Vicky.

