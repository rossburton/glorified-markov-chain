Title: Fitness (Lack Of)
Date: 2004-09-18 00:00
Tags: life
Slug: fitness-lack-of

I'm not the fittest person in the world, though this is party due to
asthema. In an attempt to loose a little excess gut and get a little
fitter, for the last month or so I've been walking from Liverpool Street
station to London Bridge station on my way to work. It turns out that I
normally keep up with the bus I would have got (in a real-life turtle
and hare race) which is very satisfying, despite other people walking to
work often staring at me as I rushed past, bag a-swinging and red-faced.
The breeze from the river and a bottle of Lemon Ice Tea Snappy are a
saviour however and by the time I'm in the office I don't look like
someone who was beaten in the street.

This all sounds good so far -- but there is bad news. Somehow in the
past month I've put on half a stone. I tell myself it's my stronger leg
muscles, but I think I've somehow developed a nice set of piecepts...
