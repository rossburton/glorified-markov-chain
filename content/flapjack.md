Title: Flapjack
Date: 2006-03-22 18:15
Tags: life
Slug: flapjack

This may be a little rash, but I have just knocked up **the best
flapjacks in the world**. Never before have oats, butter, golden syrup,
grated apple and a tiny pinch of cinnamon tasted so good.

<small>NP: <cite>Pieces Of You</cite>, Jewel</small>
