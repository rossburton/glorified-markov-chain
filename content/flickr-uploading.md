Title: Flickr Uploading
Date: 2006-06-06 12:45
Tags: life
Slug: flickr-uploading

On a whim I gave up with my personal web gallery implementation and
[switched to Flickr](http://www.flickr.com/photos/rossburton/). Now,
uploading is fun. I could use the web-based upload form, but as I can't
DnD from Nautilus into a text field that is dull, and whilst copy/paste
works that is also dull. I can email images but I can't send lots at
once (they have a 12M limit per email), and have to remove the signature
and GPG signing for every message. Also dull. I'll get a billion people
shouting "use F-Spot", which I tried, but it doesn't want to import some
of my photos, and it doesn't work with my photo storage either, so that
isn't an option for now.

Thus, I'm considering writing a small Nautilus extension in Python, so
that in the context menu of every image there is a <cite>Upload to
Flickr</cite> option. I'd prefer not to re-invent the wheel so does
anyone else has written a simple GNOMEy uploading tool for Flickr?
Thanks!
