Title: Flickr
Date: 2007-11-21 11:00
Tags: life
Slug: flickr

Some interesting Flickr news today, first some old news I only just
heard: the [two billionth
photo](http://flickr.com/photos/88646149@N00/2000000000/) was uploaded
this month. Impressively, whilst it took three and a half years for the
first billion pictures to be uploaded, the second billion took only
*three months*.
([source](http://www.kullin.net/2007/11/flickrs-second-billion-took-three.html)).

Second, the Flickr team have just added [some new
features](http://blog.flickr.com/en/2007/11/20/a-page-on-flickr-for-every-place-in-the-world/),
specifically a [world map with popular tags
overlaid](http://flickr.com/map) and a new
[Places](http://flickr.com/places/) interface, providing a summary of a
particular location. The new map has temporarily lost the ability to
zoom in straight away (press Search then Go to get the zoom bar), but
the Places interface is amazing fun.

<small>NP: <cite>People's Instinctive Travels and the Paths of
Rhythm</cite>, A Tribe Called Quest</small>
