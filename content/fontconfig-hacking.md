Title: FontConfig Hacking
Date: 2005-04-01 09:40
Tags: tech
Slug: fontconfig-hacking

Last night Keith Packard gave me commit access to `fontconfig` and I
committed the first iteration of my patch, which reduces memory
consumption and speeds up pattern matching by ensuring pattern keys are
canonical. For my system, 25Kb was saved and `strcmp` called 25% less,
which isn't bad for a patch which changes 6 lines.

The next step is to expand the scope of the patch to pattern values as
well as keys. I have a working patch which reduces the memory footprint
of `fc-list` by another 140Kb, but it's a little ugly at the moment.

<small>NP: <cite>Buena Vista Social Club</cite></small>
