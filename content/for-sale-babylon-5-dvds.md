Title: For Sale: Babylon 5 DVDs
Date: 2007-06-20 19:40
Tags: life
Slug: for-sale-babylon-5-dvds

In the continuing effort to clear some space and pay some bills, I'm
selling my collection of Babylon 5 DVDs. I have the boxed sets of series
1 through to 5, in great condition: they've been watched once and then
put on the shelf, where they've stayed.

Each series retails at £57, I'm asking for £90 for the complete set
(plus shipping, or we meet in Cambridge or London for the cost of a
beer). If anyone is interested, [please mail
me](mailto:ross@burtonini.com).

**Update:** I've got a buyer!
