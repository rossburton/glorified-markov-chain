Title: For Sale: Camera Bag
Date: 2008-01-01 19:30
Tags: life
Slug: for-sale-camera-bag

I'm selling my camera bag and lens case as I never use them, and thought
I'd offer them here first before they hit eBay.

Camera bag: [Lowepro Toploader
65AW](http://www.lowepro.com/Products/Toploading/allWeather/Toploader_65_AW.aspx).
Very good condition: it's mostly been in a cupboard apart from three
weeks in India a while ago. I'm thinking £25 including delivery to the
UK.

Lens case: [Lowepro
1W](http://www.lowepro.com/Products/Accessories/lens_cases/Lens_Case_1W.aspx).
This is as-new, and has never left the office. £10 including delivery.

If anyone is interested, [drop me a mail](mailto:ross@burtonini.com).

<small>NP: <cite>Another Late Night: Zero 7</cite></small>
