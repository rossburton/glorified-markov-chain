Title: For Sale: Discworld Novels
Date: 2007-05-01 12:30
Tags: life
Slug: for-sale-discworld-novels

In order to make some space and a little cash, I'm selling my collection
of <cite>Discworld</cite> novels. I have the first 24 in paperback, but
sadly not *Lords and Ladies*. They are all in very good quality and I'm
offering the lot for about £60. Reasonable offers accepted, and this is
for collection or personal delivery if I'm feeling nice (say, you live
in Cambridge). I can provide photographic evidence of the books if
required, I just need to get them out from behind another pile of books.

Anyone interested? If so, [mail me](mailto:ross@burtonini.com).

<small>NP: <cite>Soulmates</cite>, Nobody</small>
