Title: For Sale: IBM ThinkPad X22
Date: 2008-03-09 17:10
Tags: tech
Slug: for-sale-ibm-thinkpad-x22

In an effort to clear up the utter mess which is my home office, I'm
selling my old laptop. It's an IBM ThinkPad X22 (ultra-portable), with
(and this is from memory) a Pentium 3 Mobility at 733MHz, 640MB RAM, and
a 20GB HDD. I think. (**update: 40GB HDD**). It has built-in wired
ethernet but no built-in wireless, though I can throw in the
Orinoco-based wi-fi card I've been using. There is also the UltraSlice
micro-docking station with a hotpluggable CD drive/HDD bay. It will come
booting Debian, but it has a Windows 2000 license and I'm sure I have
the CD somewhere in the attic. The main caveat is that the screen hinges
have lost their grip so it is best used either closed as a router or
network music box, or against something to keep the screen from falling
open. :)

So, anyone want to make an offer? I'll put it on eBay if nobody wants
it, but I thought I'd offer it out to the Planets first. If anyone is
interested ping me and I'll go and turn it on to double-check the
specifications.
