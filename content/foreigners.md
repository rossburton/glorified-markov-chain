Title: Foreigners
Date: 2005-11-08 11:45
Tags: life
Slug: foreigners

“I'm not a racist but I'm moving out of London, there are too many
foreigners around.”

Why do I seem to attract this sort of person?

**Update:** Note that there should be quotation marks around first line,
some renderers are ignoring the &lt;q&gt; element and not rendering the
quotes. Those quotes change the meaning of this post quite a bit!

<small>NP: <cite>Best Of</cite>, Stevie Wonder</small>
