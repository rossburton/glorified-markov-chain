Title: Free The (Crispy) Bits!
Date: 2005-03-07 20:17
Tags: tech
Slug: free-the-crispy-bits

Recently I've been joining the memory reduction mission in a hard-core
stylee, by poking at the lower libraries in the GNOME stack.

A few partial patches to Pango were submitted which chipped away at the
non-constant data bit by bit, before Owen decided to knock the wall down
and do a comprehensive review, saving 12K per process.

Fontconfig is an interesting program to work on, and during the hacking
it is quite easy to end up with a Fontconfig which doesn't think there
are any fonts, or more amusingly gives you Isabella or Mistral when
asked for Vera. However, I did eventually fix these bugs and now
Fontconfig patterns (the core data type) use less memory and are a lot
faster. Excellent stuff, but Fontconfig is still allocating a lot of
memory when there are many fonts (`fc-list | wc -l` says 435 faces on my
desktop), which Pango then copies for some reason (probably a sensible
one but what do I know).

Of course, working on this more will have to wait until I get back from
India...
