Title: Freecycle
Date: 2008-03-14 17:30
Tags: life
Slug: freecycle

In the last fortnight I have managed to
[Freecycle](http://freecycle.org) the following objects:

-   Bathroom scales
-   A safe
-   An Orange Pay As You Go SIM
-   A wooden chopping board
-   A Bluetooth headset
-   Six iPod cases
-   A Bodum teapot
-   A radio walkman
-   A Sharp portable minidisc recorder
-   A Nikon APS camera
-   Five belts
-   Two boxes of word fridge magnets
-   A 802.11g CardBus card
-   An external USB sound device

It sounds a little like the Generation Game, I know. Combined with a bin
bag full of junk, I can actually see the bottom of my drawers now!
