Title: Friday Linkage
Date: 2005-05-27 16:10
Tags: life
Slug: friday-linkage

I can't be bothered to blog much today as it's far too hot for that sort
of thing (disclaimer: it's only 29 degrees, but I'm still ill), so I'll
provide a few links instead.

My local cinema is one of the 209 to be selected for a [grant to go
digital](http://www.ukfilmcouncil.org.uk/news/?p=D4A157780cd7d24E47LmQ4394634).
Very cool, and as part of the deal they've agreed to show more
non-Hollywood movies, which is always good news.

LinuxDevices.com has the [presentation Nokia gave at LinuxWorld
NY](http://www.linuxdevices.com/files/article057/). Pretty interesting
reading for anyone after every bit of information about the super-cool
770.

<small>NP: <cite>Simple Things</cite>, Zero 7</small>
