Title: From: installer@ftp-master.debian.org
Date: 2003-12-29 00:00
Tags: tech
Slug: from-installerftp-master-debian-org

[Jeff](http://www.gnome.org/~jdub/blog/), congratulations on your [first
upload to Debian](http://www.gnome.org/~jdub/blog/1072669654). I think I
still have a copy of my first mail from
`installer@ftp-master.debian.org` in my Debian/ maildir somewhere. Glad
to have done the actual upload for you.
