Title: Furious Anger, Part Two
Date: 2006-03-30 17:20
Tags: life
Slug: furious-anger-part-two

Thursday 23^rd^ March:

![DAYLIGHT
ROBBERY](http://www.burtonini.com/images/express-20060323.jpg)

Friday 30^th^ March:

![NHS CUTS](http://www.burtonini.com/images/express-20060330.jpg)

In the space of seven short days, they've managed to go from hard-line
right-wing, give us tax cuts, don't "throw it away" on the state; to
full-on fund the NHS more "Daily Express Crusade" your childen are All
Going To Die.

If these headlines were in [The Onion](http://www.theonion.com) they'd
be a fine example of tabloid spin and use of language. But they are
serious, and I hate this paper.

<small>NP: <cite>Ágætis Byrjun</cite>, Sigur Rós</small>
