Title: Furious Anger
Date: 2006-03-23 09:30
Tags: life
Slug: furious-anger

Today's Daily Express front page:

![DAYLIGHT
ROBBERY](http://www.burtonini.com/images/express-20060323.jpg)

That nasty Brown “throwing away” your money on state spending, such as
the NHS, schools, pensions... A week later the same paper will be
moaning about the state of the NHS, another pension crisis, or how
education standards are falling.

Actually, that's wrong. [Another paper](http://www.dailymail.co.uk/)
will moan about that, the Express will just have [Diana
Exclusives](http://www.bigdaddymerk.co.uk/mailwatch/index.php?cat=8).

<small>NP: <cite>Trouser Jazz</cite>, Mr Scruff</small>
