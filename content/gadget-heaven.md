Title: Gadget Heaven
Date: 2004-03-17 00:00
Tags: tech
Slug: gadget-heaven

It's been a good few days for gadgets. On Monday morning I made my first
Internet connection on my laptop via GPRS on my T68i (over Bluetooth) —
which was surprisingly easy once I'd figured out `rfcomm` and the
relevant PPP lines. Some day soon I'll document exactly what I did for
others.

Today my new minidisc player turned up, a Sony MZ-N710 (my old one is so
dirty it only works when it is upright). Ten minutes of poking at
hotplug scripts and a quick patch of `libnetmd`, and the relevant device
node has its permissions changed and I was finally adding titles to one
of my disks. This is probably all I'll use the NetMD stuff for: I don't
have large quantities of music on my computer to copy anyway and the
`libnetmd` people are still decoding the DRM used by Sony. My hifi has
optical out, so I already get perfect copies (well, before ATRAC kicks
in at least) with track markers.

I'm now thinking about writing a small Rhythmbox patch so that I can
title my discs there, but I fear the Wrath Of Colin Walters if I do that
before I finish the CD writing patch...

NP: Brand New Second Hand, Roots Manuva
