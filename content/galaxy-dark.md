Title: Galaxy Dark
Date: 2008-05-14 12:06
Tags: life
Slug: galaxy-dark

I got a free bar of the new <cite>Galaxy Dark</cite> with my shopping
yesterday, which is basically a dark chocolate (50%) version of a Galaxy
bar. Well, I say that, but...

> “The smooth Galaxy way to enjoy dark chocolate... deeply smooth,
> intensely delicious and not at all bitter.”

This should be called <cite>Galaxy Fail</cite>. It looks like dark
chocolate but is pumped with sugar so it has a weird sickly sweet taste,
nothing like the creamy taste of the original Galaxy. I predict this
product will be binned soon.
