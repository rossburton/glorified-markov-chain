Title: Gaming
Date: 2003-08-08 00:00
Tags: tech
Slug: gaming

[ScummVM](http://www.scummvm.org) 0.5, the excellent SCUMM interpreter
(<cite>Monkey's Island</cite>, <cite>Day of the Tentacle</cite>,
<cite>Sam and Max</cite>, etc) has been released. This is good news.
Even better news is that the <cite>Beneath a Steel Sky</cite> developers
assisted in this release, and even re-released BASS as freeware!

However, it appears I am out of practise in this form of point-and-click
adventure game. After 10 minutes of not being able to do anything in the
first room but die, I had to resort to a walk-through. Now I'm stuck
again, Joey has the new welder shell but I can't get the key out of the
store room. I'm determined not to use the walk-through again! I
foolishly had a walk-through when I played <cite>Monkey Island</cite>
for the first time, and instead of giving me weeks of fun I completed
the game in a day, as I had a quick look whenever I was slightly stuck.
I have learnt my lesson, and never again shall I repeat it.
