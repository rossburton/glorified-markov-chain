Title: GammaarrRRRrr!!
Date: 2003-08-15 20:09
Tags: tech
Slug: gammaarrrrrrr

It's a perfect mix of a useful tool (calibrate the gamma for your
monitor) and the strange pirate fascination in \#gnome-hackers. It's
GammaarRRr!.

This is version 0.1. It sucks, though it does work. It's full of bugs,
but it has potential. Download a [tarball
here](http://www.burtonini.com/computing/gammaarRRr-0.1.tar.gz).
