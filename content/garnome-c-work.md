Title: GARNOME, C++, Work
Date: 2002-11-28 00:00
Tags: tech
Slug: garnome-c-work

Well, it finally started to happen. The Java project at work is starting
to enter "maintain" mode instead of "develop" mode, so I get to play
with C++ and GTK+.

However, we're running RH73 boxes which don't come with GTKMM 2
installed, so we have to build that. That's easy, I say, we'll use
GARNOME -- it rocks. So we use GARNOME, and I find out that the 2.0.x
branch has been dropped for the 2.1.x branch, which is too scary for
everyone to use (although I personally use it on my desktops and
laptop).

To cut a long story short, I just became the "Alan Cox of GARNOME" as
Jeff put it, the maintainer for a GARNOME which builds the stable GNOME
2.0.x releases...
