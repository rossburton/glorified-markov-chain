Title: Gates FUDing Again
Date: 2006-03-16 17:00
Tags: tech
Slug: gates-fuding-again

Man, Bill Gates is pissed that the [OLPC](http://laptop.org/) project
isn't using Windows ([from
Reuters](http://news.yahoo.com/s/nm/20060316/tc_nm/microsoft_gates_dc)):

> "If you are going to go have people share the computer, get a
> broadband connection and have somebody there who can help support the
> user, geez, get a decent computer where you can actually read the text
> and you're not sitting there cranking the thing while you're trying to
> type," Gates said.

Interesting, considering just a few months ago his opinion of OLPC was
subtly different ([via New York Times, quoted in Linux
Pipeline](http://www.linuxpipeline.com/blog/archives/2006/01/sour_grapes_and.html)):

> According to several people familiar with the discussions, Microsoft
> had encouraged Mr. Negroponte to consider using the Windows CE version
> of its software, and Microsoft had been prepared to make an
> open-source version of the program available.

For the potential market of a million schoolchildren Microsoft will do
anything. Take that market away and give it to Linux of all people, and
they'll FUD you to death.

<small>NP: <cite>Motion</cite>, Cinematic Orchestra </small>
