Title: Generating Random vCards
Date: 2007-06-06 15:00
Tags: tech
Slug: generating-random-vcards

Again I found myself wanting a pile of realistic vCards to test with,
and decided to write a decent and extendable solution for once (instead
of the hilarity of
[this](http://burtonini.com/bzr/eds-tests/dummy-ebook/e-book-backend-dummy.c)).
A while ago I discovered [Barnum](http://code.google.com/p/barnum/),
which can generate pseudo-realistic data by containing large data sets
of names, street addresses, zip codes and credit card numbers. I
extended it a little, adding support for creating an email address from
a given name (it looks weird when the contact's name is John Smith but
the email is john.doe@bar.com) and generating birth dates, then wrote a
tiny script to write valid vCards. Et voilà:

    $ ./gen_vcard.py 
    BEGIN:VCARD
    VERSION:3.0
    UID:barnum-1107705517
    FN:Caleb Barone
    N:Barone;Caleb;;;
    BDAY:1955-05-06
    EMAIL;TYPE=WORK:cbarone@blanditvelit.eu
    X-JABBER;TYPE=HOME:cbarone@eratdolore.eu
    ADR;TYPE=HOME:;;8270 Kahlden Lane;Thompson;CT;06277;USA
    END:VCARD

If this is useful to anyone else, you'll want
[Barnum](http://code.google.com/p/barnum/), my patched
[gen\_data.py](http://burtonini.com/computing/gen_data.py), and
[gen\_vcard.py](http://burtonini.com/computing/gen_vcard.py).

<small>NP: <cite>More Than Music</cite>, Murphin</small>
