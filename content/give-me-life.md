Title: Give Me Life
Date: 2004-04-30 00:00
Tags: life
Slug: give-me-life

Congratulations [Jeff](http://www.gnome.org/~jdub/blog/1083280558) and
[Pipka](http://www.pipka.org/blog/1083272709?flav=roses) on your
engagement!

It sounds like your proposal was a lot smoother than my own. We were in
Rome over Easter, and our hotel (in the old part of the city) had a
*wonderful* view over the city from the roof terrace. I was forming
grand plans of proposing up on the terrace, with the city lights and
fine wine... Then it turns a bit to cold to sit outside at 11pm and our
hotel didn't sell wine, only small cans of beer. Not quite the vision I
had, but a million things could have ruined the vision (someone else on
the terrace for example). However, everything from that point went to
plan and I still find myself grinning uncontrollably when I realise that
in under 3 months we'll be Mr and Mrs!
