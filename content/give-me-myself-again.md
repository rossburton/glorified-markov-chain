Title: Give Me Myself Again
Date: 2004-04-30 00:00
Tags: life
Slug: give-me-myself-again

![I Am Me](http://www.burtonini.com/iam.png)

The last few weeks have been totally mad: wedding planning is getting
more intense with only 11 calendar weeks left. We've been spending every
evening this week planning the gift list as we should send it off this
weekend, and last week I even managed to have an opinion on flowers — a
decisive one no less. Wedding list planning started out as a fun task
(essentially, picking your own presents) and started turning into a task
when we had to make sure there is a good range of gifts available for
everyone and we've covered everything. Didn't add a DVD recorder for the
TV, but we did add a cute Sony Hi-Fi for the bedroom.

Add in the stress from work and I'm 100% knackered. In a typical twist
of reality, the espresso coffee I bought yesterday wasn't ground fine
enough and makes an espresso which appear to be the mutant love-child
between a weak espresso and a small cappuccino: it takes like a small
strong coffee with a crema which is more like foam. I would say "what
next", but that feels like tempting fate.

NP: <cite>Little Earthquakes</cite>, Tori Amos. Though anyone who has
the album and was reading chronologically probably knew that already...
