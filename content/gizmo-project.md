Title: Gizmo Project
Date: 2005-09-14 09:13
Tags: tech
Slug: gizmo-project

There are many "Skype-killers" out there at the moment, but until
GoogleTalk has a Linux client for it's VOIP service the best of the
bunch appears to be the [Gizmo Project](http://www.gizmoproject.com./).
It uses SIP, an open standard unlike Skype, and they just started
shipping a Linux client (albeit in beta form). Even better news is that
it appears that the source for the client will be available shortly, and
it uses GTK+ 2.

![Gizmo Project
screenshot](http://www.burtonini.com/computing/screenshots/gizmo.png){}

It also has a dependency on Apple's Bonjour, so I guess it will
automatically find other Gizmo Project users on the local network. If
the client will connect to a local server, then that is a really nice
it-just-works implementation of VOIP for internal office calls.

<small>NP: <cite>Dub Come Save Me</cite>, Roots Manuva</small>
