Title: Glib and Serial Port Help
Date: 2007-12-09 14:15
Tags: tech
Slug: glib-and-serial-port-help

So I want to write a small program to read and write data to a serial
port (115200 baud, 8N1) using `GIOChannel`. No matter what I try, I
can't seem to get input callbacks from the channel. :( Does anyone have
any small working examples of using a `GIOChannel` to drive a serial
port I can look at?

<small>NP: <cite>Mungo's Hi-Fi at Exodus</cite></small>
