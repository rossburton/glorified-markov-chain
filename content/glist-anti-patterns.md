Title: GList Anti-patterns
Date: 2009-07-16 16:11
Tags: tech
Slug: glist-anti-patterns

    g_list_length(children);
    for (int i = 0; i < (int)num; i++) {
      GList * child = g_list_nth(children, num - i - 1);

[**FAIL**](http://www.google.com/codesearch/p?hl=en&sa=N&cd=3&ct=rc#NVng75fSisY/mozilla/widget/src/gtk2/nsWindow.cpp&q=g_list_length%20-glib&l=3972)

    if (g_list_length(nb_pages) != 0) {

[**FAIL**](http://www.google.com/codesearch/p?hl=en&sa=N&cd=2&ct=rc#emmXna8sEVo/wxGTK-2.4.0/src/gtk/notebook.cpp&l=322)

    for( i=0; i < g_list_length( GTK_CLIST(clist)->selection; i++ ){
      gint row = (gint)g_list_nth_data( GTK_CLIST(clist)->selection, i);

[**TURBOFAIL**](http://mail.gnome.org/archives/gtk-app-devel-list/1999-April/msg00037.html)
