Title: gnome-app-install Moved
Date: 2005-03-31 16:15
Tags: tech
Slug: gnome-app-install-moved

Mainly a heads-up for the translation teams, but `gnome-app-install` is
now maintained in GNOME CVS instead of an Arch repository on
`burtonini.com`. Translate my pretties, translate!

<small>NP: <cite>Hinterland</cite>, Aim</small>
