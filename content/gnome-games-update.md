Title: GNOME Games Update
Date: 2003-03-10 00:00
Tags: tech
Slug: gnome-games-update

The GNOME Games are going well, I feel. The stable series is hardly
changing, which is a good sign; and the unstable branch is progressing
nicely. The major news is that I have removed several games, which were
crap/broken/useless. I am also working on revamping the GConf usage in
the games, and finishing the HIGification of the interface (check out
Mahjongg in CVS).

However, I am looking for a co-maintainer to take over the games after
GNOME 2.4. I am more interested in working on programs like DevHelp
(which Micke appears to be trying to give to someone else) than the
games. I think that the games are a fantastic way of introducing new
maintainers to the GNOME system, as they are not essential and a bug in
them does not bring down an entire desktop. Interested people email
me...
