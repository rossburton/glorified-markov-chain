Title: GNOME in Pixar
Date: 2003-11-15 00:00
Tags: tech
Slug: gnome-in-pixar

> <cite> Saw Finding Nemo last night. Very, very cool to see Sawfish all
> over the 'making of' documentary and one shot that briefly contained a
> gnome panel. </cite> - Luis Villa

I can inform everyone that Pixar is now using Metacity and, well,
[Devil's Pie](http://www.burtonini.com/blog/computers/devilspie).
Sawfish was a little too crackful for them so they switched to GNOME 2
with Metacity and Devil's Pie to provide a small amount of crack.
