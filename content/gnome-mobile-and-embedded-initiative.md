Title: GNOME Mobile and Embedded Initiative
Date: 2007-04-19 17:30
Tags: tech
Slug: gnome-mobile-and-embedded-initiative

Well, it's finally public: the [GNOME Mobile and Embedded Initiative
(GMAE)](http://www.gnome.org/mobile/) is ready to rock and roll.
[OpenedHand](http://www.o-hand.com) has been a part of this from the
very beginning, working on the platform before it was a platform, and
helping to shape the platfom once the contributors with a common
interest got together.

It has been really exciting to see GMAE go from a twinkle in
[Jeff's](http://perkypants.org/blog/) eye to a real project, with real
companies involved and real products using it. Hopefully this momentum
will continue, and we'll achieve Complete World Domination in the
embedded market by... well, 2010 sounds reasonable.

<small>NP: <cite>Remembranza</cite>, Murcof</small>
