Title: GNOME Mobile Moduleset
Date: 2008-04-15 16:10
Tags: tech
Slug: gnome-mobile-moduleset

I just committed to JHBuild three new modulesets, `mobile-2.24`,
`pimlico` and `matchbox`, so that GNOME people wanting to develop
against the [GNOME Mobile](http://gnome.org/mobile/) platform can use
tools they know to build everything they need.

`mobile-2.24`

:   This changes GConf and EDS to use the DBus ports, and provides
    `meta-mobile-platform` which builds the complete platform.

`pimlico`

:   This builds Contacts, Dates and Tasks, providing `meta-pimlico`.

`matchbox`

:   This builds Matchbox Panel, Matchbox Desktop, Matchbox Keyboard and
    Matchbox Window Manager, providing `meta-matchbox`.

Also, [Poky](http://pokylinux.org/) is building images nightly with the
complete platform in, which will let you build and test software in a
PDA-style environment with QEMU, running on x86 or a number of ARM-based
devices (such as Nokia N800, Sharp Zaurus or OpenMoko).

Last week at the Collaboration Summit in Austin (which I couldn't attend
for [personal
reasons](http://www.burtonini.com/blog/life/baby-2008-04-07-09-50))
there was a day-long GNOME Mobile meeting, as a result of which there is
now a long list of packages which need to be considered for addition to
the Platform (such as HAL, Gypsy and Geoclue), and a few changes (such
as replacing gnome-vfs with gvfs). I hope to review the proposals fairly
shortly, so that we can hopefully make an initial GNOME Mobile 2.24
platform release alongside the Desktop release in September.

In other news, [this is exactly what the Internet is
for](http://laughingsquid.com/a-cat-playing-the-theremin/).

<small>NP: <cite>Blue Moon Station</cite>, Solar Fields</small>
