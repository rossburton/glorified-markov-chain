Title: GNOME Packaging
Date: 2004-03-21 00:00
Tags: tech
Slug: gnome-packaging

So Christian Marillat decided against working with the seventeen-strong
Debian GNOME Team and orphaned all of his packages in dramatic style,
spreading the orphan message across the changelogs of all the packages.

This is good, as the GNOME Team generally agrees on a way of working
which Christian doesn't agree with, i.e. pushing GNOME 2.6 into Sid now
on the assumption that there won't be any problems with it. We're
currently building GNOME 2.6 into Experimental, and have his a nasty
GTK+ 2.4 bug, where it crashes on startup. Eek.

NP: Stevie Wonder - The Definitive Collection
