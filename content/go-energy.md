Title: Go Energy
Date: 2006-07-07 14:50
Tags: tech
Slug: go-energy

To counter the Stop Energy I'm famed for (think of the
<cite>Listen</cite> saga), I'm going to blog some Go Energy.

[Sputnik](http://oss.codepoet.no/sputnik/) is a great little Internet
radio player. The UI is heavily based on Muine, it uses Python and
GStreamer, and pretty much Just Works. It's only up to version 0.0.3 but
it's been my main source of music for the last few days, so it does what
it needs to well. I'd like to see it displaying the song title and
artist somewhere, but that is about it really.

[NFlick](http://maemo.org/maemowiki/ApplicationCatalog2006#head-7b663fd945bcc60fbd6c5b7900e837a5ff99ec9c)
is a Flickr set browser for the Nokia 770. As it is only version 0.1 is
has some flaws: it eats huge amounts of memory (no running it at the
same time as the web browser). crashes a fair bit, doesn't cache images,
and doesn't support the hardware keys. However, when it works, it works
well: this is just the sort of tool I want on the 770.

<small>NP: <cite>Digitally Imported - Lounge</cite></small>
