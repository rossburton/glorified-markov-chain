Title: Go Fluendo!
Date: 2004-04-20 00:00
Tags: tech
Slug: go-fluendo

Sure, [Fluendo](http://www.fluendo.com/) may sound like a company which
makes pipes and toilets, but today's announcement that they are
[sponsoring xiph.org to finish Ogg
Theora](http://www.fluendo.com/press/releases/PR-2004-01.html) is
totally cool.

End-to-end open video streaming is an admirable goal, and something
which would be great to see. Thomas has even said that they aim to be
able to stream [GUADEC](http://guadec.org/) this July over the net,
which would be very impressive if they get that far so quickly.

Go Fluendo!

NP: <cite>Dummy</cite>, by Portishead. Still got the bass problem,
looking [at
these](http://products.sony.co.uk/productdetail.asp?id=14_39_2638).
