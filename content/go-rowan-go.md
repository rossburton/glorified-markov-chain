Title: Go Rowan Go
Date: 2006-03-21 10:00
Tags: life
Slug: go-rowan-go

Vicky and myself have said this before, but I'll say it again. For the
top man in the Church of England, the current [Archbishop of
Canterbury](http://en.wikipedia.org/wiki/Archbishops_of_Canterbury)
Rowan Williams is surprisingly moderate, progressive and possibly even
cool (via [The
Guardian](http://www.guardian.co.uk/religion/Story/0,,1735730,00.html)):

> The Archbishop of Canterbury, Rowan Williams, has stepped into the
> controversy between religious fundamentalists and scientists by saying
> that he does not believe that creationism - the Bible-based account of
> the origins of the world - should be taught in schools.
>
> …
>
> Dr Williams spoke of his determination to hold the third-largest
> Christian denomination together in its row over the place of gay
> clergy. He was also highly critical of parts of the church in Africa
> and said he did not wish to be seen as "comic vicar to the nation",
> speaking out on issues where he can make no difference.
