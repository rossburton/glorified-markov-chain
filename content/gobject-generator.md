Title: GObject Generator
Date: 2005-11-24 18:25
Tags: tech
Slug: gobject-generator

It's cheap, it's nasty, but it works: take the pain out of writing the
same strings a hundred times by only writing them six with the super
k-rad [GObject Generator](http://burtonini.com/cgi/gobject.py)!

<small>NP: <cite>Blue Album</cite>, Orbital</small>
