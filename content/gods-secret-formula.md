Title: God's Secret Formula
Date: 2003-09-16 00:00
Tags: life
Slug: gods-secret-formula

So I've borrowed this book called <cite>God's Secret Formula</cite> by
Dr. Peter Plichta. It claims to be an amazing piece of work proving that
prime numbers are the fundamental basis of the universe, that the number
19 links everything, etc etc. Most amusing.

I'd probably be less skeptical about the book if I hadn't been noticing
serious errors in the book all day. He is claiming that without the moon
there would be no tides (the sun effects the tides too); that the number
of days in an average pregnancy and the difference between 0° Celsius
and absolute zero are the same, 273 (Celsius is an arbitrary scale); and
that 361 is "nearly" 360. The man is a nutcase.

Sadly Googling for his name produces hundred of fanboy sites, but no
critical analysis of his work. Anyone know of any links? I'd like to see
how many more incorrect statements he has claimed as "truth".
