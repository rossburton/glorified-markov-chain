Title: GOOD MORNING!
Date: 2004-03-16 00:00
Tags: life
Slug: good-morning

One of the pains of commuting into London is the dash past the people
handing out flyers, samples and free magazines at the train stations
(although I must admit [it's not all
bad](http://www.burtonini.com/blog/life/weekend-20040309)). Today was a
painful day though...

Past the ticket barriers, my mind is running on auto-pilot, set to Avoid
Other Commuters and Get To Moorgate. All of a sudden, a number of
incredibly bright suits (I guess there must have been people inside them
too) shouted "GOOD MORNING!!" at me. Argh, you vision of hell, stop
offending my senses! Begone foul being! It was only 08:15, people are
not meant to be that awake, and frankly I don't want to be that awake
either. I've no idea how Virigin Airlines (only God knows what a
shouting man has to do with flying, but they did have women in hostess
uniforms smiling as well) managed to get these people to be so awake at
such an ungodly hour of the day.

NP: Simple Things, Zero 7. Note to self: find the other minidiscs.
