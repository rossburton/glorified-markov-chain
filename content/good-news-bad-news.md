Title: Good News, Bad News
Date: 2005-07-07 12:15
Tags: life
Slug: good-news-bad-news

Good news: my course pack for [The Art Of
Photography](http://www.oca-uk.com/courses/photography/p1a.php) at the
Open College of Arts arrived today, so I'm now a student again, albeit a
part-time one. This should be fun...

Bad news: London was bombed this morning. Everyone I know in London is
okay, but Allen cut it close: he should have been in Liverpool Street
when it kicked off there, but luckily was starting work late today. So
far explosions in six train stations and on one bus have been confirmed
but hard numbers are very hard, even three hours after the event.

<small>NP: <cite>Pre-emptive Strike</cite>, DJ Shadow (with the BBC news
accompanying it)</small>
