Title: Good News
Date: 2003-12-21 00:00
Tags: tech
Slug: good-news

Quickie: Good news for Sound Juicer -- I shall be shortly buying a new
hard drive for my laptop (thanks to Christian Schaller) and thus have
the space to build GNOME 2.5 and GStreamer 0.7. Expect migration to
these to start in the new year, hopefully without losing GNOME
2.0/GStreamer 0.6 support of course.
