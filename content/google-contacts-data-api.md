Title: Google Contacts Data API
Date: 2008-03-06 17:15
Tags: tech
Slug: google-contacts-data-api

Those nice people at Google have finally opened their [Contacts
API](http://code.google.com/apis/contacts/). Now, Evolution already has
a Google Calendar backend, so does anyone fancy writing a Google
Contacts addressbook backend? If someone with C/GObject knowledge is
interested, I'll happily provide assistance on the Evolution side.
