Title: Google Juice!
Date: 2005-11-07 10:32
Tags: life
Slug: google-juice

Poor [Guido Boehm](http://lenix.de/) is suffering a lack of Google
Juice, as searching for him results in the Devil's Pie home page.
Hopefully this will help [Guido](http://lenix.de/) out!
