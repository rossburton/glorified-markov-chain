Title: Google Map Hacking
Date: 2005-07-14 16:40
Tags: tech
Slug: google-map-hacking

I'm very late to this, but I've done a bit of a hack to mix Google Maps
and the [GnomeWorldWide](http://live.gnome.org/GnomeWorldWide) map, the
results of which can be [seen
here](http://www.burtonini.com/gnome.html). The next step is to extract
more data from the wiki page, such as the name of the person, and add
this information to the markers.

<small>NP: <cite>Schoolhouse Funk</cite>, DJ Shadow</small>
