Title: Growth
Date: 2006-02-17 12:03
Tags: life
Slug: growth

As we are with Henry every day, we didn't really notice him turn from
this:

![Small Henry](http://www.burtonini.com/photos/Misc/img_3948-small.jpg)

into this in the space of two months:

![Big Henry](http://www.burtonini.com/photos/Misc/img_4314.jpg)

Time for a new bed soon I guess!

<small>NP: <cite>Ray Of Light</cite>, Madonna</small>
