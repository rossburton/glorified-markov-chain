Title: GStreamer Bounty
Date: 2006-06-23 15:45
Tags: tech
Slug: gstreamer-bounty

I'm willing to start a bounty for someone to fix/port/implement the
required pieces of GStreamer so that Sound Juicer can rip a whole CD,
fully tagged (all tags SJ writes must be correctly encoded), to MPEG4
AAC (as used natively by iTunes). A requirement is that a music player
that support MPEG4 files can see the tags without any extra work: the
primary test for this will be importing the ripped files into iTunes.

I think there are many people out there who would like this to happen,
so I want to arrange a group bounty. [Bounty
County](http://bountycounty.org) looked promising but I have to specify
how much the bounty is up front, I can't just let the fund grow as
people donate what they can. The Bounties section on
[Launchpad](http://launchpad.net) has the same limitation. Does anyone
know a reputable site for managing donations like this, or should I just
get register a new email address on PayPal and get people to donate
there?

Also to gauge how much interest there is in this, can anyone who would
consider donating leave a comment?

<small>NP: <cite>Vertigo</cite>, Groove Armada</small>
