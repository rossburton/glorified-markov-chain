Title: GStreamer DVD Transcoding
Date: 2006-04-19 18:50
Tags: tech
Slug: gstreamer-dvd-transcoding

Dear Lazyweb...

So I have a number of VOB files from a DVD, and wish to turn them into a
single Ogg Vorbis file. Does anyone have a GStreamer pipeline that works
directly on VOB files (and not reading via `dvdreadsrc`) and will
combine the multiple VOBs into a single output file?
