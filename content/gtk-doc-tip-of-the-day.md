Title: Gtk-Doc Tip Of The Day
Date: 2005-08-09 17:05
Tags: tech
Slug: gtk-doc-tip-of-the-day

Today's gtk-doc Tip Of The Day is that everyone should put
`<index></index>` into their `module.sgml` file. Then gtk-doc will
generate a nice index in the HTML for you! You can even get really
extravagant and do what GTK+ does:

>     <index>
>       <title>Index</title>
>     </index>
>     <index role="deprecated">
>       <title>Index of deprecated symbols</title>
>     </index>
>     <index role="2.2">
>       <title>Index of new symbols in 2.2</title>
>     </index>
>     <index role="2.4">
>       <title>Index of new symbols in 2.4</title>
>     </index>
>     <index role="2.6">
>       <title>Index of new symbols in 2.6</title>
>     </index>
>     <index role="2.8">
>       <title>Index of new symbols in 2.8</title>
>     </index>

Sweet.

<small>NP: <cite>Red Bird</cite>, Heather Nova</small>
