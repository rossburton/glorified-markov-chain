Title: GTK+ Is My Bitch
Date: 2007-06-26 13:40
Tags: tech
Slug: gtk-is-my-bitch

Thanks to some pointers from Kris, I've got some subtle new bling in
Tasks. The core of the code is
[KotoCellRendererPixbuf](http://svn.o-hand.com/view/tasks/trunk/libkoto/koto-cell-renderer-pixbuf.c?view=markup),
a `GtkCellRendererPixbuf` subclass which implements the `activate`
virtual method, so that it is clickable. However because the other
renderer in the column is not activatable (a text renderer), clicking on
the row activates the pixbuf renderer. A quick bit of logic in the
activate virtual function to look at where was clicked solves this, and
now Tasks shows clickable icons when a task has a URL set.

![Tasks](http://burtonini.com/computing/screenshots/tasks-url.png){}

I particularly like how space for the icon isn't allocated in every row,
so if there is no icon the text extends all the way across the view.

<small>NP: <cite>The Herd</cite>, The Herd</small>
