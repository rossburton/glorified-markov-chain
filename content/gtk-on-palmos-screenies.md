Title: GTK+ on PalmOS Screenies
Date: 2006-02-23 17:15
Tags: tech
Slug: gtk-on-palmos-screenies

Scroll down a bit
[here](http://babelfish.altavista.com/babelfish/trurl_pagecontent?lp=ru_en&url=http%3A%2F%2Fmobile-review.com%2Fexhibition%2F3gsm-wmsimb-2006.shtml)
and there are some photos of GTK+ running on a prototype Palm device.
Very cool, hopefully they get the development kits out this year.

It should also be known that Richard Purdie is currently my hero. I
can't say why at the moment, you'll have to come to FOSDEM to find out
why!

<small>NP: <cite>Modus Operandi</cite>, Photek</small>
