Title: GTK+ Secrets
Date: 2007-03-21 13:11
Tags: tech
Slug: gtk-secrets

In the last week or so I've discovered a couple of GTK+ tricks I didn't
know, so I thought I'd share them.

1.  Control-Backspace in an entry deletes from the cursor to the
    beginning of the text. I had previously hacked up custom binding for
    Control-u, but this is bound by default. Thanks Emmanuele for
    pointing this out. **Update:** as proof that my fingers are still
    using C-u, Control-Backspace actually deletes a word and not the
    entire entry.
2.  Middle-clicking on a scroll bar will move directly to that point,
    instead of moving towards that point by a page. Very useful, I'm
    glad Mitch mentioned this in a bug.

<small>NP: <cite>Didn't It Rain</cite>, Songs:Ohia</small>
