Title: GTK+ Tip of the Day, Sound Juicer news
Date: 2007-05-17 09:55
Tags: tech
Slug: gtk-tip-of-the-day-sound-juicer-news

First, a tip of the day. Thanks to [Matthew
Garrett](http://mjg59.livejournal.com/74651.html) for pointing out that
GLib 2.14 will have API for creating timers with second granularity
(instead of millisecond): `g_timeout_add_seconds()`. This means that
multiple timers scheduled to go off at roughly the same time will be
fired at once, resulting in power savings.

I'm pleased to see this land in GLib, and can't wait for GLib 2.14 now
as it is looking really good: GRegex and GSequence in particular are
interesting new additions.

In Sound Juicer news, Luca Cavalli has ported the gedit/Epiphany plugin
code to SJ. It's definitely work in progress at the moment, but it [is
coming together
nicely](http://bugzilla.gnome.org/attachment.cgi?id=88296&action=view).
Follow [\#311688](http://bugzilla.gnome.org/show_bug.cgi?id=311688) if
you want to keep up with the changes. The first two plugins I'd like to
see from both feature and proof of concept angles are more complete CD
playback controls (repeat and random), and downloading of album cover
art.

<small>NP: <cite>Delay</cite>, Dub Tractor</small>
