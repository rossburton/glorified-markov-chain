Title: GU4DEC is Over
Date: 2003-06-24 18:58
Tags: tech
Slug: gu4dec-is-over

Okay, I admit it. After the last GUADEC entry I got just a little bit
drunk, then had a little hang over, and was a little busy for the next
few days. I'll summarise.

Day III of GUADEC. Alan Cox's speech on DRM, IP, Copyright et al was
very interesting. That was followed by the final talk in which Glynn was
given a small prize (via email, but was told then) and a thunderous
applause for arranging the conference, and Luis was awarded the Pants Of
Thanks (don't worry, they were trousers) for being Bug Meister through
the GNOME 2.0 development. Finally there was a futile effort at a quiz,
as Telsa had lots of questions sorted. In a pub situation it would have
been great fun, but in a room with WiFi... let's just say some of the
questions were slightly easier than intended. Still good fun though.

Then a rush back to the room, quick change, and wandering for food. It
ended up being a Debian and GStreamer team dinner, which suited me just
fine. After a lovely piece of salmon and not enough wine, we paid up and
wandered towards Msrs. Maguires for drinks. That was a damn nice pub,
summarised by Matthrew Garrett very well: <cite>They gave me good beer
and then played obnoxious loud music at me</cite>. They had a local
micro-brewery, I can recommend the Weiss and (dear God my head) the
Rusty, a rather poor band, and a very large speaker on the stairs aimed
at the punters. We moved upstairs after a while, where it was hotter,
busier, but importantly, not as loud.

Found Murray Cumming upstairs, chatted about GTKMM and the GNOME Release
Team. Had more to drink. Tried to say more than Hello to Telsa again,
failed. Drunk more. Avoided becoming the gnome-media maintainer, thank
god. Drunk still more, Dan and Helen went home. Became the new
co-maintainer of DevHelp. Still more drinks. At this point I noticed
that every night I've been out drinking, its always Debian developers
and Swedes left at the end, propping up the bar. The GStreamer crew
don't drink too badly either.

At this point I'll skip straight to the morning after. [This
photo](/photos/GU4DEC/morning-after.jpg) should be more than enough to
explain how I felt (thats Matthew Garret, Thom May and myself btw).

I'll skip past flying from Dublin "Boring" Airport, the highlight of
which was watching iTunes rip a CD, giving me ideas for future Sound
Juicer developments.

Johan Dahlin was staying over at ours for a couple of nights for his
connecting flight to Sweden. Thank god he was hanging around just
outside arrivals, I had nightmarish images of running around the airport
trying to find him. Back to my place, introduce V to Johan, nice cup of
tea (with milk, the Swedes don't do this either) and wandered down the
road to test a new Indian restaurant which looked good. Johan has never
had Indian so we kept it simple and suggested a biyriani, which luckily
he (claimed to) like. He managed to eat all of it, which was more than I
could manage, so it can't have been too bad. The Indian restaurant on
Apton Road, Bishop's Stortford, gets 8/10. Back to our place, have
another nice cup of tea (no biscuits, sadly), Simpson's and then bed.

Friday should have been a good day. It was nice and warm, a little windy
maybe, but the day was long and we had nothing to do. For lunch we
popped into the Host cafe, but my eye was itching and watering, which
was a pain. On the way down to another cafe for food V noticed my eye
and said, "oh god its really bad, like it was that Summer". Oh joy. A
dive into Boots confirmed this would be another A&E visit, my eye was
very sore, inflamed and a fetching orange/red colour -- not white any
more. After sitting in A&E for only 30 minutes -- far better than last
time -- Allergic Conjunctivitis was diagnosed, again. Whilst I was there
I also had an eye pressure test (I haven't got glaucoma yet, but my Dad
has), and discovered that the last time I had this identical problem was
four years ago *to the day*. Spooky. Now I've got to take bloody eye
drops for the next two weeks.

I got back home late that afternoon to discover than V had been an
excellent host to Johan, they'd been shopping for dinner, sorted out a
package he needed to sent, picked up my Red Dwarf Series 1 and Best Of
Monty Python DVDs and lazed in the sun. We also discovered that we can't
pronounce Johan's name correctly. The closest I can come is "Ewan" in a
Welsh accent...

That evening we went down our local pub, pulling Allen and Dave along
for a drink. Allen has been sacked for turning up to work 2 minutes late
-- The Joy of Temping. As last orders were called I made the mistake of
asking Johan if he wanted another drink...

...and he nearly fell over on the way out of the pub. To be fair, he
only started drinking beer a few months ago and he was mixing a little
that night, trying out John Smiths before going back to Carling. I tried
the Guinness but it seemed inferior to the good stuff served back in
Dublin...

On the way back home we knocked on Elliot's (my sister's partner) door
to see if they were -- they were! This was cool, I hadn't seen them for
a few weeks as they were doing the trekking thing in Morocco. Had a good
look at photos, chatted to my sister about her career changes (not
teaching any more), photography courses (£100 for an evening class
apparently), the conference and other random stuff. All this time Johan
discovered that Elliot's taste in music and his own match quite well --
this led to Johan jumping around with alcoholic enthusiasm, switching
CDs after a few minutes. Elliot and Wiz loved this, shouting "We need
more Swedes here!". Johan calmed down. People ate dodgy kebabs. I got
cold sitting outside. Time to go home, Dave walked back to his place
whilst Allen slept on our sofa.

At seven the next morning I pulled myself up to say farewell to Johan,
and went straight back to bed. The rest of the weekend was uneventful --
a little shopping, changing the fish water, cleaning, wedding planning.
Quality time with V mainly, which was nice after 4 days in Dublin and 2
with Johan and Allen around.

Summary: GU4DEC rocked. I hope to go to GVADEC ("GUADEC 5" is so
boring). Johan is a great person in reality as well as in IRC. Guinness
is better in Dublin. Planning weddings is a lot of work. Sun pens
[rule](/photos/GU4DEC/sun-pen.jpg).

In other news I'm off to Kefalonia in two weeks, which will be nice. I
really must keep to my schedule at work...
