Title: Guacamayo 0.2
Date: 2012-05-22 11:30
Tags: guacamayo, yocto
Slug: guacamayo-0-2

Last week we (well, mostly Tomas if I'm honest) made our first release
of [Guacamayo](https://github.com/Guacamayo/meta-guacamayo), a reference
Linux distribution for media playback devices in a networked world. The
core technologies are the usual suspects: Yocto for the distribution,
GStreamer and PulseAudio, Rygel and Media Explorer.

The first release caters for the "connected speakers" use-case.  On boot
it connects automatically to the network over ethernet (wi-fi coming
soon) and using Rygel exposes a UPnP/DLNA MediaRenderer, with
hot-plugged USB speakers automatically switched to if plugged in. I've
been happily using it on a laptop with my
[Raumfeld](http://www.teufelaudio.co.uk/audio-streaming.html) system,
and Tomas has tested it on a BeagleBoard with a UPnP control point app
on his Android phone.

So, what's next?  I'm working on a web-based configuration tool for the
headless systems, and Tomas is integrating Media Explorer so we'll have
something you can plug into a TV.  Tomas is testing this on a Zotac ZBox
at the moment, and any week now I'll have a RaspberryPi to experiment
with.

If you're interested and want to know more, we have a small
[wiki](https://github.com/Guacamayo/meta-guacamayo/wiki) at GitHub and
you can often find us in `#guacamayo` on FreeNode.
