Title: Guacamayo in LWN
Date: 2012-06-14 11:41
Tags: guacamayo
Slug: guacamayo-in-lwn

The esteemed editors at [LWN](http://www.lwn.net/) have decided to write
a [small article](http://lwn.net/Articles/501493/) about
[Guacamayo](https://github.com/Guacamayo), giving an overview of the
project and what our plans are.  At the moment it's subscriber-only but
on the 21st June it will be publicly available.  We're quite happy with
the result, Nathan contacted us earlier in the week to clarify some
points that we hadn't made clear so it's probably the best source of
information of Guacamayo now!
