Title: Guacamayo Media Server
Date: 2012-08-30 12:31
Tags: guacamayo, yocto
Slug: guacamayo-media-server

Last night I merged the work by our lovely interns Emilia and Mihai to
add a media server image to [Guacamayo](http://guacamayo-project.org/).
Basically this is a DLNA "Digital Media Server" implemented using
Rygel's `media-export` plugin.

It's early days, mainly because it only exposes the demo content so far
and there is no easy way to add or remove media (SFTP as `root` doesn't
count!), but it's certainly a solid step in the right direction.

Thanks Emilia and Mihai!
