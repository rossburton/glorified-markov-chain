Title: GUADEC
Date: 2007-07-18 12:15
Tags: tech
Slug: guadec-2

Currently at GUADEC. We had the joint OpenedHand/Collabora party last
night, which explains the headache I guess. Also I'm glad I bought spare
jeans because somehow a glass of Pimms managed to get all over them.

I also saw KDE 4 running today. Best quote: “Well at least maximise
works”.

Alex's GVFS talk was good this morning, I totally love it and can't wait
for it to land in glib. The file monitoring API alone makes me want to
have Alex's babies (but it appears I've [been beaten to
it](http://blogs.gnome.org/alexl/2007/06/04/enosleep/)). I've already
written an application using `libgio`, which was amazingly simple. It's
a shame that so many people were asking questions during the talk
instead of waiting until the end, because he had to skip the last few
slides.
