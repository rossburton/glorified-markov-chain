Title: GUADEC 2006 Photos
Date: 2006-07-11 11:30
Tags: tech
Slug: guadec-2006-photos

On Sunday I spent a good hour or so sorting and uploading the photos
from GUADEC 2006. Most of the photos were taken by Daniel Stone, after
he stole my camera during the Maemo One Year Old party. Anyway, [here
they are on
Flickr](http://www.flickr.com/photos/rossburton/sets/72157594193158116/).

<small>NP: <cite>Colour the Small One</cite>, Sia (via Last.fm)</small>
