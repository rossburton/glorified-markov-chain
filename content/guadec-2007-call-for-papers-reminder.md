Title: GUADEC 2007 Call for Papers Reminder
Date: 2007-03-07 15:15
Tags: tech
Slug: guadec-2007-call-for-papers-reminder

The GUADEC 2007 paper submission deadline is rapidly approaching: five
days left and counting! That's right, if you want to talk at GUADEC this
year in Birmingham, England, then you have until **Monday 12^th^ March**
to submit your proposal.

We need talks about desktop integration, embedded systems, large
deployments, and profiling. We need tutorials on using GNOME from
Python, or Perl, or C\#. Sessions explaining how to use a certain
application, or why a new library being developed is the best thing
since sliced bread. Scheduled hackfests to fix bugs in certain
applications, and discussions to work out complicated problems we have.

Basically, **we need you**. Propose your session for GUADEC now!

<small>NP: <cite>The Man with a Movie Camera</cite>, The Cinematic
Orchestra</small>
