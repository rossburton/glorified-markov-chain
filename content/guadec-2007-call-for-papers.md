Title: GUADEC 2007 Call For Papers
Date: 2007-02-12 18:05
Tags: tech
Slug: guadec-2007-call-for-papers

The GNOME Users and Developers European Conference (GUADEC) invite you
to participate in the 8th annual conference on the 15-21st July 2007 in
Birmingham, England.

The deadline for proposals is Monday 12th March. Successful candidates
will be selected and notified by the GUADEC organising committee.
Unsuccessful candidates will still have an opportunity for their session
to be scheduled during the Approach Weekend or After Hours.

For more information, and to submit a proposal, please go to
<http://www.guadec.org/callforpapers>.
