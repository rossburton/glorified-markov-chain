Title: GUADEC 2007 Pre-Call for Papers
Date: 2007-02-09 14:00
Tags: tech
Slug: guadec-2007-pre-call-for-papers

I plan on announcing the GUADEC 2007 Call for Papers in the next day or
so, so I want everyone to put their thinking caps on and consider giving
a talk this summer. There are several topics I'd like to see a good set
of talks on:

-   **10x10**. How are we doing on the 10x10 plan: this includes
    feedback from [people who have done large
    deployments](http://davelargo.blogspot.com/), to people working on
    [making GNOME work in the
    enterprise](http://primates.ximian.com/~federico/news.html), and
    people [putting GNOME](http://maemo.org/) in [embedded
    devices](http://gpephone.linuxtogo.org/).
-   **Integration**. GNOME needs tighter integration within itself, be
    this [showing presence in relevant
    applications](http://www.galago-project.org/), [integrating
    chat/voip into the desktop](http://telepathy.freedesktop.org/wiki/),
    and tighter integration between applications. Do you have a cool
    application that [makes GNOME easier to
    use](http://code.google.com/p/avant-window-navigator/), or something
    that [integrates applications, documents, and
    people](http://beatnik.infogami.com/Gimmie) into a cohesive whole?
-   **Future**. Lots of new code is being written, do you have anything
    interesting? A replacement for gnome-vfs, progress on Project
    Ridley, a new implementation of something fundamental like the
    panel?

