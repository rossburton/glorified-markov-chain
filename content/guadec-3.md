Title: GUADEC
Date: 2006-06-27 11:30
Tags: tech
Slug: guadec-3

At GUADEC, in the courtyard just outside the main entrance. I'm just out
of my talk which went surprisingly well: I was pretty nervous but I
don't think I stuttered or ummed enough to be annoying. The projector
was a pain in the arse though, Emmanuele's projector in the warm up
weeked worked at 1400x1050, yet this one only only supported 640x480:

![](http://burtonini.com/computing/screenshots/projector.png)

Who needs more than 640x480 when you have 12560Hz!

The presentation being scaled down so much meant a little last minute
hacking to reduce the font sizes, and some images didn't fit at all so
they had to be viewed in Inkscape. Ah well.

For the curious/bored the slides are available online: [Taming The
Beast: Porting Evolution Data Server To
DBus](http://burtonini.com/computing/taming-the-beast.pdf). ~~They are
are sequence of PNGs~~ **Update**: The presentation is a PDF, bested
viewed in Evince in Presentation mode (press F5), produced by the most
excellent [Opt](http://svn.o-hand.com/view/misc/trunk/opt/), so sadly
you don't get to see the gorgeous fades. The [source is also available
online](http://www.burtonini.com/computing/taming-the-beast.tar.gz) for
the cool people running Opt.
