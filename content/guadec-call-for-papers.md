Title: GUADEC Call For Papers
Date: 2007-03-12 10:20
Tags: tech
Slug: guadec-call-for-papers

Today is the GUADEC 2007 Call for Papers deadline, and submissions are
still coming in fast and furious. However a few people who would submit
a paper cannot do so today due to existing commitments, and as I'm an
exceedingly generous individual the deadline is now extended until
**Thursday 15^th^ March**. That's the day after the GNOME 2.18 release,
so if you want to submit a proposal and have a GNOME Release Party then
remember to [submit](http://guadec.org/callforpapers) *then* party.

We also need more **Lightning Talks**. A Lightning Talk is simply a five
minute presentation, so typically in an hour slot there will be ten or
so talks on different topics. They are easy to present (being so short)
and fun to watch (as there is a wide range of topics covered). They are
a great way to announce a new project, demo something very cool (IIRC,
David Reveman got a standing ovation when he demod Compiz in Stuttgart),
or give a progress report. [Submit your lightning talk
today!](http://guadec.org/callforpapers)

<small>NP: <cite>Foley Room</cite>, Amon Tobin</small>
