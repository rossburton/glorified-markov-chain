Title: GUADEC Core Schedule Announced
Date: 2007-04-23 21:30
Tags: tech
Slug: guadec-core-schedule-announced

A preliminary schedule for the [Core Days is now
available](http://www.guadec.org/node/651). People who are scheduled for
the Core days should be receiving an email shortly, and I hope to
release a schedule for the Warm Up and After Hours tomorrow.

Sorry for the delay in producing this, I know for many people they
needed to know as soon as possible. Basically, the submissions this year
was both excellent and numerous, which made selection very hard!

<small>NP: <cite>Listen and Learn</cite>, Hexstatic</small>
