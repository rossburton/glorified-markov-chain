Title: GUADEC Rejections
Date: 2006-04-27 15:04
Tags: tech
Slug: guadec-rejections

[Christopher Aillon
says](http://christopher.aillon.org/blog/dev/gnome/20060427-guadec.html):

> Well, I just got back from the Desktop Linux Summit to find an email
> awaiting me telling me that my proposal for a talk on NetworkManager
> was rejected this year for GUADEC. ... I can see why getting
> contributors into GNOME is rather difficult. Even though I'm going to
> continue working on GNOME because I'm paid to, it's rather
> disheartening to go through this. Looks like it will start to be a
> work-only thing instead of a work-and-free-time thing. To all you who
> have somehow made it into the "in crowd", have fun in Catalonia. I
> probably won't be going.

To be fair there were two Network Manager talks submitted, one by
[Christopher](http://guadec.org/node/293) and another by [Robert
Love](http://guadec.org/node/254). What is worse: telling one of the two
that their talk isn't accepted, or having two talks at GUADEC on the
same subject?

<small>NP: <cite>August and Everything After</cite>, Counting
Crows</small>
