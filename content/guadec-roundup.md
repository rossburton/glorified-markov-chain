Title: GUADEC Roundup
Date: 2007-07-29 17:20
Tags: tech
Slug: guadec-roundup

This is probably the last GUADEC roundup to hit the Planets, considering
that it finished a week ago. But, hey, I've been busy.

GUADEC was, as usual, a blast. Less sun, sea, and beach parties than
last year; more rain, clouds, and parties in fake-Australian bars with
too-loud music. If the weather was better we could have hijacked the
landlocked beach, which would have been fun.

As an educational conference I totally failed at GUADEC, out of an
entire week of talks I managed to attend a grand total of seven talks.
Between the GNOME Mobile meeting sprawling across two afternoons, a few
meetings with clients, occasional attending our stand and having to
leave Thursday afternoon meant that there just wasn't enough time.

As a photographic experience I [mostly
failed](http://flickr.com/photos/rossburton/sets/72157600952460055/). I
took more photos at Matthew's excellent <cite>Clutter Foo</cite> talk
than the rest of GUADEC itself, and more photos at the ping pong
tournament than the others combined. From playing with my camera at the
tournament I now know that the Sigma 17-70 lens is no way up to the
challenge of sports photography: too slow and too short. My next lens
however will be Sigma's new 30mm f/1.4, I've been eyeing it for a while
now and Daf had given in to temptation so I could have a play with it.
Sports photography will have to wait.

As a member of the organising team however, I don't think I totally
failed. There were a few last minute snafus in the schedule but on the
whole it appeared to hold together, the main problem being there being
just too many good talks on at the same time. Congratulations of course
are due to Paul, Thomas, Bastien and Rob, who kept the conference
running on track all week.

Next year, Istanbul. That *will* be interesting.

<small>NP: <cite>F♯ A♯ ∞</cite>, Godspeed You! Black Emperor</small>
