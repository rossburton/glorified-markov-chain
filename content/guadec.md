Title: GUADEC
Date: 2008-07-29 21:40
Tags: tech
Slug: guadec

Hmm, so I never did blog a GUADEC roundup. In two words: it rocked.
Congratulations to Baris and everyone else who organised it!

In other late GUADEC news I finally reviewed the rest of my GUADEC
photos and [uploaded them to
Flickr](http://www.flickr.com/photos/rossburton/sets/72157606166808992/).
I'll try and not take a month to upload next time, honest!
