Title: GUPnP Action
Date: 2008-06-30 14:00
Tags: tech
Slug: gupnp-action

Action around GUPnP has been really hotting up recently. Jorn is back
from ~~the dead~~ studying and demonstrating that he hasn't lost his
touch by refactoring the various audio/visual widgets spread around our
toy projects into
[libowl-av](http://svn.o-hand.com/repos/misc/trunk/libowl-av/), adding
Vala bindings, and then writing a
[MediaRenderer](http://svn.o-hand.com/repos/gupnp/trunk/gupnp-media-renderer/)
implementation on top of that. This means we now have reference
implementations of the full media specification in the form of
gupnp-media-server (server), gupnp-av-cp (control), and
gupnp-media-renderer (playback).

Also Johan Kristell posted to the list for the first time with an
implementation of the [Digital Security Camera
specifications](http://www.upnp.org/standardizeddcps/digitalsecuritycamera.asp),
both server and client. [GUPnP Network
Camera](http://www.kristell.se/network-camera/) currently only supports
still images, but as it is based on GStreamer video can't be far away.
