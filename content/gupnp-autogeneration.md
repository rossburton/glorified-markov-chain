Title: GUPnP Autogeneration
Date: 2008-05-22 16:25
Tags: tech
Slug: gupnp-autogeneration

The problem with GUPnP is that (like DBus) when programming from C you
need to specify the types of each argument when making a method call:

    gupnp_service_proxy_send_action (proxy,
                                       "AddPortMapping", &error,
                                       /* In arguments */
                                       "NewRemoteHost", G_TYPE_STRING, "",
                                       "NewExternalPort", G_TYPE_UINT, external_port,
                                       "NewProtocol", G_TYPE_STRING, "TCP",
                                       "NewInternalPort", G_TYPE_UINT, internal_port,
                                       "NewInternalClient", G_TYPE_STRING, internal_host,
                                       "NewEnabled", G_TYPE_BOOLEAN, TRUE,
                                       "NewPortMappingDescription", G_TYPE_STRING, desc,
                                       "NewLeaseDuration", G_TYPE_UINT, 0,
                                       NULL,
                                       /* Out arguments */
                                       NULL);

Now, that really is quite tiresome. It basically means that you need to
have the service reference to hand when coding, because you need to know
the name and type of each argument. Luckily for DBus part of `dbus-glib`
is a binding tool which can create type-safe wrappers so that making
method calls is much easier. Wouldn't it be nice if there was something
similar for GUPnP, which generated inline functions with prototypes like
this:

    static inline gboolean
    AddPortMapping (GUPnPServiceProxy *proxy,
                    char * in_NewRemoteHost,
                    unsigned int in_NewExternalPort,
                    char * in_NewProtocol,
                    unsigned int in_NewInternalPort,
                    char * in_NewInternalClient,
                    gboolean in_NewEnabled,
                    char * in_NewPortMappingDescription,
                    unsigned int in_NewLeaseDuration,
                    GError **error);

Well, now there is. I've put the [initial code
here](http://burtonini.com/computing/gupnp-bind.py) but will be moving
this into GUPnP itself shortly. The next task is to add asynchronous
wrappers just as in dbus-glib, but that shouldn't be too hard.
