Title: GUPnP Bindings Generation
Date: 2008-05-23 16:40
Tags: tech
Slug: gupnp-bindings-generation

I've now finished the first draft of the bindings generation tool for
GUPnP, which is now part of `libgupnp` itself. I've added both blocking
and non-blocking wrappers, so if you wanted to get the external IP there
is the choice of this for blocking calls:

    char *ip;
    GetExternalIPAddress (proxy, &ip, &error);

Or this for non-blocking calls:

    static void
    external_ip_cb (GUPnPServiceProxy *proxy, char * ip,
                    GError *error, gpointer userdata)
    {
      // ...
    }
    ...
      GetExternalIPAddress_async (proxy, external_ip_cb, NULL);

I've ported my test applications to use the bindings, which are
available in [this Bazaar
repository](http://burtonini.com/bzr/gupnp-demos). It appears to work
quite well, I just need to test it against all of the official service
descriptions and add a few small features.
