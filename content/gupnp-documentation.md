Title: GUPnP Documentation
Date: 2008-06-10 17:15
Tags: tech
Slug: gupnp-documentation

What started off as a quick tutorial to writing a service using GUPnP
turned into a week of reviewing and writing more GUPnP documentation.
It's all landed in our Subversion repository now but if anyone wants to
see how to write a [UPnP
client](http://burtonini.com/computing/gupnp-docs/client-tutorial.html),
[implement the UPnP networked light bulb
service](http://burtonini.com/computing/gupnp-docs/server-tutorial.html),
or just browse the beginnings of the
[glossary](http://burtonini.com/computing/gupnp-docs/glossary.html),
then I have a local copy of the [latest documentation
online](http://burtonini.com/computing/gupnp-docs/).

<small>NP: <cite>Aerial</cite>, 2562</small>
