Title: GUPnP Repositories
Date: 2009-01-13 17:45
Tags: tech
Slug: gupnp-repositories

Zeeshan created a clone of the GUPnP repository at
[Gitorious](http://gitorious.org/projects/gupnp) today, so to any
contributors to GUPnP: feel free to [clone the
repository](http://gitorious.org/projects/gupnp/repos/mainline) there so
that we can all benefit from a distributed version control system being
used as it should be.

<small>NP: <cite>Rendez-Vous (Mexico)</cite>, Erik Truffaz featuring
Murcof</small>
