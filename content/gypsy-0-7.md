Title: Gypsy 0.7
Date: 2009-08-06 15:52
Tags: tech
Slug: gypsy-0-7

Earlier in the week someone pointed out over email that considering the
entire geolocalisation thing is starting to come together, it's not
great that Gypsy (the modern GPS daemon for the modern desktop) appears
dead. Well, it's not quite dead, and to prove it I fixed the bugs that
were stopping me from uploading it into Debian. Specifically, the hard
requirement to run it as `root` and the lack of DBus auto-starting (to
be fair, when it was written this wasn't supported on the system bus).
These are now fixed at last and Gypsy 0.7 is available to download from
[freedesktop.org](http://gypsy.freedesktop.org/).

Packages for Debian are in the upload queue now, and I believe everyones
[favourite frockney](http://hadess.net/) is working on updating Fedora
now.

<small>NP: <cite>Oneric</cite>, Boxcutter</small>
