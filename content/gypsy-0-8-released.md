Title: Gypsy 0.8 Released
Date: 2010-06-09 17:05
Tags: tech
Slug: gypsy-0-8-released

As acting release engineer of the [Gypsy](http://gypsy.freedesktop.org/)
project (a GPS mux, if you didn't know) I'm proud to announce the
release of Gypsy 0.8. So, what's new?

-   Support the Nokia N810 integrated GPS. If someone can verify that
    this works for the N900 too, that would be great.
-   Ability to dump the parsed NMEA to the console for debugging
-   Fixed over-eager old-school Garmin detection
-   Support reading NMEA from named pipes and FIFOs
-   Support seting the baud rate on ghetto GPS devices that don't
    default to a baud rate that actually works (Globalsat ND-100 and
    BlueNext BN-903S, I'm looking at you)
-   Support NMEA &lt; 2.3

Many thanks to Jussi Kukkonen for patch review, and Bastien Nocera for
patch review and new features.

The big question of course is what of the future? So far we've got some
rough ideas. An overhaul of the device interaction layer is definitely
required as actaully getting NMEA is becoming more complex: for
integrated 3G/GPS chips you need to talk to oFono/ModemManager to get a
socket, for some embedded GPS devices you need a proprietary binary that
writes to a pipe, and so on. There are some new features we're
considering too: server-side proximity detection and update rate
limiting.
