Title: Gypsy Hacking
Date: 2007-12-17 10:30
Tags: tech
Slug: gypsy-hacking

Last night I sat down and hacked a bit more on my
[Gypsy](http://folks.o-hand.com/iain/gypsy/) status monitoring tool
thingy. Basically, it shows you all of the information from the GPS, and
is mainly useful for checking that the data being sent by Gypsy makes
sense.

![Gypsy
Status](http://burtonini.com/computing/screenshots/gypsy-status.png)

It still needs a fair amount of work, the layout of the labels on the
left is clearly sub-optimal and I'd like to have a marker on the
satellite image to precisely locate you, but apart from that it's coming
together nicely.

<small>NP: <cite>Storm</cite>, Heather Nova</small>
