Title: Gypsy Packages
Date: 2007-11-12 16:10
Tags: tech
Slug: gypsy-packages

So now that Iain has finally released his [baby to the
world](http://folks.o-hand.com/iain/gypsy/), I should release my Debian
packages too. Now, they work for me, but there is a huge disclaimer
around them. **gypsy-daemon runs as root.** I plan on fixing this
shortly and it should be fairly simple, but I wanted to get these
packages out there for early testers quickly. Brave Debian Sid and
Ubuntu Feisty users can grab them from [my
repository](http://burtonini.com/debian). Gutsy users who moan about me
not building packages and can't rebuild them on their own are not
hardcore enough for this, so don't bother asking.

<small>NP: <cite>Untrue</cite>, Burial</small>
