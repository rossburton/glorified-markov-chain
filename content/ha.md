Title: Ha!
Date: 2004-04-19 00:00
Tags: tech
Slug: ha

[Rosanna writes](http://webwynk.net/zana/changelog.html#1082065213):

> For some strange reason, Epiphany thought my printer was filled with
> A4 paper. I'm not even sure where I would \[find\] A4 paper around
> here.

Ha! Now you know what it's like to live in Europe, where computers and
printers continually revert back to Letter paper, and the sight of
`PC LOAD LETTER` is enough to bring someone's blood to the boil.
Continually seeing Unites States at the top of a list and having to
scroll to the bottom to find United Kingdom. Pressing United Kingdom in
an install routine (\*cough\* Red Hat \*cough\*) and then wondering why
British is *not* the default keyboard layout. Argh!

I'm sorry, Rosanna appears to have hit a nerve...
