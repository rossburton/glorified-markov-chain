Title: Hacking Update
Date: 2003-09-09 00:00
Tags: tech
Slug: hacking-update

I rewrote my `nautilus-cd-burner` patch last night, hopefully Bastien
can help me fix the `growisofs` code which I deleted, and Alex won't
find too many problems with it. Once this is in I can work on the UI for
Rhythmbox CD writing.

My patches to Sound Juicer to make the metadata lookup async are looking
good too, a small cleanup and they're done. I owe Bastien a beer for
making Sound Juicer his pet project for 10 minute hacks, and fixing a
serious number of bugs for me. Great work that man.

[Mark Finlay](http://sisob.tuxfamily.org/) has been talking about
scanning software for GNOME. I'm interested in coding some of this up,
as I have a scanner under my desk which only needs a suitable cable (I
really must scrape together the money for this). My £0.02 on this topic:

-   Abstract scan types make perfect sense for the basic UI, such as
    "Web Quality", "Printable" etc.
-   Then have an Advanced button (well, disclosure widget), with
    resolution, brightness, contrast, etc sliders.
-   I can't decide where rotate/mirror etc should go though. Probably in
    the main UI.
-   I was also thinking of eye candy when selecting regions: fade
    (desaturate and lighten) any areas which are not going to
    be scanned.

