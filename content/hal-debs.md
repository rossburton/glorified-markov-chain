Title: HAL .debs
Date: 2004-01-26 00:00
Tags: tech
Slug: hal-debs

In fit of red-hot packaging action I produced some HAL debs. Yes, I know
someone else has already filed an ITP. But they didn't reply to my mail
within 12 hours, so I got bored and created some myself...

Get them at the [usual place](http://www.burtonini.com/debian/) in the
`experimental` distribution. You'll need `udev` 0.014 from Marco d'Itri
of course.
