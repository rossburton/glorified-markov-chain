Title: Harsh Trolling
Date: 2008-01-14 17:30
Tags: tech
Slug: harsh-trolling

This has to be some of the [harshest
trolling](http://groups.google.com/group/mozilla.dev.apps.firefox/browse_thread/thread/bca2c92496161015%20)
I've seen for a long time. If it's not a troll, then someone needs anger
management sessions.

<small>NP: <cite>Central Reservation</cite>, Beth Orton</small>
