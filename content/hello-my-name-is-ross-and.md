Title: Hello, my name is Ross and...
Date: 2002-10-10 18:41
Tags: life
Slug: hello-my-name-is-ross-and

I have a Kylie addiction. After falling in love with Florence (from The
Magic Roundabout) due to her voice being Kylie's, I decided to see help.
I can't afford a therapist, so I [resorted to
Emacs](/kylie-therapist.txt).
