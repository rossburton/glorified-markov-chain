Title: Help Interweb!
Date: 2007-02-28 11:50
Tags: tech
Slug: help-interweb

I've upgraded my Ubuntu laptop to Feisty, and now when I press
Control-Left or Control-Right instead of moving to the next/previous
word (as it did in Edgy and as far as I recall Sarge too) I just get
`;5D` and `;5C`. Does anyone know how to fix that?

<small>NP: <cite>Storn</cite>, Heather Nova</small>
