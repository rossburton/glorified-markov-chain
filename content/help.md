Title: Help!
Date: 2006-02-21 12:41
Tags: tech
Slug: help

I have a ThinkPad T40p, with an ATI Radeon inside. It has got a nice
shiny TV-out plug on the side, and I have a short cable that ends in a
composite video plug. My TV has a composite video socket on the front.

When I press Fn-F7 (switch display), nothing appears on the TV. I'm
expecting lots of pain to get this working: so far I am running `fglrx`
and have the following line in `xorg.conf`:

    Section "Device"
            Identifier  "ATI Graphics Adapter 0"
            Driver      "fglrx"
            Option "VideoOverlay" "on"
            Option "OpenGLOverlay" "off"
            Option "DesktopSetup" "clone"
    EndSection

Anyone know the magic to make this actually work?

<small>NP: <cite>Dreaming Wide Awake</cite>, Lizz Wright</small>
