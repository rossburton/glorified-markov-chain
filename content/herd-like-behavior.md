Title: Herd Like Behavior
Date: 2006-03-15 11:40
Tags: life
Slug: herd-like-behavior

Last week on the magnificent (for people with puppies at least)
<cite>It's Me Or The Dog</cite> they featured a battery-powered
bacon-flavoured bubble machine. Wow! we thought, and ordered one over
the weekend.

And so, it appears, did the rest of the nation:

> Dear Customer  
> Thank you for ordering Fetch Pet Bubble products from Seapets. Due to
> the unprecedented demand following the Channel Four television
> program, all stocks of these products, that were in this country were
> gone within hours of the broadcast.
>
> Fortunately we work very closely with Fetch Pet and have been assured
> that we will be the first to receive supplies as the next batch is
> flown in from the USA. We expect this to clear customs within a few
> days and we should be able to resume deliveries next week.

Poor Henry has to wait for bacon flavoured bubble goodness. In other
news my 28-135mm IS USM lens arrived today, and it's lovely. Shots
online later.

<small>NP: <cite>Out Of Season</cite>, Beth Gibbons and Rustin
Man</small>
