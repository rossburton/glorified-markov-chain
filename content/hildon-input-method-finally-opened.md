Title: Hildon Input Method Finally Opened
Date: 2007-09-07 14:30
Tags: tech
Slug: hildon-input-method-finally-opened

I knew having the GNOME wiki Recent Changes feed in Google Reader was a
good idea... I just noticed a number of pages being created under the
[`HildonInputMethod`](http://live.gnome.org/Hildon/HildonInputMethod)
tree. Hooray, the Hildon Input Method is finally open source!

<small>NP: <cite>We Made It For You</cite>, The Boats</small>
