Title: Hitch Hiker's Cast
Date: 2004-01-30 12:41
Tags: life
Slug: hitch-hikers-cast

Disney have [annouced who will be
in](http://www.hollywoodreporter.com/thr/film/article_display.jsp?vnu_content_id=2079307)
the upcoming Hitch Hikers Guide to the Galaxy movie. Martin Freeman is
Arthur (as we already knew) and I think he'll do well. Mos Def is an
amusing choice for Ford, but I've not seen a film he has been in so I
can't really comment. The oddly spelt [Zooey
Deschanel](http://www.imdb.com/name/nm0221046/) is Tricia/Trillian.

I notice there is no mention of who is going to play Zaphod. I'm
guessing it's safe to assume he will be CGI...
