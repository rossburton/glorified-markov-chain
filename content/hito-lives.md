Title: Hito Lives!
Date: 2007-06-18 19:50
Tags: tech
Slug: hito-lives

Hito is alive!

![Hito](http://burtonini.com/computing/screenshots/hito-2.png)

I admit that the contact preview needs some work, there is some tasty
code in Contacts I need to refactor.

So, what is happening here? The tree view is a `HitoContactView`, which
subclasses `GtkTreeView`. This displays the contents of a
`HitoContactStore`, a subclass of `GtkListStore` which holds the
`EContacts` from an `EBookView`. In between the store and the view is a
`HitoContactFilterModel`, which can filter both on groups (currently the
most useful implementation is `HitoCategoryGroup`, which groups on the
`CATEGORY` vCard field) and text, which does decomposed matches against
the contact's name.

The combo box is a `HitoGroupCombo`, which controls the group being
filtered. The entry is the only widget which isn't a subclass (yet), and
controls the text being filtered. The final bit of magic is
`HitoGroupStore`, which collects categories from contacts and creates
`HitoCategoryGroups` as required (this is used as the model for the
combo).

Now for the hard work of writing a usable preview widget, and the
nightmare of a decent contact editor...

<small>NP: <cite>Live At the Union Chapel</cite>, Damien Rice</small>
