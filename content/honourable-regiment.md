Title: Honourable Regiment?
Date: 2003-10-13 00:00
Tags: life
Slug: honourable-regiment

So a friend of mine wants to join the Territorial Army, specifically the
[Honourable Artillery Company](http://www.army.mod.uk/hac/index.html).
This makes me wonder, is there an Unhonourable Artillery Company? Are
they the people who shell hospitals and refugee camps?
