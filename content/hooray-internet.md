Title: Hooray Internet!
Date: 2008-03-11 17:30
Tags: life
Slug: hooray-internet

About an hour ago my ADSL router started flashing again, so I'm finally
back on the Internet. Whilst I could read some mail and keep up with
office gossip by using my phone as a modem, it wasn't exactly great
thanks to the lack of 3G coverage out here in the fens.

If you were wondering why I haven't replied to your mails, reviewed your
patches, or if I've been ignoring you, then I'm back! I can't promise
that I won't continue ignoring you though...
