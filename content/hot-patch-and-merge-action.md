Title: Hot Patch And Merge Action
Date: 2006-07-24 11:30
Tags: tech
Slug: hot-patch-and-merge-action

For the last few weeks I've been trying to get as many of the non-DBus
specific changes to EDS we've made in the eds-dbus port, to ease
re-basing the port against EDS 1.8 for eventual upstreaming. Many thanks
to the Evolution maintainers for reviewing so many patches so quickly, I
just hope I broke nothing in the process!

-   Added API: `e_book_query_vcard_field_exists`,
    `e_vcard_get_attribute`, `e_vcard_get_param`,
    `e_vcard_attribute_remove_value`,
    `e_vcard_attribute_remove_param_value`,
    `e_data_book_view_notify_update_prefiltered_vcard`
-   Extended `EContactPhoto` structure to support remote photos
-   Changed name parsing tables to avoid relocations
-   Fix "all contacts" optimisations so it actually happens
-   Increased performance and reduced memory usage of the default
    addressbook backend

Out of the thirty commits to the addressbook since since April, half
were merged from the DBus port!

<small>NP: <cite>Feedback</cite>, Jurassic 5</small>
