Title: Howl?  Avahi!
Date: 2005-08-27 10:03
Tags: tech
Slug: howl-avahi

Planet Debian appears to have caught the mDNS bug, and as expected
everyone is mumbling that Howl is non-free, has bugs, and generally
sucks. Obviously not enough people noticed the [recent ITP I
filed](http://bugs.debian.org/324990) for
[Avahi](http://www.freedesktop.org/Software/Avahi), a LGPL mDNS server
stack and client library. Packages are making their way into Ubuntu
Breezy shortly and then the we're going to try and get it into Debian
Experimental (it depends on `python2.4-gtk2` and DBus 0.3, neither of
which are in Sid).
