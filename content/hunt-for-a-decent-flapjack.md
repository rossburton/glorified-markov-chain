Title: Hunt For A Decent Flapjack
Date: 2004-06-23 00:00
Tags: life
Slug: hunt-for-a-decent-flapjack

Inspired by my [recent
success](http://www.burtonini.com/blog/life/good-coffee-200400611) with
coffee and flapjacks I thought I'd grab a quick bite on the way to work
at London Bridge. AMT have a posh looking coffee stand over platform 2,
all polished brass and dark wood, which should be rained upon by
chemical hellfire.

The woman serving me nearly gave me a decaf espresso. Yes, a *decaf*
espresso. Obviously she should be taken outside and shot for that alone,
but it actually gets worse. They put foamed milk on my espresso without
asking me, which turned a lovely strong coffee shot into a strange
small-cappuccino-with-a-kick. I'm not entirely sure why anyone would
want to put milk (frothed or not) onto an espresso in the first place,
and I suppose it is good that the option is there (for the same people
who ask for a diet Coke with a Big Mac, I presume), but I asked for an
espresso. I think they'll find most people consider an espresso to be
sans bovine juice. Finally, the flapjack looked like oats which had been
left to soak in left-over cooking oil and then chopped into squares,
which isn't a great surprise considering the usual ingredient of butter
(or margarine if the manufacturer is being cheap) was replaced by
vegetable oil and "hydrogenated" vegetable oil (whatever that is). End
result: tastes like the cheap syrup you accidently make when dissolving
sugar cooks too much, without a hint of oat. I now feel slightly queasy,
like I'd eaten a small stick of lard.
