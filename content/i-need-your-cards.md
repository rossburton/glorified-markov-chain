Title: I Need Your Cards!
Date: 2005-10-14 16:20
Tags: tech
Slug: i-need-your-cards

To cut a long story short I've rewritten the vCard parser in Evolution
but need to make sure it actually works correctly. What I'd really
appreciate is for everyone out there to send me some vCards so I can
build a vCard parser regression test for Evolution.

Obviously make a copy of any contacts you'll send and replace any
sensitive information (change all phone numbers to 1234, all addresses
to Foo Bar Street, etc). As this has to test as many corner cases as
possible vCards with entirely ASCII values produced by Evolution are
pretty boring. Instead I'd like:

-   Cards using non-ASCII locales. Finnish, Russian, Hindi, Korean, etc
    are all welcomed.
-   Cards produced by programs other than Evolution. Even if you don't
    actually use an Apple, importing a few contacts from Evolutoin into
    the OS X address book and then exporting them again would be good.
-   Bizarre fields actually being used. I'd like to see real-world cards
    using the linked (instead of embedded) photos, complicated use of
    the TYPE fields with custom entries, etc. Note that cards from
    Evolution don't count here, I can produce those easily.

Thanks! Email any vCards you have to [me](mailto:ross@burtonini.com),
although you must agree that they'll hopefully end up being distributed
with `evolution-data-server` under the LGPL.

<small>NP: <cite>Dreamland</cite>, Madeleine Peyroux</small>
