Title: I Waited
Date: 2006-08-30 09:45
Tags: life
Slug: i-waited

I waited. I prayed. I begged. I cried. [Eventually, they
listened](http://www.threadless.com/product/383/The_Communist_Party?streetteam=rossyb).

<small>NP: <cite>Under The Pink</cite>, Tori Amos</small>
