Title: I Worry
Date: 2007-11-07 17:00
Tags: life
Slug: i-worry

> “I phoned Camelot and they fobbed me off with some story that -6 is
> higher — not lower — than -8 but I'm not having it.”

As much as I admire your determination Tina, I really do think that you
should give in on this (from [Manchester Evening
News](http://www.manchestereveningnews.co.uk/news/s/1022757_cool_cash_card_confusion)
via [Good Math, Bad Math](http://scienceblogs.com/goodmath/)).

<small>NP: <cite>Groove Salad</cite>, Soma.fm</small>
