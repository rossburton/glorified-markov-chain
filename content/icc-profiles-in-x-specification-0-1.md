Title: ICC Profiles In X Specification 0.1
Date: 2005-06-24 10:21
Tags: tech
Slug: icc-profiles-in-x-specification-0-1

After an interesting discussion on `openicc-list`, I am releasing
version 0.1 of the ICC Profiles In X specification. I'm currently
re-arranging my Bazaar archives so it's not available in source form
yet, but the [specification in available as
HTML](http://www.burtonini.com/computing/x-icc-profiles-spec-0.1.html).

So far acceptance has been remarkable. I've patched Eye Of Gnome, Sven
committed support to GIMP, Krita (a KDE drawing app) might support it
soon, and there is an open bug with Scribus. Finger's crossed for world
domination!

<small>NP: <cite>Out From Out Where</cite>, Amon Tobin</small>
