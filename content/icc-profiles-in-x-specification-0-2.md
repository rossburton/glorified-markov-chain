Title: ICC Profiles In X Specification 0.2
Date: 2007-02-21 10:15
Tags: tech
Slug: icc-profiles-in-x-specification-0-2

About 18 months after the 0.1 release of this specification comes 0.2.
This is a very simple update and now specifies how to handle
Xinerama-style setups where a single root window has multiple physical
screens (thanks to Kai-Uwe Behrmann).

ICC Profiles In X, version 0.2, can be [downloaded
here](http://burtonini.com/computing/x-icc-profiles-spec-0.2.html).

<small>NP: <cite>Takk</cite>, Sigur Rós</small>
