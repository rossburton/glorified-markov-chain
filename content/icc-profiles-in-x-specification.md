Title: ICC Profiles In X Specification
Date: 2007-11-29 00:46
Tags: tech
Slug: icc-profiles-in-x-specification

This specification defines a property in X which contains the ICC
profile for the screen. Using this property, applications can
colour-correct images for display. So far Eye of Gnome, GIMP, Krita,
UFRaw and Inkscape support the property.

The latest version of the specification is 0.2.

-   [ICC Profiles In X version
    0.2](http://www.burtonini.com/computing/x-icc-profiles-spec-0.2.html)

