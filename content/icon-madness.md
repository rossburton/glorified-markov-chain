Title: Icon Madness
Date: 2007-09-24 20:40
Tags: tech
Slug: icon-madness

Last week I had to use Vicky's laptop briefly, which sadly runs Windows
XP. I was greeted with this monstrosity:

![status bar](http://burtonini.com/computing/screenshots/oh-my-god.png)

The baby Jesus is beyond crying. For the curious, from left to right
there is battery charge, wireless network status, InstallShield Update
Manager, Java Updates, Windows Updates, Lastfm Scrobbler, Lenovo Client
Security Password Manager, Bluetooth network connection (never used),
Wired LAN network connection (never used), volume, ThinkVantage Access
Connections wireless status, IBM Message Centre, Epson printer status,
ThinkVantage Access Connections, Adobe Photo Downloader, GoogleTalk,
Bluetooth status, QuickTime, Windows Security Alerts. After 30 minutes
of removing software and disabling some more, I finally got this down to
seven icons. I wish there was a way to disable Windows from showing the
status of every network connection though.

<small>NP: <cite>Sketches of Spain</cite>, Miles Davis</small>
