Title: Icon Request...
Date: 2007-02-20 18:30
Tags: tech
Slug: icon-request

I'm just finishing off a 0.1 release of <cite>Tasks</cite>, a minimal To
Do application for GNOME (yes, it's another EDS frontend). However, I
don't have a rocking icon and can't seem to find one in the GNOME or
Tango icon theme. Does anyone know of a cool icon I could use, or does
anyone out there fancy flexing their drawing muscles and knocking up a
quick icon?
