Title: Idiotic British Press
Date: 2007-08-05 19:10
Tags: life
Slug: idiotic-british-press

Daniel pointed me towards [this blog](http://5cc.blogspot.com/) today, a
great mix of insightful thought and humour to generally ridicule the
British right-wing tabloids. [This
article](http://5cc.blogspot.com/2007/07/is-it-racist-were-there.html)
for example is fantastic.
