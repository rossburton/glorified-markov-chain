Title: I'm Doomed
Date: 2007-05-18 19:30
Tags: tech
Slug: im-doomed

[Srini mailed the
list](http://mail.gnome.org/archives/evolution-hackers/2007-May/msg00022.html)
so it's official now: I've just joined the Evolution team as
<cite>Addressbook Maintainer</cite>. I've a terrible feeling this will
be the end of me, but I'm sure the journey will be fun! Thanks to Srini
for asking me to join the team, it's a great pleasure to be invited to
maintain such a high-profile component as Evolution. I've not really
touched the code of Evolution itself, focusing more on Evolution Data
Server, but that is good because Srini has been focusing more on
Evolution so together we should make a good team.

My primary goal is to merge and polish the DBus port of EDS. The current
plan is to attempt to land the port in time for GNOME 2.20, although
disabled by default. Hopefully enough developers will be ~~foolish~~
brave enough to enable it and report any bugs they find, so that we
might even be able to enable it by default in GNOME 2.20. This is a
tight plan but it might just be achievable.

<small>NP: <cite>Music City Soul</cite>, Beverley Knight</small>
