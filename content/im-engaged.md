Title: I'm Engaged!
Date: 2003-04-23 00:00
Tags: life
Slug: im-engaged

I did one more thing in Rome... I asked Vicky if she would marry me.
Thankfully, she said yes!

This, obviously, led me to becoming The Happiest Man In England. If you
see someone rushing through London with a ThinkGeek Caffinne t-shirt and
a huge grin, it's probably me!

At the moment we are looking at a summer wedding in August 2004. A jazz
band, fairy lights, barbecue in the evening, honeymoon to Italy and
other fabulous ideas are planned.
