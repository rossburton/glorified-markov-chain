Title: In Print
Date: 2006-08-14 20:36
Tags: tech
Slug: in-print

Those nice people at [Linux Magazine](http://www.linux-magazine.com/)
have written [an article covering Devil's
Pie](http://www.linux-magazine.com/issue/70/Brightside_Devils_Pie.pdf).
Apart from the traditional mis-spelling of my surname, it's a great
article. Thanks Linux Magazine!
