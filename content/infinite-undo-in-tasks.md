Title: Infinite Undo in Tasks
Date: 2007-09-18 11:14
Tags: tasks, tech
Slug: infinite-undo-in-tasks

Yesterday I landed in Subversion a branch of
[Tasks](http://pimlico-project.org/tasks.html) I've been working on to
remove all confirmation dialogs and replace them with infinite
undo/redo. I'm really pleased with the end result, there are no more
dialogs getting in the way and every action is undoable. To implement
this I adapted `MarlinUndoManager` from
[Marlin](http://live.gnome.org/Marlin) by the most excellent Iain
Holmes.

The basic design is that the application has a global <cite>undo
context</cite>. When an operation which should be undoable occurs, you
<cite>start</cite> the context, add as many <cite>undoables</cite> as
required, and then <cite>end</cite> the context. Allowing a single
undoable action to consist of multiple undoables lets the application
reuse fine-grained logic to build coarse user-level actions (in Tasks
this is used to build the Remove Completed action from multiple Delete
Task actions). To make integrating this into applications even easier, I
wrote a `GtkAction` which reflects the state of an undo context, so it
is trivial to add redo/undo to the interface.

![](http://burtonini.com/computing/screenshots/tasks-undo.png)

There is one remaining task left before this is ready to be released:
undo support in the edit dialog entries. Once this is done, the next
release of Tasks will be announced. I also plan to work with Iain on
cleaning up this code and submitting it for inclusion into GTK+.

<small>NP: <cite>Money Jungle</cite>, Duke Ellington, Charles Mingus,
Max Roach</small>
