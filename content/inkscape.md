Title: Inkscape
Date: 2005-07-27 22:19
Tags: tech
Slug: inkscape

Last year when we were making our wedding invitations Inkscape was only
on it's second release and was quite buggy. It got to the point where we
were designing the invites in Inkscape and then setting the font and
printing from Sodipodi, as Inkscape refused to load Type 1 fonts and
print images correctly. However, what a difference a year makes!
Inkscape 0.42 was just released, and is starting to look [very
impressive](http://inkscape.org/screenshots/gallery/inkscape-0.42-CVS-flowtext.png).
Good work guys!

<small>NP: <cite>X&Y</cite>, Coldplay</small>
