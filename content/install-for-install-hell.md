Title: Install For Install HELL
Date: 2004-02-27 20:12
Tags: tech
Slug: install-for-install-hell

Argh GSM is driving up the wall. Jesus, ISO7816 is *appallingly* bad.

Glad to see there is [finally
motion](http://www.newsforge.com/programming/04/02/26/2253251.shtml)
around the subject of open-sourcing Java. It will be very interesting to
see how this goes, what is going to be open-sourced, etc. A common set
of the core Java library would be great news, as there are numerous
silly but very annoying bugs in Sun's libraries.

Work on PyPNG has started up again, I've finally made PNG chunks
individual classes, so manipulating the files is a lot easier. Hopefully
I'll clean this up soon and make a usable tarball.

Related work on my RDF-based web gallery is... slow. Twisted is very
nice, but the documentation is not great so I'm having to work from
other examples. However, I do finally have a working image gallery,
which displays captions and thumbnails based on embedded data inside PNG
files (captions from RDF, thumbnails are generated from another script).
This seems to be working nicely, but it's damn slow -- I need to cache
the data I guess.

NP: Parachutes, Coldplay
