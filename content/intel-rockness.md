Title: Intel Rockness
Date: 2006-08-14 12:20
Tags: tech
Slug: intel-rockness

Whilst trying to get the `modesettings` branch of the Intel X driver
working, I noticed this in the LCD probe:

    II) I810(0): redX: 0.569 redY: 0.342   greenX: 0.312 greenY: 0.544
    (II) I810(0): blueX: 0.149 blueY: 0.132   whiteX: 0.313 whiteY: 0.329

IBM (this X60 is still branded IBM, not Lenovo) deserve some serious
props for actually setting all of the data provided by the EDID spec. I
best I should hack up a tool to generate ICC profiles from the primaries
specified at startup.
