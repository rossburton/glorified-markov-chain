Title: Interesting News
Date: 2004-02-11 00:00
Tags: tech
Slug: interesting-news

Alert people would have seen these already, but just in case:

-   [The NHS rolls out a trial of
    JDS](http://www.silicon.com/hardware/desktops/0,39024645,39117233,00.htm)
-   "Windows is struck my worst ever bug". eEye discovered bugs
    ([AD20040210](http://www.eeye.com/html/Research/Advisories/AD20040210.html)
    and
    [AD20040210-2](http://www.eeye.com/html/Research/Advisories/AD20040210-2.html))
    in Microsoft's ASN.1 code *in July*, patches finally released. I'd
    expect the exploits to appear next week.
-   And finally, [Star Wars 4-6 are coming out on
    DVD!](http://www.dvdfile.com/software/dvd-video/archive/2004/02_10.html)

