Title: Introduction to D-BUS
Date: 2004-10-19 00:00
Tags: tech
Slug: introduction-to-d-bus

Quick post: I was updating my CV and noticed that I neglected to blog
about my D-BUS article, [<cite>Connect Desktop Applications Using
D-BUS</cite>](http://www-106.ibm.com/developerworks/linux/library/l-dbus.html),
being published on IBM developerWorks. It was posted whilst I was on my
honeymoon, which is probably why I never got around to write about it.
Hopefully the code examples still compile...
