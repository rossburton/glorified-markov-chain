Title: iPhone Connection Status
Date: 2011-07-06 09:51
Tags: tech
Slug: iphone-connection-status

Just a little Python hack...

    $ ./iphonemon.py 
    Found Ross Burton’s iPhone
    3 3G
    1 3_75G
    2 3_75G
    3 3_75G
    2 3_75G
    3 3_75G
    2 3G

Next step: a visual interface.
