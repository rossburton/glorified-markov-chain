Title: iPhoneApMon in Shell
Date: 2011-11-17 18:24
Tags: tech
Slug: iphoneapmon-in-shell

This really isn't how it should look, but it's a 15 minute hack in
Javascript to give me *something* that doesn't involve a terminal.

![](http://burtonini.com/computing/screenshots/iphoneapmon.png)

One day I'll find the time to integrate this properly into the network
menu...
