Title: Is It Crack Or Not?
Date: 2005-06-14 17:13
Tags: tech
Slug: is-it-crack-or-not-2

This is the first installment of a semi-regular (read: as regularly as I
can be bothered) feature, Is It Crack Or Not? The idea is that there are
often ideas in the GNOME community which may or may not be crack, so
here the people can share their views. So without further ado, let's get
on.

The proposal is that the screen shot button in GNOME should be extended
to record video as well as take static screen shots. Jeroen among others
has done a [mockup of the
interface](http://www.xs4all.nl/~jeroen/screenshots/Screenshot-Screenshot-2.png)
for this feature. There are lots of open questions, such as will it
record audio, how does one stop the recording without there being a Stop
button in the interface, what codec to use, and does a normal user know
the difference between Ogg Theora and MPEG 4? The important question
however is **is it crack or not?** Leave your opinion in the comments!
