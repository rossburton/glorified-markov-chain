Title: Is It Crack Or Not?
Date: 2005-09-16 12:33
Tags: tech
Slug: is-it-crack-or-not

Now for the second <cite>Is It Crack Or Not?</cite> competition. This
one is short and sweet, so everyone can play.

Sound Juicer has a genre list containing the most popular genres, such
as Rock, Soul, Jazz, and Spoken Word. The question is: should Soundtrack
be considered a genre?

My take is that it shouldn't, as Soundtrack means "music from a film",
which must have a specific genre. For example, the soundtrack for
<cite>The Commitments</cite> is Soul, and the soundtrack to <cite>Star
Wars</cite> is ~~Classical~~ Orchestral.

And yes [Shaum](http://blogs.gnome.org/portal/shaunm), I'll add Funk
once I've branched, honest. I noticed that you entered it into the
documentation screenshots to spite me. `:)`
