Title: Is that...
Date: 2004-08-24 00:00
Tags: life
Slug: is-that

...a phone ringing in my pocket, or just [Phantom Cell Phone
Vibrations](http://www.actsofvolition.com/archives/2004/july/phantomcell).
Thank you for noticing this [Scott](http://www.netsplit.com/blog), I
feel a little bit more normal now.

<small> NP: <cite>Ray of Light</cite>, Madonna. Probably her only album
I'd admit to owning. </small>

PS: Yes, I realise this is a totally lame way of getting back into
blogging (release announcements don't count), but I've been a little
busy. Soon, I promise.
