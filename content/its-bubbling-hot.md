Title: It's Bubbling Hot
Date: 2008-04-24 17:47
Tags: tech
Slug: its-bubbling-hot

    $ cat /proc/acpi/thermal_zone/*/temperature
    temperature:             84 C
    temperature:             90 C

Maybe it's time to get a dedicated build machine, my poor laptop gets
quite toasty when building Poky. Then again it seems happy enough, so
maybe I should just use an external keyboard to avoid boiling my hands.

<small>NP: <cite>Oneric</cite>, Boxcutter</small>
