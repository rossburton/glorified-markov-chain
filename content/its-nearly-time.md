Title: It's nearly time...
Date: 2003-06-13 00:00
Tags: life
Slug: its-nearly-time

...for [GU4DEC](http://www.guadec.org)! People have started arriving in
Dublin already, and I want to be there now! Sadly, I'm turning up first
thing Monday morning, so it looks like I'll miss the majority of the
partying.

Dublin being Dublin, I best get out my wooly jumpers, warm coats,
waterproof jackets, trekking boots...
