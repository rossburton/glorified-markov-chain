Title: It's Nice When A Plan Comes Together
Date: 2005-05-16 16:55
Tags: tech
Slug: its-nice-when-a-plan-comes-together

It's really nice when, after a week or so of poking, prodding, and
getting confused, that the right [2-line
change](http://lists.ximian.com/archives/public/evolution-patches/2005-May/010788.html)
can have a large effect.

Before:  
![Massif
chart](http://www.burtonini.com/computing/screenshots/eds-before.png)  
Note how `evolution-data-server` has leaked 800K in 25 seconds whilst
running a test application.

After:  
![Massif
chart](http://www.burtonini.com/computing/screenshots/eds-after.png)  
*Much* better. As I said, it's nice when a plan comes together.

<small>NP: <cite>Groove Salad</cite>, Soma FM</small>
