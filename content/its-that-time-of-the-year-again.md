Title: It's That Time Of The Year Again
Date: 2007-12-21 16:50
Tags: life
Slug: its-that-time-of-the-year-again

It starts off just as you'd expect.

> “Yes, it’s that time of year again. No sooner does an important
> traditional religious holiday roll around than the PC-brigade feel the
> need to strip-mine it of its original significance, just so’s no-one’s
> feeling get upset.”

However, it doesn't quite continue as you were thinking.

> “It is the Christians who have the most gall of all, daring to attach
> the name of some first-century Palestinian to a once-proud British
> festival. ‘Yule’ I can live with, despite its being a continental
> bastardisation of our British pronunciation ‘Geola’, but ‘Christmas’
> is just wrong. You even have to mispronounce ‘Christ’ to say it.”

An excellent piece of satire from Nathaniel Tapley on [Liberal
Conspiracy](http://www.liberalconspiracy.org/2007/12/14/its-that-time-of-year-again/),
which is well worth a read for a good laugh.

<small>NP: <cite>Giant Steps</cite>, John Coltrane</small>
