Title: I've Finally Had Enough
Date: 2007-08-02 10:25
Tags: tech
Slug: ive-finally-had-enough

    $ mail -s unsubscribe debian-devel-REQUEST@lists.debian.org
    .

The continual bickering and resistance to any change is driving me mad.
I'm switching to reviewing the threads in gmane once a week from now on.

<small>NP: <cite>Pocket Symphony</cite>, Air</small>
