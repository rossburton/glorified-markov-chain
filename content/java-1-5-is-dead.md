Title: Java 1.5 Is Dead...
Date: 2004-06-28 00:00
Tags: tech
Slug: java-1-5-is-dead

...long live Java 5.0.

Why can't people who market software count? Windows 1, 2, 3, 3.1, 3.11,
95, 98, 2000, XP. Java 1.0, 1.1, 1.2 aka 2.0, 1.4 aka 2.0, 1.5 aka 5.0.
It appears that the defining attribute of open source software is a
predictable versioning scheme (even if it is a little odd at first, as
in Havoc's experiment with the Fibonacci series as version numbers for
Metacity).

<small>NP: Simple Things, Zero 7</small>
