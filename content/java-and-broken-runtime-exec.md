Title: Java and broken Runtime.exec()
Date: 2003-05-28 00:00
Tags: tech
Slug: java-and-broken-runtime-exec

Oooh, Sun have pissed me off.

`java.lang.Process.getInputStream()` returns a `InputStream`. Internally
on Linux this is really a `FileInputStream`, as they are file
descriptors. So far so good -- I'd expect Sun to return the
`FileInputStream` and if we want it buffered, we can wrap it in a
`BufferedInputStream` just like the rest of the JDK. Return a simple
object and allow flexibility by letting the users layer on top of it.

But nooo, that's sensible. Sun decided to return a
`BufferedInputStream`. Which breaks when I want to send a single byte to
the process, and get a single byte back. Bastards. This is incident ID
186623 with Sun, and I want to see progress dammit!

Note that [Kaffe](http://www.kaffe.org) does The Right Thing and returns
a `FileInputStream`...
