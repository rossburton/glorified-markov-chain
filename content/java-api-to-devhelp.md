Title: Java API to DevHelp
Date: 2002-01-04 00:00
Tags: tech
Slug: java-api-to-devhelp

I finally got annoyed with having to use Galeon as a API viewer, when
more suited tools such as the wonderful DevHelp exist.

30 minutes of Perl hacking on a train and I had a script which will run
over a Java2 API documentation tree and build a .devhelp file, with an
entry in the table of contents for every package, and every method and
class in the index. DevHelp now takes about 20 seconds to start (the
index is 4 megs) but looking for a certain API is just so much easier.

The script is available from
[here](/computing/java-api-to-devhelp.pl.txt). If it works (or not) for
you, please mail me.
