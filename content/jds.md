Title: JDS
Date: 2003-12-08 00:00
Tags: tech
Slug: jds

So Sun's GNOME-based JDS appears to be kicking some serious butt. After
the Israeli and Chinese government, [our own NHS is trialling it
too](http://observer.guardian.co.uk/business/story/0,6903,1101344,00.html).

<small> PS: Damn it is cold today. I hate it when it's cold enough that
my headphone cables refuse to flex and thus keep on falling out of my
ears. Dammit. </small>

<small> NP: Macy Gray, <cite>Relating to a Psychopath</cite>, live at
the Old Vic Theatre, London. Maybe I like Macy Gray after all. </small>
