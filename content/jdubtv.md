Title: jdub!TV
Date: 2005-03-02 13:35
Tags: life
Slug: jdubtv

Jeff announced that jdub!TV was back up again, so off I dutifully went
to watch Our Glorious Release Manager at work. Of course, this meant
pausing my musics so I could hear Jeff.

> &lt;ross&gt; what a choice, do i listen to jdub occasionally talk, or
> put Fear Of Fours back on  
> &lt;jdub&gt; ross: rock!  
> &lt;jdub&gt; ross: i will put it on for yoU!

Problem solved! Of course, I did spend the next minute or so getting the
jdub!TV and my hifi in sync...

<small>NP: <cite>Fear Of Fours</cite>, Lamb (twice)</small>
