Title: JhAutoBuild
Date: 2006-05-09 21:55
Tags: tech
Slug: jhautobuild

Thanks to the excellent [JhAutoBuild](http://jhbuild.bxlug.be/), I
discovered a problem in my recent Sound Juicer refactoring that even a
`make distcheck` didn't detect, causing a build failure. It's now
confirmed to be fixed, as another machine has [just done a clean
build](http://jhbuild.bxlug.be/builds/2006-05-09-0005/logs/sound-juicer/).
Sweet.

As thanks I'm contributing possibly the slowest build machine in the
world: my LinkStation NAS box. It's a PowerPC running at about 100MHz,
with 64MB of RAM. I hope I've got enough of the dependencies installed,
you can follow it's rather slow progress
[here](http://jhbuild.bxlug.be/builds/2006-05-09-0008/tinderbox). 25
minutes in and it's still building the first module, `libxml2`. Go
Melchett go!

<small>NP: <cite>Protection</cite>, Massive Attack</small>
