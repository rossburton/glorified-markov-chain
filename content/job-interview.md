Title: Job Interview
Date: 2003-06-13 00:00
Tags: life
Slug: job-interview

Just got off the phone from Vicks -- she has a job interview at one of
the colleges at Cambridge Uni next week! Cool! It's when I'm at GUADEC
so I'll have to be supporting via proxy, but I have a good feeling about
it.
