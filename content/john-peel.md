Title: John Peel
Date: 2004-10-26 00:00
Tags: life
Slug: john-peel

The legendary Radio 1 DJ John Peel [died
today](http://news.bbc.co.uk/1/hi/entertainment/tv_and_radio/3955289.stm)
from a heart attack at 65. This is tragic news, as listening to John
Peel's eclectic late-night radio show was a regular event for me in the
evening. He had a wonderful voice, a habit of mumbling to himself,
forgetting the names of the songs he was playing, talking to his
producer, and playing the strangest mix of music ever heard of national
radio (death metal followed by folk followed by trance is usual), and we
loved him for it all.

John, you will be missed.

<small>NP: <cite>Automatic For The People</cite>, R.E.M.</small>
