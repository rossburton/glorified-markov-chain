Title: Joy and Pain
Date: 2004-01-14 00:00
Tags: tech
Slug: joy-and-pain

Joy: Ben Kahn looked at my contact lookup applet last night, and said it
looked good. I also aquired a large To Do Now list, which I'm half-way
though. Then Luis mentioned that the Groupwise team wanted to write such
an applet, and were pleased to know it exists already. Fab -- now to see
if the rumours that it might appear in xd2-unstable will become true.

Pain: my lower left back tooth is obviously having sympathy pains for
[Daniel](http://www.livejournal.com/users/terryfish/25296.html) -- I've
assumed until now that it was just moving again and making my gums sore,
but the top of the tooth looks worryingly non-white. I'm going to spend
10 minutes cleaning my teeth tonight with a new toothbrush, it *will*
turn out to be stains.

NP: [Dub Come Save
Me](http://www.amazon.co.uk/exec/obidos/ASIN/B000067UTN), by Roots
Manuva
