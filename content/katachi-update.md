Title: Katachi Update
Date: 2007-07-23 09:40
Tags: tech
Slug: katachi-update

It appears that some people actually want to see Katachi in action
before they try it. I can't imagine why, it only requires two libraries
to be installed from version management, both of which are slightly
obscure. So, here is a screenshot.

![Katachi](http://burtonini.com/computing/screenshots/katachi-0.1.png){}

Also some other people don't know where to get GVFS and GtkImageView
from. [This](http://www.gnome.org/~alexl/git/gvfs.git/) is the git tree
for GVFS, and GtkImageView is
[here](http://trac.bjourne.webfactional.com/). GtkImageView 1.1.0 should
work fine if you fix the trivial compile warnings, so maybe I should
ditch my local SVN checkout and use the release for now.
