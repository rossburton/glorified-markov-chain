Title: Kefalonia Photos
Date: 2003-08-14 00:00
Tags: life
Slug: kefalonia-photos

I finally scanned some of the photos from Kefalonia!

[![](/photos/Kefalonia/thumb-agia-efimia-old-people.png)](http://www.burtonini.com/photos/Kefalonia/agia-efimia-old-people.jpg)
[![](/photos/Kefalonia/thumb-agia-efimia-ross-sea.png)](http://www.burtonini.com/photos/Kefalonia/agia-efimia-ross-sea.jpg)
<!-- <a href="http://www.burtonini.com/photos/Kefalonia/agia-efimia-vicky-boat.jpg"><img src="/photos/Kefalonia/thumb-agia-efimia-vicky-boat.png"/></a> <a href="http://www.burtonini.com/photos/Kefalonia/agia-efimia-vicky-sea.jpg"><img src="/photos/Kefalonia/thumb-agia-efimia-vicky-sea.png"/></a> -->
[![](/photos/Kefalonia/thumb-apartments.png)](http://www.burtonini.com/photos/Kefalonia/apartments.jpg)
[![](/photos/Kefalonia/thumb-assos-coast.png)](http://www.burtonini.com/photos/Kefalonia/assos-coast.jpg)
[![](/photos/Kefalonia/thumb-drogorati-caves.png)](http://www.burtonini.com/photos/Kefalonia/drogorati-caves.jpg)
[![](/photos/Kefalonia/thumb-dusk.png)](http://www.burtonini.com/photos/Kefalonia/dusk.jpg)
[![](/photos/Kefalonia/thumb-fiskardo-boats.png)](http://www.burtonini.com/photos/Kefalonia/fiskardo-boats.jpg)
[![](/photos/Kefalonia/thumb-goat.png)](http://www.burtonini.com/photos/Kefalonia/goat.jpg)
[![](/photos/Kefalonia/thumb-mellisani-lake.png)](http://www.burtonini.com/photos/Kefalonia/mellisani-lake.jpg)
[![](/photos/Kefalonia/thumb-myrtos.png)](http://www.burtonini.com/photos/Kefalonia/myrtos.jpg)
[![](/photos/Kefalonia/thumb-skala-ross-relaxing.png)](http://www.burtonini.com/photos/Kefalonia/skala-ross-relaxing.jpg)
[![](/photos/Kefalonia/thumb-vicky-fiskardo-flowers.png)](http://www.burtonini.com/photos/Kefalonia/vicky-fiskardo-flowers.jpg)
[![](/photos/Kefalonia/thumb-vicky-melon.png)](http://www.burtonini.com/photos/Kefalonia/vicky-melon.jpg)
