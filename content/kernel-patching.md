Title: Kernel Patching
Date: 2008-01-23 17:10
Tags: tech
Slug: kernel-patching

In my [recent PowerTOP
adventures](http://www.burtonini.com/blog/computers/wakeups-2008-01-20-17-50)
I discovered a few timers which could be removed. One was a polling loop
in the PCMCIA driver, which I disabled because the interrupts are
unreliable, apparently. This turns out to be totally correct, with the
polling disabled it doesn't notice me inserting a CF, so I can't do
anything. I'll leave this on the "something to pester Richard about when
he is less busy" list.

The next driver related poll on the list was from a IrDA module. Now, it
shouldn't be doing anything because I have nothing apart from the
drivers loaded. Even unloading the real drivers and just loading
`irda.ko` caused wakeups, so I hunted around and with lots of
[Samuel's](http://sortiz.org/) help (he took my concept patch, and made
it actually compile!) we produced a patch which [was merged into David
Miller's 2.6.25 tree
today](http://git.kernel.org/?p=linux/kernel/git/davem/net-2.6.25.git;a=commit;h=efb495930ce55bdcc60569ef44309d88675ff970).
Excellent, I'm now a kernel hacker!
