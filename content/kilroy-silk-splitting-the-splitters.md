Title: Kilroy-Silk Splitting The Splitters
Date: 2005-01-17 00:00
Tags: life
Slug: kilroy-silk-splitting-the-splitters

Today I hear rumours that the orange-skinned Robert Kilroy-Silk is going
to leave [UKIP](http://www.ukip.org) and [form a new Euro-sceptic party
called Veritas](http://news.bbc.co.uk/1/hi/uk_politics/4181455.stm). As
a voter who thinks that Kilroy-Silk is a complete twit and most of UKIP
deserve to be slapped, I think this is a tremendous idea! Previously
people who were gun-totting Euro-sceptic Daily Mail readers voted Tory.
Then UKIP came along and started to steal the hard-core voters, scaring
the Tories into complete pandemonium and rarely seen action. Now if
Veritas actually exist, get funding, and take off, that vote could be
split three ways. Of course the Tories have a rather predictable set of
policies (Tories cutting taxes? Surely not!) so this should be quite an
easy task... Brown for PM!

<small>NP: <cite>Zwischen zwei und einer Sekunde</cite>, DJ
krill.minima</small>
