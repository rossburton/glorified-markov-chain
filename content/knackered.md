Title: Knackered
Date: 2007-07-29 18:00
Tags: life
Slug: knackered

This weekend, we tidied the garden a little. This involved some
gardening and throwing away rubbish.

Thirteen large bin bags of garden waste, and a skip full of doors,
concrete and metal tubing later, we've finished the easy tasks. Next
week the new door and paving slabs arrive, so the hard work can begin. I
haven't been sleeping too well recently so bought a herbal sleep remedy
today, but I don't think I'll need any help tonight.

In other news,
[Jekyll](http://en.wikipedia.org/wiki/Jekyll_%28TV_series%29) finished
last night. An excellent series with some interesting twists and a good
mix of horror and comedy, I highly recommend it to anyone who can watch
it (I believe it's being aired on BBC America next Saturday, and I'm
sure its all over the interwebs).
