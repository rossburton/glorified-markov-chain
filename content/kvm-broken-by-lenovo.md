Title: KVM Broken By Lenovo
Date: 2006-12-12 21:30
Tags: tech
Slug: kvm-broken-by-lenovo

God damn it, I spent the last hour fixing the Debian packaging for
[KVM](http://kvm.sourceforge.net/) and building it for Edgy, and I'm
rewarded with this:

    [17179660.420000] kvm: disabled by bios

I had a look in the BIOS but couldn't see anything obious to enable this
again.

Lenovo, you suck. And I was loving the X60 *so* much.
