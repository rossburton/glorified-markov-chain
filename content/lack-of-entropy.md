Title: Lack of Entropy
Date: 2006-08-14 18:00
Tags: tech
Slug: lack-of-entropy

A while back I upgraded CUPS on my NAS device to CUPS 1.2, and as my
configuration file had bitrotted I decided to erase it and start again.
It was looking good, CUPS detected the USB printer like they said it
would, but then it just stopped working. I had to upgrade to a SSL
connection to perform administration tasks, and it just wasn't working.
`strace` shows the daemon was just running a `select` loop, I had no
idea what the problem was.

Today after a bit more searching the solution hit me. It's trying to
create a self-signed SSL key, and SSL keys need random data. Now, what
creates random data on Linux? Mouse activity (the NAS has no mouse),
keyboard activity (no keyboard), and disk activity (it gets used once a
week). A quick poke with sysctl proved my problem:

    $ sysctl  kernel.random.entropy_avail
    kernel.random.entropy_avail = 8

Hm, eight bytes with which to create a SSL key from, that just won't
work. Solution: `find /`. I ran that in one terminal with `sysctl` in
another, and could watch the entropy pool get filled with IDE data, to
be used by CUPS. After twenty seconds of disk activity, the certificate
was finally generated. Yay, a working printer!

So, for Google and anyone else out there: if you see
`Generating SSL server key...` in the CUPS log and then CUPS hangs,
check your entropy levels.
