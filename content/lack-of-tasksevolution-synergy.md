Title: Lack of Tasks/Evolution Synergy
Date: 2007-02-25 11:15
Tags: tech
Slug: lack-of-tasksevolution-synergy

I'd noticed back when [Dates](http://projects.o-hand.com/dates) was
started the `libecal` was failing to notify Dates when events in
Evolution changed, and vice versa. This was magically fixed when Dates
moved over to `ESource`, so I didn't continue investigating it.

However, Tasks doesn't use `ESource` yet and people had noticed that if
you add items to Tasks, they don't appear in Evolution or the panel.
This is a major problem, so I build EDS and debugged the problem. Turns
out that the system tasks store is at `~/.evolution/tasks/local/system/`
but when using `ESource` it is at `~/.evolution/tasks/local/system`
(note the lack of a slash) and as backends are looked up using literal
string comparisons, these don't get the same backend so you don't get
change notifications, and even worse modifying the task list with one
application will overwrite the other's changes.

Luckily [the fix was
trivial](http://bugzilla.gnome.org/show_bug.cgi?id=333507), and I hope
that it gets reviewed soon. **Update:** committed!
