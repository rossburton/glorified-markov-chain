Title: Language
Date: 2004-01-27 00:00
Tags: life
Slug: language

So Bastien has aquired [a Scottish
accent](http://www.advogato.org/person/hadess/diary.html?start=325) from
reading Irvine Welsh. I hope the same doesn't happen for me -- I'm
currently reading [The Adventure of
English](http://www.amazon.co.uk/exec/obidos/ASIN/0340829915/) and if I
start speaking in Old English I'll get some very odd looks...
