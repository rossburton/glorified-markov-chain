Title: Last Day Of Peace
Date: 2004-01-04 00:00
Tags: tech
Slug: last-day-of-peace

Well, today is the last day of my holiday, back to the daily grind
tomorrow. Typically, my Christmas cough is nearly gone... I got ill the
day before the holiday started and it lasted until the end.

I spent most of Saturday watching [Return of the
King](http://www.imdb.com/title/tt0167260/) -- a deserving finalé to the
story. Peter Jackson did very well, on the whole I was very pleased with
the translation from book to screen.

After LotR, I spend a while getting kernel 2.6.1-rc1-mm1 built. It even
booted after I stopped making the IDE and ext3 drivers as modules... I
found a udev .deb, but as I wanted to play with
[HAL](http://www.freedesktop.org/Software/hal) and maybe even
[gnome-volume-manager](http://primates.ximian.com/~rml/blog/archives/000302.html),
I need it built with [D-BUS](http://dbus.freedesktop.org/). Several
email later, Daniel Stone had packaged D-BUS 0.20, and the udev packager
was making positive noises about packaging the latest release and adding
D-BUS support.

I then felt brave and built 2.6 for my ThinkPad... which didn't go as
well. When I use APM (works 100% in 2.4.22) the kernel refuses to
suspend when I shut the lid, and once I'd configured ACPI (I thought) I
could get it to sleep, but not to wake again -- I had to take the
battery out to reboot. So much for 2.6 on my laptop, anyone got any
ideas?

Hopefully I'll be able to get a new DevHelp release out soonish, which
actually builds against GNOME 2.6. Then I need to fix libgnomecups which
isn't showing some jobs for me. And at some point, I'll get around to
fixing Sound Juicer, and finishing the CD writing patch for Rhythmbox...

NP: [Love Is Here](http://www.amazon.co.uk/exec/obidos/ASIN/B00005OB09),
by Starsailor.
