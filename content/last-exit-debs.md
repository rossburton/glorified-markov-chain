Title: Last Exit debs
Date: 2006-07-10 10:12
Tags: tech
Slug: last-exit-debs

[Iain](http://blogs.gnome.org/iain) has just released [Last Exit
1.0](http://www.o-hand.com/~iain/last-exit/), triumphantly
feature-complete when compared to the official player. I've created
Debian packages that are currently sitting in the [NEW
queue](http://ftp-master.debian.org/new.html), until they hit Sid there
are packages in [my repository](http://burtonini.com/debian). ~~They are
targetted at Sid, but should install fine on Dapper. Tell me if they
don't and I'll do a rebuild.~~ Update: packages built on Dapper are now
available too. **Another Update:** hopefully non-broken packages for Sid
and Dapper are now uploaded.

<small>NP: <cite>Neighbour Radio</cite>, Last.fm</small>
