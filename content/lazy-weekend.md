Title: Lazy Weekend
Date: 2004-02-17 00:00
Tags: life
Slug: lazy-weekend

Last weekend was good -- it started well with Vicky making me a huge
breakfast fry-up for Valentine's Day, which was really nice. After much
lazing we finally got on the train to London, several hours later than
planned so we didn't go to the [Tate
Modern](http://www.tate.org.uk/modern/), but dived straight into
shopping. Popped into the nightmare which in Hennes on a Saturday to
pick up a few bargains, then did the HMV Sale (3 for £20, so we got
three Morcheeba albums) and discovered
[Selectadisc](http://www.selectadisc.co.uk) (it's spookily like the
store in [High Fidelity](http://imdb.com/title/tt0146882/), but with
less lighting), buying another three albums at far more reasonable
prices ([Moon
Safari](http://www.amazon.co.uk/exec/obidos/ASIN/B0000262YS),
[Lamb](http://www.amazon.co.uk/exec/obidos/ASIN/B000001EOS) and [The
Soul Sessions](http://www.amazon.co.uk/exec/obidos/ASIN/B0000YHJMS)).

Thanks to Oxford Street being what it is, we were throughly shopped out
now so searched for a pub which had seats and beer, and found one on
Berwick Street. As we walked in we noticed that there was football on
the tele, but it wasn't one of *those* pubs so we could still have a
quiet pint. It slowly dawned on me that this wasn't football, but rugby,
which I had totally forgot to record...

Beer, crisps, silently fighting couples; the usual until 19:00 at which
point we trotted off to Zizzi's on Wigmore Street. Now I love the food
in Zizzi's (good pizza/pasta) and I realise that Valentine's Day is
going to be busy, but telling us that "we have to leave by quarter to
nine" is a little rude. I'd have preferred ruthlessly efficient service
and subtle hints like them bringing the bill if they had crammed
themselves full. Anyway, the Merlot flowed and the Pizza Sofia was
fantastic, so we didn't mind. Vicky even noticed we were sitting next to
a children's TV presenter from Channel 4, or BBC, or something.
Apparently I didn't seem impressed at the time, but I really was.
Honestly.
