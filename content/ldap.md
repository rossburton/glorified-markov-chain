Title: LDAP
Date: 2004-04-20 00:00
Tags: tech
Slug: ldap

I finally bluffed and fiddled enough to get OpenLDAP working as an
office-wide address book for Evolution -- which is very cool indeed.
Maybe I should buy the [O'Reilly LDAP
book](http://www.oreilly.com/catalog/ldapsa/index.html) before turning
on `pam_ldap` though...

NP: Hot Shots II, The Beta Band. <cite>They're good / Yes, I
know.</cite>
