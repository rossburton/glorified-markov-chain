Title: Leather Case for 770
Date: 2006-06-09 18:15
Tags: tech
Slug: leather-case-for-770

Anyone who reads Planet Maemo would have noticed [Nokia 770 En
Español](http://n770.bitacoras.com/archivos/2006/06/09/habemus-funda)
blog about a [leather case for the
770](http://n770.bitacoras.com/archivos/2006/06/09/habemus-funda). The
case seems like a good idea: it holds credit cards, MMC cards and so on,
and folds open to let you use the 770. However, without any PIM apps on
the 770 (such as an addressbook, calendar, syncing to your desktop)
treating it like a filofax seems a little strange.

Did I say no PIM apps? The 2006 release includes Contacts, and
[OpenedHand has plans](http://o-hand.com/blog/?p=9). Full PIM
capabilities on the 770 coming soon!
