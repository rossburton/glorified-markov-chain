Title: Leaves
Date: 2006-11-03 09:45
Tags: photography
Slug: leaves

Yesterday lunchtime after walking Henry Vicky went into our driveway
armed with lots of fallen leaves, the SLR, and the bright afternoon
winter sun. 20 minutes later and lots of technically dubious photography
(one handed grip on camera, leaf at arms length, click and hope) she had
some fantastic Autumnal leaf shots.

[![Treasure](http://static.flickr.com/121/287131492_9988e759cc_s.jpg)](http://static.flickr.com/121/287131492_9988e759cc.jpg "<h2>Treasure</h2><br /><a href="http://www.flickr.com/photos/vickyburton/287131492/">flickr photopage</a>")
[![Embers](http://static.flickr.com/112/287117643_8d0df1d623_s.jpg)](http://static.flickr.com/112/287117643_8d0df1d623.jpg "<h2>Embers</h2><br /><a href="http://www.flickr.com/photos/vickyburton/287117643/">flickr photopage</a>")
[![On
fire](http://static.flickr.com/113/287011465_6574fcfea2_s.jpg)](http://static.flickr.com/113/287011465_6574fcfea2.jpg "<h2>On fire</h2><br /><a href="http://www.flickr.com/photos/vickyburton/287011465/">flickr photopage</a>")
[![Autumn's
Beauty](http://static.flickr.com/106/286994942_5560b3a967_s.jpg)](http://static.flickr.com/106/286994942_5560b3a967.jpg "<h2>Autumn's Beauty</h2><br /><a href="http://www.flickr.com/photos/vickyburton/286994942/">flickr photopage</a>")

**Update**: of course a Javascript library to turn image links into
magic floating popups won't work in RSS feeds, so if you want to see
them in Flickr go to [Vicky's Nature
set](http://www.flickr.com/photos/vickyburton/sets/72157594358493058/).

Yesterday I ordered a cheap lightbox so we'll hopefully be able to take
similar shots in a more controlled environment without wind and camera
shake getting in the way. Obviously they won't be as good, I've found
that often the first casual attempt at a shot is the best, and the
harder you try, the worse the shots get.

<small>NP: <cite>Protection</cite>, Massive Attack</small>
