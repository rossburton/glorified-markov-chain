Title: Leftie?
Date: 2006-01-17 11:05
Tags: life
Slug: leftie

Thomas wonders if I'm [really a leftie, or a capitalist
swine](http://thomas.apestaart.org/log/?p=334). Well, 99% of my music
collection is only on CDs, but I must admit to having 5G of MPEG4 music
on my iPod... Maybe when the Linux on iPod project has working power
management I'll re-encode it all to Ogg Vorbis.

<small>NP: <cite>Groove Salad</cite>, Soma FM</small>
