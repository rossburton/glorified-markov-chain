Title: Lenovo, I Take It Back
Date: 2007-01-23 17:10
Tags: tech
Slug: lenovo-i-take-it-back

Those nice people at Lenovo have listened to the complaints about the
lack of support for the virtualisation hardware, and [released a new
BIOS
version](http://www-3.ibm.com/pc/support/site.wss/document.do?sitestyle=lenovo&lndocid=MIGR-63144).
Thanks Lenovo, my only complaint about the X60 is now fixed.
