Title: Lens Opinions
Date: 2006-07-21 14:17
Tags: photography
Slug: lens-opinions

My main camera lens at the moment is the EF 28-135mm IS USM, which
although excellent for a non-L lens is limited as on a non-fullframe
DSLR the widest it goes is 45mm. Now if I had a decent wide angle lens
such as the 10-22mm this wouldn't be a problem, but the only lens I own
going wider than 28mm is the kit 17-55, which isn't great.

Thus I've been thinking about selling the 28-135 and buying the 17-85mm
IS USM, which is clearly targetted as being the 28-135 for cropped
digital SLRs. I'll loose the extra zoom but in return gain the wide
angle in a single lens. However, the optical quality of the 28-135 is
excellent for a non-L lens, and I'm worried that the 17-85 isn't up to
the same quality. Are there any really picky camera geeks out there who
have used both the 28-135 and 17-85 and can help me decide?

<small>NP: <cite>Prose Combat</cite>, MC Solaar</small>
