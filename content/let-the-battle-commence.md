Title: Let The Battle Commence
Date: 2006-02-20 17:16
Tags: tech
Slug: let-the-battle-commence

[AIGLX](http://fedoraproject.org/wiki/RenderingProject/aiglx) is very,
very interesting. I think this is a superior solution to the current
Novell hack-fest (Xegl is where the love is but that doesn't work at
all, and Xglx is just **so** sick) that I'm very interested in having a
play with this.

As [Mallum](http://butterfeet.org/blog) said, the Great 2006 Novell vs
Red Hat War Of The Eye Candy has started. The the battle commence!

<small>NP: <cite>Songs In The Key Of Life (disc 2</cite>, Stevie
Wonder</small>
