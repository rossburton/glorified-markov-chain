Title: LFTP, I Love You
Date: 2006-04-27 17:42
Tags: tech
Slug: lftp-i-love-you

`lftp` rocks. I always loved it in the past as it let me do tab
completion in FTP, but now I discover that it not only handles HTTP and
SFTP, but has a `mirror` command.

    lftp -c mirror sftp://o-hand.com/srv/foo/bar ftp://user:pass@ftp.example.com/pub/bar

That is very, very useful right now.

<small>NP: <cite>The Antidote</cite>, Mocheeba</small>
