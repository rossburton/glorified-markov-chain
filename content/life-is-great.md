Title: Life Is Great
Date: 2005-01-13 00:00
Tags: life
Slug: life-is-great

Recently life has been excellent. I've a job I enjoy doing, work I find
interesting, and have possibly the best boss in the world.

In totally unrelated news, I see a blogger in Edinburgh [was sacked for
venting
frustration](http://www.guardian.co.uk/uk_news/story/0,,1388249,00.html)
about his job in [his blog](http://www.woolamaloo.org.uk/).

<small>NP: <cite>Out of Season</cite>, Beth Gibbons and Rustin
Man</small>
