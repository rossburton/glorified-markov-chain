Title: Life, T-Shirts, and Music
Date: 2005-03-07 15:23
Tags: life
Slug: life-t-shirts-and-music

Life? Life is good. We're off to India and Nepal for two weeks tomorrow,
which promises to be totally excellent. This weekend was quite busy, we
went to my parents on Friday evening for a family meal and drinks, which
was good fun. I finally got to show my dad the 300D, and took some
photos during the evening.

[![](http://www.burtonini.com/photos/Random/thumb-img_0927.jpg){
}](http://www.burtonini.com/photos/Random/img_0927.jpg)
[![](http://www.burtonini.com/photos/Random/thumb-img_0931.jpg){
}](http://www.burtonini.com/photos/Random/img_0931.jpg)
[![](http://www.burtonini.com/photos/Random/thumb-img_0932.jpg){
}](http://www.burtonini.com/photos/Random/img_0932.jpg)
[![](http://www.burtonini.com/photos/Random/thumb-img_0933.jpg){
}](http://www.burtonini.com/photos/Random/img_0933.jpg)

To try and finish off the series I had to get Vicky to take one of me on
Saturday.

[![](http://www.burtonini.com/photos/Random/thumb-img_0948.jpg){
}](http://www.burtonini.com/photos/Random/img_0948.jpg)

Then on Sunday, we went over to Vicky's mum's house and cooked a meal
for six people. We had salmon, roast potatoes, Yorkshire puddings, and
vegetables, with apple crumble for pudding. Thankfully there were no
major panics and it all tasted good, so that is okay. We normally cook
for two, so cooking for six was quite a shock!

Oh, and on Wednesday or Thursday or some time we actually had snow which
lasted more than an hour. It had gone by lunchtime, but I took some
photos of the grass peeping out through the snow in our garden and with
a little GIMP magic blew the snow right out (I would be a man and use a
flash gun, but mine isn't quite strong enough). Very simple picture, but
I quite like it.

[![](http://www.burtonini.com/photos/Misc/thumb-snow-grass.jpg){
}](http://www.burtonini.com/photos/Misc/snow-grass.jpg)

T-Shirts? T-Shirts are cool, especially ones from Threadless. Last week
I bought [Death By
Music](http://www.threadless.com/product/172/Death_By_Music) and
[Captain
Awesome](http://www.threadless.com/product/179/Captain_Awesome), both of
which are really cool. They'll be getting more of my money later.

Music? Music is plentiful. Recently I've been listening to lots of music
I've wanted to try but have never listened to, so I borrowed some
Godspeed You! Black Emperor, Squarepusher, and Tricky, and picked up
more Miles Davis, DJ Shadow, and Thievery Corporation. All in all I've
been pleased, it's all interesting and enjoyable, although Vicky doesn't
entirely agree with me... I think she called GYBE "crap", and didn't
like DJ Shadow.

<small>NP: <cite>The Cosmic Game</cite>, Thievery Corporation</small>
