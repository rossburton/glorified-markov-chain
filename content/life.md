Title: Life
Date: 2003-09-29 00:00
Tags: life
Slug: life

This coming weekend Vicky and myself are off to see one of her aunts get
married, this obviously calls for smart clothes. Of which I own little.
My casual wear (jeans/cords/combats with t-shirt/casual shirt/jumper) is
the same as my work wear (did I mention I love my office?), so I had to
buy some new trousers/shirt/tie. I'll skip past huge amounts of dull
trouser shopping and just say that I've gone for black trousers and a
dark red shirt/tie combo, nothing too radical but quite cool.

Vicky, however, is something else. Damn. Cream flowing skirt from
Monsoon, with either a blank sparkly top (Monsoon again) or a woolen
jumper, and black pointy shoes. She looks terrific. `:-)`

Had a John/Allen/Dave around for a few drinks Friday night, which soon
turned into pizza with 5 bottles of wine, or maybe 6. I can't quite
remember. I must have been a little drunk: I said to John that going out
and cycling together though the woods in the winter would be a good idea
when blatantly it would be a) dirty b) tiring and c) exhausting. Not to
mention d) knackering. Damn you, red wine, DAMN YOU.
