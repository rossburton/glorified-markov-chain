Title: LinkedIn
Date: 2008-01-31 13:30
Tags: tech
Slug: linkedin

I generally thought that LinkedIn was pretty useless for people like me.
I have a community of like-minded associates available via Planet Gnome
and so on, so apart from collecting friends it is pretty useless.

But recently it's been becoming quite useful. For large companies it
generally appears to be company policy that contact with open source
projects is done via anonymous email domains, like GMail. This obviously
makes it tricky to guess where someone is from when they appear on a
mailing list... but LinkedIn to the rescue. Search for a name and hey
presto, their CV!

<small>NP: <cite>Artists Like: Amon Tobin</cite>, Last.fm</small>
