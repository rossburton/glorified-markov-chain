Title: Linux Wakeups
Date: 2008-01-20 17:50
Tags: tech
Slug: linux-wakeups

To satisfy an idle curiosity, I installed
[PowerTOP](http://www.lesswatts.org/projects/powertop/) on my Zaurus to
see how many wakeups a second it was doing. It started out at 112w/s,
which isn't that great, but Richard told me that I should give our
2.6.24-rc8 kernel a go, because it has `NO_HZ` defined. To our surprise
it booted, and to cut a long story short my Zaurus is currently idling
at **5.4** wakeups per second, and 11% of those are about to be patched
out of the kernel.
