Title: Local Branches In Bazaar For Fun And Profit
Date: 2005-07-08 12:16
Tags: tech
Slug: local-branches-in-bazaar-for-fun-and-profit

I've been meaning to blog about this for some time now, so don't expect
a long and detailed entry.

As [previously
mentioned](http://www.burtonini.com/blog/computers/xicc/eog-icc-2005-06-24-17-27)
I've now got my EoG ICC patches in an Arch repository. Doing this was
surprisingly trivial. Thanks to the Bazaar people at Canonical (I've
been talking to Robert Collins, but there are probably more people
involved) there is a selective mirror of the GNOME CVS modules on
[`http://bazaar.ubuntu.com`](http://bazaar.ubuntu.com). Assuming the
default archive is set, a local module can be created as a branch of
another with a single command:

    baz branch http://bazaar.ubuntu.com/gnome@bazaar.ubuntu.com/eog--MAIN--0 eog--xicc--0

This created a module in my repository called `eog--xicc--0`, which I
can checkout as usual and edit as I want. At some point I'll want to
merge any changes from upstream, so in the working directory I perform a
merge:

    baz merge gnome@bazaar.ubuntu.com/eog--MAIN--0

Note that I don't need to specify the full location as the branch
command added the archive name to location mapping. Simply sort out any
conflicts, and them commit the changes.

Easy! This Bazaar thing is really growing on me.

<small>NP: <cite>Sounds From The Verve Hi-Fi</cite>, Thievery
Corporation</small>
