Title: Local Elections
Date: 2003-05-14 00:00
Tags: life
Slug: local-elections

It was that time again -- time to pick what evil scheming despot I want
to run our local council.

Tories are Tories, and will never get my vote. Beyond that, I'm easy. I
generally vote for Labour or Liberal Democrats, favouring strategic
voting over who I actually prefer, to keep the Tories out if possible.
Labour have been moving their way slowly around the colour wheel, they
used to be the red party where now they are somewhat purple. The only
thing to balance this out is the Tories who appear to be doing the same:
currently somewhere around turquoise, about to collide with the Green
party. Liberal Democrats in generally in tune with my peaceful lefty
attitude of not killing people and not selling public services, so I'd
like to vote for them.

The entire area I live (Hertfordshire/Essex) is a huge Tory base. On a
politically coloured map, there are a few small red dots in a sea of
blue. Cue the strategic voting... not much of a change here I believe
(apart from the [BNP getting a seat in
Broxbourne](http://news.bbc.co.uk/nol/shared/bsp/hi/vote2003/locals/html/40.stm)),
but at least my area is Labour, with LibDem and Tory equal second. Still
in an [ocean of
blue](http://news.bbc.co.uk/nol/shared/bsp/hi/vote2003/locals/html/93.stm)
though.

Postscript: I am still *highly* amused with the rate the Liberal
Democrats are gaining seats -- maybe at the next election they will be
the "second party". I can't wait to find out.
