Title: London Nights
Date: 2005-02-12 00:00
Tags: life
Slug: london-nights

On Saturday, after spending an hour of hell in Debenhams buying two
suitcases, we went down to St Paul's Cathedral with our camera. The sun
had just set and as I was taking the camera out of my bag, they turned
the floodlights on.

[![photo](http://www.burtonini.com/photos/Random/thumb-img_0512.png){
}](http://www.burtonini.com/photos/Random/img_0512.jpg)
[![photo](http://www.burtonini.com/photos/Random/thumb-img_0514.png){
}](http://www.burtonini.com/photos/Random/img_0514.jpg)

From St Paul's it is a pretty short walk to the Millennium Bridge, but
as ever I couldn't remember the route and ended up taking the scenic
route, including an overpass, to find the river. Luckily heading south
through any path available from St Paul's will reach the Thames at some
point, so we reached the Millennium Bridge,

[![photo](http://www.burtonini.com/photos/Random/thumb-img_0525.png){
}](http://www.burtonini.com/photos/Random/img_0525.jpg)
[![photo](http://www.burtonini.com/photos/Random/thumb-img_0532.png){
}](http://www.burtonini.com/photos/Random/img_0532.jpg)

By now the chill was starting to get to our extremities (like thighs and
necks), so we headed back to St Paul's tube station along a path I can't
find the name of anywhere.

[![photo](http://www.burtonini.com/photos/Random/thumb-img_0545.png){
}](http://www.burtonini.com/photos/Random/img_0545.jpg)
[![photo](http://www.burtonini.com/photos/Random/thumb-img_0552.png){
}](http://www.burtonini.com/photos/Random/img_0552.jpg)

After a nightmare journey (St Pauls to Tower Bridge in the cold, without
using the Circle or District lines, anyone?) we made it to the
<cite>Liberty Bounds</cite> for a drink (or two) and dinner. There were
photos of the skyline and the Tower of London, but they were terrible so
are not appearing here!
