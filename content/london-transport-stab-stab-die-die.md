Title: London Transport Stab Stab Die Die
Date: 2009-09-27 15:51
Tags: life
Slug: london-transport-stab-stab-die-die

Sometimes I really, *really* hate London. A trip to London on Saturday,
in theory: Leave Ely 16:26, arrive Kings Cross 17:34, change to
Piccadilly line and arrive Covent Garden at 17:54. Dinner then the 21:52
train back home, arriving 23:10.

A trip to London on Saturday, in practise. Leave Ely 16:26, arrive Kings
Cross 10 minutes late. Change to Piccadilly line and stand outside the
closed barriers for 15 minutes because of overcrowding. Give up on the
tube, catch a number 59 bus to Aldwych: 10 minutes to reach Euston
(faster to walk) and gave up after sitting in gridlock on Russel Square
for 15 minutes. Eventually get rather empty Piccadilly line from Russel
Square to Covent Garden, arriving 18:50. Oh, and then [the
restaurant](http://www.wahaca.co.uk/) said it would be a two hour wait
for a table for four.

That said, the return journey wasn't exactly a barrel of laughs. The
tube was behaving so that took the expected ten minutes, but then we
just missed the train back home and the next train wasn't for over an
hour. Jump into a taxi to Liverpool Street to catch the 22:26... to
discover there are engineering works and we'd have to get a bus for two
hours. Attempt to get the tube back to Kings Cross... more engineering
works so that was out. *Another* taxi back to Kings Cross and we finally
get on the last train home, arriving at 00:35.

Just for extra fun I'm in London for the Moblin 2.0 Release Party on
Monday and there are *yet more* engineering works, so if I miss the
22:15 I'll be on a bus for half the journey. Stab stab stab.
