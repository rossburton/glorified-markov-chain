Title: Long Time No Update
Date: 2003-10-10 00:00
Tags: life
Slug: long-time-no-update

Well, I've been busy. Last weekend we went to a wedding (second attempt
for the bridge and groom, they had previously divorced), and this week
I've been attempting to nurse an ill Vicky whilst being kept working
hard at work.

I've also had to stop myself from screaming "Oh My God!" in the street
when I remember that California elected the Terminator as governor. This
is a disturbing sign, American politics is turning into a soap opera,
where the most exciting and colourful characters win. <cite>Governor
Academy</cite> or <cite>Presidential Idol</cite> anyone?

Our last Amazon purchase (it is amazing how ingrained "Amazon" has
become, in just a few years. I remember the days when Amazon only sold
books...) was the new Dido album and Damien Rice's <cite>O</cite>.
Dido's new album sounds like Dido's old album to me, but I'm not her
greatest fan (pleasant background music, nothing more). <cite>O</cite>
however, is great fun: starts with acoustic guitars and vocals, wanders
into a serious acoustic jamming session and finishes with <cite>Silent
Night</cite> in a hidden track.

I also finished reading the last of Ken McCloud's Star Fraction series
(for want of a better name), <cite>The Sky Road</cite>. Yet again a
great book, switching between two eras (2040 and more like 2400), set in
Scotland, with social-political debate, mainly
socialism/communism/capitalism. I think I'd describe it as
thinking-man's sci-fi.

And before I forget, Peter Plichta is still an arse.
