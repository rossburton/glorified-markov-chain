Title: Look-alikes
Date: 2005-06-01 19:18
Tags: tech
Slug: look-alikes

I lost count of the number of times people said to me, "You're Ross
Burton? You look nothing like your hackergotchi!" [Robert
Love](http://rlove.org/) even went as far as quantifying just how much
unlike: third place. First place of course goes to [Bastien
Nocera](http://www.hadess.net/?start=540), who everyone thought was
black when the most appropriate description is "pasty with dark fur".
Just who is number two is an unanswered question.

I'll knock up a new hackergotchi for myself in a minute, but here is a
new one for Bastien I made from a photo I took at the GUADEC Parrrrty:

![Bastien Nocera](http://www.burtonini.com/photos/Misc/hadess.png)

<small>NP: <cite>Tourist</cite>, St. Germain</small>
