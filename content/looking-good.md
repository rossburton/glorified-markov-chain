Title: Looking Good
Date: 2003-03-10 12:00
Tags: life
Slug: looking-good

Life isn't bad at the moment. Finances are looking good, work is looking
good, I'm happily in love. Everything is good.

Another reason for my happiness is that I have just become an author! My
article, *Wrapping GObjects in Python*, will be published by IBM on
their *developerWorks* site this week, giving me a groovy new entry for
my CV, and \$1000. That will help pay off my mountainous debts
somewhat...

It's a shame that the day the article was confirmed the life of my
laptop battery dropped to a pathetic 40 minutes. A quick hunt online
produced a "bargain" replacement at £146, which is better than the £170
I normally see, but still... Li-Ion batteries are not cheap.
