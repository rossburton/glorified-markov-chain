Title: Lotus Notes On Linux?
Date: 2006-03-08 10:35
Tags: tech
Slug: lotus-notes-on-linux

From
[Groklaw](http://www.groklaw.net/article.php?story=20060305214231974):

> At the end of the presentation, Andreas Pleschek revealed that the
> laptop he used for the presentation was running a pre-release of their
> new platform, the Open Client. It is actually a Red Hat work station
> with IBM's new Workplace Client, which is built in Java on top of
> Eclipse. Because of Eclipse, it runs on both Linux and Windows, and
> they have been able to reuse the C++ code in Lotus Notes for Windows
> to run it natively on Linux via Eclipse. Internally in IBM, for years,
> they have had a need to run Lotus Notes on Linux, and now they can.
> And they will offer it to their customers. Workplace uses Lotus Notes
> for mail, calendar, etc. and Firefox as their browser. For an office
> suite, they use OpenOffice.org.

Does anyone know more details about how the C++ code in Lotus Notes for
Windows is magically turned into native Linux code via Eclipse? I don't
entirely understand what they've done here. Maybe the backend is the
original C++ with a new frontend built using SWT?

<small>NP: <cite>Liquid Swords</cite>, GZA</small>
