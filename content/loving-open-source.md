Title: Loving Open Source
Date: 2005-11-08 09:58
Tags: tech
Slug: loving-open-source

[This article](http://www.codeproject.com/system/NoDeleteDelay.asp) on
debugging and fixing a bug in Windows without the source is engrossing,
in the way that an accident is engrossing. Sometimes you forget that 99%
of the source code for the programs you use daily is available for free,
and things like this can be changed easily. God I love Open Source...

Speaking of which, do you love Open Source? [Opened
Hand](http://www.o-hand.com) are on the look out for interesting
developers; skills in embedded Linux, GTK+, and DBus are an advantage.
If you think you are up to it, [email us](mailto:jobs@o-hand.com).

<small>NP: <cite>August and Everything After</cite>, Counting
Crows</small>
