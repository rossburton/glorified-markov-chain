Title: <scream>Arghhh!</scream>
Date: 2003-03-16 00:00
Tags: life
Slug: ltscreamgtarghhhltscreamgt

Paintball rules.

Went paintballing yesterday for Vicky's brother's (Pete) advance
birthday present. Advance as his birthday is in May, and paintball in
Summer is not as fun... too damn hot.

After losing one game and drawing another, we decided that team
communications and tactics are required... This lead to a triumphant
win, with an advance party grabbing the flag, the rest of the team
shouting, screaming and firing to catch up, and then advancing up the
field to win. The marshals said that when we did a large battle cry and
run, the other team were actually running away from dug-in positions.
Fantastic.

After that win a certain member of the other team thought "it would be
funny" to totally cheat and ruin a game, by masquerading as a member of
our team (22 people in DPMs and goggles look alike) and stealing our
flag. This did not go down well, and tempers flared. Despite this, the
rest of the games were played fairly, and we had an excellent 4-2 win at
the end of the day.

However, the mission to get Vicky to go paintballing next time isn't
going well. Pete strained his ankle, Steve has a 7" bruise down one arm,
and I have managed to strain most of the muscles in my back. After Deep
Heat, pain killers, baths and lots of massage I can just move my neck.
It's still very sore to move, and my back is killing me. Not good. :(
