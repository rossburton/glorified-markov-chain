Title: Lunacy
Date: 2006-04-10 14:42
Tags: life
Slug: lunacy

[Andrew Brown](http://www.thewormbook.com/helmintholog/) has an mostly
interesting editorial in the Guardian's daily news summary <cite>The
Wrap</cite>, which can often have some fantastic lines.

> <cite>Criminal lunacy is an accurate shorthand for a lot of the Bush
> government's actions.</cite>

<small>NP: <cite>Pieces Of You</cite>, Jewel</small>
