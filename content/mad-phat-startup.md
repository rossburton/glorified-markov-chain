Title: Mad Phat Startup
Date: 2004-08-25 00:00
Tags: tech
Slug: mad-phat-startup

I see that [Kyle wants to
see](http://kyle.mcmartin.ca/blog/2004/08/24/#2004.08.24) some of the
cooler Canonical/SSDS features.

> <cite> I'm also anxious to try the "pimp ass bootscreen" and "bitching
> laptop" work they've done... </cite>

[Daniel pointed
out](http://fooishbar.org/daniel/blog/tech/madPhatTotallyRad-2004-08-24-19-43.html)
that these features do not exist, but Mad Phat Startup and Totally Rad
Laptop Support do. I saw a preview of Mad Phat Startup at the
conference, and I feel that "mad phat" is a poor term to describe it.

Mad Phat Banging Startup is more appropriate. My productivity is going
to nosedive when my desktop gets it... I'll be continually rebooting to
bask in the magnificence of it.

I can't comment on the Totally Rad Laptop Support, but I expect it will
kick arse (or at least stop laptops exploding) assuming you're a member
of the X40 World Domination Gang. My personal conspiracy theory is that
X40s are sentient and control the mind of their owners, and the people
around them, willing them to buy more X40s. After a few months of this,
when everyone has an X40, they will **turn evil**. I suggest staying
clear of the X40 World Domination Gang, and purchasing a metal foil hat.

Note: Mad Phat Startup may rock, but it doesn't rock as much as the
Jeffoblaster.

<small> NP: <cite>Daybreaker</cite>, Beth Orton </small>
