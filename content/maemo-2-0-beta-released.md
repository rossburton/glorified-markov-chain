Title: Maemo 2.0 Beta Released
Date: 2006-06-09 15:05
Tags: tech
Slug: maemo-2-0-beta-released

I'll keep this short as I'm rather busy... Maemo 2.0 beta has been
released, along with Internet Tablet OS 2006 beta. More information is
available at the [Maemo](http://maemo.org/#date_09062006) site. This
release has had *huge* amounts of love and it's far more polished than
the 2005 release, but sadly the beta release is missing the VOIP
functionality.

**Update**: seems I was wrong, the SDK may be missing the VOIP software,
but the Internet Tablet image is [fully
featured](http://bergie.iki.fi/blog/internet-tablet-os-2006-beta-is-out.html).
