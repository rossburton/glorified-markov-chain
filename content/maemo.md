Title: Maemo
Date: 2005-05-25 15:40
Tags: tech
Slug: maemo

Silly name, great idea. Nokia have just announced the [Nokia 770
Internet Tablet](http://www.nokia.com/nokia/0,,74866,00.html), a frankly
gorgeous hand-held tablet which just happens to be based on [Debian,
GTK+, GStreamer and DBus](http://www.maemo.org/).

**Update:** of course this announcement wasn't a complete surprise
considering I work for [Opened Hand](http://www.o-hand.com/), who have
been working with Nokia on this for the last two years.
