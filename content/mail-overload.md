Title: Mail Overload!
Date: 2004-05-03 00:00
Tags: tech
Slug: mail-overload

Blimey, some Debian developers do seem to like their name in list
archives... At the moment I have 1808 unread messages in my
`debian-devel` folder and 1077 unread in `debian-private`. I was very
close to selecting hundreds of messages at a time and deleting them, but
then I discovered a gem from Our Glorious Founder, Ian Murdock:

> <cite>If I were dead, I'd be rolling over in my grave right about
> now.</cite> — Ian Murdock

Classic. Enough of the flaming and fighting people, let's get back to
getting Sarge ready!

NP: <cite>Brand New Second Hand</cite>, Roots Manuva.
