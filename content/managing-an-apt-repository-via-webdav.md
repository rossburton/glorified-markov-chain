Title: Managing an APT Repository via WebDAV
Date: 2006-12-06 11:30
Tags: tech
Slug: managing-an-apt-repository-via-webdav

<!-- -*- Mode: html -*- -->

At [work](http://o-hand.com/) we've just bought online another server,
so we have a true public/private server split. The private server is
only accessible via secure methods, and the public server only has a few
users to reduce the possibility of people brute-forcing their way in.
However, the public server holds the APT repositories for
[debian.o-hand.com](http://debian.o-hand.com/) and
[maemo.o-hand.com](http://maemo.o-hand.com/). Before the split this was
not a problem, after building the package `dput` was used with the `scp`
method, and then `mini-dinstall` was run as the user to update the
repository. Now that there are minimal accounts on the new server, we
can't `scp` the packages to the server, or `ssh` in to run
mini-dinstall. Running m-d in daemon mode solves the latter, but there
is still the problem of getting the files on the server in the first
place.

Enter WebDAV. The public server already has a number of WebDAV shares
(secure client shares, Subversion, and so on) so it was trivial to
export the `mini-dinstall` incoming queues over WebDAV. With a [custom
init script](http://burtonini.com/computing/mini-dinstall) to start
`mini-dinstall` in daemon mode, anyone with the right permissions can
upload files via WebDAV. One option here is to use Nautilus, but I like
the safety of `dput`. A day of hacking, with lots of screaming at
Python's `urllib2`, produced [this patch to dput that adds HTTP
uploading](http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=142023).
I've been using it for a few days now and it seems to be working well,
so hopefully it will be merged into Sid soon.

<small>NP: <cite>Xmas in Frisko</cite>, soma.fm</small>
