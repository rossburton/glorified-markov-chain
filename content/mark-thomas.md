Title: Mark Thomas
Date: 2003-04-10 12:00
Tags: life
Slug: mark-thomas

A couple of nights ago (Tuesday, the 8th) I went to see a live [Mark
Thomas](http://www.mtcp.co.uk/) show. A very enjoyable show, he is
wonderful at telling stories, making the entire audience collapse with
laughter, and then can suddenly make the entire room silent with a
serious point about the terrors the Western world manages to inflict on
other nations.

Lots of interesting facts, like the poor drunk American who is in prison
for three years for mentioning the "day of judgement" and "a burning
Bush". The number of people dying daily due to lack of action from us.
The propaganda about the current war, forced in our faces every second
of every day, until the war is over (as far as the media is concerned),
at which point it will be ignored and never mentioned again.

I am now a proud white ribbon wearer, supporting How About Not Killing
Everyone?

A small piece of advice from the wise -- if you see MT live, and he
starts to talk about John Major and Edwina Curry... look away. Quickly.
You shall be haunted for the rest of your days if you do not.
