Title: MasqMail
Date: 2005-11-07 09:58
Tags: tech
Slug: masqmail

My laptops all run [MasqMail](http://innominate.org/kurth/masqmail/) so
that they can always send mail, no matter where I am. This is good, and
works very well. However, masqmail is [suffering from
non-maintenance](http://bugs.debian.org/cgi-bin/pkgreport.cgi?src=masqmail).
So, do I change my MTA, or do I adopt MasqMail?

Ideally I'd switch to another MTA, as long as it's simple and will
automatically change the delivery method depending on some external data
(generally the name of the current network interface). Any suggestions?
Or should I start reviewing the bugs and hijack the package?

**Update**: Oliver Kurth, previously absent maintainer of MasqMail, just
did a new upstream release and uploaded to Debian. Dilemma over!

<small>NP: <cite>In Between Dreams</cite>, Jack Johnson.</small>
