Title: mDNS Name Resolution
Date: 2005-03-03 14:26
Tags: tech
Slug: mdns-name-resolution

Today I made mDNS name resolution work, but first a little background
about my network. I have a desktop PC called `eddie`, a laptop called
`hactar`, a Netwinder cunning called `netwinder`, and an iPaq h5500
called `ipaq`. All of these machines connect to a wireless ADSL router
and ask it for an IP address using DHCP, it gives them one and the DNS
server, and all is good... until I want to `ssh` into the Netwinder from
my laptop, and then `scp` files onto the iPaq, as I don't know the IP
addresses.

Until now my solution has been to do a broadcast ping with
`ping -b 192.168.10.255` and try the IPs which respond, but now I've
finally found a sane [mDNS name resolution plugin for
NSS](http://0pointer.de/lennart/projects/nss-mdns/). This is trivially
installed and configured (add `mdns4` to the `hosts` line in
`/etc/nsswitch.conf`), but depends on a mDNS responder to be running on
each machine. Luckily, Howl is currently in Debian (though not for
long), so after quickly installing it on all of the machines to my
surprise it Just Worked:

    ross@hactar ~
    $ getent hosts netwinder.local
    192.168.10.104  netwinder.local
    $ getent hosts ipaq.local
    192.168.10.105  ipaq.local
        

Excellent!

<small>NP: <cite>Vertigo</cite>, Groove Armada</small>
