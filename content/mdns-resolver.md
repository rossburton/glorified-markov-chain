Title: mDNS Resolver
Date: 2004-06-30 00:00
Tags: tech
Slug: mdns-resolver

Mike Hearn yesterday pointed me at someone who was planning on writing
mDNS support for glibc, probably as a NSS provider. In hindsight that is
blatantly the right thing to do, so I mailed him to see if he wanted
someone to help test it. When he replied he told me that Apple had added
a mDNS NSS provider to their implementation a few days ago. And what do
you know, it works. The NSS library only works I run their `mdnsd`, so
I'll have to dig around to see if this can be changed.
