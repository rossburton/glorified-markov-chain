Title: Medusa Rockness
Date: 2004-01-29 17:03
Tags: tech
Slug: medusa-rockness

So Curtis might be switching [Medusa to
Redland](http://members.cox.net/sinzui/blog/medusa/2004-01-28.html).
This is very cool. OOo documents have a pile of metadata which is
trivial to access (OOo documents are zip files, the file `meta.xml`
contains Dublin Core and custom OOo metadata) and can be mangled into
RDF easily. I'm following in [Dave
Beckett's](http://www.ilrt.bris.ac.uk/people/cmdjb/) footsteps by
putting RDF directly into PNG and JPEG files. MP3 files normally contain
rubbish, but with Sound Juicer the metadata is at least consistent, if a
little lacking (MusicBrainz doesn't handle genre or year for a start).

If Medusa can slurp all of this metadata, and applications will actually
set it, then we have a seriously cool metadata-driven desktop.
Personally I can't wait.
