Title: Merry Christmas
Date: 2003-12-24 00:00
Tags: life
Slug: merry-christmas

Last blog before Christmas, I'll probably sneak one in before the new
year when I check my ~~spam~~ email.

I'm off to Vicky's parents later this afternoon for a few days, and then
off to my parents with Vicky for a belated Christmas Day on the 27th.
I'm sure my sister will have managed to get several rolls of film
developed overnight from her trip to India, hopefully she won't make me
too jealous... :)

Happy Hogswatch everyone, have a good time.
