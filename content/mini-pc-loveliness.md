Title: Mini PC Loveliness
Date: 2007-08-19 12:00
Tags: tech
Slug: mini-pc-loveliness

I've just discovered the [AOpen MP965-DR mini
PC](http://minipc.aopen.com/Global/spec.htm), and blimey it's lovely.
Does anyone out there run have one? If I got one it would be under the
television and always on, so heat and noise are important factors to me.
