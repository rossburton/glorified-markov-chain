Title: Misc for Sale
Date: 2014-05-11 20:55
Slug: misc-for-sale

As we're moving house we are having a bit of a clear out, and have some
techie gadgets that someone else might want:

-   Netgear DGN3500 ADSL/WiFi router. £15.
-   ~~Bluetooth GPS dongle. £5.~~
-   Seagate Momentus 5400.6 160GB 2.5" SATA hard disk (ex-MacBook). £5.

Those prices are including UK postage. Anyone interested?
