Title: Modern X Architecture
Date: 2005-06-07 12:30
Tags: tech
Slug: modern-x-architecture

It appears that some people are getting a bit confused about how all
these pieces of the modern, GL-based, X architecture works. I'm no
expert on this but I think I understand how it all joins together. I'll
define some terms and hopefully things will be clearer.

X Server

:   The program which draws the contents of the windows onto
    your screen. This is the program which controls your display and
    receives your input.

X Client

:   A program which people generally think of as applications, such as a
    word processor or a calculator. This does the real work and tells
    the server what to draw. Due to the client/server nature of X,
    clients may be running on any machine, local or remote, whereas the
    server generally runs on the computer in front of you.

Window Manager

:   Whilst X provides functions to position windows, X itself doesn't
    specify how the windows are manipulated or positioned, i.e. it
    provides the mechanism but not the policy. Implementing the policy
    is the role of the window manager, and whilst most window managers
    are pretty much the same (see `twm`, Metacity, or Enlightenment), it
    does mean some weird and wonderful window managers can be created
    (see [Matchbox](http://projects.o-hand.com/matchbox/) or
    [Ratpoison](http://www.nongnu.org/ratpoison/)).

    Hopefully these three terms are familar to everyone who is still
    reading!

X Extensions

:   The X protocol was designed with future extension in mind, and
    recently there has been a flurry of activity here. Recent major
    extensions include Render (a decent drawing API), Damage (clients
    can receive notification of redraw events on other windows, good for
    VNC servers or programs like
    [Xresponse](http://projects.o-hand.com/xresponse/)), Fixes (as the
    name implies it fixes a number of omissions, such as a better
    clipboard protocol), and Composite (clients can control how the
    windows are displayed).

Composite Manager

:   This is where it gets fun. Combining the Holy Trinity of modern X
    extensions, Composite, Damage, and Render, leads to the concept of a
    composite manager. When compositing is enabled windows created by
    clients are not drawn to the display directly, but to
    off-screen images. The composite manager is then responsible for
    drawing the windows to the screen in any way it wants. A basic
    composite manager would do nothing but copy the pixels (and thus
    appear idential to running no composite manager), `xcompmgr` adds
    real-time transparent drop shadows to top-level windows, and
    Luminocity wobbles the windows when they are moved.

    Note that a composite manager doesn't have to be a window manager,
    but they often are. `xcompmgr` is just a composite manager, whereas
    Luminocity is both a window manager and a composite manager.

So, where do all of these new program fit?

Xgl is a new X server which uses GL to draw. Not only are the core X
drawing primites accelerated (like they are on most servers these days),
but Render is also accelerated. As the OpenGL specification alone
doesn't provide enough API for an X server (for example it doesn't cover
manipulating the display size, or input), there are a number of Xgl
versions. Xglx uses the GLX extension and is effectively a
GL-accelerated Xnest/Xephyr clone. Xegl uses the Embedded OpenGL
specification, which adds the required features so that an application
can control the entire display and not just a window. With this, Xegl
can run directly on the hardware. Note that instead of calling GL
directly Xgl uses Glitz, a GL-backed 2D graphics library originally
created for Cairo, which matches the semantics of the Render extension.

Luminocity is pure crack. Pure, 100%, pristine crack, rolled into lines
by naked Brazilian hand maidens. Who said Owen was a code purist? That
said, it is a very good design for a composite manager test bed. To use
Luminocity you need two X servers: a target display and a source
display. Luminocity reads window contents from the source display and
draws them to the target display in a (optionally full-screen) window,
by being the composite manager on the source and a normal client on the
target. The source display needs to support the Holy Trinity and XTest,
so generally is `Xfake` from the `kdrive` X server (which doesn't have a
visible framebuffer), and the target display is whatever X server you'd
normally run as long as it supports hardware-accelerated GL. Whilst this
is a seriously over-engineered solution and requires more than a few
hacks (XTest is used to forward events between the servers), the result
is that time taken to restart it after a build is drastically reduced,
as when Luminocity is killed both of the X servers and all clients are
not killed too.

The really interesting program we all want to see is `glxcompmgr`,
demonstrated at GUADEC to manic cheers and applause. From what I
understand, this is a classic compositing manager in design in that it
works with a single display. It achieves fabulous visual effects by
extensively using GL, and thus with hardware acceleration is very fast.
My guess is that it just draws to a full-screen GL window and so doesn't
require Xgl, but will work on any server.

<small>NP: <cite>Babylon Rewound</cite>, Thievery Corporation</small>
