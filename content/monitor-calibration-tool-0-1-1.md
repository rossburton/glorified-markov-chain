Title: Monitor Calibration Tool 0.1.1
Date: 2003-10-01 01:57
Tags: tech
Slug: monitor-calibration-tool-0-1-1

I finally got around to looking at Bastien's patch to make the black
point screen disappear on a click, without having to resort to killing
the process. Cool!

Instead of merely applying the patch (bah!) I looked at it, understood
it, and then removed half of it. It appears to work for me, and even
closes on mouse clicks too, so it seems good enough to me. However, if
some of this removed code turns out to be useful rest assured I'll
release 0.1.2.

Download location:
[`monitor-calibration-0.1.1.tar.gz`](http://www.burtonini.com/computing/monitor-calibration-0.1.1.tar.gz)

Random Unrelated Note: the tabloid-sized <cite>Independent</cite> is a
damn fine idea, and could only be improved by stapling the spine. I am
keeping my copy of the first issue in case this ever becomes a legendary
milestone -- I just missed owning the famous Gillian Anderson FHM, and
that was worth 50 quid two weeks after it came out.
