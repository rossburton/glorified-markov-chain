Title: Monitor Calibration Tool 0.1
Date: 2003-09-12 17:41
Tags: tech
Slug: monitor-calibration-tool-0-1

GammaarRRr! has grown up and become very sensible. New name, new
release, same version number.

`monitor-calibration-tool` allows you to set the gamma and black point
of your monitor. It does not save the settings at the moment, but it
will soon. Note that if you hit the <cite>Black Point</cite> button
you'll have to `alt-tab` to a terminal and kill the process manually...
I couldn't get the keyboard grab to work. Sorry about this, but because
of this bug alone I'm not installing a `.desktop` file.

Download location:
[`monitor-calibration-0.1.tar.gz`](http://www.burtonini.com/computing/monitor-calibration-0.1.tar.gz)
