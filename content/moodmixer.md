Title: MoodMixer
Date: 2005-01-18 00:00
Tags: tech
Slug: moodmixer

Today a web site I wrote the backend for went live. The
[MoodMixer](http://www.9thing.com/moodmixer.php) for
[E4](http://www.channel4.com) generates "relaxation tracks" that are
thinly disguised as advertising for that day's television (or was it the
other way around?).

It's a nice mix of Python, `sox`, and `lame`, with the speech systhesis
done by AT&T's NaturalVoices after discovering that `festival` is pretty
good at sounding like a robot but not great at sounding even vaguely
like a human. 115 people have used it so far, and from looking at the
logs I see that everyone in the world but me has a Hotmail account.

<small>NP: <cite>Urban Hang Suite</cite>, Maxwell</small>
