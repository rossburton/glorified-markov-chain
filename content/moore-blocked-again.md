Title: Moore Blocked, Again
Date: 2004-05-05 00:00
Tags: life
Slug: moore-blocked-again

[Michael Moore](http://www.michaelmoore.com/), author of [Stupid White
Men](http://www.amazon.co.uk/exec/obidos/ASIN/0060392452), has had his
[latest film (Fahrenheit 9/11) blocked by
Disney](http://news.bbc.co.uk/1/hi/entertainment/film/3685633.stm). For
some reason I'm not totally surprised by this: Disney isn't exactly the
world's most liberal and experimental company. Well, it's being shown at
Cannes so hopefully either Disney cave in, or someone will take it from
them. If all else fails I'm sure someone will get a screener at
Cannes...
