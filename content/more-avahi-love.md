Title: More Avahi Love
Date: 2006-02-13 13:10
Tags: tech
Slug: more-avahi-love

Today's Avahi Love posting covers publishing SFTP servers. My NAS box
runs SSH, so I can use the `ssh:` method in gnome-vfs to browse the
filesystem (it uses SFTP). The magic service file is:

>     <?xml version="1.0" standalone='no'?>
>     <service-group>
>       <name replace-wildcards="yes">Remote Files on %h</name>
>       <service>
>         <type>_sftp-ssh._tcp</type>
>         <port>22</port>
>       </service>
>     </service-group>

Restart Avahi and then with a suitable Avahi-enabled gnome-vfs, there
will be entries in <cite>Places → Network Servers</cite> for each
machine that file is installed on. Kick arse!
