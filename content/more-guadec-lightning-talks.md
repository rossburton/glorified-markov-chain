Title: More GUADEC Lightning Talks
Date: 2007-07-13 19:20
Tags: tech
Slug: more-guadec-lightning-talks

We've had so many great Lightning Talk submissions this year that we're
going to have to split the session into two. There is still the original
session at 15:00, but there is now another session at 17:00 for any
talks that don't fit into the first session.

If you are doing a lightning talk, please remember to either check that
your laptop works with the projector in the hall perfectly **before**
the talks start, or put your talk on a USB memory stick -- something
portable such as PDF or S5 please -- so you can use the provided laptop
(a ThinkPad T43, 1024x768).
