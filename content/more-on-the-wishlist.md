Title: More on the "Wishlist"
Date: 2003-12-11 00:00
Tags: tech
Slug: more-on-the-wishlist

It appears I got lots of comments on my blog last night... I'll field
them here. Note that the long post by Andy which ends "powertools are
not part of GNOME Desktop" was actually by me, no idea why it says Andy.

"If you do a skin of GTK+ 2.x for EMACS" -- there is a GTK+ 2 port of
emacs already -- "like there is one for OOo" -- all OOo does is copy the
theme colours, my patches to the GNOME control center (in 2.4) mean that
emacs should do that for you too.

Paul Gnuyen -- you don't want "viewports" over "workspaces". You want
the implementation of virtual desktop to include spanning windows across
desktops. That is fine, I thought there was a bug about this but I can't
seem to find it at the moment :( The spanning issue I think might be
possible if you fiddle with the X virtual desktop size too, but I've
only got a single screen so I don't know the details of this.

Eugenia -- I was waiting for your reply. `:)` I understand that low-tech
journalists and "normal" users will bitch, moan and generally complain
about things which annoy them without going through the proper channels
to get the problems solved. However, I expected you, someone who was
recently a member of the nautilus mailing list, to be up to date on what
is happening in GNOME development. It is a little late posting a
wishlist for GNOME when 2.5 has just his the API freeze, and contains a
large number of the entries on your list. I expect you to file bugs all
the time, to be active on the usability lists, because "you care" (as
you say). Or I'd expect such an article to be passed to some of the
people close to GNOME development first so that they can point out the
differences in direction/features which already exist/etc etc.

I want GNOME to be a commercial desktop. I want GNOME on the [NHS
desktops](http://observer.guardian.co.uk/business/story/0,6903,1101344,00.html),
in [our government](http://www.theregister.co.uk/content/4/34440.html),
one as many desktops as possible. I wrote Sound Juicer to be a CD ripper
for normal people, as the existing rippers are not very user friendly.
It is this focus on end-users which GNOME has, 800,000 users inside the
NHS will not want a powerful text editor, or an integrated IDE, but a
powerful email client, a easy to use web browser, a easy system to use.
Yes, there should be comprehensive CD burning software using the GNOME
library, and video editing software using GTK+/GStreamer, but at the
moment there are larger fish to fry. We need the best email client
(luckily Evolution 2 is looking excellent) and the cleanest browser
(Epiphany is developing so fast and so well) before we can look at
other, less important, programs.

In a related note, I see Federico has fiddled with the [new file
chooser](http://primates.ximian.com/~federico/news-2003-12.html#10), and
renamed the Frobnicate button... For people who didn't know what
"[Frobnicate](http://www.catb.org/~esr/jargon/html/F/frobnicate.html)"
meant, the [Jargon File](http://www.catb.org/~esr/jargon/) is a handy
reference for "[Lart](http://www.catb.org/~esr/jargon/html/L/LART.html)"
too.

Finally, contact-lookup-applet now sports auto-completion. I hope to get
0.3 out soon.
