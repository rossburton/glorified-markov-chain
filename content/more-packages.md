Title: More Packages...
Date: 2003-06-11 00:00
Tags: tech
Slug: more-packages

Now I have [17 packages in
Sid](http://qa.debian.org/developer.php?login=ross)\[1\], as I just
uploaded `gnome-vfs-sftp`. Scary.

`gnome-vfs-sftp` for gives you sane access to remote machines via
SSH/SFTP. "Traditional" `ssh:///` uses a shell on the remote machine and
parsing command output, which is fairly evil and buggy.

\[1\] Also see [this
URL](http://qa.debian.org/developer.php?login=ross%40burtonini.com) for
more packages from my non-Debian email address, which need to be
updated.
