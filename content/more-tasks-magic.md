Title: More Tasks Magic
Date: 2008-03-13 19:30
Tags: tasks, tech
Slug: more-tasks-magic

I finally got around to working on magic date parsing in
[Tasks](http://pimlico-project.org/tasks.html), thanks to Mallum porting
a JavaScript library to C last year. I rewrote it again this week, and
landed it in Subversion a few days ago. I'd love any brave Tasks users
to give it a go, especially people who don't use English. They'd need to
translate the new strings, but I want to check that the technique I'm
using is portable between languages.

Feedback on what magic strings should be detected would be great too.
Currently it detects "today", "tomorrow", "yesterday", "this
\[weekday\]" and "next \[weekday\]". Next up is "by|due|on \[local date
representation\]", but what else would be useful?
