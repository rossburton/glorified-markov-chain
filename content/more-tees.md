Title: More Tees
Date: 2005-04-01 12:58
Tags: life
Slug: more-tees

I think this is becoming an addiction... I just placed another order
with Threadless, specifically [Fred and the Giant
Eel](http://www.threadless.com/product/211/Fred_and_the_Giant_Eel?streetteam=rossyb),
[Game-Set-Match](http://www.threadless.com/product/133/Game-set-match?streetteam=rossyb),
[Are Trees
Electronic?](http://www.threadless.com/product/202/Are_Trees_Electronic?streetteam=rossyb),
and [He Lives By The
Ocean](http://www.threadless.com/product/208/He_Lives_By_The_Ocean?streetteam=rossyb).
Soon I'll have more t-shirts than I could ever need...

<small>NP: <cite>Dub Fever</cite>, King Tubby</small>
