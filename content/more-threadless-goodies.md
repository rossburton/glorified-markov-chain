Title: More Threadless Goodies
Date: 2006-05-14 13:00
Tags: life
Slug: more-threadless-goodies

Yet again I succumbed to the
[Threadless](http://www.threadless.com/?streetteam=rossyb) sale, and
bought three new tees:

-   [The Outdoor
    Mix](http://www.threadless.com/product/397/The_Outdoor_Mix?streetteam=rossyb)
-   [Landline's
    Revenge](http://www.threadless.com/product/441/Landline's_Revenge?streetteam=rossyb)
-   [Pandamonium](http://www.threadless.com/product/178/Pandamonium?streetteam=rossyb)

They are all very, very cool, although I am particularly loving
<cite>The Outdoor Mix</cite> at the moment. I've finally managed to get
a first-run tee too, <cite>Landline's Revenge</cite>.
