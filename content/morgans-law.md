Title: Morgan's Law
Date: 2005-06-13 16:51
Tags: life
Slug: morgans-law

After a brief discussion with Allen over whether eating crunchy bread
would cause you to lose weight on the grounds that your jaws are doing
exercise, we formulated Morgan's Law Of Calorific Denial:

> More Food = More Exercise = The Thinner You Get

Like all great laws (such as <cite>E=mc^2^</cite>) it is straight to the
point. I feel a best-selling book about diet plans involving Morgan's
Law coming up...

<small>NP: <cite>O</cite>, Damien Rice</small>
