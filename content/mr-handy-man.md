Title: Mr. Handy Man
Date: 2003-04-07 12:00
Tags: life
Slug: mr-handy-man

On Saturday I turned into Mr. Handy Man. Our CD collection is getting
rather large (\~270 CDs), and the current CD rack is filled to capacity,
with a 3 foot tall tower of CDs next to it. I decided to build a new one
myself, there being no decent and cheap CD racks designed for this many
CDs.

I measured and planned and drew, and eventually had a design for a rack
nearly 6 feet high, and more than a foot wide \[1\]. My Dad and me went
down the local timber yard, bought the pine (which they cut for us) for
a mere £10, and set about building it.

The building went very well. My Dad is pretty damn good at DIY so we had
all of the tools we needed, and a few hours later we had our new CD
rack, and Vicky had caught the sun \[2\]. We did have a few minor
problems -- one shelf was sticking slightly out (five minutes with a
chisel sorted that), and the MDF back was put on backwards, showing the
huge price sticker to the world, but on the whole its a damn fine and
solid CD rack, which holds a grand total of 350 CDs, for £15. We have
two and bit shelves free, so have space for another 80 or so CDs...
shouldn't take long to fill that.

Of course, we then spend much of Sunday afternoon filling the damn
thing... To celebrate we bought *Solid Bronze*, the Best of Beautiful
South, and a *Black Sunday* by Cyprus Hill.

\[1\] The exact measurements for anyone wanting to build their own:
1700mm x 370mm x 150mm, in 15mm pine.  
\[2\] That's a polite phrase for having a pink nose and arms.
