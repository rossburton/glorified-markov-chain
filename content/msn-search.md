Title: MSN Search
Date: 2004-07-01 00:00
Tags: tech
Slug: msn-search

Microsoft have a [technology
preview](http://techpreview.search.msn.com/) of their new MSN search
engine. The litmus test is, of course, searching for "ross burton".

The good news: top hit is the root of my blog. However, so are the
2^nd^, 3^rd^, 4^th^... In fact all but five of the results in the first
*five pages* are from my site, and those others are all pages from the
Burton (the snowboarding people) web site. Amusingly, they have shown me
the index pages from every day I've blogged on.

Why not see for yourself how well Google searches for ["Ross
Burton"](http://www.google.com/search?q=ross%20burton), compared to
[Microsoft's new
engine](http://techpreview.search.msn.com/results.aspx?q=ross+burton).
Thank you Google for <cite>More results from www.burtonini.com</cite>!
