Title: Multitouch and GNOME
Date: 2007-02-14 09:44
Tags: tech
Slug: multitouch-and-gnome

The multitouch system legend Jeff Han released another demo recently,
which I [saw on
MacRumours](http://www.macrumors.com/2007/02/12/more-multitouch-from-jeff-han/).
As usual it's a combination the usual gestures to move around a map and
dragging objects around with basic physics, but then I was surprised to
see a RHEL desktop appear. Watch the demo and look out for the morphing
demo near the end.

Chances are this is using [MPX](http://wearables.unisa.edu.au/mpx/),
which is being integrated into XInput 2 as we speak.

<small>NP: <cite>Dreaming Wide Awake</cite>, Lizz Wright</small>
