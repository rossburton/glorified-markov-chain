Title: Musicless
Date: 2004-01-05 00:00
Tags: life
Slug: musicless

When I left the house this morning, my minidisc player claimed to have a
full battery.

20 minutes later, the music stopped. Dead battery. Not good. Normally I
get around this by playing some music from my laptop, but I've only got
music on the lappy when I'm taking a download from Sharing The Groove
home. Life is unfair at times.

When I bought this minidisc player it could go 10 days on a single
charge (at \~3 hours a day), but now the player is so dusty inside it
spends most of its time trying to seek, thus rapidly eating the battery.
The rumours of the \$65 iPod are *very* interesting...

NP: Nothing.
