Title: Mutually Exclusive PulseAudio streams
Date: 2013-02-28 16:12
Tags: tech
Slug: mutually-exclusive-pulseaudio-streams

The question of mutually exclusive streams in PulseAudio came to mind
earlier, and thanks to Arun and Jens in `#gupnp` I discovered the
PulseAudio supports them already. The use-case here is a system where
there are many ways of playing music, but instead of mixing them PA
should pause the playing stream when a new one starts.

Configuring this with PulseAudio is trivial, using the
`module-role-cork` module:

    $ pactl load-module module-role-cork trigger_roles=music cork_roles=music

This means that when a new stream with the "music" role starts, "cork"
(pause to everyone else) all streams that have the "music" role. Handily
this module won't pause the trigger stream, so this implements exclusive
playback.

Testing is simple with `gst-launch`

    $ PULSE_PROP='media.role=music' gst-launch-0.10 audiotestsrc ! pulsesink
    Setting pipeline to PAUSED ...
    Pipeline is PREROLLING ...
    Pipeline is PREROLLED ...
    Setting pipeline to PLAYING ...
    New clock: GstPulseSinkClock

At this point, starting another `gst-launch` results in this stream
being paused:

    Setting state to PAUSED as requested by /GstPipeline:pipeline0/GstPulseSink:pulsesink0...

Note that it won't automatically un-cork when the newer stream
disappears, but for what I want this is the desired behaviour anyway.
