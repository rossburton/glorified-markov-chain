Title: My Life: The Opera
Date: 2005-01-10 00:00
Tags: life
Slug: my-life-the-opera

It's been a while since I've blogged (if I say this any more I'm going
to setup a macro for it) and since it's one of my New Years Resolutions
to blog more (sadly I neglected to blog that, not a good start) I
thought I best get on with it. Christmas and the New Year was good, the
first event was the [Opened Hand](http://www.openedhand.com/) Christmas
meal at [Rasa](http://www.rasarestaurants.com/) in London. Overall good
fun with excellent food, despite an absolutely hellish journey home.
Next was First Christmas at my mother-in-law's house, where we stayed
from Christmas Eve for a few days with lots of good food and wine. Then
on the 28^th^ we went to my mother's for Second Christmas, where there
was yet more good food and wine (and
[photos](http://www.burtonini.com/photos/2004-Christmas/)). I'm starting
to get worried about my waistline post-Christmas, and am refusing to
weigh myself for a few weeks so that my piecepts have a chance to go...

For the New Year [Dave](http://www.daveclayton.co.uk/) and
[Allen](http://www.monkey-spanking.co.uk) came over, for what started as
a very civilised buffet we prepared during the day, and rapidly turned
into a drunken discussion of great music, <cite>Cranium</cite>, the
relative merits of the old Trivial Pursuit verses the new, and standing
to sing <cite>Auld Lang Syne</cite> for some reason. [Incriminating
evidence](http://www.burtonini.com/photos/2004-NewYear/) is available if
you think you are brave enough.

After Christmas, with my redundancy payment burning a hole in my pocket,
I joined the Digital Gang and bought a Canon EOS-300D for the
(effective) bargain price of £485. I totally love the camera, as I've
had a EOS-300V I felt at home straight away. The quality of the pictures
is great and thanks to the 1.6 multiplier on lenses my 50mm f1.8 becomes
a pretty nice 90mm portait lens, whilst the 28-90mm from the 300V now
goes up to 144mm. All I need now is a [bag to put it
in](http://www.tamrac.com/5201.htm) and a decent flash. I've started to
put some [passable photos
online](http://www.burtonini.com/photos/Random/) in my gallery, which
will grow a lot faster now that I don't have to spend time scanning
photos.

As some people may have guessed from the title of this article I've
recently seen <cite>Jerry Springer: The Opera</cite>. The magical BBC
aired a recording of it on Saturday night, to a chorus of mass
complaining and demonstration. Somehow 47,000 people knew, before they
saw it, that they would be offended by the blasphemous content and
swearing, some reports claiming 8000 expletives in the show. It turns
out that this ludicrous number was taken my multiplying the number of
expletives in the show by the number of people saying them, so if the
chorus swears once that is 27 expletives. And as for blasphemy... yes it
satirised Christianity but if an organisation can't take satire then it
needs to take a long hard look at itself. To be fair the Church of
England publicly stated that it doesn't think JSTO was blasphemous, but
that didn't stop people [getting carried
away](http://news.bbc.co.uk/1/hi/entertainment/tv_and_radio/4147801.stm),
first with TV license burning and now [sueing the BBC for
blasphemy](http://news.bbc.co.uk/1/hi/entertainment/tv_and_radio/4161109.stm).
**Get a grip people**. Satire is a form of criticism, albeit in a
comedic form. If you can't handle criticism, what are you so worried
about? Is Jesus swearing and admitting "I'm a bit gay" really worth all
of this?

In other news, I was pleased to see that [legal music downloads exceeded
single
sales](http://news.bbc.co.uk/1/hi/entertainment/music/4155385.stm) for
the first time, in the last week of December. Really this isn't a great
surprise and was bound to happen at some point: when singles are
primarily bought by teenagers, and a song can be downloaded for £0.79
from the iTunes Music Store but costs £3.99 in HMV, it's obvious where
the tech-savvy teenagers (that's probably most of them) will go. Even
better news is that sales from downloads will be included in the
official singles chart. After so much demonising from the record
companies, downloading music is finally hitting the mainsteam.

<small>NP: <cite>Something Wicked This Way Comes</cite>, The
Herbaliser</small>
