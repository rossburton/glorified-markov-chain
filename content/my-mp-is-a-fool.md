Title: My MP Is A Fool
Date: 2007-11-28 15:00
Tags: life
Slug: my-mp-is-a-fool

[My
MP](http://www.theyworkforyou.com/mp/mark_prisk/hertford_and_stortford)
finally replied to my letter where I questioned his signing of the
[Early Day Motion regarding homoeopathic
hospitals](http://edmi.parliament.uk/edmi/EDMDetails.aspx?EDMID=33006&SESSION=885).
Whilst defending homoeopathy he said that doctors should be allowed to
prescribe homoeopatic treatments, which I expect many homoeopaths
wouldn't like as the homoeopath/patient interaction is pretty much where
the cure is. Whilst I was still laughing at that, I came across this.

> “All therapies should be considered equally, and decisions on whether
> or not to provide them on the NHS should be evidence-based, as is the
> case with all other conventional medicines and treatments.”

I'm about to write to him, asking for this evidence. I haven't seen any,
and I'm sure the medical profession would like to see it too.

<small>NP: <cite>Burning Off Impurities</cite>, Grails</small>
