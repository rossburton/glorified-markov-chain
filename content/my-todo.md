Title: My TODO
Date: 2003-03-19 00:00
Tags: tech
Slug: my-todo

Oh my. My personal To Do list of Software I Must Hack On is growing...

-   CD ripping tool (for Rhythmbox probably). I'm about to start on
    this, using GStreamer.
-   CD writing tools. `nautilus-cd-burner` does the data side of this
    once I hacked it to write ISO images. All Rhythmbox needs is
    playlist -&gt; CD image support.
-   GTK+ interface to Bugzilla. Jereon started this (bugzilla-buddy),
    and it was looking good.
-   GUI to Devil's Pie...
-   GNOME Chat needs some love.
-   GConf Editor needs to be able to set default and mandatory values. I
    have a patch which nicely exposes a GConf bug...

