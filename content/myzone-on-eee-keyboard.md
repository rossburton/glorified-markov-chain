Title: Myzone on Eee Keyboard
Date: 2009-06-15 18:00
Tags: tech
Slug: myzone-on-eee-keyboard

Asus had previously announced the Eee Keyboard, which isn't a keyboard
but more a netbook with a full sized keyboard and *wireless HDMI*. The
end result being that this is the ideal companion to your huge 1080p LCD
television in the front room for light browsing and so on.

Now the Eee Keyboard also has a small touchscreen by the side of the
keyboard, which had generally been shown displaing a calendar and the
time. Fairly useful but nothing that interesting. However, they have
recently demonstrated Moblin 2 running on the Eee, including the Myzone
social desktop update thingy.

![Myzone on Eee Keyboard](http://burtonini.com/images/eee-myzone.png)

Now this is pretty neat. I don't know how the touchscreen is related to
the main display, but a custom Moblin 2 panel and Myzone tailored to
fill the touchscreen would be really cool. Now, where can I get an Eee
Keyboard from...

<small>NP: <cite>Arecibo Message</cite>, Boxcutter</small>
