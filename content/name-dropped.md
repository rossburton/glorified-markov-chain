Title: Name Dropped
Date: 2006-03-22 15:55
Tags: tech
Slug: name-dropped

Jeff Waugh was [kind enough to name drop Devil's
Pie](http://perkypants.org/blog/2006/03/22/seriously-belgium/) at FOSDEM
whilst being interviewed for Source21.nl.

> ...We haven't been talking about some of the really cool stuff you can
> do with gnome. Things like Devil's Pie or Brightside, which make the
> window manager work in totally different ways: they take Metacity
> which is a very conservative, very simple, it just manages your
> windows kind of window manager, and they turn it into something which
> does edge flipping and all kinds of cool stuff, a programmable window
> environment, those kind of things.

Yay Jeff!

<small>NP: Last.fm Neighbour Radio</small>
