Title: nautilus-dpkg 0.2
Date: 2003-07-01 18:27
Tags: tech
Slug: nautilus-dpkg-0-2

`nautilus-dpkg` 0.2 is now available, [download
here](/computing/nautilus-dpkg-0.2.tar.gz). At the moment, this is only
a Nautilus property page for `.deb` files.

This release simply adds a Show Depends button, and starts towards
working correctly with Nautilus 2.4. Debian packages are also available
from [my repository](/debian/) (in `experimental`).
