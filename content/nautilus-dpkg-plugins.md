Title: Nautilus Dpkg Plugins
Date: 2003-02-25 00:00
Tags: tech
Slug: nautilus-dpkg-plugins

Thanks to the Python/GNOME2 bindings, it is possible to write Nautilus
plugins in Python. I have written a property page extension for `.deb`
files ([screenshot](/computing/screenshots/dpkg-property.png)).

A package is ready, but Sid doesn't have the required packages yet. For
now, a source tarball is
[here](/debian/experimental/nautilus-dpkg_0.1.orig.tar.gz)

Feature requests welcome! Mail me with feedback! I might do a VFS
method, so going to `dpkg://` in Nautilus will list installed packages.
