Title: Nearly Christmas
Date: 2003-12-22 00:00
Tags: life
Slug: nearly-christmas

<cite> 'Twas a few nights before Christmas  
And not a sound could be heard  
Apart from the sniffing of Ross and Vicky  
Thanks to their nasty colds. </cite>

Massive Chinese meal last night -- partly due to ordering the large set
meal, and partly due to a mistake and us getting more than we expected
-- and I'm still feeling fat. We spent all of last night sniffing
instead of sleeping, which was fun, so I'm also tired. Grand plans of
hacking thrown away, instead I've got to do last-minute card writing and
present buying.

NP: [Sensations of
Tone](http://www.amazon.co.uk/exec/obidos/ASIN/B0000070J0/), by Gol.
