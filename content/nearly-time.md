Title: Nearly Time!
Date: 2004-07-15 00:00
Tags: life
Slug: nearly-time

You could say I've been a little busy recently. Last minute wedding
planning is hectic, and on top of that there has been the nightmare
which is porting out code on an Infineon chip, with the deadline being
the day of the wedding. I've spent the last week frantically working on
the port whilst trying to get knowledge into other people's heads (of
course those two tasks are mutually exclusive). I've also managed to
have a stag day and meet other people for drinks in the week, whilst
rapidly copying as much music onto the iPod Vicky for me as an early
wedding present. All in all I can't wait for 12 days of lazing in Italy!

So, as of this morning I'm official on annual leave until 2^nd^ August,
and won't be reading emails at all from the 17^th^. If you really want
to contact me I can only suggest purchasing a wedding present [from our
gift
list](https://www.thegiftregistry.info/CGIBIN/PRIAMLNK.CGI?MP=FINDLIST%5EGIN330&GUESTLISTNO=WL05096)
and adding the message there. Gift list notifications are always read.
;)

<small> NP: The Best of James Brown </small>
