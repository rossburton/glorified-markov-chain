Title: Nepal
Date: 2005-02-01 00:00
Tags: life
Slug: nepal

In five weeks time we're off to India and Nepal for a two week holiday,
which promises to be an amazing experience. Well, that is assuming we
can actually get into Nepal...

This morning the King [sacked the government and declared a national
emergency](http://news.bbc.co.uk/1/hi/world/south_asia/4224855.stm) in
the conflict against the Maoist rebels, who want to change from
constitutional monarchy to a communist republic. There is no
communications, no air travel, and in general it's not a great place to
be ([official government
statement](http://www.fco.gov.uk/servlet/Front?pagename=OpenMarket/Xcelerate/ShowPage&c=Page&cid=1007029390590&a=KCountryAdvice&aid=1013618386271))
at the moment.

Balls, balls, and thrice balls. Hopefully Nepal will stabilise with
amazing speed, but I think it's time we check the insurance terms and
conditions.

<small>NP: <cite>Sultans of Swing</cite>, Dire Straits</small>
