Title: Network Manager 0.7
Date: 2007-09-25 17:20
Tags: tech
Slug: network-manager-0-7

Over the weekend I spent some time hacking on
[Poky](http://pokylinux.org%20), integrating Network Manager. Because
I'm hardcore I went for NM from trunk, instead of 0.6, as apparently its
approaching stability and feature completeness. This afternoon I finally
fixed enough stupid problems (`/sbin/ip` wasn't executable for instance,
thanks `install -s`) that it connected to the office wireless and let me
browse to Google.

![NM in Poky](http://burtonini.com/computing/screenshots/poky-nm.png)

Thanks to Dan Williams for answering my stupid questions about Network
Manager.
