Title: Network Oddity
Date: 2014-01-08 12:38
Slug: network-oddity

This is... strange. Two machines, connected through cat5 and gigabit
adaptors/hub.

    $ iperf -c melchett.local -d
    ------------------------------------------------------------
    Server listening on TCP port 5001
    TCP window size: 85.3 KByte (default)
    ------------------------------------------------------------
    ------------------------------------------------------------
    Client connecting to melchett.local, TCP port 5001
    TCP window size: 64.0 KByte (default)
    ------------------------------------------------------------
    [  4] local 192.168.1.7 port 35197 connected with 192.168.1.10 port 5001
    [  5] local 192.168.1.7 port 5001 connected with 192.168.1.10 port 33692
    [ ID] Interval       Transfer     Bandwidth
    [  4]  0.0-10.0 sec  1.08 GBytes   926 Mbits/sec
    [  5]  0.0-10.0 sec  1.05 GBytes   897 Mbits/sec

Simultaneous transfers get \~900MBits/s.

    $ iperf -c melchett.local -r
    ------------------------------------------------------------
    Server listening on TCP port 5001
    TCP window size: 85.3 KByte (default)
    ------------------------------------------------------------
    ------------------------------------------------------------
    Client connecting to melchett.local, TCP port 5001
    TCP window size: 22.9 KByte (default)
    ------------------------------------------------------------
    [  5] local 192.168.1.7 port 35202 connected with 192.168.1.10 port 5001
    [ ID] Interval       Transfer     Bandwidth
    [  5]  0.0-10.0 sec   210 MBytes   176 Mbits/sec
    [  4] local 192.168.1.7 port 5001 connected with 192.168.1.10 port 33693
    [  4]  0.0-10.0 sec  1.10 GBytes   941 Mbits/sec

Testing each direction independently results in only 176MBits/sec on the
transfer to the iperf server (melchett). This is 100% reproducible, and
the same results appear if I swap iperf client and servers.

I've swapped one of the cables involved but the other is harder to get
to, but I don't see how physical damage could cause this sort of
performance issue. Oh Internet, any ideas?
