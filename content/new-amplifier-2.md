Title: New Amplifier
Date: 2003-10-16 00:00
Tags: life
Slug: new-amplifier-2

I just won an auction on eBay for a replacement amplifier, a [Cambridge
Audio A500](http://www.cambridge-audio.co.uk/products/a500.htm).
Somebody tried to outbid me just as the auction closed (2 seconds before
to be exact), but this has happened to me in the past (a month ago for
the same amp), so 30 seconds before the auction closed I upped my bid
too, safe in the knowledge that if nobody tried to sneak in I wouldn't
pay more, but if someone did I'd probably bounce past their bit.

It worked as the [bidding
history](http://cgi6.ebay.co.uk/aw-cgi/eBayISAPI.dll?ViewBids&item=3051630585)
shows, and I should be getting the amp Monday. So, anyone want a good
condition NAD pre-amp and a slightly broken NAD power amp? The power amp
should be trivial to fix if you know how to.

In other news I'm feeling slightly dizzy whilst sitting down, and have a
horrible feeling I'm about to start developing the 'orrible viral
infection Vicky is just getting over... or maybe its the [ill people
around
me](http://www.guardian.co.uk/guardianpolitics/story/0,3605,1063833,00.html)
on the way to work.
