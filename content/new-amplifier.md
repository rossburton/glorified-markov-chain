Title: New Amplifier
Date: 2003-10-24 00:00
Tags: life
Slug: new-amplifier

Yesterday I got my new Cambridge Audio A500 amplifier plugged in, wired
up, and pumping. I even bought it some chunky speaker cable as a
"welcome to your new home" present... It seems a lot happier than my old
NADs, which had too much power for the size of my room (they volume has
*never* gone above 1/3) and lead to some of the detail being lost. Oh,
and they were broken too. Anyone who still wants my old NADs (1000
pre-amp, 2100 power-amp), mail me, for £30 they are yours.

I'm going to miss saying "look at the size of my NADs".
