Title: New Artwork
Date: 2006-02-15 18:00
Tags: tech
Slug: new-artwork

Matthew Garrett was briefly my hero today (taking over from Iain Holmes)
for uploading into Dapper `xserver-xgl` and Compiz. Very nice it is too,
but it's a bit too crashy on my laptop for any real work.

Shortly afterwards he was replaced by Lapo Calamandrei, who told me he
was working on some new Sound Juicer artwork if I was interested. Hell
yes, I said, and it turns out he has managed to draw exactly what I
wanted the Sound Juicer icon to be, several years ago. He rules.

![New
artwork](http://burtonini.com/computing/screenshots/sj-new-icon.png)

<small>NP: <cite>Wish You Were Here</cite>, Pink Floyd</small>
