Title: New Battery
Date: 2003-03-13 12:00
Tags: tech
Slug: new-battery

Finally bought a new battery for my laptop, after the life from a full
charge dropped to 42 minutes... £120 later, and the battery has a better
performance than the original -- it is managing 3:30 easily, whilst
compiling.

I am one happy hacker. *And*
[LoudMouth](http://people.codefactory.se/~micke/loudmouth/) was
released, so I've got debs in the [usual place](/debian/unstable).
