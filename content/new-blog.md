Title: New Blog!
Date: 2012-05-21 11:40
Tags: life
Slug: new-blog

I've been meaning to migrate away from pyblosxom to WordPress for a long
time now, but have never found the time to work out how to migrate the
many comments.  Then my blog host crashed and the comments were all
lost, problem solved!  Some hacking and pain later, and here I am
blogging with WordPress on hardware with support. Maybe I'll blog more,
maybe not.  Who knows what the future holds!
