Title: New Design
Date: 2003-12-20 00:00
Tags: life
Slug: new-design

Huzzah, a design for my blog which isn't stolen straight from Movable
Type, though in hindsight it does remind me of Jeff Waugh's design.

It's not totally finished yet, the padding/margins still needs to be
sorted out in places, but overall I'm happy with it. Apart from the
header, the entire design is layed out in ems so it should scale very
nicely. Even the sidebar is "20 ems" wide and the main content expands
to fill the space remaining, which took a while to figure out.

Finally, thanks to [Jose
Luis](http://www.abeatexperience.com/experience/) for taking some
wonderful photos, one of which I used as the header.
