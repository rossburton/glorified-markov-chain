Title: New Gypsy Release
Date: 2008-03-27 16:00
Tags: tech
Slug: new-gypsy-release

<abbr title="Gypsy! Blingtacity! gnome-cd!!">Coding Legend</abbr>
[Iain](http://blogs.gnome.org/iain/) has just released [Gypsy
0.6](http://gypsy.freedesktop.org), the all-new GPS multiplexing daemon
which focuses on being lean and easy to use, and not on, erm, putting
your GPS on the Internet or something weird.

Because I'm fairly lame there are not matching Debian packages yet, but
I'll get around to that tomorrow. In other news, a very nice man called
Ian Lawrence wrote [a buzzword-compliant
tutorial](http://www.ianlawrence.info/random-stuff/django-bluetooth-and-gps-on-ubuntu-mobile)
where he uses Gypsy to talk to a Bluetooth GPS, tests it with my [Gypsy
Status 10-minute
hack](http://www.burtonini.com/blog/computers/gypsy-2007-12-17-10-30),
and then uses Django to redirect the user to the relevant geohash.org
page.

<small>NP: <cite>Remembranza</cite>, Murcof</small>
