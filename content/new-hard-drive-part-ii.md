Title: New Hard Drive, Part II
Date: 2004-01-07 00:00
Tags: tech
Slug: new-hard-drive-part-ii

    $ df
    Filesystem            Size  Used Avail Use% Mounted on
    /dev/ide/host0/bus0/target0/lun0/part4
                           33G  4.7G   26G  16% /
    /dev/ide/host0/bus0/target0/lun0/part2
                           38M   11M   26M  31% /boot
    /dev/ide/host0/bus0/target0/lun0/part1
                          4.0G  2.7G  1.4G  68% /mnt/win

Yay! It's not exactly a massive hard drive, but this is a laptop, so its
quite respectable. GNOME 2.5 from CVS, here I come!
