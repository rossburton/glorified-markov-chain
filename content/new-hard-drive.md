Title: New Hard Drive
Date: 2004-01-06 00:00
Tags: tech
Slug: new-hard-drive

Sitting right next to me is the hard drive adaptor for my Thinkpad
slice. Sitting in the post office at home is my new 30G hard drive for
my laptop. Tomorrow morning I plan to get up at an obscene time, grab
the drive, and do some partitioning on the way to work.

GNOME 2.5 here I come! A permament chroot for building Debian packages
in! GStreamer 0.7! Thanks must go to Christian Schaller who contributed
hard cash towards the cost of the hard drive.
