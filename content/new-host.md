Title: New Host
Date: 2002-12-11 10:59
Tags: tech
Slug: new-host

Last night the DNS updated for my new hosting site:
`evilgeniuses.org.uk`. Many thanks to Nerdfest.org, but without Python
2.2 the scripts I wanted to run won't work... :(

The blog is now running with
[pyblosxom](http://roughingit.subtlehints.net/pyblosxom) instead of
blosxom, which shouldn't mean much of a different to you, but a lot to
me -- I can hack away at it.
