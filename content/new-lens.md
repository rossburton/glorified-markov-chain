Title: New Lens
Date: 2005-02-21 18:30
Tags: life
Slug: new-lens

Last week Hubert mentioned the new EOS-350D (very interesting
improvement to the 300D) which I knew about already, and the new [EF-S
60mm f/2.8 macro
lens](http://www.canon.co.uk/for_home/product_finder/cameras/ef_lenses/macro_lenses/ef-s_60mm_f2.8_macro_usm/index.asp)
which I missed. Nice lens, I'm sure it will be way out of my budget but
I want one anyway: sticking two close-up filters on a 50mm isn't quite
the right way to do good macro work. Nice to see Canon introduce more
EF-S lenses, EF lenses work fine but thanks to the 1.6x multiplier they
sometimes just feel wrong. The competition is also hotting up, with
Sigma producing a [30mm
f/1.4](http://www.sigma-imaging-uk.com/lenses/dclenses/30mm.htm) (price
TBD) and a [18-125mm
f/3.5-5.6](http://www.sigma-imaging-uk.com/lenses/dclenses/18-125mm.htm)
(\~£230). The 30mm would become a nice replacement for my 50mm f/1.8
which became an 80mm with DSLR, and the 18-125mm seems like a good
compact general purpose to tele lens.

<small>NP: <cite>Feed Me Weird Things</cite>, Squarepusher</small>
