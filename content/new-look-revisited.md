Title: New Look, Revisited
Date: 2002-09-13 20:50
Tags: tech
Slug: new-look-revisited

I gave up on the XML + mod\_perl + other mojo, and went straight back to
basics with [Blosxom](http://www.raelity.org/apps/blosxom), a very
simple and lean blogger. Plain text files, a single perl script, and a
few templates give me blogging niverna.
