Title: New Maintainer for Postr!
Date: 2009-11-12 10:49
Tags: tech
Slug: new-maintainer-for-postr

After months of neglect by myself, Postr has a new maintainer! Step
forward Germán Póo-Caamaño, everyone's favourite Chilean, who has been
hard at work migrating to
[git.gnome.org](http://git.gnome.org/cgit/postr), merging patches and
fixing bugs (the Upload button works!), and creating a [new project
page](http://projects.gnome.org/postr/).

Now all I need is for someone to adopt Sound Juicer...
