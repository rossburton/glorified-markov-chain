Title: New Music
Date: 2005-02-14 00:00
Tags: life
Slug: new-music

Last week I was very, very, bad and spent a while at the [Second
Sounds](http://www.secondsounds.com/) web site, basically working from A
and going through to Z. £40 and a few days later, a pile of CDs turned
up:

-   <cite>100th Window</cite>, Massive Attack
-   <cite>Vehicles and Animals</cite>, Athlete
-   <cite>Out Of Season</cite>, Beth Gibbons and Rustin Man
-   <cite>The Very Best Of</cite>, The Eagles
-   <cite>Dub Fever</cite>, King Tubby
-   <cite>Gorillaz</cite>, Gorillaz
-   <cite>Remedies</cite>, Herbaliser

Ahem. I best find more space in the CD rack...
