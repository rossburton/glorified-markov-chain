Title: New Phone, SyncML Hell
Date: 2006-03-08 10:15
Tags: tech
Slug: new-phone-syncml-hell

Yesterday my new phone, a Nokia 6230i, arrived. This morning I tried to
use Multisync to restore the backup of the contacts I took trivially
(via IrMC) from my old K700i.

Well, that wasn't trivial. The 6230i uses SyncML which is groovy and
everything, but is also **hell** to configure. Has anyone actually made
this work? Ideally I'd have the phone as the SyncML server so that I can
start syncs from my laptop running Multisync, but the manual for the
phone doesn't exactly make it obvious how I'm supposed to connect to a
machine via something other than GPRS (I want to use Bluetooth of some
sort).

Help!
