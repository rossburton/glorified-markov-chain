Title: New Phone
Date: 2004-09-23 00:00
Tags: tech
Slug: new-phone

Today my new mobile phone, a cute little Sony Ericsson K700i, arrived.
This is a marvel of technology which <cite>Just Works</cite> and
<cite>Does The Right Thing</cite> from beginning to end.

First I had to move my address book from old phone (a T68i). This isn't
as easy as just it used to be, when one could save it on the SIM, as
both my old and new phones use a more powerful data format and you lose
data when exported to the SIM. I knew that both phones supported IrMC, a
synchronisation protocol, so I installed Multisync. Despite having a
rather overly-complex interface, it pretty much does what it says. I
created a new synchronisation pair, one end of which was a Bluetooth
phone and the other end was the Backup source, which simply stores what
it is sent. Doing the synchronisation was trivial: point the Bluetooth
source at my old phone and ask it to sync, and seconds later I had all
of my contacts on my laptop in vCards. Change the Bluetooth source to my
new phone, tell the Backup source to Restore All, and ask it to sync
again. Another 5 seconds later and my address book is on the new phone,
complete with Home/Work/Mobile annotations.

Next, to do something about the look of the interface. The default theme
is not too bad, but I do feel like a walking Vodafone advert. I quickly
searched around the Internet and downloaded a number of `.thm` files,
which are themes. I expected these to be some proprietary format, but
Nautilus did a MIME sniff and swore they were tarballs. I doubted this,
double-clicked, and the theme opened in File Roller... Themes for this
phone are a tarball of images (PNG, JPEG and GIF for the themes I had)
and an XML file to define the theme. Getting the themes onto the phone
was no trouble at all thanks to Edd's excellent `gnome-bluetooth` tools,
right-click on the file, press <cite>Send via Bluetooth</cite> and
select the right phone.

But the surprises didn't end there... I had a look through the supplied
images, most of which were photos but there were a few short (and
mediocre) movies. One of them caught my eye as it had very sharp lines
and flat colours, and found out that it is actually an animated SVG
file. Invalid is may be (the `xlink` namespace isn't declared) but
Inkscape could still open it, despite not understanding animated SVGs.

So what is next? The phone has a calendar/task list so I'll probably try
(again) to use the Evolution calendar for more than birthdays, and
export it to the phone (and my iPod, so I've no excuse for missing an
appointment). All in all, a very satisfying play with a new phone. I'm
don't expect the media player to support Ogg Vorbis or Ogg Theora, but
XML, SVG and PNG is a very good step in the right direction. Kudos to
Sony Ericsson!

<small>NP: <cite>Music For The Mature B-Boy</cite>, DJ Format</small>
