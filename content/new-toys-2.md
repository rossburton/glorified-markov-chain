Title: New Toys
Date: 2005-05-24 13:05
Tags: life
Slug: new-toys-2

Today was a good day for toys.

My
[LinkStation](http://www.buffalotech.com/products/product-detail.php?productid=71&categoryid=16)
is now booting Debian Unstable, and have finally put a decent version of
Samba on it so I can mount drives with CIFS. Apart from being a nice
small network storage device, now that it's Debian I can do Interesting
Things with it. My first hack will be to add a USB sound card, so that I
can play Internet radio streams into my hifi.

Then those nice people from CityLink arrived and gave me a [Canon Ixus
400](http://www.burtonini.com/photos/Misc/ixus.jpg) we bought off eBay.
As much as I love the 300D, it's not exactly a pub camera. The Ixus is
far smaller and I'll probably be bringing it to GUADEC.

My final toy was a [Velbon MAXi
343](http://www.velbon-tripod.com/maxi.htm) tripod. For a bargain price
of £50 this is a pretty good tripod, it's very small when packed (45cm)
but expands to 155cm+30cm, which is plenty for most people. It's full of
little nice features like rubber feet which screw in to reveal spikes,
legs which expand by flicking a switch, and an all-metal head.

[![Tripod](http://www.burtonini.com/photos/Misc/thumb-tripod-1.jpg){
}](http://www.burtonini.com/photos/Misc/tripod-1.jpg)
[![Tripod](http://www.burtonini.com/photos/Misc/thumb-tripod-2.jpg){
}](http://www.burtonini.com/photos/Misc/tripod-2.jpg)
[![Tripod](http://www.burtonini.com/photos/Misc/thumb-tripod-3.jpg){
}](http://www.burtonini.com/photos/Misc/tripod-3.jpg)

In other news, I'm developing a snotty cold. I hope it will go before
GUADEC...

<small>NP: <cite>100th Window</cite>, Massive Attack</small>
