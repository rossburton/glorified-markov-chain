Title: New Toys
Date: 2004-09-09 00:00
Tags: life
Slug: new-toys

Ah, new toys. I worked at home Tuesday without telling Vicky of any
particular reason apart from "because I can", and waited for two
deliveries...

First a new lens for our camera arrived, a 50mm f/1.8. It's not USM so
the focus motor sounds more like a scream than a whisper, but at £55 it
was a bargain. Now I get to play around with low-light photography.

Secondly, a new monitor, an Iiyama E431S 17" LCD monitor. They claim
it's "ivory" which I was hoping would be white, but they mean "beige".
However, the image quality is great and I was pleased to see a sRGB
button in the menus. All I need now is a ICC X extension and my monitor
calibration tool is useless.

I didn't tell Vicky about all this as she has been saying how ugly my
computer is recently -- 17" monitor and a mid-tower, beige and clunky,
until the original iMac came out I didn't have this problem :) -- and I
thought I'd surprise her. When Vicky finally noticed yesterday evening
she was really pleased, and having lots of space on the desk is...
liberating.

<small>NP: <cite>Best of Sting and The Police</cite></small>
