Title: New version of XChat-Notify
Date: 2005-04-26 15:57
Tags: tech
Slug: new-version-of-xchat-notify

I finally got around to merging a patch sent by Bas van der Lei, so I
can now release [version 0.2 of
xchat-notify](http://www.burtonini.com/computing/notify.py). Hooray!

Not a lot of changes, I made a better choice of icon (it now uses the
themed xchat icon), and Bas added a tooltip. The next stage is to make
clicking on the icon show XChat. After that, I think it's finished.

<small>NP: <cite>Fear Of Fours</cite>, Lamb</small>
