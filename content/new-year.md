Title: New Year
Date: 2004-01-01 00:00
Tags: life
Slug: new-year

Happy New Year everyone!

Hopefully it will be a good one. New hard drive for the laptop coming
soon, meaning far more space for hacking on cutting-edge GNOME. Getting
married in July, to my wonderful fiancée Vicky. And maybe even the next
release of Debian will be out in 2004, which will contain \~20 of the
packages I maintain.

Glynn: yes, Counting Crows rock and <cite>August and Everything
After</cite> was a wonderful album (one of my favourite albums), but
they haven't been able to match it since.

NP: [Parachutes](http://www.amazon.co.uk/exec/obidos/ASIN/B00004U9MS),
by Coldplay.
