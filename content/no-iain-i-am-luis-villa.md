Title: No Iain, I Am Luis Villa
Date: 2008-04-17 14:40
Tags: tech
Slug: no-iain-i-am-luis-villa

Iain, you are [clearly an
imposter](http://blogs.gnome.org/iain/2008/04/17/i-am-luis-villa/). And
this [perfect-sighted
intruder](http://tieguy.org/blog/2008/04/16/new-headshot/), whoever he
is, should be hunted down, because I am Luis Villa!

![I Am Luis!](http://burtonini.com/images/i-am-luis.jpg)
