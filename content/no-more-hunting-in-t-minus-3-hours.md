Title: No More Hunting in T Minus 3 Hours
Date: 2005-02-17 21:03
Tags: life
Slug: no-more-hunting-in-t-minus-3-hours

As of midnight tonight the barbaric "hobby" of fox hunting is finally
banned in England, so the news was saturated with outside broadcasts
from a mix of resigned last-ever hunts and determined last-legal hunts.
The resigned people have cunning alternatives involving artifical fox
scent and running around in the woods for a day, which seems like it
would work just as well without the scent and dogs; and the defiant
people are telling the world about all of the loopholes they've found
and that no amount of prison and fining will stop them hunting foxes
with dogs.

What strange people. So far the best argument for hunting foxes is that
it's "an age-old tradition" (the argument about keeping fox numbers
under control as they are pests doesn't really work with me, when many
weekends they don't actually kill any foxes), so I presume these are the
same people who want to bring back public hanging, slavery, child
beating, no voting for women, and other "age-old traditions" which have
been recently stopped.

Most amusing were the people moaning about the changes to their lives
after the ban. Future circulation of <cite>Horse and Hound</cite> is
unpredicable (unless it turns into an underground hunting magazine,
<cite>Foxx 'n' Hounz</cite>), shops selling hunting gear have seen a 90%
drop in sales, but the most amusing (in a dark way) was the man who
claimed he'd have to shoot all 90 of his hounds and he doesn't want to.
I'm pretty sure there is a better solution to surplus dogs: giving
away/selling them as pets maybe (assuming they don't chase and kill
other animals, like domestic cats), or even selling them to people in
other countries where fox hunting is practised. I hear the south of
France is getting ready for a boom in fox hunting from British people,
and frankly they are welcome to them.

<small>NP: <cite>Sketches of Spain</cite>, Miles Davis</small>
