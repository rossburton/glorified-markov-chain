Title: No News Is Good News
Date: 2003-09-29 00:00
Tags: tech
Slug: no-news-is-good-news

For people out there who are not running Sound Juicer from CVS (boring
fools), I'll provide a little update.

-   The album artist on multiple-artist CDs is handled correctly
    at last. I finally ripped a CD which was tagged correctly in
    MusicBrainz!
-   I've added error handling support, so if the pipeline has a problem
    you'll actually know about it.
-   I've also made patches for GStreamer, so the `cdparanoia` element
    actually reports errors.
-   Frederic Crozat has fixed various problems when cancelling the rip,
    and fixed drive detection with using `devfs`.
-   Bastien Nocera has been quite so far, only adding a dialog for when
    the `cdparanoia` element can't be found. :) However, I think he
    cracked MPEG encoding crashing bug today, so that is good.

I also plan on getting the threaded MusicBrainz lookup sorted in the
next few days. Now that the code base is working fine with signalled
metadata testing the threaded signal emision should be trivial.
