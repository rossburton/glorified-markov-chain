Title: Not Long Now
Date: 2004-06-23 00:00
Tags: life
Slug: not-long-now

<small> \[non-GNOME/Debian posting: <cite>Planet</cite> peeps skip this
if you are after linked list implementations or arguments about the
Social Contract\] </small>

It's not long now until the wedding -- only 3½ weeks left! The plans are
slowly coming together: hopefully no-one else has left the band, we've
finally picked two readings (<cite>The Promise</cite> and <cite>The Owl
and the Pussycat</cite>), the bar license if off to the magistrates and
we've changed the reception colour scheme again. It does seem to us that
either people are ignoring the gift list we spent weeks arranging, or
are leaving it until the last minute: despite nearly 100 people coming
to the reception only 10 have selected a gift. C'mon people, the good
ones are going fast! And we want that new kitchenware! `:-)`

We also finally had most of the replies back, so we sorted through them
and updated the attending numbers. Originally I thought that a
75-capacity room for the ceremony would be big enough, but obviously
not. So far we're at 74 and we've still got a few unconfirmed members of
family. Luckily we're not having the ceremony in the local registry
office which I believe only holds 60-odd people: then we'd be forced to
pick from family as well as friends...

Sometimes the arranging of the day overtakes the reason for the day, so
I have to sit back and think about why we're doing this. That puts a
smile on my face, and despite worrying about what could go wrong (a
trait of mine I'm not particularly fond of) I'm pretty confident the
party will be wonderful. How could it not be?

<small>NP: Nothing. Damn minidisc player broke.</small>
