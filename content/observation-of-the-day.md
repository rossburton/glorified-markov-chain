Title: Observation Of The Day
Date: 2004-05-11 00:00
Tags: tech
Slug: observation-of-the-day

Several short but sweet observations of the day.

-   <cite>10,000 Hz Legend</cite> by Air is very, very poor compared to
    <cite>Moon Safari</cite>. The first two tracks are an exercise in
    strange audio effects on vocals, and track 3 sounds like a bad 80s
    rock song.
-   How does anyone get anything done with a `make` which doesn't
    support pattern matching rules? Being forced to use a non-GNU `make`
    is right up there with dripping water torture.
-   Today I discovered a very nice Toshiba laptop, which is totally
    unavailable to the European market. I suspect it's a Japanese model,
    and was *very* sexy (as far as laptops can be, anyway).

<small>NP: <cite>10,000Hz Legend</cite>, Air.</small>
