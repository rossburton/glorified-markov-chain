Title: Offered
Date: 2006-09-18 22:33
Tags: life
Slug: offered

Thanks to the recent purchase of a EOS 20D, I have a good condition
fully boxed Canon EOS 300D (that's a digital SLR camera) with the kit
18-55mm lens, all of the accesories, a UV filter and 1GB CF card to
sell. I'll be hitting eBay in a few days with this, but I thought I'd
see if there is anyone out there who wants it first. I'm thinking that
£250 is a reasonable price.

Interested? [Email me](mailto:ross@burtonini.com).
