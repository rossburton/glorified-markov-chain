Title: OfflineIMAP, ConsoleKit, GNOME Keyring
Date: 2008-11-04 20:00
Tags: tech
Slug: offlineimap-consolekit-gnome-keyring

Over the weekend I finally got fed up with Evolution struggling to
connect to work's "IMAP" server (Exchange 2007), and switched to using
OfflineIMAP to sync the mail to a local Maildir. This as expected worked
pretty well, and I'm now hidden from the nasty lag on the server.
However, I've had to write my top secret Intel password into
`.offlineimaprc`, which sucks. Then I had a cunning plan...

GNOME Keyring will store passwords in a pretty secure manner, so somehow
I need to fetch the password from there. A quick look at the OfflineIMAP
manual revealed that I can write Python functions which return the
password, so I should be abe to hook into the keyring from OfflineIMAP.
This should be fairly simple:

    import gobject, gnomekeyring

    # The keyring needs to know the application name
    if gobject.get_application_name() is None:
      gobject.set_application_name("offlineimap")

    def keyring(user, host):
      keys = gnomekeyring.find_network_password_sync(user=user, server=host, protocol="imap")
      # First one will do nicely thanks
      return keys[0]["password"]
    ...
    remotepasseval = keyring("rburton", "imapmail.intel.com")

After writing a [small
tool](http://burtonini.com/bzr/keyring-utils/set-password.py) to add the
key to the keyring, to my surprise this worked first time. I bounced
with glee, but ten minutes later I had error messages from OfflineIMAP
running from cron in my inbox...

GNOME Keyring uses an environment variable to find the daemon, which
isn't set in a cron environment. GNOME Keyring will fall back to using
DBus to find the daemon, but the DBus session bus environment variable
isn't set. DBus will fall back to reading the session bus address from
the X root window, but DISPLAY isn't set so that doesn't work either...
EPIC FAIL.

But, I thought, I upgraded to Network Manager 0.7 last week which bought
in ConsoleKit. If I ask ConsoleKit for my sessions I should be able to
find a session with has an X connection, then I can set DISPLAY
appropriately and then the chain described above will work, and I'll
have my password. Shockingly, this worked first time too:

    import dbus, os
    if not os.getenv("DISPLAY"):
      # Get the ConsoleKit manager
      bus = dbus.SystemBus()
      manager_obj = bus.get_object('org.freedesktop.ConsoleKit', '/org/freedesktop/ConsoleKit/Manager')
      manager = dbus.Interface(manager_obj, 'org.freedesktop.ConsoleKit.Manager')
      
      # For each of my sessions..
      for ssid in manager.GetSessionsForUnixUser(os.getuid()):
        obj = bus.get_object('org.freedesktop.ConsoleKit', ssid)
        session = dbus.Interface(obj, 'org.freedesktop.ConsoleKit.Session')
        # Get the X11 display name
        dpy = session.GetX11Display()
        if dpy:
          # If we have a display, set the environment variable
          os.putenv("DISPLAY", dpy);
          break

(man, I really with python-dbus had a better syntax for getting objects
with a specific interface)

So there you go, integrating OfflineIMAP with the GNOME Keyring via
ConsoleKit and DBus. Surprisingly this was pretty easy to do, thanks to
DBus and the magic provided by ConsoleKit it is 100% hack free.
