Title: Oh Dear Lord
Date: 2006-02-08 15:30
Tags: tech
Slug: oh-dear-lord

[This](http://listengnome.free.fr/) has quite
[amazing](http://listengnome.free.fr/img/screenshot/accueil.png)
[amounts](http://listengnome.free.fr/img/screenshot/wikipedia.png) of
[crack](http://listengnome.free.fr/img/screenshot/albumcover.png).

It looks like someone thought, "Hey, some people like Muine and some
other people like Rhythmbox. Let's glue them together!". Please make it
stop.

**Update:** apparently people think I'm mocking the author. Yeah, maybe
a little. However, there are some serious cracky features in there,
which I personally don't like, and as this is a personal blog this is
where I get to say that sort of thing. Of course there is a hell of a
lot of effort in this program (albeit in Python so its easier to get
stuff working), but why is this another program?

We already have music players. If I only count the blessed and
pratically-blessed players, there is <cite>Totem</cite>,
<cite>Rhythmbox</cite> and <cite>Muine</cite>, all with different UI
designs. The author of <cite>Listen</cite> must have at least partially
liked one of those (from the screenshots he liked the playlist of
<cite>Muine</cite> but the overall design of <cite>Rhythmbox</cite>) and
then *worked on one of those*, say adding a pane to Rhythmbox to do what
he wanted instead of duplicating large amounts of non-trivial code. It's
likely that the result of this would look nothing like
<cite>Listen</cite>, as I still firmly believe that it is ugly.

Of course what he does in his time is his choice and none of my
business, so if he wants to write another music player he can, but if
someone wants to make a real contribution to the community they should
interact with the community in the first place. Otherwise it's just a
pet project and if it duplicates existing programs, will likely remain a
pet project. Maybe this comes down to centralised source code verses
distributed source code (CVS vs Arch), but I doubt it. Downloading a
tarball to start patching is easy, and for most people creating an Arch
branch isn't exactly trivial.

**Update 2:** [This is a very amusing open
letter](http://www.sacredchao.net/~piman/journal/archives/2006/02/08/copyright-infringement/).
