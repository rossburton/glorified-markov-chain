Title: Oh My God
Date: 2005-06-06 20:25
Tags: tech
Slug: oh-my-god

What an evening! Not only did [Apple actually switch to Intel
processors](http://www.apple.com/pr/library/2005/jun/06intel.html) for
their laptop and desktop ranges, but Debian Sarge has actually been
released!

Well, the announcement for Sarge isn't out yet, but the FTP server says
it all:

    lrwxrwxrwx    1 1176     1176            5 Jun 06 18:33 stable -> sarge
    lrwxrwxrwx    1 1176     1176            4 Jun 06 18:33 testing -> etch

Woohoo!
