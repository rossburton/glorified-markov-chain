Title: One Railway
Date: 2004-04-05 00:00
Tags: life
Slug: one-railway

This week the rail operator where I live, [WAGN](http://www.wagn.co.uk),
was taken over by [One Railway](http://www.one-westanglia.co.uk/).
<abbr title="West Anglia Greater Northern">WAGN</abbr> was an amusing
name in a way, and One is going for the "unity" they are bringing by
merging several different train operators in the area (maybe it wasn't a
good idea to sell the train network, Maggie) but I am waiting for the
jokes about what exactly there is "one" of. One train? Just the One
driver? One safety inspector? Only One wrench for the engineers? All
seem possible at times...

NP: Mustang Sally, Wilson Pickett
