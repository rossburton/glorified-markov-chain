Title: One Year
Date: 2005-11-01 09:40
Tags: life
Slug: one-year

Wow, a year ago today I woke up and started my [first
day](http://www.burtonini.com/blog/life/job-20041103) working for
[Opened Hand](http://www.o-hand.com/). It's been an absolute riot, when
I started it was just Matthew and myself and the Nokia work was a
mystery project, now the Nokia 770 is being shipped to developers and
there are five programmers.

In other news, we finally got the mortgage agreed so hopefully will be
moving house in December.

<small>NP: <cite>Bitches Brew</cite>, Miles Davis</small>
