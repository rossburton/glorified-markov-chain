Title: Open Sourcing of eds-sync
Date: 2007-05-25 10:20
Tags: tech
Slug: open-sourcing-of-eds-sync

It took a while, but Nokia finally relicensed
[eds-sync](https://garage.maemo.org/svn/eds/trunk/eds-sync/) under the
GPL. Hooray!

eds-sync is one of the many components that you don't see on the Nokia
N800, but is essential to the integrated chat/voip interface. It's role
is basically to do full two-way synchronisation between any number of
remote rosters (generally Jabber/XMPP rosters on the N800) and the
Evolution Data Server addressbook. It also downloads avatars, so that
they are available on disk for display in applications.

<small>NP: <cite>Live at the Jazz Café</cite>, Fink</small>
