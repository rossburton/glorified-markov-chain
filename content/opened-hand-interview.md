Title: Opened Hand Interview
Date: 2005-06-16 17:30
Tags: tech
Slug: opened-hand-interview

Last week [Opened Hand](http://www.o-hand.com) was interviewed about our
involvement with the [Nokia 770](http://www.nokia.com/770), the
resulting Newsforge article [just went
online](http://mobile.newsforge.com/mobility/05/06/08/1948202.shtml?tid=97&tid=2).

<small>NP: <cite>Take London</cite>, The Herbaliser</small>
