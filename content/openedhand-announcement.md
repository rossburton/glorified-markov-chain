Title: OpenedHand Announcement
Date: 2006-10-16 17:35
Tags: tech
Slug: openedhand-announcement

It came to our attention at the Summit that people were not aware that
OpenedHand had hired GNOME theme master, Thomas Wood. Well, we have, he
started at the beginning of the month and has been rocking since.

Welcome Thomas!
