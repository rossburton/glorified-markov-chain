Title: OpenedHand World Domination Progressing Well
Date: 2006-05-17 14:50
Tags: tech
Slug: openedhand-world-domination-progressing-well

The plans for World Domination by [OpenedHand](http://o-hand.com) are
going well. So well infact that we need more help: we're hiring an
Embedded Linux System Engineer and a Technical Project Manager. For more
details, see the [OpenedHand Jobs page](http://o-hand.com/jobs/).

<small>NP: <cite>Always Beating</cite>, L'Onomatopeur</small>
