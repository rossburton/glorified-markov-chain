Title: OpenMoko
Date: 2007-08-21 10:30
Tags: tech
Slug: openmoko

Sean Moss-Pultz from OpenMoko [announces the new
interface](http://lists.openmoko.org/pipermail/announce/2007-August/000018.html):

> “We went back to the drawing board with OpenedHand -- lead by their
> vast experience with GTK+, Matchbox, and mobile user interfaces -- and
> redesigned an incredibly promising new interface.”

I finally got my hands on a GTA01 last week, which was promptly flashed
with the latest software. The new interface is pretty damn cool, with
smooth colours, clear icons and subtle gradients. I'm sure Thomas will
blog with more details and screenshots at some point.
