Title: OProfileUI
Date: 2007-04-16 19:30
Tags: tech
Slug: oprofileui

Excellent news for the day: Rob announced
[OProfileUI](http://projects.o-hand.com/oprofileui) and released version
0.1.1. OProfileUI is a graphical interface to OProfile, similar to
Sysprof, but with the advantages that it uses OProfile and has a
client/server architecture. Why is this so great?

**OProfile:** [OProfile](http://oprofile.sourceforge.net/) is in
mainline Linux and shipped in most distributions. OProfile can profile
both user space and kernel space, and works on x86-32, x86-64, IA64,
PPC, ARM, MIPS, Sparc and more (although OProfileUI has only been tested
against x86-32 and ARM so far).

**Client/Server:** with a client/server design you can run the
lightweight `oprofile-server` on the target device (this only links to
GLib) and run the interface on your desktop machine, moving the
intensive work of analysing the profile data from the target to the more
powerful desktop. This also lets you profile one machine doing tasks
where it is normally tricky to run a profiler, such as logging in to
GNOME from GDM, by moving the control interface to another machine.

I've been fanboying [Rob](http://www.robster.org.uk) over this for some
time now, I'm really pleased that it has finally been released. Let's go
profile!
