Title: ORBit--; DBus++
Date: 2009-08-17 20:47
Tags: tech
Slug: orbit-dbus

Today I finally merged the `dbus-hybrid` branch of Evolution Data Server
into `master`, which ported the addressbook part of EDS to use DBus
instead of Bonobo. There are bound to be some bugs in this so if you are
running EDS from master and find a bug, *please* file it in GNOME
Bugzilla.

Now to finish reviewing the calendar port and merge that too...

<small>NP: <cite>Session 2</cite> - The Herbaliser Band</small>
