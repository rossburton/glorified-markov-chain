Title: OSNews Does It Again
Date: 2005-03-01 09:47
Tags: tech
Slug: osnews-does-it-again

Yet again, OS News has taken a simple [post to a mailing
list](http://mail.gnome.org/archives/desktop-devel-list/2005-February/msg00453.html)
and turned it into a [full
article](http://osnews.com/comment.php?news_id=9850), complete with
corporate overtones and stating that it's a final decision.

I'm starting to re-adjust my belief that OS News is slightly better than
Slashdot...

<small>NP: <cite>Hot Shots II</cite>, The Beta Band</small>
