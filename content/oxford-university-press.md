Title: Oxford University Press
Date: 2006-05-08 13:30
Tags: life
Slug: oxford-university-press

OUP rock my world. Thanks to a [fantastic
deal](http://www.oup.com/online/englishpubliclibraries/), the public
libraries in England have shared the cost of a two-year nation licence
for a range of their online resources. I just go to [Oxford
Reference](http://www.oxfordreference.com/), enter my library card
number, and bingo: access to the full text of the OED, and much more.

<small>NP: <cite>The Garden</cite>, Zero 7</small>
