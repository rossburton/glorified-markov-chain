Title: Packaging Duties
Date: 2003-07-23 00:00
Tags: tech
Slug: packaging-duties

A hell of a lot happened in two weeks... Gossip had two releases, so
Jordi updated my LoudMouth packages and uploaded a Gossip package for me
to take over later (should be uploading today).

Also `gtk-doc` 1.1 finally made it into Sid, so I can build
`libgircclient` again. Yes, this means that updated GnomeChat packages
are building as I type. I'm pointing people to my Amazon.co.uk wishlist
for this :)

In other news, it appears that GNOME 2 is entering `testing` well, as
most of my packages have finally been accepted.
