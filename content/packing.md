Title: Packing
Date: 2006-10-03 20:45
Tags: life
Slug: packing

Tomorrow I fly to Boston for the Summit and the embedded GNOME hackfest.
Packing clothes took about five minutes: pants, socks, tees, jumpers.
Packing the electronic toys was a bit more complicated...

-   ThinkPad X60 plus charger
-   Canon EOS plus charger
-   Canon IXUS plus charger (for Vicky)
-   Nokia 770 plus charger
-   Zaurus SL-C1000 plus charger and wifi card
-   USB card reader
-   My Nokia phone charger
-   Vicky's phone charger

*Please* can people start using the same chargers? I'm impressed that
the Zaurus charger is actually a PSP charger, but I'm not happing having
to take two Nokia chargers and two Canon chargers.
