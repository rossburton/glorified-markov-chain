Title: About Ross
Slug: about

Ross Burton.

geek, mountain biker, father.

I am a software engineer at [Arm], as a Principal Engineer on the [Yocto Project].  Previously I worked at [Intel] on the Yocto Project but also Moblin, MeeGo, and Tizen; generally on the netbook UX but with a brief diversion to a kick-arse TV prototype that didn't quite manage to live on as [Guacamayo].

Prior to Intel I was single-handedly responsible for turning OpenedHand from a one-man band to a company with employees (by being the first employee), where we did ground-breaking work on embedded Linux for Nokia, Garmin, Vernier, and more.

I also like to ride my bike up and down hills badly, avoiding smooth roads as much as possible.

[Arm]: https://www.arm.com/
[Intel]: https://www.intel.com/
[Yocto Project]: https://www.yoctoproject.org/
[Guacamayo]: http://guacamayo.github.io/
