Title: Parkinson
Date: 2004-03-05 00:00
Tags: life
Slug: parkinson

Last night Vicky and myself went to the good ol' BBC to see
<cite>Parkinson</cite> being recorded. My suspicions that this filming
would be airing this Saturday were confirmed — the guests were Patrick
Stewart, Lionel Richie and Harry Connick Jnr. It was an excellent show,
Parkie and Patrick Stewart were excellent together, both slowly
regresssing into Northern accents and reminising about the poverty of
their youth. Harry Connick Jnr played two classic songs, and is a very
relaxed, amusing and shocking ("pile of buttons", eek) guest. Lionel
Richie played his new single and was also good fun. Even the warm-up
comedian was quite amusing, making the production team the butt of most
of his jokes and learning how to chat up Harry Connick Jnr's band in
Turkish.

Overall a great night out, and considering the cost of the tickets
(nothing), great value for money! I shall be investigating into getting
ourself on the very long queue for <cite>Have I Got News For You</cite>
next.

NP: Simple Things, Zero 7.
