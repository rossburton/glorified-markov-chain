Title: Patches
Date: 2004-08-24 00:00
Tags: tech
Slug: patches

People who think it's acceptable to mail me patches for features I am
interested in, whilst at the same time re-indenting the entire file,
should be forced to remove the reindentation in a hex editor by hand
before sending the patch in again.

That, or die in a great big chemical fire, or something.
