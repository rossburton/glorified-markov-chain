Title: So long Wordpress, thanks for all the exploits
Date: 2016-08-26 23:10
Tags: life
Slug: so-long-wordpress

I've been meaning to move my incredibly occasional blog away from Wordpress for a long time, considering that I rarely use my blog and it's a massive attack surface.  But there's always more important things to do, so I never did.

Then in the space of ten days I received two messages from my web host, one that they'd discovered a spam bot running on my account, and after that was cleared and passwords reset *another* that they discovered a password cracker.

Clearly I needed to do something.  A little research led me to [Pelican], which ticks my "programming language I can read" (Python), "maintained", and "actually works" boxes.  A few evenings of fiddling later and I just deleted both Wordpress and Pyblosxom from my host, so hopefully that's the end of the exploits.

No doubt there's some links that are now dead,  all the comments have disappeared, and the theme needs a little tweaking still, but that is all relatively minor stuff.  I promise to blog more, too.

[Pelican]: http://www.getpelican.com
