Title: Perils Of Commerical Software
Date: 2007-11-08 19:00
Tags: tech
Slug: perils-of-commerical-software

    daniels: lightroom makes me so unbelievably content
    daniels: if i were a doe-eyed schoolgirl
    daniels: and you were showing me this in the back of your car
    daniels: i'd let you take me, there and then

I knew showing Daniel <cite>Adobe Lightroom</cite> was a bad idea... It
is a truly majestic application though, with that killer combination of
a very clean interface and incredibly well thought-out features.
