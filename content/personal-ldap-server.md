Title: Personal LDAP Server
Date: 2004-01-21 00:00
Tags: tech
Slug: personal-ldap-server

I've been thinking recently about the fact that I have 4 separate
address books on my computers, all managed by Evolution. My laptop has
one, my home desktop has one, my work desktop has one, and we have
started configuring an LDAP server at work too. Obviously none of these
are synced and this is a right mess.

So, why can't I run a personal LDAP server? I was thinking about a
minimal LDAP implementation (just enough to keep Evolution happy) which
writes Maildir-style to many files, meaning Unison can be used to sync
any changes. Or write a new backend for OpenLDAP which writes multiple
files instead of this posh bdb business. Or use OpenLDAP with the
default backend and use the LDAP Sync protocol, which may or may not do
what I want.

Of course this hits problems -- an LDAP server needs to open a port, so
what port does it open if it is started by a user, and what happens if
multiple people login on the same machine. SLP or D-Bus could be used to
find the LDAP server, but this is getting rapidly too complicated.

I know Havoc was wondering about LDAP for every user, and I've heard
rumours that MacOS X comes configured with a bit of the LDAP server for
every user. If anyone knows of answers to my problems, please contact
me.

NP: [From the Choirgirl
Hotel](http://www.amazon.co.uk/exec/obidos/ASIN/B0000062S6/), by Tori
Amos (very loudly).
