Title: Philosophical Sifting
Date: 2007-12-10 09:30
Tags: life
Slug: philosophical-sifting

> “The Daily Mail, as you know, is engaged in a philosophical project of
> mythic proportions: for many years now it has diligently been sifting
> through all the inanimate objects in the world, soberly dividing them
> into the ones which either cause - or cure - cancer. The only tragedy
> is that one day, amongst the noise, they might genuinely be on to
> something, and we would simply laugh. That day has come.” ― [<cite>A
> Rather Long Build Up To One
> Punchline</cite>](http://www.badscience.net/2007/12/a-rather-long-build-up-to-one-punchline/)

A classic example of the <cite>Mail</cite> taking a valid scientific
press release (that *is* a surprise) and twisting it into a scare story
for no real reason. I wish I could understand how this works to sell
papers, although I guess at the end of the day regurgitating a press
release with added anger and Moral Outrage without doing any relevant
research is a pretty cheap way of filling column inches.

<small>NP: <cite>F♯ A♯ ∞</cite>, Godspeed You Black Emperor!</small>
