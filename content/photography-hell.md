Title: Photography Hell
Date: 2003-12-17 00:00
Tags: life
Slug: photography-hell

Photography Hell is when you want to, but can't, take a photo.

A few months ago when I was going home I was treated to an incredible
sunset on the way out of London, cumulating in a gorgeous deep purple to
bright orange sky reflected in a lake. This made me wish that I could
just stop the train, get out, and take some photos. However, after a
week or so the sunset started occurring when I was on the tube, so the
wish faded.

The wish is back again, though now I am treated to dawns instead. Sharp
frost and mist across rolling fields, a clear sky and the half-moon,
trees masked against the sun rising, with aeroplane vapour trails
crossing the sky. I really should find the time to take a photography
course, or just experiment, and try and capture some of this.
