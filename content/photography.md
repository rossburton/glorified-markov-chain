Title: Photography
Date: 2005-06-15 10:45
Tags: life
Slug: photography

Last Thursday we had a few hours of summer (it's raining again now) so I
dived out at lunch and took some photos in the garden and the local
cemetery before the inevitable clouds rolled in.

[![photo](http://www.burtonini.com/photos/Random/thumb-img_2419.jpg "Reaching For The Sun"){
}](http://www.burtonini.com/photos/Random/img_2419.jpg)
[![photo](http://www.burtonini.com/photos/Random/thumb-img_2439.jpg "Anonymous"){
}](http://www.burtonini.com/photos/Random/img_2439.jpg)
[![photo](http://www.burtonini.com/photos/Random/thumb-img_2446.jpg "Forgotten"){
}](http://www.burtonini.com/photos/Random/img_2446.jpg)

<small>NP: <cite>Central Reservation</cite>, Beth Orton</small>
