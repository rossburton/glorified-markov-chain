Title: Photos from FOSDEM 2006
Date: 2006-03-02 09:50
Tags: tech
Slug: photos-from-fosdem-2006

Last night I finally got around to putting up the [photos from FOSDEM
2006](http://www.burtonini.com/photos/200602-FOSDEM/). They still need
to be captioned though, and Oh My God I need to create a new theme for
<cite>Tate</cite>.

<small>NP: <cite>Selected Ambient Works 95-92</cite>, Aphex Twin</small>
