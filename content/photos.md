Title: Photos
Date: 2003-12-29 00:00
Tags: life
Slug: photos

So I've scanned the photos on my Dad's cheap-and-cheerful scanner (fast
and colour-accurate scans, but the images are slightly blury) and will
put them online in a series of blog entries. First of all, a final photo
from Kefalonia:

[![](http://www.burtonini.com/photos/Kefalonia/thumb-ross-beach.png)](http://www.burtonini.com/photos/Kefalonia/ross-beach.jpg)

My God, I'm not pulling a silly face!
