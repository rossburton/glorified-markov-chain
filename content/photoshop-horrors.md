Title: Photoshop Horrors
Date: 2008-03-25 09:46
Tags: life
Slug: photoshop-horrors

Thanks to [Photoshop
Disasters](http://photoshopdisasters.blogspot.com/2008/03/daily-mail-dont-do-brown-acid.html),
this Photoshop horror cheered me right up.

![Photoshop
Failure](http://burtonini.com/images/dailymail-cipriani.jpg)  
[Original
source](http://www.dailymail.co.uk/pages/live/articles/sport/rugby.html?in_article_id=543050&in_page_id=1&in_page_id=1),
although the image was just pulled from the site.
