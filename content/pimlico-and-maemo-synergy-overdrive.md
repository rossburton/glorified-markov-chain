Title: Pimlico and Maemo Synergy Overdrive!
Date: 2007-07-11 12:40
Tags: tech
Slug: pimlico-and-maemo-synergy-overdrive

We've had Dates on Maemo for a long time now, but now and again people
asked for Contacts. We never really bothered to build Contacts because
it isn't ported to Maemo, so doesn't fit in to the environment. Well,
yesterday I found myself with a clean Maemo 3.2 Scratchbox (quite
unusual I assure you) so thought I'd build a package anyway. It's not
integrated into Maemo, but it's in [our
repository](http://maemo.o-hand.com) now.

Hopefully this will cause a fanbase of programmers to spontaneously
appear who will help us develop the next generation of Contacts, which
is modular and thus far easier to port to various platforms.

Along with my recent Tasks on Maemo announcement, all of Pimlico is now
available for Maemo. I'll have a N800 with the full suite on at GUADEC
if anyone wants to see it.

<small>NP: <cite>This Too Shall Pass</cite>, Breakage</small>
