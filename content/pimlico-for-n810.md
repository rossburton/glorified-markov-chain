Title: Pimlico for N810
Date: 2008-02-05 20:10
Tags: tasks, tech
Slug: pimlico-for-n810

I *finally* got around to hacking Evolution Data Server to build without
the addressbook, and still work. This isn't hard work, its just very
dull: I must have rebuild EDS from scratch at least twenty times this
evening.

The good news is that there are now Contacts, Dates and Tasks packages
for Chinook, also known as ITOS 2008, also known as the software on the
N800 and N810. Sorry it took so long!
