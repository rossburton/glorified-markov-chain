Title: Pimlico Mailing List
Date: 2007-05-22 10:20
Tags: tech
Slug: pimlico-mailing-list

A quick heads-up for anyone interested in the Pimlico suite of Contacts,
Dates and Tasks. There is now a mailing list available for discussion,
simply mail
[`pimlico+subscribe@o-hand.com`](mailto:pimlico+subscribe@o-hand.com) to
subscribe.

<small>NP: <cite>The Silent Ballet Volume 2</cite></small>
