Title: Pimlico on Maemo Chinook
Date: 2007-12-21 15:30
Tags: tasks, tech
Slug: pimlico-on-maemo-chinook

Every since the Maemo Chinook beta was relased, people have been asking
when we're going to make [Pimlico](http://pimlico-project.org) packages
available. I'll skip over the fact that Pimlico is open source, so
building a package yourself is trivial (I suppose its a good thing that
there are users who can't do that!).

Now, Pimlico consists of Contacts, Dates and Tasks. Contacts doesn't
have a Maemo port as such and the device already has an addressbook of
sorts, so I tend to leave that until last. That leaves Dates and Tasks,
both of which require an all-new Evolution Data Server to be built,
because Nokia strip down EDS for Maemo and don't install the calendar
component to save disk space. This generally shouldn't be a problem as
we've been building replacement EDS packages for some time now which
adds the calendar component and restores some functionality to the
common libraries. So, [Rob](http://www.robster.org.uk/) set about
re-syncing our packaging with the Chinook EDS (there are several
Maemo-specfic patches we obviously need to include) and started a build.
Then our plan started to fall apart.

To cut a long story short, the Chinook's Application Manager has an
additional sanity check which wasn't in previous releases. The gist of
it appears to be that a package <cite>foo</cite> from repository
<cite>A</cite> cannot upgrade package <cite>foo</cite> from repository
<cite>B</cite>. Or, `libebook` from `maemo.org` cannot be upgraded to
`libebook` from `o-hand.com`. I can see how this can stop people
accidently breaking their device by installing broken core packages, but
it's also stopping us provide core packages with enhanced functionality.

Never fear, we have a plan. It's not incredibly pretty and will take a
day or so of tiresome recompiles to get working, but we'll get there.
That said, we've all been busy and now it is the Christmas holiday... so
I wouldn't bet on being able to run Dates and Tasks on your N800/N810
before the new year.

As a reward for patience, however, there are Contacts 0.8 packages for
Chinook in [our repository](http://maemo.o-hand.com/).

<small>NP: <cite>Last.fm Recommendation Radio</cite></small>
