Title: Pimping Contacts and Dates
Date: 2006-08-11 16:15
Tags: tech
Slug: pimping-contacts-and-dates

Those nice people at [Linux.com](http://www.linux.com) have published an
[article about Contacts, Dates, and the DBus port of
EDS](http://www.linux.com/article.pl?sid=06/08/04/2158214). Basically it
says that [Dates](http://projects.o-hand.com/dates) rocks,
[Contacts](http://projects.o-hand.com/contacts) rocks, and so does the
[DBus port of EDS](http://projects.o-hand.com/eds). Rock on, thanks
Nathan!

<small>NP: <cite>Neighbour Radio</cite></small>
