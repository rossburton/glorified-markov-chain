Title: pkg-config files for X.org
Date: 2005-04-22 19:45
Tags: tech
Slug: pkg-config-files-for-x-org

If you want to be one of the cool gang and try things like
[Luminocity](http://live.gnome.org/Luminocity) and
[Xephyr](http://www.freedesktop.org/Software/Xephyr), but can't be
bothered to build all of those X libraries from CVS just to get the
`.pc` files (after all, Hoary's X.org has all of the extensions needed),
why not install `xorg-pkgconfig` from my [Debian
repository](http://www.burtonini.com/debian/)!

It's a total hack: I copied all of the `.pc` files from my laptop, which
has the complete X platform built from CVS on, and changed the paths
from `/home/ross/bin/BUILD` to `/usr`. It's a nasty hack, but it let me
build Luminocity. My next step is to try and build `kdrive` and see if
this idea actually works...

<small>NP: <cite>Lamb</cite>, Lamb</small>
