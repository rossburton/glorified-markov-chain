Title: Plans for GammaarRRr
Date: 2003-08-20 00:00
Tags: tech
Slug: plans-for-gammaarrrr

After discovering my first GammaarRRr user, Maria Blackmore, we had a
long chat about what GammaarRRr should be able to do. After much
thinking I've developed a plan.

Main Features:

-   Black point calibration
-   per-Display gamma calibration for arbitrary target gamma values.
-   A graphical editor for calibrating the gamma
-   A GNOME applet to select gamma corrections quickly

This is the current gamma calibration dialog:

[![Calibration dialog](http://www.burtonini.com/computing/screenshots/gammaarRRr-1.png){width="300"}](http://www.burtonini.com/computing/screenshots/gammaarRRr-1.png)

The applet when it starts will set the gamma correction to the default
setting the user has set for that display. This means that adding the
applet to your panel will make the gamma correction Just Work once it
has been calibrated for the machines you use.
