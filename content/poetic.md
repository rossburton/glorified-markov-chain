Title: Poetic
Date: 2005-12-02 09:22
Tags: life
Slug: poetic

There is something quite satisfying about [Mail
Watch](http://www.mailwatch.co.uk) auctioning a [Daily Mail Christmas
Carols album on
eBay](http://cgi.ebay.co.uk/ws/eBayISAPI.dll?ViewItem&item=4802632556),
with the proceeds going to [Asylum
Welcome](http://www.asylum-welcome.supanet.com/), an asylum-seeker
support charity based in Oxford.

Let the bidding commence!
