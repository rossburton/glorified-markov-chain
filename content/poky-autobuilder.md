Title: Poky Autobuilder
Date: 2007-08-03 11:30
Tags: tech
Slug: poky-autobuilder

God bless buildbot. Once Poky was released Richard, Marcin and myself
started pushing upgrades into Poky trunk, resulting in 96 commits during
the day. These generally worked on our personal machines, but thanks to
the incremental autobuilder (which compiles all of the packages for five
different targets) all of the problems were caught rapidly and fixed by
the end of the day. As a well-done present the nightly autobuilder
(which does a clean build) managed to build everything successfully for
almost all of the targets. Impressive stuff really, without buildbot I
doubt we'd have been able to upgrade autotools, Linux, X, and GNOME in
one day and still produce working images the next.

<small>NP: <cite>Butterfly Wings Make</cite>, Orla Wren</small>
