Title: Poky Linux 3.0 "Blinky" Released
Date: 2007-08-01 11:30
Tags: tech
Slug: poky-linux-3-0-blinky-released

After much blood, sweat and tears, [Poky Linux
3.0](http://pokylinux.org/) (also known as <cite>Blinky</cite>) is
released. Poky is an OpenEmbedded derivative, focused on building GNOME
Mobile-based images for embedded and handheld devices such as the Sharp
Zaurus or the Nokia N800.

Aside from the usual set of software updates and infrastructure
improvements, Blinky contains the first public release of Sato, an
example GTK+/Matchbox based PDA environment aimed at devices with high
resolution, physically small screens. This includes a new Matchbox panel
and desktop, the [Pimlico](http://pimlico-project.org/) suite of PIM
applications, and a fast, clear GTK+ theme.

Pretty much everyone at OpenedHand has been involved in Poky to some
extent, so it's great to see a release. Personally I'm really pleased
with how far Blinky has advanced over the last release, Clyde, and will
shortly be working on a branch of Blinky to integrate the complete GNOME
2.20 release set.

If anyone is interested in Poky then it is easy to try: there are
[pre-built images](http://pokylinux.org/releases/blinky-3.0/) for a
several devices: the Nokia N800 and several Sharp Zaurus models; and
Qemu images for both ARM and Intel. Give it a go and tell us what you
think!

<small>NP: <cite>Love and Affection</cite>, Joan Armatrading</small>
