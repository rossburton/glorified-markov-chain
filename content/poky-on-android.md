Title: Poky on Android
Date: 2007-11-13 18:00
Tags: tech
Slug: poky-on-android

God, [Richard is such a hero](http://www.rpsys.net/wp/?p=13). The day
after Android is released, he has [Poky](http://pokylinux.org) building
and booting on their emulator. There are a number of caveats, such as
starting DBus causes their emulator to crash, but it is the first step
towards being able to boot Poky on any future Android phones.

![Android in
Poky](http://burtonini.com/computing/screenshots/poky-android-2.png){width="253"}

Full-size images and more available on Richard's blog. Note that the
non-existent theming is due to the XSettings not being set, because DBus
won't start.

<small>NP: <cite>Tiger, My Friend</cite>, Psapp </small>
