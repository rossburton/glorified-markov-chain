Title: Poky "Pinky" 3.1 Released
Date: 2008-03-03 18:55
Tags: tech
Slug: poky-pinky-3-1-released

    From: Richard Purdie <richard@openedhand.com>
    Subject: [poky] Poky Version 3.1 (Pinky) Released

    It gives me great pleasure to announce a new release of Poky, version
    3.1 (Pinky).

[Poky 3.1](http://pokylinux.org) is released! We've been hard at work
for this one, mainly deep in the guts to make it more portable and
powerful. Of interest to GNOME developers is that Pinky is shipping a
complete GMAE 2.20 platform, and a plugin for Anjuta to make building
and deploying software for your target device trivial.

We've also got a sweet new web site and possibly the cutest mascot ever.
I, for one, can't wait to stroke a plush Beaver.
