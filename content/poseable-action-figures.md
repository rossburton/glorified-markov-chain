Title: Poseable Action Figures
Date: 2006-09-04 12:00
Tags: life
Slug: poseable-action-figures

Last Friday was Vicky's last day at the OU, as next month she hopes to
be accepted onto a teacher training course for A-level Psychology. On
Friday her desk was covered in fairy lights, cards and presents,
including a Teacher Startup Kit: pain killers, an apple, a diary, and a
poseable action figure of Sigmund Freud.

[![Freud](http://static.flickr.com/88/233655644_21125fd16d_m.jpg){
}](http://www.flickr.com/photos/rossburton/233655644/)

I'm not sure what worries me more: that someone at the OU found a
poseable action figure of Freud, or that somewhere in Cambridge there is
a store that sells Freud action figures, and probably more...

<small>NP: <cite>Piss Frond</cite>, Dead Voices on Air</small>
