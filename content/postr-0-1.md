Title: Postr 0.1
Date: 2006-10-01 17:50
Tags: tech
Slug: postr-0-1

I've finally got around to making a 0.1 release of Postr, my Flickr
uploading tool. It has a few nice features:

-   Supports drag and drop, of both files and raw image data (upload
    straight from the GNOME screenshot tool without saving to a file)
-   Set title, description and tags across a selection of multiple
    images
-   Supports new-style Yahoo! accounts
-   Displays the remaining upload quota for heavy/non-pro users

I'm making a 0.1 release now as I've been working on a [new Flickr
library](http://burtonini.com/bzr/flickrpc/) using Python and Twisted
for asynchronous operation, and want to get a 0.1 release out before I
rewrite everything. You can follow the development in the [Bazaar
branch](http://burtonini.com/bzr/postr/postr.dev), or get the [Postr 0.1
tarball](http://burtonini.com/computing/postr-0.1.tar.gz).

Obligatory screenshot:

![Postr](http://burtonini.com/computing/screenshots/postr-2.png){}
