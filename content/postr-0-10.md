Title: Postr 0.10
Date: 2008-01-04 10:55
Tags: tech
Slug: postr-0-10

A new year, a new release of Postr. This release has some useful bug
fixes. Now to finish off that grand refactoring...

-   Use the GNOME proxy if set
-   Don't try and upload images over 10Mb, as Flickr will reject them
-   Add a big Upload button to the window
-   Fall back on ISO-8859-1 when reading metadata
-   Don't show error dialogs with no message

The [tarball is here](http://burtonini.com/computing/postr-0.10.tar.gz),
and packages for Debian are building now.
