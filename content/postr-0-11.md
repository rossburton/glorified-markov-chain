Title: Postr 0.11
Date: 2008-04-20 16:50
Tags: tech
Slug: postr-0-11

I finally got around to fixing the very annoying text wrapping problem
in postr.dev, I thought I best release Postr 0.11:

-   Add Send To Group options
-   Add Privacy and Safety options
-   Use a multi-line entry for the Description field
-   Show the user's name in the status bar
-   Fix the resizing of the preview

The [tarball is here](http://burtonini.com/computing/postr-0.11.tar.gz),
and packages for Debian have been uploaded.
