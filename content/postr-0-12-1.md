Title: Postr 0.12.1
Date: 2008-05-27 10:00
Tags: tech
Slug: postr-0-12-1

I just made a quick Postr 0.12.1 release to fix authentication with
non-trivial HTTP handler strings. If you can't login to Flickr with
Postr, then this release *should* fix it for you.

The [tarball is
here](http://burtonini.com/computing/postr-0.12.1.tar.gz), and packages
for Debian are being built now.

In other news postr.dev has seen a lot of development and is looking
pretty damn neat at the moment.
