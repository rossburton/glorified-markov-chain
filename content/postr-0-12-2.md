Title: Postr 0.12.2
Date: 2008-06-15 15:10
Tags: tech
Slug: postr-0-12-2

Another point release of
[Postr](http://burtonini.com/blog/computers/postr) which should fix
Flickr authentication for good this time. Also the file size limit has
been increased to 20Mb to match the new Flickr limits.

The [tarball is
here](http://burtonini.com/computing/postr-0.12.2.tar.gz), and packages
for Debian are being worked on next.
