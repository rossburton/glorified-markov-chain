Title: Postr 0.12.3
Date: 2008-12-19 15:00
Tags: tech
Slug: postr-0-12-3

A small point release to fix some small bugs before it's 2009...

-   The Upload button now works
-   Don't delete images if the upload fails

The [tarball is
here](http://burtonini.com/computing/postr-0.12.3.tar.gz), and Debian
packages are building now.

<small>NP: <cite>Live at the Royal Albert Hall</cite>, The Cinematic
Orchestra</small>
