Title: Postr 0.12
Date: 2008-04-23 10:30
Tags: tech
Slug: postr-0-12

A quick Postr 0.12 release, mainly to fix an annoying bug but there are
some neat new features here too.

-   Update the status bar after uploading
-   Add a Switch User menu item
-   Add Add/Remove buttons to the image pane
-   Install the Nautilus extension to the new extension path
-   Don't select groups when the name is clicked
-   Don't display errors when posting to moderated groups
-   Show a warning on exit if there are images to upload (thanks
    Germán Póo-Caamaño)

The [tarball is here](http://burtonini.com/computing/postr-0.12.tar.gz),
and packages for Debian have been uploaded.
