Title: Postr 0.2
Date: 2006-12-05 20:55
Tags: tech
Slug: postr-0-2

![Postr](http://burtonini.com/computing/screenshots/postr-2.png){}

Postr 0.2 is finally out. I didn't get around to finished the all new
Flickr API, but instead there are a lot of bug fixes.

-   Rewrite threading logic to fix various weird missing upload and "4
    of 2" bugs.
-   All-new progress dialog (Ross, Germán Poo Caamaño)

You can follow the development in the [Bazaar
branch](http://burtonini.com/bzr/postr/postr.dev), or get the [Postr 0.2
tarball](http://burtonini.com/computing/postr-0.2.tar.gz).
