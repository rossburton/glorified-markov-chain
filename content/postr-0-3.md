Title: Postr 0.3
Date: 2006-12-16 10:06
Tags: tech
Slug: postr-0-3

Postr 0.3 is out. A few typo fixes in my code, and an update to the
latest `flickrapi.py` to fix uploads after the Flickr servers decided to
be more pedantic. This was proof that I am the only user of Postr, as it
didn't work for a good week or so.

-   Update flickrapi.py to version 10
-   Fix references to the token

You can follow the development in the [Bazaar
branch](http://burtonini.com/bzr/postr/postr.dev), or get the [Postr 0.3
tarball](http://burtonini.com/computing/postr-0.3.tar.gz).
