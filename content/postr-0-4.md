Title: Postr 0.4
Date: 2007-01-21 23:00
Tags: tech
Slug: postr-0-4

Postr 0.4 is finally out. This has an all-new Flickr library that uses
Twisted, so I don't need to use threads any more. I've had lots of
contributions to this release, in fact the change log is too long to
summarise... Special features include a Nautilus context menu, GtkUnique
support, and a lack of thread related crashes.

Thanks to Germán Poó Caamaño, Emmanuele Bassi, Dean Sas and Daniel Stone
for their patches and feedback.

You can follow the development in the [Bazaar
branch](http://burtonini.com/bzr/postr/postr.dev), or get the [Postr 0.4
tarball](http://burtonini.com/computing/postr-0.4.tar.gz).
