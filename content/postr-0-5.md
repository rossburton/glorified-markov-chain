Title: Postr 0.5
Date: 2007-02-09 17:30
Tags: tech
Slug: postr-0-5

Postr 0.5 is out. This has a few fixes:

-   Catch errors throw by EXIF or IPTC parsing
-   New flickrpc (cleaner code, works with Python 2.5)

You can follow the development in the [Bazaar
branch](http://burtonini.com/bzr/postr/postr.dev), or get the [Postr 0.5
tarball](http://burtonini.com/computing/postr-0.5.tar.gz).
