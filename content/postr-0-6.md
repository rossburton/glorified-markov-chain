Title: Postr 0.6
Date: 2007-06-05 19:45
Tags: tech
Slug: postr-0-6

Postr 0.6 is here! What is new I hear you ask. Well:

-   Really parse embedded IPTC data
-   Sweet new look for the side bar (thanks Lucas Rocha)
-   Ability to select a set to upload the pictures too
-   New icon (thanks Andreas Nilsson)

The tarball is [in the usual
place](http://burtonini.com/computing/postr-0.6.tar.gz), and I'll make
Debian packages shortly.

<small>NP: <cite>Konfusion</cite>, Skalpel</small>
