Title: Postr 0.7
Date: 2007-06-13 11:30
Tags: tech
Slug: postr-0-7

Postr 0.7 is out, fixing a couple of very nasty bugs.

-   Don't silently stop uploading if no set was selected
-   Unquote URIs when accepting drags

Tarballs at [the usual
place](http://burtonini.com/computing/postr-0.7.tar.gz), packages
heading towards Debian shortly.

<small>NP: <cite>Flight 602</cite>, Aim</small>
