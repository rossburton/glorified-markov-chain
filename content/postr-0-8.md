Title: Postr 0.8
Date: 2007-08-21 10:10
Tags: tech
Slug: postr-0-8

Finally, a new Postr release. Nothing amazing here, just some internal
refactoring and better error handling. If an error occurs when talking
to Flickr a dialog box will popup, which will help a great deal.

The [tarball is here](http://burtonini.com/computing/postr-0.8.tar.gz),
and packages for Debian/Ubuntu are building now.

<small>NP: <cite>Sleep</cite>, DJ Olive</small>
