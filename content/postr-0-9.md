Title: Postr 0.9
Date: 2007-09-23 15:35
Tags: tech
Slug: postr-0-9

A quick release of Postr fixing a few little bugs.

-   Fix handling of more EXIF tags
-   Add 24x24 icon (thanks Michael Monreal)
-   Add the version number to the UI (thanks Claudio Saavedra)

The [tarball is here](http://burtonini.com/computing/postr-0.9.tar.gz),
and packages for Debian/Ubuntu are building now.
