Title: Postr Bugs
Date: 2007-05-14 14:30
Tags: tech
Slug: postr-bugs

Not that Postr has any bugs (cough), but if you happen to find any then
they can be filed in the [all new Postr
product](http://bugzilla.gnome.org/browse.cgi?product=postr) on GNOME's
Bugzilla.

<small>NP: <cite>Minima Moralia</cite>, Chihei Hatakeyama</small>
