Title: Postr Category
Date: 2007-04-10 11:50
Tags: tech
Slug: postr-category

Quick poll for Planet Gnome, and any other interested folks. Should
Postr, my Flickr uploading applicaton, appear in the Graphics submenu on
the panel, or the Internet submenu? Answers on a postcard to the usual
address, or post a comment if you want. Thanks!

<small>NP: <cite>Rideau</cite>, Tape</small>
