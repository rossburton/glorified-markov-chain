Title: Postr - GNOME Flickr Uploader
Date: 2009-11-12 17:56
Tags: tech
Slug: postr-gnome-flickr-uploader

Postr is a Flickr uploading tool for the GNOME desktop, which aims to be
simple to use but exposing enough of Flickr to be useful.

![Flickr
Uploader](http://burtonini.com/computing/screenshots/postr-2.png){}

I don't maintain Postr any more, so please see the new [project
page](http://projects.gnome.org/postr/) for the latest news and sources.
