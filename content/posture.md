Title: Posture
Date: 2005-10-27 20:00
Tags: life
Slug: posture

Recently I've started to suffer from using a laptop as my primary work
machine, namely stiff neck, sore back, etc. So I went shopping and
purchased a PS/2 to USB cable, and an
[iCurve](http://www.griffintechnology.com/products/icurve/). I did toy
with other laptop stands but they were either expensive, or incredibly
ugly (think huge sheets of grey steel). The iCurve on the other hand is
invisible...

![My Desk](http://www.burtonini.com/photos/Misc/desk.jpg){}

Here my unsed desktop's mouse and keyboard are plugged into the USB
dongle, which through the magic of the kernel 2.6 input layer Just Works
without any action at all.

When I want to use the desktop -- a monthly event generally -- I can
just whip the USB adaptor from the laptop and plug it into the desktop.
Then I can watch Windows XP fail miserably to handle hardware changing
between reboots. It appears that although Windows managed to handle the
keyboard moving from PS/2 to USB, it refused to acknowledge that the
mouse wasn't plugged into the PS/2 port still.. I had to figure out the
keyboard navigation for the login screen and then remove/insert the
mouse again. Not a great pain, but I bet that will catch out some normal
users.

Anyway, the iCurve totally rocks and is a great example of design via
simplicity: it's just a curved piece of plastic but the laptop sits
firmly on it. I recommend it to everyone who uses a laptop all day.
