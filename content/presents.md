Title: Presents!
Date: 2004-09-10 00:00
Tags: tech
Slug: presents

An unexpected "we called and you were out" card arrived from Royal Mail
this week. I wondered what it was for an evening, and picked it up on
the way to work the next day. It's a package from Amazon, but I didn't
order anything recently. I also don't have anything on pre-order, so at
this point I started doubting my sanity. Do I order CDs in my sleep? It
would explain where my money goes every month...

I opened it up on the way to the station, looked at the CD, was very
confused for a few seconds, read the invoice and discovered this:

> <cite>"thanks for a useful and often-used app."</cite>

Aaah, suddenly it all becomes clear. I *knew* it was a good idea to put
a link to my Amazon wishlist next to Sound Juicer on my web page...

The invoice is anonymous so thanks, whoever you are.
