Title: Public Perception of Immigration
Date: 2005-05-12 13:15
Tags: life
Slug: public-perception-of-immigration

The excellent [Observer Blog](http://blogs.guardian.co.uk/observer/) has
an [interesting
post](http://blogs.guardian.co.uk/observer/archives/2005/05/12/what_came_first_the_antichicken_headlines_or_public_hostility_to_the_egg_.html)
about a recent MORI survey:

> Regular readers of different papers were asked 'what percentage of the
> British population do you think are immigrants to this country?'
>
> The highest bid came from Daily Star and Sun readers - 26 per cent.
> Next up, Daily Mirror on 25 per cent, then in order, The Express - 21
> per cent; Mail - 19 per cent; Telegraph - 13 per cent; Guardian - 11
> per cent; Times 10 - per cent; The Indie - 9 per cent; FT - 6 per
> cent.
>
> The UK average guess is 21 per cent. And the actual figure? That would
> be 7 per cent. So as a nation we're only 3 times out of proportion on
> this one.

<cite>Daily Star</cite>, <cite>Mirror</cite>, and <cite>Sun</cite>
readers think that more than *1 in 4* of the country are immigrants?
It's interesting both how the more right-wing papers give the impression
of more immigration, and how almost everyone over-estimates to some
degree.

<small>NP: <cite>Entroducing.....</cite>, DJ Shadow</small>
