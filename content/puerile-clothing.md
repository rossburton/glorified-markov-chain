Title: Puerile Clothing
Date: 2004-12-16 00:00
Tags: life
Slug: puerile-clothing

Today I jumped on the bandwagon and started a [Cafepress
store](http://www.cafepress.com/burtonini). There is one more
Tube-related design I have in mind, and I'm currently thinking of bad
puns involving [Opened Hand](http://www.openedhand.com) (who got a new
web site this week, by the way).

<small>NP: <cite>Thinner ambient dub mix</cite>, DJ krill.minima</small>
