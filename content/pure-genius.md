Title: Pure Genius
Date: 2004-12-10 00:00
Tags: life
Slug: pure-genius

Sometimes the solution to a hard problem [is so
simple](http://www.telegraph.co.uk/news/main.jhtml?xml=/news/2004/11/13/ntrain13.xml&sSheet=/news/2004/11/13/ixhome.html).
