Title: Pyblosxom hacking
Date: 2003-05-06 00:00
Tags: tech
Slug: pyblosxom-hacking

Yesterday I embarked on some Python hacking and wrote my first plugin
for Pyblosxom. It allows each blog category to have a "body text"
article, which only appears when that category is viewed directly and is
always the first entry.

Very useful for turning pyblosxom into a combination static/blog engine,
so my Sound Juicer pages have a static entry at the top explaining what
it is, and then the news afterwards. Groovy.
