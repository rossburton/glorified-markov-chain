Title: Pyblosxom Plugins
Date: 2004-01-06 00:00
Tags: tech
Slug: pyblosxom-plugins

Over the weekend I wrote a small plugin for Pyblosxom to let me set the
title of a particular section. For example, normally the document title
in the Sound Juicer page would be "Burtonini - computers/sound-juicer",
but now I can change it to just "Sound Juicer". The is very simple, if
it can find a file called `.title` in the current section, it reads the
first line and uses that as the document's title. Download it
[here](http://www.burtonini.com/computing/section_title.py).

Now all I need to do is rewrite the body plugin (note how the Sound
Juicer section always has a certain article at the top) and release it,
and make a book list plugin. Yes, I know these is one already, but I
don't like the way it works.
