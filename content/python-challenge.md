Title: Python Challenge
Date: 2006-09-07 23:10
Tags: tech
Slug: python-challenge

Here is a quick challenge for any Python gurus out there. Go and [grab
the Postr source](http://burtonini.com/bzr/postr/) (you'll need bzr) and
run `src/postr.py`. Marvel at how simple it makes uploading images to
Flickr, and how well it works.

Then run `python setup.py install --prefix=prefix` and run
<cite>prefix</cite>/bin/postr. Try to upload an image and note how it
hangs. A little debugging reveals that it is hanging on
`mimetools.choose_boundary()`, and I have no idea why. I've probably
made a stupid typo somewhere, can anyone spot the problem?
