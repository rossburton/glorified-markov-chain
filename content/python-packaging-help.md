Title: Python Packaging Help
Date: 2007-08-24 14:15
Tags: tech
Slug: python-packaging-help

[`pyclutter`](http://packages.debian.org/unstable/source/pyclutter) just
got accepted into the Debian archive, and I'd love some
review/assistance from anyone who understands the new Python Packaging
Policy. Bonus points if any patches also work on Ubuntu Feisty (which
has `python-central 0.5.12`). Thanks in advance for any feedback!

<small>NP: <cite>Sweetness</cite>, Bonobo</small>
