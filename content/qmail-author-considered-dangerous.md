Title: qmail Author Considered Dangerous
Date: 2006-08-31 09:55
Tags: tech
Slug: qmail-author-considered-dangerous

Thanks to LWN I was reminded of the, erm, *interesting* license and
[security guarantee](http://cr.yp.to/qmail/guarantee.html) of qmail.
Specifically:

> In March 1997, I offered \$500 to the first person to publish a
> verifiable security hole in the latest version of qmail: for example,
> a way for a user to exploit qmail to take over another account. My
> offer still stands. Nobody has found any security holes in qmail.
> \[...\] In May 2005, Georgi Guninski claimed that some potential
> 64-bit portability problems allowed a \`\`remote exploit in
> qmail-smtpd.'' This claim is denied. Nobody gives gigabytes of memory
> to each qmail-smtpd process, so there is no problem with qmail's
> assumption that allocated array lengths fit comfortably into 32 bits.

Erm. Well. I'm not sure what to say. Assuming that array lengths
(`size_t`, IIRC) is a 32-bit type even on 64-bit architectures is wrong.
Defending it is insanity.

<small>NP: <cite>Layered</cite>, Antibreak</small>
