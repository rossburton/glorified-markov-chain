Title: Qt 4.2 Preview with Glib Mainloop
Date: 2006-07-03 15:45
Tags: tech
Slug: qt-4-2-preview-with-glib-mainloop

> “To enable closer integration with the GNOME deskop environment and
> tools, Qt 4.2 now contains support for the Glib eventloop”
> \[[via](http://doc.trolltech.com/4.2/qt4-2-intro.html)\]

Next step: World Domination!

<small>NP: <cite>This Is My Demo</cite>, Sway</small>
