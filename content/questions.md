Title: Questions
Date: 2005-05-30 15:29
Tags: tech
Slug: questions

GUADEC is cool, and is resulting in people asking lots of questions.
When will the latest XGL server be committed so we can switch desktops
by rotating a cube? Will GNOME make 10x10? (10% installed base by 2010).
Will the N770 be an amazing success, or a product only used by geeks
just because it runs Linux? Will the weather be nice, instead of too hot
or too wet? Will Jeff ever stop bleeding?

The people demand answers, but only time can tell.
