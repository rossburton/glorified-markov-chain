Title: Quick Update
Date: 2004-01-13 00:00
Tags: life
Slug: quick-update

Today is the last day of working at SENaPS for Vicky, and from tomorrow
she'll be a Learning Support Assistant at a primary school in Roydon,
which hopefully will be far more enjoyable.

Logarithmic [maps of the
universe](http://www.astro.princeton.edu/~mjuric/universe/) sound weird,
but are pretty cool.

Wedding invitations are looking good -- 95% are printed now. I'm glad to
say they are produced using 100% Open Source software.
