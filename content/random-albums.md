Title: Random Albums
Date: 2005-08-31 10:28
Tags: life
Slug: random-albums

God bless Amazon Wishlists! Today we got a veritable mountain of mail
which wasn't totally unexpected as Vicky ordered some U2 albums
yesterday. However, one package from Amazon was for me...

Opening it up I found <cite>Divine Madness</cite> by Madness, with this
comment on the dispatch note:

> Its madness out there. Thanks for sound juicer.

Amazon don't put the sender's name on gift purchases, so whoever you
are, thank you!

<small>NP: <cite>Divine Madness</cite>, Madness</small>
