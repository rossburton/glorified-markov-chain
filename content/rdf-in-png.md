Title: RDF in PNG
Date: 2004-01-28 09:48
Tags: tech
Slug: rdf-in-png

A long-running goal of mine is to write a image gallery for my web page,
so the screenshot and photo galleries can have categories, titles,
dates, etc. Being a fan of metadata, XML, Dublin Core etc, I've planned
to do this by embedding the metadata into the image files instead of
relying on a database.

A little poking and emailing comes up with interesting links. Dave
Beckett has [embedded RDF into a
PNG](http://www.ilrt.bris.ac.uk/people/cmdjb/2001/04/rdf-icon/) by using
a `tEXt` field called 'Metadata'. However, he doesn't have any nice
tools to do this and usually uses `pnmtopng` (which allows the user to
specify text chunks). I poked a libpng but that doesn't really allow me
to fiddle with the chunks.

So I'm announcing PyPNG! A very small and rather poor Python (plus a
smidge of C to do CRCs) library with grand ambitions. At the moment I
can copy a PNG file (by reading the chunks, and then writing them
again), display the text chunks, et la piece de resistance: a tool to
set the content of an arbitary text chunk! PNGs with embedded RDF, here
I come.

Once I've fiddled with the library design a little I'll write a PNG
Explorer (hmm, `png:///` in Nautilus is tempting) and a Metadata Editor
for PNG files. Then I'll try and do exactly the same for JPEG files.
Finally of course I'll have to write the web front-end.

No downloads yet, but if you want the source ask for it. Hopefully I'll
have a sane tarball of PyPNG done this week though.
