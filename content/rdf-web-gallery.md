Title: RDF Web Gallery
Date: 2004-12-30 14:40
Tags: tech
Slug: rdf-web-gallery

For the last few weeks I've been playing with the third iteration of a
web gallery which uses RDF at it's core. The first two iterations were
dynamic and generally sucked as I was learning
[Redland](http://librdf.org/) and [Twisted](http://twistedmatrix.com/)
at the same time, so I relaxed the requirements a little and the third
iteration is an offline tool.

Currently called <cite>RAWK</cite>, but possibly to be renamed to
<cite>Tate</cite>, it scans a directory of images, extracting metadata
from anything available. `stat` calls, filenames, thumbnail files,
summary files and RDF fragments are all slurped into a data structure
which is then dumped out as RSS 1.0, featuring a variety of schemas.
This is then transformed with [Kid](http://splice.sourceforge.net/kid)
into a HTML file, which is uploaded.

The code is a little crufty in places at the moment so I'm not letting
anyone else see it yet, but the resulting HTML (and RSS) is already
online, specifically a
[BBQ](http://burtonini.com/photos/200408-KarenBBQ/) and [our
wedding](http://burtonini.com/photos/Wedding/).

<small>NP: <cite>Something Wicked This Way Comes</cite>,
Herbalizer</small>
