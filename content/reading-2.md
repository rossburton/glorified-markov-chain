Title: Reading
Date: 2003-06-27 00:00
Tags: life
Slug: reading-2

Damn, out of books to read on the train again. Recently I've been
reading <cite>The Cassini Division</cite> and <cite>The Stone
Road</cite> (Ken MacCloud), <cite>A Clockwork Orange</cite> (I also
finally watched the film, which is tame compared to the book), <cite>War
of the Worlds</cite> (H.G. Wells), <cite>The Code Book</cite>,
<cite>Stupid White Men</cite> and <cite>Fast Food Nation</cite>.

I am reading <cite>A Short History of Almost Everything</cite> (Bill
Bryson) at the moment at home, but it's a hefty hardback book which I
don't fancy lugging around London. It is very good though -- Bill's
amusing rambling writing style suits the subject matter very well, as he
explains why the oceans are salty, why they don't get more salty, what a
proton is made from, how geology was invented and just how far a parsec
is. He throws in lots of interesting annecdotes which wouldn't have been
in a more technical book, such as how a certain professor would practise
fossil-hunting in full academic wear instead of something a little more
practical.

However, I need books for my holiday. We're off to a in a week or so,
and I'm going to need something to read. So far I've got <cite>Stand On
Zanzibar</cite>, which is supposed to be very good and is, more
importantly, long. Any suggestions for good long books? I don't fancy
<cite>War and Peace</cite> though, and have just finished reading
<cite>LotR</cite>.
