Title: Reading
Date: 2004-02-18 00:00
Tags: life
Slug: reading

Over the weekend I finished [Dude, Where's My
Country](http://www.amazon.co.uk/exec/obidos/ASIN/0713997001) and [The
History of
English](http://www.amazon.co.uk/exec/obidos/ASIN/0340829915/), and just
started reading [Raw
Spirit](http://www.amazon.co.uk/exec/obidos/ASIN/1844131955).

<cite>Dude</cite> is the sequel to <cite>Stupid White Men</cite> and
contains exactly what you'd expect really. I feel that <cite>Stupid
White Men</cite> was a better book, but this is still a good read, with
many interesting facts, quotes and investigations. Oprah for President!

<cite>The History of English</cite> is a great book for anyone
interested in the history and development of the English language. The
author (Melvin Bragg, who had previously done a short radio series and
then a TV series on the same topic) looked at the way English has
changed dramatically over time, and puts the changes into historical
context. An interesting read.

NP: Simple Things, Zero 7
