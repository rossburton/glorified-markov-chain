Title: Recent Files in Sound Juicer
Date: 2006-03-30 17:09
Tags: tech
Slug: recent-files-in-sound-juicer

Now that fellow Opened Hander
[Emmanuele](http://log.emmanuelebassi.net/) has landed the `GtkRecent`
code into GTK+, I started to think about if I could use it in Sound
Juicer. Consider this a personal <cite>Is It Crack Or Not</cite>.

Should Sound Juicer add songs it successfully rips to the recent files
list, so they are trivially opened from the Recent Documents menu and so
on? Answers on a postcard, or if you don't have my address, in the
comments on this posting.

<small>NP: <cite>Regulate</cite>, Warren G</small>
