Title: Remote X11 on OS X
Date: 2014-01-21 11:17
Tags: osx, tech
Slug: remote-x11-on-os-x

I thought I'd blog this just in case someone else is having problems
using XQuartz on OS X as a server to remote X11 applications (i.e. using
ssh -X somehost).

At first this works but after some time (20 minutes, to be exact) you'll
get "can't open display: localhost:10.0" errors when applications
attempt to connect to the X server. This is because the X forwarding is
"untrusted" and that has a 20 minute timeout. There are two solution
here: increase the X11 timeout ([the maximum is 596
hours](http://b.kl3in.com/2012/01/x11-display-forwarding-fails-after-some-time/))
or enable trusted forwarding.

It's probably only best to enable trusted forwarding if you're
connecting to machines you, well, trust. The option is
`ForwardX11Trusted yes` and this can be set globally in
`/etc/ssh_config` or per host in `~/.ssh/config`.
