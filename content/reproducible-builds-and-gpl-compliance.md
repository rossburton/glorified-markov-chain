Title: Reproducible builds and GPL compliance
Date: 2014-05-15 23:27
Tags: tech, yocto
Slug: reproducible-builds-and-gpl-compliance

LWN has a good [article on GPL
compliance](http://lwn.net/Articles/598371/) (if you're not a subscriber
you'll have to wait) that has an interesting quote:

> Developers, and embedded developers in particular, can help stop these
> violations. When you get code from a supplier, ensure that you can
> build it, he said, because someone will eventually ask. Consider using
> the Yocto Project, as Beth Flanagan has been adding a number of
> features to Yocto to help with GPL compliance. Having reproducible
> builds is simply good engineering practice—if you can't reproduce your
> build, you have a problem.

This has always been one of the key points that we emphasis when
explaining why you should use the Yocto Project for your next product.
 If you're shipping a product that is built using fifty open source
projects then ensuring that you can redistribute all the original
sources, and the patches that you've applied, and the configure options
that you've used, and any tweaks to go from a directory of binaries to a
bootable image isn't something you can knock up in an afternoon when you
get a letter from the [SFC](http://sfconservancy.org/).  Fingers crossed
you didn't accidentally use some GPLv3 code when that is considered
toxic.

Beth is awesome and has worked with others in the Yocto community to
ensure all of this is covered.  Yocto can produce license manifests,
upstream sources + patches archives, verify GPLv3 code isn't
distributed, and more.  All the work that is terribly boring at the
beginning when you have a great idea and are full of enthusiasm (and
Club-Mate), but by the time you're shipping is often nigh on impossible.
 Dave built the kernel on his machine but the disk with the right source
tree on died, and Sarah left without telling anyone else the right flags
to make libhadjaha actually link...  it'll be fine, right?
