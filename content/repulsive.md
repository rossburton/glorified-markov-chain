Title: Repulsive
Date: 2008-03-03 09:15
Tags: life
Slug: repulsive

From the most excellent <cite>[Flat Earth
News](http://www.amazon.co.uk/gp/product/0701181451?ie=UTF8&tag=1799&linkCode=as2&camp=1634&creative=6738&creativeASIN=0701181451)</cite>,
a rip-roaring (I've always said that phrase should be used more) tale of
corruption, falsehood and propaganda in journalism:

> “The readers are never wrong. Repulsive, maybe, but never wrong.” -
> Piers Morgan, as editor of the <cite>Daily Mirror</cite>, referring to
> how he lost circulation due to the paper's stance against the Iraq
> invastion.

<cite>Flat Earth News</cite> is a great book, and I can recommend it to
everyone who is disappointed with the state of global journalism, and
even more to anyone who thinks journalism is in general doing a good
job.
