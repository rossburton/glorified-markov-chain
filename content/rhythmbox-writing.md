Title: Rhythmbox Writing
Date: 2003-08-29 00:00
Tags: tech
Slug: rhythmbox-writing

I managed to tick off an item in my long-term To Do list yesterday, by
finishing the first draft of a patch for Rhythmbox to burn playlists to
CD.

The patch isn't great at the moment -- changes to nautilus-cd-burner
need to be rewritten, and the Rhythmbox patch is more proof-of-concept
than release quality. However, it works and I've been playing two CDs I
wrote using it today. Hopefully I can clear up the patches over the next
week or so and get the changes into CVS.

PS: Anyone want a Coolermaster Heat Pipe fan for an Athlon up to
2000+XP? [Mail me](mailto:ross@burtonini.com).
