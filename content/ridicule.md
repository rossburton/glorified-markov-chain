Title: Ridicule
Date: 2008-05-15 13:40
Tags: life
Slug: ridicule

> “The problem for mainstream pop since the 70s is that metal has
> siphoned off many of the best freaks and losers.”

It's not often you read an article in the Guardian about Adam and The
Ants, Finnish Battle Metal bands, and being "cool", but [today I
did](http://blogs.guardian.co.uk/music/2008/05/in_metal_ridicule_is_nothing_t.html).
