Title: Roku SoundBridge
Date: 2007-10-03 11:15
Tags: life
Slug: roku-soundbridge

Yesterday my new NAS arrived, to replace my aging and failing hacked
Linkstation. As part of the bundle I also received a [Roku
SoundBridge](http://www.rokulabs.co.uk/stores/product_family_view.do?pubID=4409&familyID=72287),
which was a nice surprise. Basically, it's a consumer-orientated device
which plays music from iTunes or Internet radio, which you would plug
into a hifi or powered speakers. I'd heard of these before but I've been
using my old ThinkPad X22 for this duty for a while now, and MPD has
served me well. I thought I'd give it a go, and I'm actually really
impressed with it.

Physically the SoundBridge is pretty good looking: a sliver and black
ten inch cylinder about two inches in diameter, with a large LCD panel
on the front. When turned on it found my wireless network, asked for the
WEP key, and promptly upgraded its firmware. Once all that was done, it
let me select from two libraries: Vicky's Music or Internet Radio. Vicky
was running iTunes on her laptop which exports the library over DAAP, so
I listened to Tori Amos whilst I explored the Internet Radio options.
Then I listened to the most excellent Groove Salad on SomaFM (apparently
the \#4 station on the Roku Radio charts). At this point I discovered
that there was a SoundBridge link in Epiphany, the SoundBridge uses mDNS
to publish the web control panel: a useful application of clue from
Roku. Then it just got better. The SoundBridge will stream from DAAP and
UPnP servers (they pimp mt-daapd and SlimServer), and announces the web
interface over mDNS and UPnP. There is a [web
site](http://www.rokuradio.com) which indexes Internet radio streams,
currently it has over 5000 entries. This site uses a Java applet
(currently only tested in Windows though, I haven't installed Java yet)
to talk to your SoundBridge so it can show the currently playing station
and tell it to play another station. *Then* I discovered this in the
manual.

> Geeks - read this. The M-bridge has a command line interface that you
> can telnet to for piddling abut. You will need to telnet to port 4444.
> Type "?" at the command prompt to see a list of commands. ... M-bridge
> has a built-in UPnP AV "media renderer". This protocol can be used to
> control the M-bridge from your own software.

The SoundBridge supports both a custom protocol (documented in a
200-page PDF) and the standard UPnP protocol for controlling it. They
even documented the signals the remote control uses. This is probably
one of the most hackable "consumer" devices I've seen for a long time,
short of the N800. Well done Roku, you've created a damn neat product
which actually does just work out of the box.

<small>NP: <cite>theJazz</cite>, Internet radio</small>
