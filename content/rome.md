Title: Rome
Date: 2003-04-19 00:00
Tags: life
Slug: rome

Just got back from Rome -- a truly wonderful city. Everyone should go
there, as it is lovely.

Our unique hotel was just off Campo de Fiorri (excuse the spelling, I
don't have a map with me), with a roof terrace on the sixth floor
overlooking the city. Our days basically involved getting up, having
breakfast, spending half an hour on the balcony just looking at the
city, then walking around the city until lunch, eating pizza, more
walking, back to the hotel, rest for an hour, restaurant, hotel, bed.

The ancient Roman remains are amazing -- the sheer size of everything is
amazing. The Collosium is large, the Roman Forum is just huge. It seems
that everywhere you go in the old town involves walking past ancient
pillars, column and walls, idly sitting on corners and in walls.

We also went to the Vatican. We went inside, and spent the next half
hour in complete awe. It's huge, and intricate. Marble and engraving and
statues and artwork on everything and anything. Right up there on my
list of Groovy Churches.

We took three films of photos with our new Canon EOS-300V camera, when I
can get my scanner to work I'll put a few online.
