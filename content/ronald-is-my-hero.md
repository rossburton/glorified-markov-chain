Title: Ronald Is My Hero
Date: 2005-06-23 10:55
Tags: tech
Slug: ronald-is-my-hero

I was going to do it, then I wasn't. Havoc talked me out of it last
year, and Ronald talked me back into it at GUADEC. We found Havoc at
GUADEC, who was rather drunk, and convinced him it was a good idea. That
was far easier than we could ever imagine. Ronald said he'd send me a
patch in two weeks that night, and last night (after three weeks, but
I'm not counting) I [received
it](http://bugzilla.gnome.org/show_bug.cgi?id=308755).

![](http://www.burtonini.com/computing/screenshots/sj-cd.png)

Ronald Is My Hero.

<small>NP: well, you can see for yourself...</small>
