Title: Same Old Same Old
Date: 2003-10-10 00:00
Tags: tech
Slug: same-old-same-old

Dear Intel,  
Please make the Intel Pentium 4 sane.  
Love, Ross.

Intel Pentium 4's are weird. I do a pretty-much null change (from
looking at the assembler a jump table was re-ordered) and performance on
the P4 is 30% better, but the same binaries on a P3 have the expected
identical performance. I then optimised away some redundant code (again,
looking at the assembler shows many instances of large chunks removed)
and the program actually slows down. This is starting to make optimising
for speed not a Fun Game. At least the ever-handy
[Meld](http://meld.sf.net) is showing its worth again, quickly and
easily displaying the differences in two assembler files.

`monitor-calibration-tool` is going well, I've rewritten most of it and
the crack is slowly being removed. Hopefully I'll be able to get another
release out next week, which will be the first release to be useful
(unless you are really weird and want to calibrate your screen for gamma
1.0)

Finally, it appears that IBM don't want my Java Reflection article, the
corporate gits. I blame SCO for this, I'm not sure how but I think
blaming SCO is a positive action here. If my second choice doesn't want
to pay for it, then an absolutely fabulous article (if I do say so
myself) will be online here next week or so.
