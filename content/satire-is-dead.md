Title: Satire Is Dead
Date: 2007-11-02 08:10
Tags: life
Slug: satire-is-dead

I should be doing useful things like having a bath and getting dressed,
instead of blogging in my pants, but this is just too funny.

> “Foreign workers have taken every new job in Britain for the past four
> years, astonishing figures show.”

Is this from [The Daily Mash](http://www.thedailymash.co.uk/)?
[NewsBiscuit?](http://newsbiscuit.com/) No, it is the [real genuine
Express
article](http://express.co.uk/posts/view/23928/Migrants-take-all-new-jobs-in-Britain).
More evidence that the Express has been covertly taken over by
satirists.
