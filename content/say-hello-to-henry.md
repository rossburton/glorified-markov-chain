Title: Say Hello To Henry
Date: 2005-12-20 15:30
Tags: life
Slug: say-hello-to-henry

Last Friday we went and collected Henry, an incredibly cute Labrador. He
is quite a bundle of energy, so here is a picture of him falling asleep.

![Henry](http://www.burtonini.com/photos/Misc/img_3948-small.jpg){}
