Title: Scanners and Calibration
Date: 2004-02-21 00:00
Tags: tech
Slug: scanners-and-calibration

So a few weeks ago we bought a new Epson Perfection 1670 scanner. After
much comparing the [PC Pro](http://www.pcpro.co.uk) reviews with
[Dabs](http://www.dabs.com) prices and the
[Sane](http://www.sane-project.org) supported scanners list, this was
the only good scanner which is sub-£100 and supported in Linux.

The scanner software in Windows is, well, pretty average. After a bit of
configuring it works well enough, but the interface is skinned and a
little Byzantine in places. However, Epson are good enough to provide a
set of ICC files which are used by the scanning software
*automatically*, and I had already installed an ICC profile from my
monitor from Iiyama, so the scans are pretty well calibrated out of the
box.

Not as much luck in Linux. After adding the scanner to
`/etc/hotplug/usb/libsane.usermap`, and adding myself to the `scanner`
group, I can access the scanner without being root. I've built the SANE
front-ends from CVS as the current release is still GTK+ 1.2, and it
works well enough, although the scanned images are incredibly dark. A
little gimping solves this, but of course I'd like not to do this.

So, anyone managed to correctly colour-calibrate a SANE/X/GIMP/CUPS
toolchain? I have ICC profiles for the scanner and my monitor, and am
willing to work in sRGB, so in theory this shouldn't be too hard. My
limited digging to X tells me that Xcms was invented before ICC was
conceived, so can I convert between the formats? Is Xcms actually
useful? Help!
