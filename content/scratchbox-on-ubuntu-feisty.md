Title: Scratchbox on Ubuntu Feisty
Date: 2007-03-14 08:00
Tags: tech
Slug: scratchbox-on-ubuntu-feisty

A quick note to everyone out there who has been sticking with old
kernels on Feisty as 2.6.20-7 broke Scratchbox: the kernel that was
released yesterday, 2.6.20-10, works again!
