Title: Screaming Hordes of Benefit Tourists
Date: 2005-04-28 17:50
Tags: life
Slug: screaming-hordes-of-benefit-tourists

Most Brits out there will probably remember the raving hysteria from the
tabloid/right-wing press when the EU expanded to include a few more
Eastern European countries, explaining that hordes of "benefit tourists"
are getting ready to move to England and claim benefit from the state.

Well, that didn't quite happen
([source](http://news.ft.com/cms/s/e9388868-b782-11d9-9f22-00000e2511c8.html)):

> The British Home Office said 133,000 people from the eight new EU
> countries in eastern Europe signed on to its worker registration
> scheme between May and December 2004.
>
> Of that total, up to 40 per cent were already in the UK before May 1.
> Poles made up 56 per cent of the total, followed by Lithuanians and
> Slovaks. The UK has no figures on how many subsequently returned home.
>
> As for fears of an influx of benefit claimants, the UK tightened up
> its benefit rules before May 1 2004 and only 21 people from eastern
> European countries have made successful claims.

*Twenty one* successful claims for benefit. Wow, I bet the system really
felt that blow.

Of course the counter-argument is that the new "tightened benefit rules"
are too tight, as I believe you need to be working here for a year
before you can successfully claim benefit. This implies that the people
who claimed benefit were either already in the country or possibly got a
job straight away, and results in a number of people in the impossible
situation that they can't get benefit as they can't get a job, but they
can't get a job as the Job Centre won't give them personal advise
(again, you need to be working for a year to get personal advise) about
a country and job market which is alien (as seen on
[Panorama](http://news.bbc.co.uk/1/hi/programmes/panorama/4454811.stm)
last week).

It's all screwed up, basically.

<small>NP: <cite>Best Of</cite>, Toots and The Maytals</small>
