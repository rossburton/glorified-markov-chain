Title: Screw The Tube
Date: 2004-06-30 00:00
Tags: life
Slug: screw-the-tube

Today there is another tube strike, over pay or uniforms or working
conditions, or something. Note to non-Londoners: the transport union
call strikes fairly frequently for various reasons, so it does get
difficult to remember what exactly they are protesting about this time.
I get the tube from Liverpool Street (well, Moorgate) to London Bridge,
and when there is a strike I normally just walk it: it's only a 20
minute walk. Today though I thought I'd see what the buses were like.
Several things then shocked and amazed me.

One: Transport For London's web site has a good journey planner. I mean
*really* good. I told it "Liverpool Street to London Bridge" and it
offered me two tube routes, the quickest bus route, and how long it
would take to walk. Armed with the knowledge that the quickest bus is
the number 47 from bus stop K, off I went to work. At the bus stop a
quick look at the very clear route maps told me that most buses here
took the route I wanted (which is basically "along the big road"), and
more importantly, that the bus already here was one of them. On I
jumped, and stood upstairs. The joy of the new buses is that upstairs
you get a fantastic view of the bicycles weaving their way down the bus
lanes, and the hordes of City workers marching over London Bridge.
Surprise number Two was that getting the bus (even when there is a
strike so it would be busier) is quicker and easier than the tube.

Screw the tube, and kudos to Ken Livingstone for pushing the role of
buses in London -- they really do kick arse.
