Title: SCSL vs OSD
Date: 2004-05-05 00:00
Tags: tech
Slug: scsl-vs-osd

Thanks to Glynn for linking to
[<abbr title="Sun Community Source License">SCSL</abbr> vs
<abbr title="Open Source Definition">OSD</abbr>](http://www.advogato.org/person/robilad/diary.html?start=46),
an analysis of the SCSL by Dalibor Topic (a Kaffe developer). An
excellent read, detailing the evilness of the SCSL.

Whilst wandering around the net I think I found some of the best photos
from GU4DEC
[here](http://www.ubiobio.cl/~gpoo/registro-fotografico/junio-2003/img014.jpeg),
[here](http://www.ubiobio.cl/~gpoo/registro-fotografico/junio-2003/img016.jpeg)
and
[here](http://www.ubiobio.cl/~gpoo/registro-fotografico/junio-2003/img018.jpeg),
taken by Germán Poó Caamaño.
