Title: Sedna a Planet?
Date: 2004-03-15 00:00
Tags: life
Slug: sedna-a-planet

John Fleck has [mentioned the
planet-ness](http://www.inkstain.net/fleck/archives/000901.html) (or
not) of [Sedna](http://www.inkstain.net/fleck/archives/000901.html), the
planetoid recently discovered. I consider Sedna to be a planetoid at a
push, definitely not a planet, and best described as "a large Kuiper
Belt object". Or "a big lump of rock".

My argument for Sedna being not being a planet can be boiled down to two
points:

1.  Sedna is in the Kuiper Belt. The Kuiper Belt is full of large lumps
    of rock, so if Sedna is a planet, how large does a lump of rock have
    to be to not be a planet? 1000km across? 500km?
2.  The 10^th^ planet is called Rupert.

