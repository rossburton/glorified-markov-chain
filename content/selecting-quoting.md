Title: Selecting Quoting
Date: 2008-01-15 10:45
Tags: life
Slug: selecting-quoting

As we all know, the [Express are a load of xenophobic racist ignorant
misleading cocks](http://www.express.co.uk/). Thanks to Iain who pointed
me at [this glorious
post](http://enemiesofreason.blogspot.com/2008/01/you-lying-dirty-stinking-fuckwitted.html)
entitled something I won't repeat for fear of offending the children, I
discovered what is probably the best example of selective quoting in the
Express.

They said:

> “The Government's equality chief Trevor Phillips recently said the
> fear that migrants are jumping council queues for homes is fuelling
> tensions.”

Well if the government are scared that migrants magically jump up the
council house queue, then it must be true. Or is it. [The Guardian has
the full
quote](http://www.guardian.co.uk/uk_news/story/0,,2203843,00.html):

> “Mr Phillips \[chairman of the Commission for Equality and Human
> Rights\] said tensions were driven by a widespread perception that
> newcomers often received unfair advantages. "Specifically, that white
> families are cheated out of their right to social housing by
> newly-arrived migrants," he told the \[Local Government Association\].
> "I have never seen any reliable evidence to back up this claim. And
> there can be no doubt that much of the public feeling is driven by the
> careless media and racist parties."”

If you ever wonder how low the Express and co. will go to get a quote or
statistic, this is a good example. In a quote where the essence of the
message is that the scum-class tabloids and racist parties are fueling
anti-immigration views, they managed to get a quote to support a story
against immigration. Well done, Tom Whitehead, you really do deserve a
special place in hell.

<small>NP: <cite>Skreamizm Volume 4</cite>, Skream</small>
