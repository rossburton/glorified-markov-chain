Title: Shame
Date: 2007-11-02 11:11
Tags: life
Slug: shame

(wow, three blog posts — albeit short ones — in one day)

Some people appear to believe that having a letter in the Granuiad is
sad and I should be shamed or something. Honestly, I thought the email
was going to be in some online forum thingy, not actually printed, but
can I help it if even my brief flippant replies are insightful? Anyway,
you can't shame me with newspaper clippings, I've had a [like-cures-like
homeopathic treatment against
that](http://www.flickr.com/photos/rossburton/1824390645/).
