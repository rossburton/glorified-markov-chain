Title: Shared File Metadata Specification Madness
Date: 2006-03-09 10:30
Tags: tech
Slug: shared-file-metadata-specification-madness

From the [Shared File Metadata
Specification](http://freedesktop.org/wiki/Standards_2fshared_2dfilemetadata_2dspec)
on freedesktop.org:

> The only requirement for metadata names is that they are unique and do
> not overload or cause confusion with each other. To make this
> possible, all metadata is namespaced by an appropriate class based on
> the type of the file or the application name (if the metadata is
> application specific).

What *isn't* confusing about having all of the following metadata types:

-   `File.Description`
-   `Audio.Comment`
-   `Doc.Comments`
-   `Image.Description`
-   `Image.Comments`

Another ~~stroke of genius~~ flaw is `File.Accessed` (and so on): “Last
access date in format "YYYY-MM-DD hh:mm:ss"”. What timezone is this in?
EXIF made this mistake, and it hurts.

Why this specification didn't soak up the years of work done by people
on RDF, Dublin Core, EXIF-in-RDF and so on, I don't know.

<small>NP: <cite>Sounds From The Verve Hifi</cite>, Thievery
Corporation</small>
