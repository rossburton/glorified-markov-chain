Title: Shell Magic
Date: 2007-11-19 15:22
Tags: tech
Slug: shell-magic

This is a nice little bit of shell magic I discovered today:

    trap true TERM
    kill -- -$$

The first line means that when the shell receives a `TERM` signal, it
executes `true`. The normal behaviour would be to kill the process, so
this means that the shell is immune to people sending a `TERM` signal to
it. The second line sends a `TERM` signal to the process group (`-` is
the modifier to indicate a the process group of the PID, not just the
PID) of the current process (`$$`).

The end result? Everything spawned by this script which hasn't gone and
re-parented itself is killed: the ultimate in cleanup.

<small>NP: <cite>Dedications</cite>, Klimek</small>
