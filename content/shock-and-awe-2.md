Title: Shock and Awe
Date: 2007-06-12 21:58
Tags: tech
Slug: shock-and-awe-2

From [The Hacker's Guide To GPSD](http://gpsd.berlios.de/hacking.html):

> “The best way to avoid having dynamic-memory allocation problems is
> not to use malloc/free at all.”

An interesting approach to avoiding dynamic memory problems which in
general are not that hard to debug with good tools (such as Valgrind),
especially in relatively simple pieces of code like gpsd. However, in
the next section:

> “The parsing of GPGSV sentences in the NMEA driver has been a
> persistent and nasty trouble spot, causing more buffer overruns and
> weird secondary damage than all the rest of the code put together.”

Personally, I often find that not allocating buffers on the stack and
using dynamically allocated memory with length-bounded functions helps
fix buffer overruns.
