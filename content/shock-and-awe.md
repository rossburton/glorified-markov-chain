Title: (Shock and Awe)
Date: 2004-02-02 15:03
Tags: tech
Slug: shock-and-awe

They said it would never happen. They pointed and laughed and jeered.
They kicked sand in our faces. But it has finally happened.

An except from the `docbook-xml`
[4.2-8](http://lists.debian.org/debian-devel-changes/2004/debian-devel-changes-200402/msg00068.html)
changelog:

> \* XML catalog registration using dh\_installxmlcatalogs from
> xml-core, requires &gt;= 0.6; closes: \#155129, \#181362, \#223867,
> \#217367, \#222265, \#223231, \#223643, \#225382, \#230611, \#134789,
> \#214006

It's been a long time coming, but it is finally here. Thank you Adam Di
Carlo!
