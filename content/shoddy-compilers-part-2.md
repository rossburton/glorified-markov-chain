Title: Shoddy Compilers, Part 2
Date: 2004-05-10 00:00
Tags: tech
Slug: shoddy-compilers-part-2

Some may remember the original [shoddy
compilers](http://www.burtonini.com/blog/computers/crap-iar) post from
December. This is basically more of the same.

I mean, people. With this code:

    int foo(void) {
      const int i = 42;
      return i + 3;
    }

What compiler *doesn't* just optimise the body away to "return 45" when
I ask for optimised code? Well, one which also doesn't optimise away the
test in `do {...} while(0)`, that is the answer.

This compiler has a positive plethora of optimisation options: you can
optimise for either speed or size, from either level 0 to level 9. Set
to size optimisation level 9, the generated code goes something like:

> Grow stack by 2 bytes  
> Put 42 in R16  
> Put 0 in R17  
> Store R16 via Z (stack pointer)  
> Store R17 via Z+  
> Put 45 in R16  
> Shrink stack by 2 bytes  
> Return (R16/R17 pair is the return value)

Nothing quite like 6 wasted instructions out of 7 on a &lt;4MHz
processor... When people moan about GCC not doing the right thing, they
don't know the meaning of "broken compiler". Amusingly the compiler
didn't generate the code to clear R17 to 0, so it either noticed that
had already happened, or the code generator is even worse than I
thought. I'm not sure what case to believe in.

<small>NP: <cite>When It Falls</cite>, Zero 7.</small>
