Title: Shoddy Compilers
Date: 2003-12-16 22:47
Tags: tech
Slug: shoddy-compilers

Last Friday I got an email saying that the old compiler we use at work
is actually generating the test/branch code in this snippet:

>     do {
>      /* foo */
>     } while (0);

I signed, recited the habitual "crappy [IAR](http://www.iar.se)" line,
and blamed someone not turning on the optimiser.

Until now. I tried with the new "improved optimiser" compiler they sent
us, which is supposed to be a lot better. The compiler isn't as
brain-dead as the older one, which is nice. However, this new optimiser
still sucks:

>     main:
>     ?0012:
>             LDI     R16,LOW(?0013)
>             LDI     R17,HIGH(?0013)
>             LDI     R18,(?0013 >> 16)
>             CALL    puts
>             LDI     R16,LOW(0)
>             TST     R16
>             BRNE    ?0012
>             RET

Oh wonderful. This is with full size optimisations, and the result is
the same with full speed optimisations. Curse the proprietary world and
their shoddy compilers!
