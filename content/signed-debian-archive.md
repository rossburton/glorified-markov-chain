Title: Signed Debian Archive
Date: 2005-06-30 16:44
Tags: tech
Slug: signed-debian-archive

My [Debian archives](http://www.burtonini.com/debian/) are now GPG
signed, so if you are using APT 0.6 and have my archives in your
`sources.list`, you best add [this
key](http://wwwkeys.pgp.net:11371/pks/lookup?op=index&search=ross%2Barchive%40burtonini.com)
using `apt-key`.

To confirm, the key ID is `510E0293`, and the fingerprint is
`F35D 9AA1 758B 7598 9672 789E F234 8DBE 510E 0293`.
