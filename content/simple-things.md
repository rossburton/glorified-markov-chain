Title: Simple Things
Date: 2005-09-09 08:45
Tags: tech
Slug: simple-things

It's often the simple things which make me happy, for example how
Epiphany monitors web pages it opens from the local disk and will reload
them automatically when they change. Only one key press removed from the
edit - regenerate - view cycle of DocBook authoring, but it's a very
nice feature.
