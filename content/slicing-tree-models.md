Title: Slicing Tree Models
Date: 2006-03-12 14:40
Tags: tech
Slug: slicing-tree-models

Sometimes there is a need to take a slice of a `GtkTreeModel`, for
example if you have a tree of data and want to show a particular layer
of it in a `GtkIconView`. `GtkIconView` widgets will only work on list
models so there needs to be a way of taking a slice of a tree model and
presenting it as a list model. I hereby present `OwlTreeModelSlice`.

![OwlTreeModelSlice in
action](http://www.burtonini.com/computing/screenshots/slicer.png)

The tree view on the left is a `GtkTreeStore`. The icon view on the
right is displaying a `OwlTreeModelSlice`, that has been told to use the
<cite>Applications</cite> node as the root.

There are a few methods left to implement, and I need to get someone
masterful in tree model fu to review the code, but hopefully I'll be
releasing the source shortly.

<small>NP: <cite>Motion</cite>, The Cinematic Orchestra</small>
