Title: So Far So Good
Date: 2004-01-26 00:00
Tags: life
Slug: so-far-so-good

The wedding plans, so far, are going well:

We've met with the caterers and discussed exactly what we want -- which
included the news that they do a hand-made tiramisu. On the whole they
seem very professional and prepared for all eventualities, including
being prepared for torrential rain in mid-July.

We've finally sent out the invitations after a long weekend of cutting,
folding and glueing. Making a final list of people to invite, and what
part of the day to invite them to, sounded easy but turned into a
hellish task we avoided very quickly. If we invite only our family --
parents, siblings, aunts, uncles and cousins -- we'd have over 100
people already where the registry office can only seat 75... Adding to
this other relations and of course friends, and still fitting, has been
quite a challenge.

We've even booked our flights to Italy for the honeymoon. Hopefully the
hotel will confirm our booking in the next few days and then we're all
done for the honeymoon at least.

NP: <cite>Shine</cite>, by Aswad. Gotta love mid-90s pop-reggae!
