Title: So Sad
Date: 2003-07-29 12:00
Tags: life
Slug: so-sad

Last week just after we got back from holiday our washing machine broke
down. The washing machine repair man had a look, and said it's best just
to buy a new one -- to replace the main broken part would be £150 and
that is without looking at fixing the leaks and the broken filter...

Saturday we went down the road armed with a Which? report on washing
machines and a price list from [QED](http://www.qed-uk.com). After a
little haggling ("When I say price match I mean to high street
shops...") I splashed out on a new [Bosch Exxcel
2465](http://www.boschappliances.co.uk/productdetail.asp?type=HL&cat=Washing+Machines&grp=Exxcel&product_ref=WFC%202465).
This is very sad, but I was getting excited over a washing machine... 20
minute "freshen" cycles, delayed start, cold "handwash" cycles. It
rocks. :)
