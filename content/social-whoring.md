Title: Social Whoring
Date: 2008-01-04 17:20
Tags: tech
Slug: social-whoring

[Linked In](http://www.linkedin.com) always had the atmosphere of a more
serious and professional social networking site, unlike MySpace and
Facebook where people seem to collect friends like stickers when they
were younger. Then I discovered [TopLinked](http://www.toplinked.com/),
a site dedicated to letting people grow their network massively to
people they've never met. The top member has 37 thousand connections. I
just don't understand this, what is the point of having so many
connections when there is no value in the connections themselves?

<small>NP: <cite>Bricolage</cite>, Amon Tobin</small>
