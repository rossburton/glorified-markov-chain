Title: Something Ate My Avahi
Date: 2007-10-08 10:00
Tags: tech
Slug: something-ate-my-avahi

So the new NAS is happily running Debian now, and I'll write a long blog
for anyone who wants to do the same once I have one remaining problem
sorted out... Avahi doesn't work. If I start Avahi on the NAS other
machines see the announcement and the names it provides will resolve,
but after a few minutes they fail to resolve as they timeout. Unless my
router is incredibly brain-dead it can't be the cause, because the old
NAS is connected in exactly the same way and it works fine. For extra
fun, the NAS can't see the rest of the mDNS network. It's almost like
the network driver is dropping incoming multicast packets.

Before anyone asks, there is no firewall running on the NAS. Does anyone
have any ideas?

<small>NP: <cite>Remembranza</cite>, Murcof</small>
