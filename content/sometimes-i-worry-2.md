Title: Sometimes I Worry
Date: 2006-02-20 17:45
Tags: life
Slug: sometimes-i-worry-2

Sometimes I worry about the mental state of Mallum. Under continued
pressure, I'm caving in to his demands. Although I refuse to include the
`<marquee>` tag.

![Myspace Glitter Graphics, MySpace Graphics, Glitter
Graphics](http://mic.bpcdn.us/bpg0/g.gif) ![Myspace Glitter Graphics,
MySpace Graphics, Glitter Graphics](http://mic.bpcdn.us/bpg0/n.gif)
![Myspace Glitter Graphics, MySpace Graphics, Glitter
Graphics](http://mic.bpcdn.us/bpg0/o.gif) ![Myspace Glitter Graphics,
MySpace Graphics, Glitter Graphics](http://mic.bpcdn.us/bpg0/m.gif)
![Myspace Glitter Graphics, MySpace Graphics, Glitter
Graphics](http://mic.bpcdn.us/bpg0/e.gif) ![Myspace Glitter Graphics,
MySpace Graphics, Glitter Graphics](http://mic.bpcdn.us/bpg0/r.gif)
![Myspace Glitter Graphics, MySpace Graphics, Glitter
Graphics](http://mic.bpcdn.us/bpg0/u.gif) ![Myspace Glitter Graphics,
MySpace Graphics, Glitter Graphics](http://mic.bpcdn.us/bpg0/l.gif)
![Myspace Glitter Graphics, MySpace Graphics, Glitter
Graphics](http://mic.bpcdn.us/bpg0/e.gif) ![Myspace Glitter Graphics,
MySpace Graphics, Glitter Graphics](http://mic.bpcdn.us/bpg0/z.gif)

I'm sorry.
