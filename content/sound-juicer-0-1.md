Title: Sound Juicer 0.1
Date: 2003-04-11 00:00
Tags: tech
Slug: sound-juicer-0-1

Well, I've finally released Sound Juicer 0.1.

It's not perfect, thus the version number, but it works. Please read the
`README` before running the program!

I've been told that Mandrake Cooker will have packages very soon, and of
course I have Debian packages in [my repository](/debian/).
