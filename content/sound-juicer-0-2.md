Title: Sound Juicer 0.2
Date: 2003-04-29 00:00
Tags: tech
Slug: sound-juicer-0-2

Sound Juicer "Madly Deeply" 0.2 is out -- download the [tarball
here](/computing/sound-juicer-0.2.tar.gz) or a [Debian package
here](/debian).

What's New:

-   RPM spec file
-   UI for setting the extraction base path
-   GConf key to set the output filename pattern
-   GConf key to set the paranoia level of the extraction
-   Always strip / from names to avoid extra path creation
-   Option to strip special characters \[ /\*?\\\] from filenames
-   UI cleanups
-   More minor bug fixes

