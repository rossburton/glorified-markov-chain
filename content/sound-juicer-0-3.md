Title: Sound Juicer 0.3
Date: 2003-05-20 00:00
Tags: tech
Slug: sound-juicer-0-3

Sound Juicer "I Want An Aduki" 0.3 is out -- download the [tarball
here](/computing/sound-juicer-0.3.tar.gz) or a [Debian package
here](/debian).

What's New:

-   Fixes to the Musicbrainz lookup code
-   Change the cursor to the busy cursor when looking up CDs
    (Anders Carlsson)
-   Finally fixed the file and path option menus (Ryan
    Boren/Ross Burton)
-   Depend on GStreamer 0.6.1 so that Ogg tagging works
-   libbacon updates, the CD drive drop-down should work a lot better
    now
-   Columns are resizable
-   Add "Deselect All" along with "Select All"
-   Use the GNOME HTTP proxy settings (Kai Willadsen)
-   Lots of other bug fixes (Ryan Boren)
-   Track title/artist fields can be edited.

Oh, and you can pick an encoding format now. You only get metadata on
Ogg Vorbis, and FLAC may not work due to a bug in GStreamer, but it's a
start...
