Title: Sound Juicer 0.4.1
Date: 2003-06-24 22:36
Tags: tech
Slug: sound-juicer-0-4-1

Sound Juicer "Imperial Buttload" 0.4.1 is out -- download the [tarball
here](/computing/sound-juicer-0.4.1.tar.gz). Debian packages Coming Real
Soon. This is a brown paper bag release.

What's New:

-   Removed the quality slider from the preferences dialog
-   Added a Rip Completed dialog box

