Title: Sound Juicer 0.4
Date: 2003-06-24 19:13
Tags: tech
Slug: sound-juicer-0-4

Sound Juicer "Metric Buttload" 0.4 is out -- download the [tarball
here](/computing/sound-juicer-0.4.tar.gz). Debian packages Coming Real
Soon.

What's New:

-   Metadata code re-arranged
-   Deselect All disabled as appropriate
-   Check if file exist before overwriting them
-   libbacon updates
-   Finally fix a stupid typo which broke multiple artist albums
-   Disable the extract button when ripping
-   Zero-pad track numbers so they sort correctly
-   Strip : from file names to avoid confusing Windows machines
-   Use lame if its available instead of mpegaudio
-   Progress bar fixes
-   Added CD-&gt;Eject

