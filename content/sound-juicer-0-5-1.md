Title: Sound Juicer 0.5.1
Date: 2003-08-11 20:59
Tags: tech
Slug: sound-juicer-0-5-1

Sound Juicer "Who needs prefs?" 0.5.1 is out -- download the [tarball
here](http://www.burtonini.com/computing/sound-juicer-0.5.1.tar.gz).
Debian packages Coming Real Soon as usual.

What's New:

-   Fix out of range warnings in the extracting progress dialog
    (Bastien Nocera)
-   Use new tag names for Gstreamer (BN)
-   Add defaults for file and path patterns
-   Use defaults if we get "" for the file and path patterns (these fix
    the crash when going to Preferences)
-   Fix more memory leaks
-   Don't exit if a CD-ROM drive can't be found

