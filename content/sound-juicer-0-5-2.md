Title: Sound Juicer 0.5.2
Date: 2003-08-21 20:35
Tags: tech
Slug: sound-juicer-0-5-2

Sound Juicer "Who needs FLAC?" 0.5.2 is out -- download the [tarball
here](http://www.burtonini.com/computing/sound-juicer-0.5.2.tar.gz).
Debian packages building as I type.

What's New:

-   Fix the FLAC and Wave encoders
-   Fix crash on pressing Reread
-   Faster startup with mounted CDs (Bastien)

