Title: Sound Juicer 0.5
Date: 2003-07-31 23:17
Tags: tech
Slug: sound-juicer-0-5

Sound Juicer "Ευχαριστω" 0.5 is out -- download the [tarball
here](/computing/sound-juicer-0.5.tar.gz). Debian packages Coming Real
Soon as usual.

What's New:

-   A user guide! (Mike Hearn)
-   Editable album artist and title! (MH)
-   HIGified .desktop file (MH)
-   Updated libbacon for better drive detection (Bastien Nocera)
-   sound-juicer.spec fixes (Christian Schaller)
-   When Musicbrainz doesn't know the track size, calculate it (BN)
-   Display a sample of the directory to save files into (me, BN)
-   A decent eject function which doesn't depend on /usr/bin/eject
-   Do runtime checks for encoders instead of compile time (BN)
-   Fix disabled HTTP proxies (BN)
-   Free old pipelines when creating new ones (me)
-   Removed use of GtkFrame in the prefs dialog as some (broken IMHO)
    themes display always display borders (BN)

