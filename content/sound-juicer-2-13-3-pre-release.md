Title: Sound Juicer 2.13.3 (pre-release)
Date: 2006-01-18 23:05
Tags: tech
Slug: sound-juicer-2-13-3-pre-release

I've completed an initial port of Sound Juicer to GStreamer 0.10, and
have made a tarball which [can be downloaded
here](http://www.burtonini.com/computing/sound-juicer-2.13.3pre.tar.gz).
There is one small problem with it... playback doesn't work. There is
probably a simple mistake in the code somewhere, but I'm tired and
thought I'd open this up to the Many Eyes of the Internet.

The first person to fix CD playback will get a huge thank you. The first
person who fixes seeking and time display (which I hear will be broken)
will get a really large thank you. Download my pretties, and find my
bugs!
