Title: Sound Juicer 2 Branched
Date: 2004-11-20 00:00
Tags: tech
Slug: sound-juicer-2-branched

Attention everyone who builds SJ from CVS: `sound-juicer` `HEAD` is now
for developing Sound Juicer 2, if you want to stay with the stable 0.5.x
branch you must switch to the `sound-juicer-0-5` branch.

So, what will be in SJ2? Frequent readers will roughly know my plans,
and currently it just uses the GNOME Media Profiles (thanks to Dan
Berger), but it will soon have a [new user
interface](http://www.burtonini.com/computing/screenshots/sj2-mockup-main-2.png),
CD playing support, a probably a gnome-vfs method, and other really cool
things.

<small>NP: <cite>Out Of Season</cite>, Beth Gibbons and Rustin'
Man</small>
