Title: Sound Juicer 2 Mockup
Date: 2004-09-17 00:00
Tags: tech
Slug: sound-juicer-2-mockup

After a brief hacking sprint on contact-lookup-applet and Sound Juicer
genre/profiles, I thought I'd have a quick play with my Sound Juicer 2
mockup. This is the latest screenshot:

![](http://www.burtonini.com/computing/screenshots/sj2-mockup-main-2.png){}

I've merged the track numbers and extract status into a single column,
and added an icon which will represent the currently playing or
extracted track. The plan is to remove the extract progress popup and
replace it with an icon indicating the current track and use the
progress bar for more detailed progress, but I'm not really sure that
this is a good idea. I'll also need some way of showing the currently
playing track, maybe show a small "play" icon in the same place?

All comments welcome, but I am intent on turning SJ into a replacement
for for the GNOME CD Player, so stop telling me I shouldn't!

Oh and Tigert, Jimmac, if you are reading: gnome-icon-theme needs
`stock_media-eject` and `stock_media-volume-mute`. Eject should be easy
to fit in with the other media icons, and mute is just the volume icon
minus the sound waves (I've stolen from Totem which stole from
Rhythmbox). Thanks! `:)`

<small>NP: <cite>Spirit</cite>, Jewel</small>
