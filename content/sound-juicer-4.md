Title: Sound Juicer
Date: 2009-11-25 22:39
Tags: tech
Slug: sound-juicer-4

Sound Juicer is a clean, mean, and lean CD ripper for GNOME 2.

It sports a clean interface and simple preferences, aiming to do The
Right Thing and What You Mean all of the time. It requires GNOME and
GStreamer.

### Screenshots

-   [Main
    window](http://www.burtonini.com/computing/screenshots/sj-main.png)
-   [Preferences
    dialog](http://www.burtonini.com/computing/screenshots/sj-prefs.png)

### Download

Latest download:
[sound-juicer-2.28.1.tar.bz2](http://www.burtonini.com/computing/sound-juicer-2.28.1.tar.bz2).

Bugs can be reported at <http://bugzilla.gnome.org/>. View the list of
[currently open
bugs](http://bugzilla.gnome.org/buglist.cgi?product=sound-juicer&bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED&cmdtype=doit&form_name=query).

Love Sound Juicer? Want to help the developer save for the deposit on a
house? You can help!

<form action="https://www.paypal.com/cgi-bin/webscr" method="post">
<input type="hidden" name="cmd" value="_s-xclick"></input>
<input type="image" src="https://www.paypal.com/en_US/i/btn/x-click-but21.gif" border="0" name="submit" alt="Make payments with PayPal - it's fast, free and secure!"></input>
<input type="hidden" name="encrypted" value="-----BEGIN PKCS7-----MIIHFgYJKoZIhvcNAQcEoIIHBzCCBwMCAQExggEwMIIBLAIBADCBlDCBjjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAkNBMRYwFAYDVQQHEw1Nb3VudGFpbiBWaWV3MRQwEgYDVQQKEwtQYXlQYWwgSW5jLjETMBEGA1UECxQKbGl2ZV9jZXJ0czERMA8GA1UEAxQIbGl2ZV9hcGkxHDAaBgkqhkiG9w0BCQEWDXJlQHBheXBhbC5jb20CAQAwDQYJKoZIhvcNAQEBBQAEgYCJ9uJosmyPKk/GYQhhfhJaZiUE2aHV26AtY3yFCJBeqOfl78t04eIrcp/GqtKuvLXAolqSxkARwxyp3X3vUtzYrLaILsa8AE2HO+x3PkcMve1iQRQbzUv5UMrjoaaijtZr4DX2RlXTyle+J6rYGDut9j9tIhvxW7G2RLBle+GGXzELMAkGBSsOAwIaBQAwgZMGCSqGSIb3DQEHATAUBggqhkiG9w0DBwQIdBFaJ4OYD7eAcFRwbCyX81J34B8P9OKha5s+twyt8Gdi7GxtoC0YcFq9KAsJr8urI66Ffl8ealN3xBW2fMAPvqQL+wllq4TBVtdd2m77SqH5Z4cWbxkF4lrluc8Xy5DRC8HVFBcpr4951qrQolBPTSbhANILyq/cRWugggOHMIIDgzCCAuygAwIBAgIBADANBgkqhkiG9w0BAQUFADCBjjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAkNBMRYwFAYDVQQHEw1Nb3VudGFpbiBWaWV3MRQwEgYDVQQKEwtQYXlQYWwgSW5jLjETMBEGA1UECxQKbGl2ZV9jZXJ0czERMA8GA1UEAxQIbGl2ZV9hcGkxHDAaBgkqhkiG9w0BCQEWDXJlQHBheXBhbC5jb20wHhcNMDQwMjEzMTAxMzE1WhcNMzUwMjEzMTAxMzE1WjCBjjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAkNBMRYwFAYDVQQHEw1Nb3VudGFpbiBWaWV3MRQwEgYDVQQKEwtQYXlQYWwgSW5jLjETMBEGA1UECxQKbGl2ZV9jZXJ0czERMA8GA1UEAxQIbGl2ZV9hcGkxHDAaBgkqhkiG9w0BCQEWDXJlQHBheXBhbC5jb20wgZ8wDQYJKoZIhvcNAQEBBQADgY0AMIGJAoGBAMFHTt38RMxLXJyO2SmS+Ndl72T7oKJ4u4uw+6awntALWh03PewmIJuzbALScsTS4sZoS1fKciBGoh11gIfHzylvkdNe/hJl66/RGqrj5rFb08sAABNTzDTiqqNpJeBsYs/c2aiGozptX2RlnBktH+SUNpAajW724Nv2Wvhif6sFAgMBAAGjge4wgeswHQYDVR0OBBYEFJaffLvGbxe9WT9S1wob7BDWZJRrMIG7BgNVHSMEgbMwgbCAFJaffLvGbxe9WT9S1wob7BDWZJRroYGUpIGRMIGOMQswCQYDVQQGEwJVUzELMAkGA1UECBMCQ0ExFjAUBgNVBAcTDU1vdW50YWluIFZpZXcxFDASBgNVBAoTC1BheVBhbCBJbmMuMRMwEQYDVQQLFApsaXZlX2NlcnRzMREwDwYDVQQDFAhsaXZlX2FwaTEcMBoGCSqGSIb3DQEJARYNcmVAcGF5cGFsLmNvbYIBADAMBgNVHRMEBTADAQH/MA0GCSqGSIb3DQEBBQUAA4GBAIFfOlaagFrl71+jq6OKidbWFSE+Q4FqROvdgIONth+8kSK//Y/4ihuE4Ymvzn5ceE3S/iBSQQMjyvb+s2TWbQYDwcp129OPIbD9epdr4tJOUNiSojw7BHwYRiPh58S1xGlFgHFXwrEBb3dgNbMUa+u4qectsMAXpVHnD9wIyfmHMYIBmjCCAZYCAQEwgZQwgY4xCzAJBgNVBAYTAlVTMQswCQYDVQQIEwJDQTEWMBQGA1UEBxMNTW91bnRhaW4gVmlldzEUMBIGA1UEChMLUGF5UGFsIEluYy4xEzARBgNVBAsUCmxpdmVfY2VydHMxETAPBgNVBAMUCGxpdmVfYXBpMRwwGgYJKoZIhvcNAQkBFg1yZUBwYXlwYWwuY29tAgEAMAkGBSsOAwIaBQCgXTAYBgkqhkiG9w0BCQMxCwYJKoZIhvcNAQcBMBwGCSqGSIb3DQEJBTEPFw0wNTA4MjMwODUyMjFaMCMGCSqGSIb3DQEJBDEWBBQlApjWVtftMsFFkujNpQzfOq2KWjANBgkqhkiG9w0BAQEFAASBgHU6pcx35m5JWmSVmBMLS12OWjjxC0+to4e25pufAi0P8g72ejwnw62gu8yWmeJf/XXA/tMV0alDRZ/o/j60mLCzO8jp+68PnREU/4A7uL3RM4b6kg3Rnmux7Kajv8WFFbLYgv0jPz2m+k8PJfauZBOpNw+3z9mLtiOGNtOAYRon-----END PKCS7-----"></input>

</form>

