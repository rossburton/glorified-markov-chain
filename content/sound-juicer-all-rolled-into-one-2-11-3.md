Title: Sound Juicer "All Rolled Into One" 2.11.3
Date: 2005-07-01 19:24
Tags: tech
Slug: sound-juicer-all-rolled-into-one-2-11-3

Sound Juicer "All Rolled Into One" 2.11.3 is out. Tarballs are available
[on
`burtonini.com`](http://www.burtonini.com/computing/sound-juicer-2.11.3.tar.gz),
or from the [GNOME FTP
servers](ftp://ftp.gnome.org/pub/gnome/sources/sound-juicer/2.11/).

This is a *rocking* release as it's the first to feature a Play button,
thanks to Ronald Bultje. Give it hell people!

-   Can now play CDs (Ronald Bultje)
-   The genre field is now an auto-completing text entry
-   Fix various file writing bugs caused by the move to gnome-vfs

Of course thanks to the translators: Abel Cheung (zh\_TW), Adam
Weinberger (en\_CA), Frank Arnold (de), Mário Vrablanský (sk), Paisa
Seeluangsawat (th), Terance Edward Sola (nb and no).

<small>NP: <cite>Animal Magic</cite>, Bonobo</small>
