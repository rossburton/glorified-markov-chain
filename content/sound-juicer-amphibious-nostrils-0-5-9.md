Title: Sound Juicer "Amphibious Nostrils" 0.5.9
Date: 2004-01-07 19:35
Tags: tech
Slug: sound-juicer-amphibious-nostrils-0-5-9

Sound Juicer "Amphibious Nostrils" 0.5.9 -- download the [tarball
here](http://www.burtonini.com/computing/sound-juicer-0.5.9.tar.gz).
Debian packages available in [my
repository](http://www.burtonini.com/debian) and are in the upload
queue.

-   Use the new tagging API if using GStreamer 0.7.3
    (Christophe Fergeau)
-   Created files have correct permissions (me)
-   Better labels in the Preferences (Jens Knutson)
-   Several memory leaks fixed (Michael Henson)
-   distcheck fixes (Thomas Vander Stichele)

