Title: Sound Juicer "And It Ain't Even 9 In The Morning, Sorry I'm Late" 2.28.0
Date: 2009-09-23 03:54
Tags: tech
Slug: sound-juicer-and-it-aint-even-9-in-the-morning-sorry-im-late-2-28-0

Sound Juicer "And It Ain't Even 9 In The Morning, Sorry I'm Late" 2.28.0
has been released. Tarballs are available [on
`burtonini.com`](http://www.burtonini.com/computing/sound-juicer-2.28.0.tar.bz2),
or from the [GNOME FTP
servers](ftp://ftp.gnome.org/pub/gnome/sources/sound-juicer/2.28/). Very
little in the 2.27 cycle...

-   Many translations
-   Updated documentation
-   Disable paranoia on playback (Bastien Nocera)
-   Fix leaks and crashes in the metadata fetches (BN)

Did I mention that SJ could really do with a dedicated (co)maintainer?
