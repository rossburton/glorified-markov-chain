Title: Sound Juicer Bazaar Mirror
Date: 2006-10-22 18:45
Tags: tech
Slug: sound-juicer-bazaar-mirror

If anyone out there fancies hacking on Sound Juicer (hint hint) to
tackle the large number of [feature requests in
Bugzilla](http://bugzilla.gnome.org/browse.cgi?product=sound-juicer)
(hint hint), then its just become easier if you don't have a GNOME CVS
committer account: Launchpad now mirrors the Sound Juicer module into a
Bazaar (bzr-ng) repository. To grab it, simply do:

    bzr branch http://bazaar.launchpad.net/~vcs-imports/sound-juicer/main my-sj

When that finishes, you'll have a branch of the SJ module called
`my-sj`, which you can edit, commit, and otherwise hack on as you want,
and you can either easily generate diffs for submitting into Bugzilla,
or publish the branch online.
