Title: Sound Juicer "Bonnie and Clyde" 2.26.1
Date: 2009-04-10 18:04
Tags: tech
Slug: sound-juicer-bonnie-and-clyde-2-26-1

Sound Juicer "Bonnie and Clyde" 2.26.1 has been released. Tarballs are
available [on
`burtonini.com`](http://www.burtonini.com/computing/sound-juicer-2.26.1.tar.bz2),
or from the [GNOME FTP
servers](ftp://ftp.gnome.org/pub/gnome/sources/sound-juicer/2.26/). Some
crashes have been fixes:

-   Read the track artist instead of album artist in Musicbrain3
-   Don't crash if the release date is unknown
-   Read tracks when falling back to gvfs

Finally, a call for someone with deep LAME knowledge. The GStreamer LAME
element is, well, lame because it sets a number of properties to default
values that make it very difficult for LAME to work well. Someone who
understands how all of the LAME settings operate needs to sit down, vet
the settings and remove the pointless ones, unset most of the rest,
leaving the 'preset' setting as the only one which has a default value.
At the moment there are many contradictory default settings which mean
LAME produces rather badly encoded files. Any takers?
