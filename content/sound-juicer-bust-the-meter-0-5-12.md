Title: Sound Juicer "Bust The Meter" 0.5.12
Date: 2004-06-08 18:50
Tags: tech
Slug: sound-juicer-bust-the-meter-0-5-12

Sound Juicer "Bust The Meter" 0.5.12 is available -- download the
[tarball
here](http://www.burtonini.com/computing/sound-juicer-0.5.12.tar.gz).
Debian packages available in [my
repository](http://www.burtonini.com/debian) and are in the upload queue
as usual.

Some people may claim to have seen me declare version 0.5.12 in the
Changelog a month ago, and some may even go as far as brazenly stating
that I even declared 0.5.13 in the Changelog and configure.in last week.
These people are no better than child-molesting Nazi's, and are probably
**Commies** too. If you know of these "people", alert the authorities as
soon as you can. Thank you.

-   Translate the example track/album strings in Preferences
    (Mariano Suárez-Alvarez)
-   Close the Prefs dialog when the WM close button is pressed (Mariano)
-   Monitor the strip and eject prefs for changes correctly
-   Add %tN-%tA-%tT as a file pattern
-   Use a sane drive if the one in GConf doesn't exist (Bastien Nocera)
-   Better error messages when the disk is full (Bastien)
-   Mark Eject for localisation (Stephane Raimbault)
-   Don't crash if the files exists and the user skips every track
-   Make the Help button work (Brent Fox)
-   Add double quotes to the set of characters to strip when sanitising
    paths
-   Handle the GStreamer 0.8 error callback prototype
    (Christophe Fergeau)
-   Stop building with SJ 0.7.x
-   Add colons to the Prefs dialog
-   Update libbacon

<small>NP:
<cite>[Tourist](http://www.amazon.co.uk/exec/obidos/ASIN/B00004SYZ8)</cite>,
St. Germain</small>
