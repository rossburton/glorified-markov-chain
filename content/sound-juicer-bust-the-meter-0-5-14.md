Title: Sound Juicer "Bust The Meter" 0.5.14
Date: 2004-10-11 22:58
Tags: tech
Slug: sound-juicer-bust-the-meter-0-5-14

Sound Juicer "Bust The Meter" 0.5.14 is available -- download the
[tarball
here](http://www.burtonini.com/computing/sound-juicer-0.5.14.tar.gz).
Debian packages available in [my
repository](http://www.burtonini.com/debian) and are in the upload queue
as usual.

-   Don't corrupt the title/artist entries when changing CD
-   Sort the genre list at runtime (Christophe Fergeau)
-   Handle unset values in GConf (Colin Walters)
-   Fixed HAL compile (Colin Walters and Sjoerd Simons)

<small>NP: <cite>Worldwide Underground</cite>, Erykah Badu</small>
