Title: Sound Juicer "But It's All Good Cos I'm The Soul Controller" 2.14.4
Date: 2006-05-27 00:44
Tags: tech
Slug: sound-juicer-but-its-all-good-cos-im-the-soul-controller-2-14-4

Sound Juicer "But It's All Good Cos I'm The Soul Controller" 2.14.4 is
out. Tarballs are available [on
`burtonini.com`](http://www.burtonini.com/computing/sound-juicer-2.14.4.tar.gz),
or from the [GNOME FTP
servers](ftp://ftp.gnome.org/pub/gnome/sources/sound-juicer/2.14/). Some
small bug fixes:

-   Only manipulate track stores if there is something to do
-   Fix progress bar calculation (John Thacker, \#339062)
-   Fix CDIO check (John N. Laliberte, \#339303)
-   Save and restore maximised state (Luca Cavalli, \#340038)

<small>NP: <cite>Only Built For Cuban Linx...</cite>, Raekwon</small>
