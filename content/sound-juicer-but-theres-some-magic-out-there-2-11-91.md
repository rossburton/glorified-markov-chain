Title: Sound Juicer "But There's Some Magic Out There"" 2.11.91
Date: 2005-08-09 16:52
Tags: tech
Slug: sound-juicer-but-theres-some-magic-out-there-2-11-91

Sound Juicer "But There's Some Magic Out There" 2.11.91 is out. Tarballs
are available [on
`burtonini.com`](http://www.burtonini.com/computing/sound-juicer-2.11.91.tar.gz),
or from the [GNOME FTP
servers](ftp://ftp.gnome.org/pub/gnome/sources/sound-juicer/2.11/).

-   Spanish Manual! (Francisco Javier F. Serrador)
-   Add Previous/Next Track menu items (Ross, Raj M Madhan)
-   Play the selected track when Play is activated (Raj)
-   The Extract checkbox is now titleless
-   Respect the check boxes for playback (Ronald)
-   Handle playing state changes in an idle handler (Ronald)
-   Improved Play/Pause behaviour (Ronald)
-   Better volume control (Ronald)
-   Disable Reread whilst playing (Raj)

Thanks to the ever-working translators: Adam Weinberger (en\_CA), Alan
Horkan (en\_GB), Ankit Patel (gu), Clytie Siddall (vi), Francisco Javier
F. Serrador (es), Frank Arnold (de), Funda Wang (zh\_CN), Gabor Kelemen
(hu), Gnome PL Team (pl), Ilkka Tuohela (fi), Kjartan Maraas (nb),
Kjartan Maraas (no), Michiel Sikkes (nl), Miloslav Trmac (cs), Nikos
Charonitakis (el), Paisa Seeluangsawat (th), Priit Laes (et), Raphael
Higino (pt\_BR), Rostislav Raykov (bg).
