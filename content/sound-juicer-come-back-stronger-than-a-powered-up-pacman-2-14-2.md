Title: Sound Juicer "Come Back Stronger Than A Powered-up Pacman" 2.14.2
Date: 2006-04-10 18:00
Tags: tech
Slug: sound-juicer-come-back-stronger-than-a-powered-up-pacman-2-14-2

Sound Juicer "Come Back Stronger Than A Powered-up Pacman" 2.14.2 is
out. Tarballs are available [on
`burtonini.com`](http://www.burtonini.com/computing/sound-juicer-2.14.2.tar.gz),
or from the [GNOME FTP
servers](ftp://ftp.gnome.org/pub/gnome/sources/sound-juicer/2.14/). Just
a few bug fixes:

-   Don't set incorrect sortnames (Peter Oliver)
-   Don't write encoder tags, write duration tag
-   Reset the window title more
-   Use po/LINGUAS (Wouter Bolsterlee)
-   Fix id3tag plugin initialisation hopefully
-   Remove gcc-sim (Damien Charbery)

The 2.14.x series of Sound Juicer releases is dedicated to Iain Holmes,
who said he loved the Kaiser Chiefs. Or was it “loathed”, I can't
recall. Anyway, the long and never-ending series of 2.14.x releases are
dedicated to him.

<small>NP: DJ TeeBee live at The Breezeblock</small>
