Title: Sound Juicer "Cosmo Retro Intro Outro" 2.11.2
Date: 2005-06-22 20:42
Tags: tech
Slug: sound-juicer-cosmo-retro-intro-outro-2-11-2

Sound Juicer "Cosmo Retro Intro Outro" 2.11.2 is out. Tarballs are
available [on
`burtonini.com`](http://www.burtonini.com/computing/sound-juicer-2.11.2.tar.gz),
or from the [GNOME FTP
servers](ftp://ftp.gnome.org/pub/gnome/sources/sound-juicer/2.11/).

This release is the first to use gnome-vfs to write the songs, so it
will need some serious testing to make sure I didn't break anything. Can
I ask everyone running GNOME from CVS, or tracking the 2.11 series, to
rip at least one album? Please? Can everyone who tests this version
please send me an email, and I'll pester and moan until I get 20 mails.
I may even post pictures of poo cookies...

-   Uses gnome-vfs to write songs
-   Write MusicBrainz identifiers as IDs not URLs
-   Don't crash when freeing albums without date fields
-   Don't warn when looking for the Glade (Bastien Nocera)

And of course, thanks to the translators: Elian Myftiu (sq), Frank
Arnold (de), Ignacio Casal Quinteiro (gl), Kjartan Maraas (nb,no),
Martin Willemoes Hansen (da), Michiel Sikkes (nl), Miloslav Trmac (cs),
Takeshi AIHANA (ja), Jayaradha (ta).
