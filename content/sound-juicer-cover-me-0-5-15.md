Title: Sound Juicer "Cover Me!" 0.5.15
Date: 2004-12-10 11:08
Tags: tech
Slug: sound-juicer-cover-me-0-5-15

Sound Juicer "Bust The Meter" 0.5.15 is available -- download the
[tarball
here](http://www.burtonini.com/computing/sound-juicer-0.5.15.tar.gz).
Debian packages available in [my
repository](http://www.burtonini.com/debian) and are in the upload queue
as usual.

-   Handle errors when transforming filename encoding fails
    (Frederic Crozat)
-   Handle MusicBrainz saying it has found a matching album when it
    hasn't (FC)
-   Fix the "time remaining" calculations (hondaguru)
-   Updated libbacon, fixing various issues
-   Fix crashes due to the idle handler not being removed
    (Colin Walters)
-   Stop using the deprecated \_() in libgnome (Mariano Suárez-Alvarez)

<small>NP: <cite>Two Pages</cite>, 4 Hero</small>
