Title: Sound Juicer "Deaf, Dumb, and Blind, You Just Keep On Pretending" 2.12.2
Date: 2005-09-19 20:57
Tags: tech
Slug: sound-juicer-deaf-dumb-and-blind-you-just-keep-on-pretending-2-12-2

Sound Juicer "Deaf, Dumb, and Blind, You Just Keep On Pretending" 2.12.2
is out. Tarballs are available [on
`burtonini.com`](http://www.burtonini.com/computing/sound-juicer-2.12.2.tar.gz),
or from the [GNOME FTP
servers](ftp://ftp.gnome.org/pub/gnome/sources/sound-juicer/2.12/). This
release is an emergency release to fix a stupid bug:

-   Fix `--device`

