Title: Sound Juicer "Desire Is The Father Of Invention" 0.5.13
Date: 2004-09-28 21:23
Tags: tech
Slug: sound-juicer-desire-is-the-father-of-invention-0-5-13

Sound Juicer "Desire Is The Father Of Invention" 0.5.13 is available --
download the [tarball
here](http://www.burtonini.com/computing/sound-juicer-0.5.13.tar.gz).
Debian packages available in [my
repository](http://www.burtonini.com/debian) and are in the upload queue
as usual.

This releases most obvious change is a potential speed up, and a genre
drop-down. The UI for this will probably change over time so please
don't tell me how crap you think it is.

-   Add a genre drop-down
-   Update CD code, and use HAL if available
-   Depend on GTK+ 2.4 and use the new file chooser
-   Don't corrupt data if the disc is changed whilst the Rip Complete
    dialogue is open (Sean Proctor)
-   More charset escaping (Frederic Crozat)
-   Add more GStreamer iterate functions to speed ripping (David Arnold)
-   Be a bit more paranoid about incorrect GConf keys
-   Allow the MusicBrainz server to be changed
-   Lots of little bug fixes but lots of fabby people

