Title: Sound Juicer "Died To Make This Sound" 2.22.0
Date: 2008-03-10 15:36
Tags: tech
Slug: sound-juicer-died-to-make-this-sound-2-22-0

Sound Juicer "Died To Make This Sound" 2.22.0 is available now. Tarballs
are available [on
`burtonini.com`](http://www.burtonini.com/computing/sound-juicer-2.22.0.tar.bz2),
or from the [GNOME FTP
servers](ftp://ftp.gnome.org/pub/gnome/sources/sound-juicer/2.22/). Last
minute fixes, cleanups, and translations abound!

-   Fix various crashes in the preferences dialogs (thanks
    Matthew Martin)
-   Translate the genres (thanks Brian Geppert)
-   Add a paused track state (thanks Brian Geppert)
-   Use the system icons for play/record (thanks Micharl Monreal)
-   Many many translations!

Thanks to everyone who helped with Sound Juicer 2.22, there has been a
huge influx of new contributors thanks to the GHOP and gnome-love
projects.
