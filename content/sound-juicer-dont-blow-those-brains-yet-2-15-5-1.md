Title: Sound Juicer "Don't Blow Those Brains Yet" 2.15.5.1
Date: 2006-08-21 04:12
Tags: tech
Slug: sound-juicer-dont-blow-those-brains-yet-2-15-5-1

Sound Juicer "Don't Blow Those Brains Yet" 2.15.5.1 is out. Tarballs are
available [on
`burtonini.com`](http://www.burtonini.com/computing/sound-juicer-2.15.5.1.tar.gz),
or from the [GNOME FTP
servers](ftp://ftp.gnome.org/pub/gnome/sources/sound-juicer/2.15/). Hmm,
I've done very little here, and there are 91 open bugs in Bugzilla. This
sucks.

-   Update the icon cache on install (\#348972)
-   Fix intltool requirements (Kjartan Maraas)

**Update:** 2.15.5.1 is released, with a tarball that will configure.
