Title: Sound Juicer "Don't Go Back To Dalston" 2.26.0
Date: 2009-03-17 22:55
Tags: tech
Slug: sound-juicer-dont-go-back-to-dalston-2-26-0

Sound Juicer "Don't Go Back To Dalston" 2.26.0 has been released.
Tarballs are available [on
`burtonini.com`](http://www.burtonini.com/computing/sound-juicer-2.26.0.tar.bz2),
or from the [GNOME FTP
servers](ftp://ftp.gnome.org/pub/gnome/sources/sound-juicer/2.26/). Only
translation updates this time, sorry.
