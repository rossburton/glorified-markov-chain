Title: Sound Juicer "Don't Just Sit There Going Crazy On Your Own" 2.14.3
Date: 2006-04-14 21:38
Tags: tech
Slug: sound-juicer-dont-just-sit-there-going-crazy-on-your-own-2-14-3

Sound Juicer "Don't Just Sit There Going Crazy On Your Own" 2.14.3 is
out. Tarballs are available [on
`burtonini.com`](http://www.burtonini.com/computing/sound-juicer-2.14.3.tar.gz),
or from the [GNOME FTP
servers](ftp://ftp.gnome.org/pub/gnome/sources/sound-juicer/2.14/). A
small selection of bug fixes before I branch:

-   Fix reversed check for invalid iterator, allowing playback to work!
-   Add some buffering to the playback pipeline
-   Reset the window title even more
-   Namespace the copied `gsttaglibmux` to avoid conflicts

<small>NP: <cite>1st Born Second</cite>, Bilal</small>
