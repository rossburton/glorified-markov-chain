Title: Sound Juicer "Down The Middle Drops One More Grain Of Sand" 2.12.0
Date: 2005-09-05 12:17
Tags: tech
Slug: sound-juicer-down-the-middle-drops-one-more-grain-of-sand-2-12-0

Sound Juicer "Down The Middle Drops One More Grain Of Sand" 2.12.0 is
out. Tarballs are available [on
`burtonini.com`](http://www.burtonini.com/computing/sound-juicer-2.12.0.tar.gz),
or from the [GNOME FTP
servers](ftp://ftp.gnome.org/pub/gnome/sources/sound-juicer/2.12/). Very
few changes since 2.11.x:

-   Bind F1 to Help
-   **\[update\]** Kick-arse updated documentation (thanks Shaun!)

But plenty of changes for people who haven't used 2.11 including CD
playback, threaded extraction, uses gnome-vfs to write the songs, and
genre support.

Thanks to the ever-working translators: Danilo Šegan (sr), Mohammad Damt
(id), Clytie Siddall (vi), Jean-Michel Ardantz (fr), Michiel Sikkes
(nl), Roozbeh Pournader (fa).
