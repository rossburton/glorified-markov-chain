Title: Sound Juicer "Drop The Empty Pursuit Of Props" 2.21.92
Date: 2008-02-26 22:36
Tags: tech
Slug: sound-juicer-drop-the-empty-pursuit-of-props-2-21-92

Sound Juicer "Drop The Empty Pursuit Of Props" 2.21.92 is available now.
Tarballs are available [on
`burtonini.com`](http://www.burtonini.com/computing/sound-juicer-2.21.92.tar.bz2),
or from the [GNOME FTP
servers](ftp://ftp.gnome.org/pub/gnome/sources/sound-juicer/2.21/). Just
small fixes now:

-   Don't loop if the selected music directory doesn't exist
    (Matthew Martin)
-   When editing the album artist, unset the sortable artist name
-   Remove deprecated calls in BaconVolume (Michael Terry)

