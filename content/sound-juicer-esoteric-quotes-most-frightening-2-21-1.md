Title: Sound Juicer "Esoteric Quotes, Most Frightening" 2.21.1
Date: 2008-01-01 23:33
Tags: tech
Slug: sound-juicer-esoteric-quotes-most-frightening-2-21-1

Sound Juicer "Esoteric Quotes, Most Frightening" 2.21.1 is available
now. Tarballs are available [on
`burtonini.com`](http://www.burtonini.com/computing/sound-juicer-2.21.1.tar.bz2),
or from the [GNOME FTP
servers](ftp://ftp.gnome.org/pub/gnome/sources/sound-juicer/2.21/).
Special thanks to the wonderful people who worked on a few bugs I tagged
as gnome-love, and Carl-Anton for working on the SJ GHOP task!

-   Save custom genres (\#382667, thanks Jonh Wendell)
-   Make the progress bar smaller (\#460650, thanks Stefan Oderbolz)
-   Detect VFAT and NTFS filesystems and strip characters they can't
    handle (\#321436, thanks Carl-Anton Ingmarsson and GHOP)
-   Inhibit power management suspension when ripping (\#344947, thanks
    Carl-Anton and GHOP)
-   Don't crash when finished extracting (\#498500)
-   Remove the Rip Completed dialog (\#504639)
-   Set pipeline state to NULL when disposing (\#495410)
-   Escape the album and artist in the cluebar (\#504862, thanks
    Luca Cavalli)

