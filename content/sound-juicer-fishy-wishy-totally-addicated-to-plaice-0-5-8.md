Title: Sound Juicer "Fishy Wishy, Totally Addicated to Plaice" 0.5.8
Date: 2003-11-27 00:09
Tags: tech
Slug: sound-juicer-fishy-wishy-totally-addicated-to-plaice-0-5-8

Sound Juicer "Fishy Wishy, Totally Addicated to Plaice" 0.5.8 --
download the [tarball
here](http://www.burtonini.com/computing/sound-juicer-0.5.8.tar.gz).
Debian packages available in [my
repository](http://www.burtonini.com/debian), I'll upload to the archive
once I've sorted out my keys after the break in.

-   Don't enable deprecation by default, use --enable-deprecation
-   Add an --autostart option, so that extracting starts straight away
-   Add an "eject when finished" preference
-   Add an directory pattern of "no directories"
-   Disable more widgets in the main window when ripping (Brent Fox)
-   Rename Extract? to Extract
-   Use the status bar for progress notification (BF)
-   Don't crash as much (teuf)

