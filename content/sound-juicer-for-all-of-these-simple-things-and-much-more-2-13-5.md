Title: Sound Juicer "For All Of These Simple Things And Much More" 2.13.5
Date: 2006-02-12 18:50
Tags: tech
Slug: sound-juicer-for-all-of-these-simple-things-and-much-more-2-13-5

Sound Juicer "For All Of These Simple Things And Much More" 2.13.5 is
out. Tarballs are available [on
`burtonini.com`](http://www.burtonini.com/computing/sound-juicer-2.13.5.tar.gz),
or from the [GNOME FTP
servers](ftp://ftp.gnome.org/pub/gnome/sources/sound-juicer/2.13/). Bug
fixes:

-   Enable Musicbrainz tagging with GStreamer 0.10.3
-   Dutch manual (Tino Meinen)
-   Fix build with GStreamer 0.10.3
-   Disable Select All when there is no disc inserted
-   Use GnomeProgram, the help button uses it
-   Ignore unknown options instead of critically aborting
-   Remove unused variables (Ryan Lortie)

Translators: Priit Laes (et), Žygimantas Beručka (lt)
