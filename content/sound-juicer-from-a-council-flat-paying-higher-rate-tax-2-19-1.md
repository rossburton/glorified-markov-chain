Title: Sound Juicer "From a Council Flat Paying Higher Rate Tax" 2.19.1
Date: 2007-05-14 18:50
Tags: tech
Slug: sound-juicer-from-a-council-flat-paying-higher-rate-tax-2-19-1

Sound Juicer "From a Council Flat Paying Higher Rate Tax" 2.19.1 is out.
Tarballs are available [on
`burtonini.com`](http://www.burtonini.com/computing/sound-juicer-2.19.1.tar.gz),
or from the [GNOME FTP
servers](ftp://ftp.gnome.org/pub/gnome/sources/sound-juicer/2.19/).
Brown paper bag release to fix not one but two bugs causing SJ to not
start.

-   Fix type names, which caused an assertion on startup
-   Fix profile checking, which caused SJ to fail to parse any profiles
-   Fix error handling when using errno (\#438122)

