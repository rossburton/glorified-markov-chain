Title: Sound Juicer "G D M F S O B" 2.19.3
Date: 2007-08-13 17:48
Tags: tech
Slug: sound-juicer-g-d-m-f-s-o-b-2-19-3

Sound Juicer "G D M F S O B" 2.19.3 is out. Tarballs are available [on
`burtonini.com`](http://www.burtonini.com/computing/sound-juicer-2.19.3.tar.gz),
or from the [GNOME FTP
servers](ftp://ftp.gnome.org/pub/gnome/sources/sound-juicer/2.19/).

-   When removing sources be more paranoid. I hope this fixes the
    many-duplicated crasher
-   Translate the program description (\#450161, Gabor Kelemen)
-   Bump libmusicbrainz requirement

