Title: Sound Juicer "Got A Brand New Bag" 2.15.2.1
Date: 2006-05-18 01:25
Tags: tech
Slug: sound-juicer-got-a-brand-new-bag-2-15-2-1

Sound Juicer "Got A Brand New Bag" 2.15.2.1 is out. Tarballs are
available [on
`burtonini.com`](http://www.burtonini.com/computing/sound-juicer-2.15.2..1tar.gz),
or from the [GNOME FTP
servers](ftp://ftp.gnome.org/pub/gnome/sources/sound-juicer/2.15/). This
is a brown paper bag release to fix the small translations problem: they
didn't exist in 2.15.2.

**Update:**: rebuilt the tarball, this time it actually works.
