Title: Sound Juicer "Grace the Corners of Our Rooms" 2.11.1
Date: 2005-06-08 19:12
Tags: tech
Slug: sound-juicer-grace-the-corners-of-our-rooms-2-11-1

Sound Juicer "Grace the Corners of Our Rooms" 2.11.1 is finally out, the
rather late first release in the series up to GNOME 2.12. Tarballs are
available [on
`burtonini.com`](http://www.burtonini.com/computing/sound-juicer-2.11.1.tar.gz),
or from the [GNOME FTP
servers](ftp://ftp.gnome.org/pub/gnome/sources/sound-juicer/2.11/).

-   Initialise audio profiles before creating the extractor
-   Fix some pointer tests which may crash
-   Focus the list view on startup
-   Depend on nautilus-burn 2.11.1, and pass around `NautilusBurnDrive`
    objects instead of device names.
-   Don't free `GError`s in the pipeline error handler, this makes SJ
    crash
-   Add a dummy C++ file to fix builds where gcc version != g++ version
-   Uncheck each track as it is extracted
-   Put lots more metadata into the songs (MusicBrainz IDs and
    release date)
-   Check for and switch to an existing instance of SJ when starting
    (Bastien Nocera)

And of course, thanks to the translators: Adam Weinberger (en\_CA),
Ahmad Riza H Nst (id), Canonical Ltd (xh), Dafydd Harries (cy), Hendrik
Richter (de), Ivar Smolin (et), Jyotsna Shrestha (ne), Mugurel Tudor
(ro), Nikos Charonitakis (el), Raphael Higino (pt\_BR), Steve Murphy
(rw).
