Title: Sound Juicer "Harder Now With Higher Speed" 2.23.0
Date: 2008-06-05 20:34
Tags: tech
Slug: sound-juicer-harder-now-with-higher-speed-2-23-0

Sound Juicer "Harder Now With Higher Speed" 2.23.0 has finally been
released.. Tarballs are available [on
`burtonini.com`](http://www.burtonini.com/computing/sound-juicer-2.23.0.tar.bz2),
or from the [GNOME FTP
servers](ftp://ftp.gnome.org/pub/gnome/sources/sound-juicer/2.23/). Hot
new features!

-   Port to GIO (Michael Terry)
-   Update URL handling for New GIO World Order (Bastien Nocera)
-   Fix display problems with the cluebar (Pekka Vuorela)
-   Add audio preview when overwriting (Luca Cavalli)
-   Use GtkVolmeButton instead of BaconVolume (MT)
-   Fix crash when no profile is selected (Matthew Martin)
-   Add \[\]&lt;&gt; to the special character list (MM)
-   Make the year and disc entries a11y (Patrick Wade)
-   Fix error handling in CD playback (Tim-Philipp Müller)
-   Require intltool 0.40

I really need some heavy testing on the GIO rewrite, so please try and
extract tracks to as many different targets as possible. Although I
expect confirmation that using an unmounted remote location currently
fails, it should be possible to use this to write to Samba, OBEX-FTP,
and so on.
