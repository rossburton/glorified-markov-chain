Title: Sound Juicer "Hell Is Round The Corner Where I Shelter" 2.9.92
Date: 2005-02-28 20:01
Tags: tech
Slug: sound-juicer-hell-is-round-the-corner-where-i-shelter-2-9-92

Sound Juicer "Hell Is Round The Corner Where I Shelter" 2.9.92 is out,
also known as Release Candidate 1. Give this a good hammering, please.

-   Depend on nautilus-burn &gt;= 2.9
-   Set the profile key to a sensible default
-   Unselect the profile combo when no profile is selected in GConf
-   Use gi18n.h (Crispin Flowerday)
-   Use the new GTK+ about dialog (CF)
-   Display the output format in the profile chooser (Raj Madhan)
-   Quit correctly (RM)
-   Updated README

I've been really bad at crediting translators, so I'll credit everyone
who translated since 0.6 and will try and keep this up. Thanks everyone!

Adam Weinberger (en\_CA), Ankit Patel (gu), Arafat Medini (ar),
Christian Rose (sv), Christophe Fergeau (fr), David Lodge (en\_GB),
Duarte Loreto (pt), Elian Myftiu (sq), Emrah Unal (tr), Francesco
Marletta (it), Francisco Javier F. Serrador (es), Funda Wang (zh\_CN),
Gnome PL Team (pl), Hendrik Brandt (de), Jordi Mallach (ca), Kjartan
Maraas (nb), Kjartan Maraas (no), Laszlo Dvornik (hu), Leonid Kanter
(ru), Martin Willemoes Hansen (da), Maxim Dziumanenko (uk), Miloslav
Trmac (cs), Nikos Charonitakis (el), Priit Laes (et), Rajeev Shrestha
(ne), Raphael Higino (pt\_BR), Takeshi AIHANA (ja), Tino Meinen (nl),
Tommi Vainikainen (fi), Vladimir Petkov (bg), Young-Ho Cha (ko),
Žygimantas Beručka (lt)

As usual the tarball is
[here](http://www.burtonini.com/computing/sound-juicer-2.9.92.tar.gz),
but it is also available on the [GNOME FTP
server](ftp://ftp.gnome.org/pub/gnome/sources/sound-juicer/). I've not
done Debian Sarge packages yet as it needs GNOME 2.9, but Sebastien is
usually very quick with Ubuntu Hoary packages.

<small>NP: <cite>The Private Press</cite>, DJ Shadow</small>
