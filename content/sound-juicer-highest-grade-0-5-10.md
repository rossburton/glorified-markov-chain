Title: Sound Juicer "Highest Grade" 0.5.10
Date: 2004-02-05 23:33
Tags: tech
Slug: sound-juicer-highest-grade-0-5-10

Sound Juicer "Highest Grade" 0.5.10 -- download the [tarball
here](http://www.burtonini.com/computing/sound-juicer-0.5.10.tar.gz).
Debian packages available in [my
repository](http://www.burtonini.com/debian) and are in the upload
queue. This release fixes the very annoying hang on startup many people
were seeing.

-   Fix a double free, solving the hang on startup for some people
-   Translate the file/path pattern strings
-   Show estimated duration and ripping speed (Bastien)
-   Correctly kill the ripping idle callback, fixing some random crashes
-   Acknowledged that my release names are pretty weird at times

