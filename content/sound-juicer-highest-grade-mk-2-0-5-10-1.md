Title: Sound Juicer "Highest Grade Mk 2" 0.5.10.1
Date: 2004-02-05 23:51
Tags: tech
Slug: sound-juicer-highest-grade-mk-2-0-5-10-1

Sound Juicer "Highest Grade Mk 2" 0.5.10.1 -- download the [tarball
here](http://www.burtonini.com/computing/sound-juicer-0.5.10.1.tar.gz).
Debian packages available in [my
repository](http://www.burtonini.com/debian) and are in the upload
queue. This release is a brown-paper bag release to fix broken intltool
files.
