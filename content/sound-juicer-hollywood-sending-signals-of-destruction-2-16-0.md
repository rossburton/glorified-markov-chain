Title: Sound Juicer "Hollywood Sending Signals Of Destruction" 2.16.0
Date: 2006-09-05 04:09
Tags: tech
Slug: sound-juicer-hollywood-sending-signals-of-destruction-2-16-0

Sound Juicer "Hollywood Sending Signals Of Destruction" 2.16.0 is out.
Tarballs are available [on
`burtonini.com`](http://www.burtonini.com/computing/sound-juicer-2.16.0.tar.gz),
or from the [GNOME FTP
servers](ftp://ftp.gnome.org/pub/gnome/sources/sound-juicer/2.16/).

There are not a lot of interesting features in this compared to 2.14,
sorry!

-   Release date information added to metadata
-   Authenticated proxies work
-   Playback volume and window position and size saved

