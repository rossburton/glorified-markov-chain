Title: Sound Juicer "I Call Out To You And You Don't Save Me?" 2.25.3
Date: 2009-02-13 23:48
Tags: tech
Slug: sound-juicer-i-call-out-to-you-and-you-dont-save-me-2-25-3

Sound Juicer "I Call Out To You And You Don't Save Me?" 2.25.3 has been
released. Tarballs are available [on
`burtonini.com`](http://www.burtonini.com/computing/sound-juicer-2.25.3.tar.bz2),
or from the [GNOME FTP
servers](ftp://ftp.gnome.org/pub/gnome/sources/sound-juicer/2.25/). I
actually did some coding this time!

-   Put the disc number in the file name
-   Support multiple genres
-   Use libcanberra for event sounds
-   Handle custom patterns in the prefs dialog
-   Remove Musicbrainz data if the track data is changed
-   Fix disc number editing logic
-   And lots of bug fixes by many people

