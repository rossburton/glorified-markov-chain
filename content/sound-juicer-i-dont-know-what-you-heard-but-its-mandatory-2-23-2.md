Title: Sound Juicer "I Don't Know What You Heard But It's Mandatory" 2.23.2
Date: 2008-08-18 21:59
Tags: tech
Slug: sound-juicer-i-dont-know-what-you-heard-but-its-mandatory-2-23-2

Sound Juicer "I Don't Know What You Heard But It's Mandatory" 2.23.2 has
been released. Tarballs are available [on
`burtonini.com`](http://www.burtonini.com/computing/sound-juicer-2.23.2.tar.bz2),
or from the [GNOME FTP
servers](ftp://ftp.gnome.org/pub/gnome/sources/sound-juicer/2.23/). Lots
of fixes from the Amazing Matthew Martin:

-   Stop playback when the disc is re-read (Matthew Martin)
-   Only eject the disc if tracks were ripped (MM)
-   Don't try and move the non-existant temp file when skipping (MM)
-   Free the option context (Pierre Benz)
-   Don't block until n-c-b quits when copying discs
-   Fix playback track switching (MM)

