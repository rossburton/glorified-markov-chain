Title: Sound Juicer "I Don't Know Why She's Ridin' So High" 2.13.6
Date: 2006-02-28 15:45
Tags: tech
Slug: sound-juicer-i-dont-know-why-shes-ridin-so-high-2-13-6

Sound Juicer "I Don't Know Why She's Ridin' So High" 2.13.6 is out.
Tarballs are available [on
`burtonini.com`](http://www.burtonini.com/computing/sound-juicer-2.13.6.tar.gz),
or from the [GNOME FTP
servers](ftp://ftp.gnome.org/pub/gnome/sources/sound-juicer/2.13/). Yes,
this is too late for the 2.13 RC, but I thought I best get the package
out anyway. Bug fixes:

-   Rocking new icon from Lapo Calamandrei
-   UI Review attack on Preferences dialog
-   Don't cache metadata, it's a pain
-   Change the sample metadata to a real song
-   Show file extension in sample filename
-   Use × rather than x in progress bar
-   Untranslate strings that should not be translated

Translators: Ankit Patel (gu), Clytie Siddall (vi), Francisco Javier F.
Serrador (es), Gabor Kelemen (hu), Ignacio Casal Quinteiro (gl), Ilkka
Tuohela (fi), Iñaki Larrañaga (eu), Kjartan Maraas (nb), Kjartan Maraas
(no), Kostas Papadimas (el), Lasse Bang Mikkelsen (da), Leonid Kanter
(ru), Miloslav Trmac (cs), Priit Laes (et), Raphael Higino (pt\_BR),
Rhys Jones (cy), Rostislav Raykov (bg), Satoru SATOH (ja), Theppitak
Karoonboonyanan (th), Tino Meinen (nl), Vladimer SIchinava (ka), Woodman
Tuen (zh\_HK), Woodman Tuen (zh\_TW), Žygimantas Beručka (lt).
