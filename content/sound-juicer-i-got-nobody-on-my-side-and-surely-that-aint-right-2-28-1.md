Title: Sound Juicer "I Got Nobody On My Side And Surely That Ain't Right" 2.28.1
Date: 2009-11-25 22:43
Tags: tech
Slug: sound-juicer-i-got-nobody-on-my-side-and-surely-that-aint-right-2-28-1

Sound Juicer "I Got Nobody On My Side And Surely That Ain't Right"
2.28.1 has been released. Tarballs are available [on
`burtonini.com`](http://www.burtonini.com/computing/sound-juicer-2.28.1.tar.bz2),
or from the [GNOME FTP
servers](ftp://ftp.gnome.org/pub/gnome/sources/sound-juicer/2.28/).
Props to Bastien for doing most of the work here.

-   Many translations
-   Use gnome-session instead of gnome-power-manager to avoid the
    machine going to sleep (Richard Hughes)
-   Fix a few crashers when extracting an unknown CD (Bastien Nocera)
-   Fix CD-Text metadata gathering (BN)
-   Don't truncate submission URLs (BN)
-   Extract UUIDs to put in ripped files' metadata (Philipp Wolfer)
-   Fix some bugs in test program (Alex Larsson)

Bastien originally called this release <cite>Not the maintainer, lalala,
plug ears</cite> but we all know he is, right?
