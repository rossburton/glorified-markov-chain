Title: Sound Juicer "I Need A Little Love To Ease The Pain" 2.14.5
Date: 2006-07-24 22:38
Tags: tech
Slug: sound-juicer-i-need-a-little-love-to-ease-the-pain-2-14-5

Sound Juicer "I Need A Little Love To Ease The Pain" 2.14.5 is out.
Tarballs are available [on
`burtonini.com`](http://www.burtonini.com/computing/sound-juicer-2.14.5.tar.gz),
or from the [GNOME FTP
servers](ftp://ftp.gnome.org/pub/gnome/sources/sound-juicer/2.14/). This
releases rolls in the bug fixes from 2.15.4:

-   Add date information to all possible albums (Alex Lancaster)
-   Improve GStreamer error handling (Tim-Philipp Muller)
-   Don't crash when re-opening the Preferences dialog
-   Clear the genre field when re-reading the disk

