Title: Sound Juicer "I Should Be Crying, But I Just Can't Let It Show" 2.25.2
Date: 2009-02-03 20:22
Tags: tech
Slug: sound-juicer-i-should-be-crying-but-i-just-cant-let-it-show-2-25-2

Sound Juicer "I Should Be Crying, But I Just Can't Let It Show" 2.25.2
has been released. Tarballs are available [on
`burtonini.com`](http://www.burtonini.com/computing/sound-juicer-2.25.2.tar.bz2),
or from the [GNOME FTP
servers](ftp://ftp.gnome.org/pub/gnome/sources/sound-juicer/2.25/).

-   Port to Brasero (Luis Medinas)
-   Fix Solaris builds (Brian Cameron)
-   Drop libgnome (Iain Holmes, Emilio Pozuelo Monfort)
-   Fix conflicting mnemonics in the message area (Bastien Nocera)
-   Fix mb3 backend (Bastien Nocera)

