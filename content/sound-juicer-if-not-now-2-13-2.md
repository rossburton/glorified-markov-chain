Title: Sound Juicer "If Not Now" 2.13.2
Date: 2006-01-16 17:12
Tags: tech
Slug: sound-juicer-if-not-now-2-13-2

Sound Juicer "If Not Now" 2.13.2 is out. Tarballs are available [on
`burtonini.com`](http://www.burtonini.com/computing/sound-juicer-2.13.2.tar.gz),
or from the [GNOME FTP
servers](ftp://ftp.gnome.org/pub/gnome/sources/sound-juicer/2.13/). Lots
of changes here:

-   Fix handling of multiple albums (David Mandelberg)
-   Handle G\_FILENAME\_ENCODING (Colin Leroy)
-   Add Generic Name to the desktop file (Matthias Clasen)
-   Correct multiple-artist data imported from FreeDB (Bastien Nocera)
-   Remove leading periods when generating filenames
-   Add a Submit menu item to open the MusicBrainz web page
    (James Hensbridge)
-   Set the MusicBrainz proxy from gnome-vfs
-   Cache MusicBrainz metadata
-   Add support for the MusicBrainz "sortable name" attribute
    (Peter Oliver)
-   Make the Preferences dialog non-resizable, and fix layout
-   Save the main window size (using the rocking libgconf-bridge)
-   Change keybinding for Next/Previous, to avoid stomping Paste
-   Remove CDIO version check
-   Set default window icon
-   Protect against NULL drives (Marco Barisione)
-   Fix warnings with new libglade (BN)

Go testers go!

<small>NP: <cite>Tracy Chapman</cite>, Tracy Chapman</small>
