Title: Sound Juicer "It's Deeper Than The Darkest Sea" 2.21.3
Date: 2008-01-31 17:48
Tags: tech
Slug: sound-juicer-its-deeper-than-the-darkest-sea-2-21-3

Sound Juicer "It's Deeper Than The Darkest Sea" 2.21.3 is available now.
Tarballs are available [on
`burtonini.com`](http://www.burtonini.com/computing/sound-juicer-2.21.3.tar.bz2),
or from the [GNOME FTP
servers](ftp://ftp.gnome.org/pub/gnome/sources/sound-juicer/2.21/). More
hot features!

-   Add a Disc Number field, and magically populate it (Matthew Martin)
-   Add content/\* media types for Nautilus (Matthias Clasen)
-   Set a11y relationships on the cluebar (thanks Rich and Willie)
-   Fix play/pause (Bill O'Shea)
-   Handle the cdio element not being cdparanoia

