Title: Sound Juicer "It's Not Very Pretty I Tell Thee" 2.14.1
Date: 2006-04-04 08:22
Tags: tech
Slug: sound-juicer-its-not-very-pretty-i-tell-thee-2-14-1

Sound Juicer "It's Not Very Pretty I Tell Thee" 2.14.1 is out. Tarballs
are available [on
`burtonini.com`](http://www.burtonini.com/computing/sound-juicer-2.14.1.tar.gz),
or from the [GNOME FTP
servers](ftp://ftp.gnome.org/pub/gnome/sources/sound-juicer/2.14/). Lots
of bug fixes here:

-   Change CD lookup query to allow MB to proxy requests to FreeDB
-   Ship a copy of taglibid3mux and tell people to use id3mux when
    creating the MP3 profile
-   Remove SjProfileChooser, use gnome-media's widget
-   Preferences dialog isn't modal
-   Don't show the main window if there is an error
-   Handle NULL profiles without crashing the Preferences dialog
-   The duration and track number column are not resizable
    (Wouter Bolsterlee)
-   Don't hardcode compiler options, use GNOME\_COMPILE\_WARNINGS
-   Volumn should not display stock\_volume-0 for volume &gt; 0
    (Dennis Cranston)
-   Handle sortnames being NULL
-   Don't leak the GnomeProgram reference
-   Ensure the Play/Pause button stays the same size (Christian Neumair)
-   Don't progress change notifications from the pipeline
    (Christian Neumair)
-   Handle all paranoia modes (Marinus Schraal)
-   Check iterator access to shut up GCC
-   Fix the program name not being translated
-   Add MAINTAINERS
-   Install icons mode 0644 instead of 0755

Translators: Ales Nyakhaychyk (be), Clytie Siddall (vi), Daniel Nylander
(sv), Ivar Smolin (et), Kostas Papadimas (el), Petr Tomeš (cs),
Theppitak Karoonboonyanan (th), Vladimer Sichinava (ka).
