Title: Sound Juicer "It's Time Little Sparkle In My Eye To Fly" 0.6.1
Date: 2005-04-18 23:45
Tags: tech
Slug: sound-juicer-its-time-little-sparkle-in-my-eye-to-fly-0-6-1

I forgot to announce this when I did the release. Yes, it's Sound Juicer
0.6.1. This is basically SJ 0.6 with lots and lots of patches from SJ
2.10 applied, to fix bugs and add features without increasing the
dependencies beyond what is going to be in Debian Sarge. As such it
builds against GTK+ 2.6 and GNOME 2.8, whereas SJ 2.10 requires the
GNOME 2.10 stack. You can get it from the [usual
place](http://www.burtonini.com/computing/sound-juicer-0.6.1.tar.gz).

-   Ensure the pipeline has stopped when firing an error.
-   Use gnome-open instead of nautilus when opening folders
-   Fix many crashes in the Preferences dialog
-   HIGify the Preferences dialog
-   Set the default profile to a profile which exists
-   Quit correctly when ripping
-   Updated documentation

Thanks to everyone who submitted patches since 0.6, you all made this
release possible.

<small>NP: <cite>Moon Safari</cite>, Air</small>
