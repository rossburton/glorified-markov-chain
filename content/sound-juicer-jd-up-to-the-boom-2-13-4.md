Title: Sound Juicer "J'd Up To The Boom" 2.13.4
Date: 2006-01-30 21:02
Tags: tech
Slug: sound-juicer-jd-up-to-the-boom-2-13-4

Sound Juicer "J'd Up To The Boom" 2.13.4 is out. Tarballs are available
[on
`burtonini.com`](http://www.burtonini.com/computing/sound-juicer-2.13.4.tar.gz),
or from the [GNOME FTP
servers](ftp://ftp.gnome.org/pub/gnome/sources/sound-juicer/2.13/). Bug
fixes:

-   Rebuild the pipeline after every track to fix a crash in GStreamer
-   Unlock the drive door before ejecting
-   Add GTK category to the desktop file
-   Update bacon-message-connection

Translators: Adam Weinberger (en\_CA), Ales Nyakhaychyk (be), Ankit
Patel (gu), Clytie Siddall (vi), Evandro Fernandes Giovanini (pt\_BR),
Francisco Javier F. Serrador (es), Funda Wang (zh\_CN), Gabor Kelemen
(hu), Ignacio Casal Quinteiro (gl), Ilkka Tuohela (fi), Jean-Michel
Ardantz (fr), Kjartan Maraas (nb, no), Lasse Bang Mikkelsen (da),
Theppitak Karoonboonyanan (th), Tino Meinen (nl).

<small>NP: <cite>Gorillaz</cite>, Gorillaz</small>
