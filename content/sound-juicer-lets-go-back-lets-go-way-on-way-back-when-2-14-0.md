Title: Sound Juicer "Let's Go Back, Let's Go Way On Way Back When" 2.14.0
Date: 2006-03-13 04:14
Tags: tech
Slug: sound-juicer-lets-go-back-lets-go-way-on-way-back-when-2-14-0

Sound Juicer "Let's Go Back, Let's Go Way On Way Back When" 2.14.0 is
out. Tarballs are available [on
`burtonini.com`](http://www.burtonini.com/computing/sound-juicer-2.14.0.tar.gz),
or from the [GNOME FTP
servers](ftp://ftp.gnome.org/pub/gnome/sources/sound-juicer/2.14/). Bug
fixes:

-   Fix multiple album results again (John Thacker)
-   Depend on new GStreamer, remove copied code
-   Depend on new libnautilus-burn, fixing the drive selection
-   Return an error if the pipeline cannot be linked
-   Don't translate new strings
-   Set the read speed on every extract
-   Fix up cleanup logic after extracting

Translators: Ankit Patel (gu), Clytie Siddall (vi), Daniel Nylander
(sv), Duarte Loreto (pt), Elian Myftiu (sq), Francesco Marletta (it),
Francisco Javier F. Serrador (es), Funda Wang (zh\_CN), Gabor Kelemen
(hu), Gnome PL Team (pl), Hendrik Richter (de), Ignacio Casal Quinteiro
(gl), Ilkka Tuohela (fi), Jérémy Le Floc'h (br), Jordi Mallach (ca),
Kjartan Maraas (nb, no), Lasse Bang Mikkelsen (da), Leonid Kanter (ru),
Maxim V. Dziumanenko (uk), Mugurel Tudor (ro), Petr Tomeš (cs), Priit
Laes (et), Rajesh Ranjan (hi), Raphael Higino (pt\_BR), Rhys Jones (cy),
Rostislav "zbrox" Raykov (bg), Satoru SATOH (ja), Tino Meinen (nl),
Žygimantas Beručka (lt).
