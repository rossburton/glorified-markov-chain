Title: Sound Juicer "Let's Kill First The Banker" 2.16.3
Date: 2007-01-29 20:59
Tags: tech
Slug: sound-juicer-lets-kill-first-the-banker-2-16-3

Sound Juicer "Let's Kill First The Banker" 2.16.3 is out. Tarballs are
available [on
`burtonini.com`](http://www.burtonini.com/computing/sound-juicer-2.16.3.tar.bz2),
or from the [GNOME FTP
servers](ftp://ftp.gnome.org/pub/gnome/sources/sound-juicer/2.16/).
Loads of fixes!

-   Fix install target (Christian Persch)
-   Set urgency hint on completed dialog (Sebastien Bacher)
-   Add man page
-   Ensure playback is stopped when media is removed (Luca Cavalli)
-   Source cleanups (Adam Petaccia)

