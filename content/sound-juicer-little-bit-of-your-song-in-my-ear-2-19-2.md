Title: Sound Juicer "Little Bit Of Your Song In My Ear" 2.19.2
Date: 2007-06-18 17:50
Tags: tech
Slug: sound-juicer-little-bit-of-your-song-in-my-ear-2-19-2

Sound Juicer "Little Bit Of Your Song In My Ear" 2.19.2is out. Tarballs
are available [on
`burtonini.com`](http://www.burtonini.com/computing/sound-juicer-2.19.2.tar.gz),
or from the [GNOME FTP
servers](ftp://ftp.gnome.org/pub/gnome/sources/sound-juicer/2.19/).

-   Correctly read the album artist (\#393707)
-   Don't set empty track titles (\#435964)
-   Fix a11y on the play/pause button (\#364371, Patrick Wade)

