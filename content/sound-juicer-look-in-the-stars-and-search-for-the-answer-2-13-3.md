Title: Sound Juicer "Look In The Stars And Search For The Answer" 2.13.3
Date: 2006-01-19 15:21
Tags: tech
Slug: sound-juicer-look-in-the-stars-and-search-for-the-answer-2-13-3

Sound Juicer "Look In The Stars And Search For The Answer" 2.13.3 is
out. Tarballs are available [on
`burtonini.com`](http://www.burtonini.com/computing/sound-juicer-2.13.3.tar.gz),
or from the [GNOME FTP
servers](ftp://ftp.gnome.org/pub/gnome/sources/sound-juicer/2.13/). Only
one change here:

-   Port to GStreamer 0.10

Thanks to Tim-Philipp Mueller and James Livingston for helping me here.
This needs lots of testing, there are bound to be many regressions that
need hunting and fixing. Everyone grab it now!
