Title: Sound Juicer Love Hour
Date: 2007-06-20 14:30
Tags: tech
Slug: sound-juicer-love-hour

Does anyone fancy hacking for an hour on a neat little feature for Sound
Juicer, as a way of learning the code base a little? This shouldn't take
more than an hour or so, and would be good for the world.

When Musicbrainz can't find the track listing for a CD, it automatically
queries FreeDB and will attempt to return something from the garbled
nonsense that FreeDB generally contains. The user cannot tell the
difference, unless they notice that the track listings are badly
formatted, the album artist is incorrect, or the encoding is wrong. The
user will not notice any trivial issues, fix any obvious problems and
then rip the CD, or just give up now if the encoding is wrong.

This is bad.

What should happen is that Sound Juicer should subtly point out to the
user if the data is proxied from FreeDB, so that they can import it into
MusicBrainz where more data can be added and verified. What I'd like to
see is a little pane appear if a CD is from FreeDB (SJ knows this
already) with a link to the relevant import URL, so the user can quickly
import it once for the benefit of everyone else who rips the album in
the future.

Does anyone fancy hacking this? As I said, it sounds like an hour of
coding for someone who can code GTK+ but hasn't touched SJ before.

<small>NP: <cite>This Book Is About Words</cite>, Ahmad Szabo</small>
