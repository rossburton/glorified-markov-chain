Title: Sound Juicer Loving
Date: 2008-01-02 21:36
Tags: tech
Slug: sound-juicer-loving

Over Christmas a load of gnome-love bugs in Sound Juicer were closed
thanks to some wonderful people, so to keep people's interest (and try
and gain some serious co-maintainers) I've gone through the bug list and
marked [more gnome-love
bugs](http://bugzilla.gnome.org/reports/keyword-search.cgi?product=sound-juicer&keyword=gnome-love).
There are thirteen bugs marked now, all of which should be no more than
a few hours work each, making them perfect fodder for anyone who wants
to get more experience with GNOME programming.

<small>NP: <cite>Bag Lady (Dune DnB Remix)</cite>, Erykah Badu</small>
