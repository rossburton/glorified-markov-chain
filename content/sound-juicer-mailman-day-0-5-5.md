Title: Sound Juicer "Mailman Day" 0.5.5
Date: 2003-10-01 23:15
Tags: tech
Slug: sound-juicer-mailman-day-0-5-5

Sound Juicer "Mailman Day" 0.5.5 is out -- download the [tarball
here](http://www.burtonini.com/computing/sound-juicer-0.5.5.tar.gz).
Debian packages ready now.

-   Threaded MusicBrainz lookup
-   Open directory actually opens the best directory
-   Pipeline rebuilt after every track, works around gst-lame
    crashes (Bastien)
-   Correctly handle the artist in multiple artist albums
-   Try and do something useful with errors from GStreamer
-   Fix crashes when closing dialogs with escape (Frederic)
-   Fix drive detection with devfs (Frederic)
-   Check the cdparanoia plugin is present (Bastien)

