Title: Sound Juicer "Managed To Save Just A Little Bit Of Money" 2.11.92
Date: 2005-08-23 16:56
Tags: tech
Slug: sound-juicer-managed-to-save-just-a-little-bit-of-money-2-11-92

Sound Juicer "Managed To Save Just A Little Bit Of Money" 2.11.92 is
out. Tarballs are available [on
`burtonini.com`](http://www.burtonini.com/computing/sound-juicer-2.11.92.tar.gz),
or from the [GNOME FTP
servers](ftp://ftp.gnome.org/pub/gnome/sources/sound-juicer/2.11/).

-   Reset the interface when a CD is manually ejected
-   Improve the volume control (Ronald Bultje)
-   Fix distcheck
-   Use automake 1.9

Thanks to the ever-working translators: Duarte Loreto (pt), Frank Arnold
(de), Jordi Mallach (ca), Leonid Kanter (ru), Maxim V. Dziumanenko (uk),
Mugurel Tudor (ro), Nikos Charonitakis (el), Raphael Higino (pt\_BR),
Rhys Jones (cy), Takeshi AIHANA (ja), Woodman Tuen (zh\_TW), Young-Ho
Cha (ko), Žygimantas Beručka (lt).
