Title: Sound Juicer Merchandise
Date: 2006-03-01 13:30
Tags: tech
Slug: sound-juicer-merchandise

Sadly arriving two days late for wearing to FOSDEM, my trial-run of
Sound Juicer t-shirts arrived on Monday. They rock!

![Front](http://www.spreadshirt.net/image.php?type=image&partner_id=385701&product_id=2764633&img_id=1&size=huge&bgcolor_images=white)
![Back](http://www.spreadshirt.net/image.php?type=image&partner_id=385701&product_id=2764633&img_id=2&size=huge&bgcolor_images=white)

The back reads “freshly squeezed audio” if you can't make it out. Go and
[get yours now](http://sound-juicer.spreadshirt.net)!

<small>NP: <cite>Ben Folds Five</cite>, Ben Folds Five</small>
