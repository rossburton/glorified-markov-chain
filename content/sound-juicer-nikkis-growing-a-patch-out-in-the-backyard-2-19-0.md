Title: Sound Juicer "Nikki's Growing A Patch Out In The Backyard" 2.19.0
Date: 2007-05-13 19:37
Tags: tech
Slug: sound-juicer-nikkis-growing-a-patch-out-in-the-backyard-2-19-0

Sound Juicer "Nikki's Growing A Patch Out In The Backyard" 2.19.0 is
out. Tarballs are available [on
`burtonini.com`](http://www.burtonini.com/computing/sound-juicer-2.19.0.tar.gz),
or from the [GNOME FTP
servers](ftp://ftp.gnome.org/pub/gnome/sources/sound-juicer/2.19/). This
is the first release in the 2.19.x development series, after I failed to
do anything useful in 2.17.x...

-   Attempt to repair brain-dead FreeDB encoding (Ka-Hing Cheung)
-   Fix memory leaks in musicbrainz object (Cristian Persch)
-   Follow the preferred sound device (Bastien Nocera)
-   Use xdg-user-dirs (BN)
-   Use gst\_element\_make\_from\_uri instead of hardcoding cdparanoia
    (James Livingstone)
-   Display a better message after extracting (Adam Petaccia)
-   Fix detection of FreeDB albums (David Mandelberg)
-   Accessibility fixes (Rich Burridge, Patrick Wade)
-   Don't crash if libnautilus-burn doens't know of a device
    (Pascal Terjan)
-   Initialise threading earlier
-   Convert strerror() output to UTF-8 for display (PT)
-   Remove "Application" category from desktop file
-   Fix srcdir!=builddir builds (Loic Minier)
-   Internal refactor of extractor and metadata objects

