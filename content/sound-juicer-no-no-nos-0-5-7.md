Title: Sound Juicer "No no nos" 0.5.7
Date: 2003-11-11 21:34
Tags: tech
Slug: sound-juicer-no-no-nos-0-5-7

Sound Juicer "No No Nos" 0.5.7 is out -- download the [tarball
here](http://www.burtonini.com/computing/sound-juicer-0.5.7.tar.gz).
Debian packages in the upload queue already.

-   Hopefully stop crashing so often!
-   Add the GStreamer command line options
-   Fix the Invalid Encoder dialogue which wasn't going away
-   Use the correct SI units when talking about file sizes
-   Fix the path to the really, really cool orange icon
-   Disable the Extract button when there are no tracks to extract
-   Clamp the progress bar value, fixing a warning
-   Display error dialogues if we can't create a file
-   Fix up the "error" signal declaration in SjExtractor
-   Stop depending on all of the GStreamer libraries

Basically, if 0.5.6 crashed for you, try this. I've nailed a few bugs
which were causing frequent crashes.
