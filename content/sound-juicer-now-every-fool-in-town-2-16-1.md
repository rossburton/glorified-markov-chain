Title: Sound Juicer "Now Every Fool In Town" 2.16.1
Date: 2006-10-01 23:51
Tags: tech
Slug: sound-juicer-now-every-fool-in-town-2-16-1

Sound Juicer "Now Every Fool In Town" 2.16.1 is out. Tarballs are
available [on
`burtonini.com`](http://www.burtonini.com/computing/sound-juicer-2.16.1.tar.gz),
or from the [GNOME FTP
servers](ftp://ftp.gnome.org/pub/gnome/sources/sound-juicer/2.16/). Just
a couple of fixes:

-   Fix profiles where the output frequency isn't 44.1Khz (thanks
    Tim-Philip Müller)
-   Listen to the strip special characters preference and update the
    label (thanks Peter)

<small>NP: <cite>Mad Season</cite>, Matchbox 20</small>
