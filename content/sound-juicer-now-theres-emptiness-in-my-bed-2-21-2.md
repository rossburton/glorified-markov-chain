Title: Sound Juicer "Now There's Emptiness In My Bed" 2.21.2
Date: 2008-01-14 03:56
Tags: tech
Slug: sound-juicer-now-theres-emptiness-in-my-bed-2-21-2

Sound Juicer "Now There's Emptiness In My Bed" 2.21.2 is available now.
Tarballs are available [on
`burtonini.com`](http://www.burtonini.com/computing/sound-juicer-2.21.2.tar.bz2),
or from the [GNOME FTP
servers](ftp://ftp.gnome.org/pub/gnome/sources/sound-juicer/2.21/).
Again, the response from the gnome-love and GHOP tasks has been great,
and there is a lot new in this release.

-   Update documentation (Andreas Freund)
-   Check extracted filenames are not too long (Ed Catmur)
-   Add a Year entry to the main window (Andrzej Polatyński)
-   Add a Duplicate Disc menu item (Dave Meikle)
-   Show better error messages if the target directory can't be
    created (MatzeB)
-   Remove branding from the desktop file
-   Fix initial playback (Bill O'Shea)
-   Hide the progress bar unless ripping (Ghee Teo)
-   Move the cursor to always been on the currently playing/ripping
    track (Dave Meikle)
-   When editing album artist, only change track artists if it was the
    same (Bill O'Shea)

