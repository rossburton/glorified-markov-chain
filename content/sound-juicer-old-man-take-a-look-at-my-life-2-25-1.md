Title: Sound Juicer "Old Man Take A Look At My Life" 2.25.1
Date: 2008-11-05 04:33
Tags: tech
Slug: sound-juicer-old-man-take-a-look-at-my-life-2-25-1

Sound Juicer "Old Man Take A Look At My Life" 2.25.1 has been released.
Tarballs are available [on
`burtonini.com`](http://www.burtonini.com/computing/sound-juicer-2.25.1.tar.bz2),
or from the [GNOME FTP
servers](ftp://ftp.gnome.org/pub/gnome/sources/sound-juicer/2.25/).
Everyone's favourite Frockney did a huge amount of work on this, and I'm
still talking to him after he admitted that the master plan is to
replace Sound Juicer with Rhythmbox in Fedora!

-   Chain the metadata lookups (Bastien Nocera)
-   Finish the libmusicbrainz3 metadata fetcher (BN)
-   Add a GVFS metadata fetcher as fallback (BN)
-   Make libcdio option, as it breaks the GPL+Exception license (BN)
-   Export ASIN, Discogs, Wikipedia in the internal metadata (BN)
-   Lots of other cleanups to the metadata code (BN)
-   Remove copy of the id3mux plugin, assume it exists now (BN)
-   Remove Encoding field from desktop file (Pacho Ramos)
-   Add Audio to desktop categories (Patryk Zawadzki)
-   Correctly parse CDDA URLs (Matthew Martin)
-   Don't free the option context

