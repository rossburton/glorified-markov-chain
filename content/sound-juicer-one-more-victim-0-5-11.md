Title: Sound Juicer "One More Victim" 0.5.11
Date: 2004-04-13 18:30
Tags: tech
Slug: sound-juicer-one-more-victim-0-5-11

Sound Juicer "One More Victim" 0.5.11 is available -- download the
[tarball
here](http://www.burtonini.com/computing/sound-juicer-0.5.11.tar.gz).
Debian packages available in [my
repository](http://www.burtonini.com/debian) and are in the upload
queue. This release works with GStreamer 0.8!

-   Supports GStreamer 0.8
-   Eject When Finished works
-   Change the shortcut for Extract to Control-Return
-   Updated the README
-   Fix the logic in the "missing encoder" dialogue.

