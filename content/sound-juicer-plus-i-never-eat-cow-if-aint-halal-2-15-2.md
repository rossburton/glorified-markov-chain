Title: Sound Juicer "Plus I Never Eat Cow If Ain't Halal" 2.15.2
Date: 2006-05-14 19:48
Tags: tech
Slug: sound-juicer-plus-i-never-eat-cow-if-aint-halal-2-15-2

Sound Juicer "Plus I Never Eat Cow If Ain't Halal" 2.15.2 is out.
Tarballs are available [on
`burtonini.com`](http://www.burtonini.com/computing/sound-juicer-2.15.2.tar.gz),
or from the [GNOME FTP
servers](ftp://ftp.gnome.org/pub/gnome/sources/sound-juicer/2.15/). Not
a lot has been done sadly:

-   Move backend code to libjuicer
-   Save and restore window maximised state (Luca Cavalli)
-   Use GLib debug flags rather than --enable-debug
    (Przemysław Grzegorczyk)
-   Translate the help title (PG)

