Title: Sound Juicer Refactor: Testers Wanted
Date: 2007-06-04 18:00
Tags: tech
Slug: sound-juicer-refactor-testers-wanted

I've been slowly refactoring bits of Sound Juicer to make it more
manageable and in the end usable with plugins, and I'm ready to commit a
chunk of refactoring back into Subversion. However before I do so I'd
appreciate it if some others would test the changes. The changes are in
[this Bazaar repository](http://burtonini.com/bzr/sj), branch, build and
use it like you would any other Sound Juicer release.

I've refactored the track store and view into separate objects, which
meant that the spagehtti code that held the UI together had to be
rewritten. I think I tested all of the cases, but if someone could give
it a go, and check that the buttons are correctly enabled and disabled
as you select tracks, that the sorting is correct and so on, I'd be very
happy. Thanks!

<small>NP: <cite>Live EP</cite>, Bonobo</small>
