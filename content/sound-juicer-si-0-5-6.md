Title: Sound Juicer "Si!" 0.5.6
Date: 2003-10-27 18:32
Tags: tech
Slug: sound-juicer-si-0-5-6

Sound Juicer "Si!" 0.5.6 is out -- download the [tarball
here](http://www.burtonini.com/computing/sound-juicer-0.5.6.tar.gz).
Debian packages in the upload queue already.

-   Detect GStreamer 0.7 (Christian Scaller)
-   Rewrite the extractor loop, fixing many bugs
-   Disable the Reread button when extracting
-   Convert filenames to filesystem encoding (Frederic Crozat)
-   UI cleanups in the progress dialog (Paolo Borelli)
-   Quote the path given to Nautilus when pressing Open
-   Updated libbacon

