Title: Sound Juicer "Smile At My Answer" 2.11.90
Date: 2005-07-26 02:58
Tags: tech
Slug: sound-juicer-smile-at-my-answer-2-11-90

Sound Juicer "Smile At My Answer" 2.11.90 is out. Tarballs are available
[on
`burtonini.com`](http://www.burtonini.com/computing/sound-juicer-2.11.90.tar.gz),
or from the [GNOME FTP
servers](ftp://ftp.gnome.org/pub/gnome/sources/sound-juicer/2.11/).

I'm very disappointed with the lack of bug reports about the new playing
functionality in the 2.11 releases of Sound Juicer. It's either perfect
or nobody is using it, and I think I know what the correct answer is...
C'mon people, give it a hammering! Play your CDs with it for a day
instead of your usual program and tell me what you think.

-   Nicer icons when extracting (Luca Cavalli)
-   Documentation ported to use gnome-doc-utils

Thanks to the hard-working translators who are diving into action now
that the strings are frozen: Abduxukur Abdurixit (ug), Adam Weinberger
(en\_CA), Ankit Patel (gu), Clytie Siddall (vi), Francisco Javier F.
Serrador (es), Funda Wang (zh\_CN), Ilkka Tuohela (fi), Miloslav Trmac
(cs), Paisa Seeluangsawat (th), Priit Laes (et), Raphael Higino
(pt\_BR), Takeshi AIHANA (ja), Terance Edward Sola (nb), Terance Edward
Sola (no), Vladimir Petkov (bg).
