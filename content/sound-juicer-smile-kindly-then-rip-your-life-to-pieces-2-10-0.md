Title: Sound Juicer "Smile Kindly Then Rip Your Life To Pieces" 2.10.0
Date: 2005-03-07 22:55
Tags: tech
Slug: sound-juicer-smile-kindly-then-rip-your-life-to-pieces-2-10-0

Sound Juicer "Smile Kindly Then Rip Your Life To Pieces" 2.10.0 is out!

-   Rocking updated user guide (Shaun McCance is a hero)
-   Trivial UI fixes
-   Some more const keywords
-   Use G\_DEFINE\_TYPE in `SjExtractor`

And of course, thanks to the translators: Abel Cheung (zh\_TW), Emrah
Unal (tr), Paisa Seeluangsawat (th), Rajesh Ranjan (hi), Danilo Šegan
(sr).
