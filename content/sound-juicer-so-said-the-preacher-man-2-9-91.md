Title: Sound Juicer "So Said The Preacher Man" 2.9.91
Date: 2005-02-14 20:46
Tags: tech
Slug: sound-juicer-so-said-the-preacher-man-2-9-91

This is quite a late release announcement, but I'm sure nobody will
mind. Now that Sound Juicer is in GNOME 2.10 Desktop, I've started to
track the GNOME version numbers. There were also some other changes:

-   Link to nautilus-burn instead of copying libbacon
-   HIGify and use GtkFileChooserButton in the Preferences dialog
-   Use gnome-open instead of Nautilus when opening directories
-   Ensure the pipeline is not running when an error occurs
-   Make the code more likely to compile with gcc 2.95
-   Update README
-   Write empty strings, not NULL, to GConf

As usual the tarball is
[here](http://www.burtonini.com/computing/sound-juicer-2.9.91.tar.gz),
but it is also available on the [GNOME FTP
server](ftp://ftp.gnome.org/pub/gnome/sources/sound-juicer/). I've not
done Debian packages yet as it needs GNOME 2.9, but there are packages
in Ubuntu Hoary.

<small>NP: <cite>Dub Fever</cite>, King Tubby</small>
