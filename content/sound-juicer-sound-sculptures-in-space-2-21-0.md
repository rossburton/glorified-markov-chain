Title: Sound Juicer "Sound Sculptures In Space" 2.21.0
Date: 2007-12-20 17:20
Tags: tech
Slug: sound-juicer-sound-sculptures-in-space-2-21-0

Sound Juicer "Sound Sculptures In Space" 2.21.0 is finally out. Tarballs
are available [on
`burtonini.com`](http://www.burtonini.com/computing/sound-juicer-2.21.0.tar.bz2),
or from the [GNOME FTP
servers](ftp://ftp.gnome.org/pub/gnome/sources/sound-juicer/2.21/).

-   Add a cluebar when the CD isn't in Musicbrainz (thanks Luca
    Cavalli, \#452047)
-   Fix the multiple album select dialog (thanks Rob Bradford, \#500815)
-   Install 48x48 PNG icon (thanks Andreas Nilsson, \#502933)
-   Change Deselect All shortcut to Control-Shift-A (thanks Ted
    Gould, \#501442)
-   Add Overwrite All/Skip All buttons to the overwrite dialog (thanks
    Michael Chudobiak, \#130782)

