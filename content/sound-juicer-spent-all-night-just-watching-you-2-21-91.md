Title: Sound Juicer "Spent All Night Just Watching You" 2.21.91
Date: 2008-02-14 16:28
Tags: tech
Slug: sound-juicer-spent-all-night-just-watching-you-2-21-91

Sound Juicer "Spent All Night Just Watching You" 2.21.91 is available
now. Tarballs are available [on
`burtonini.com`](http://www.burtonini.com/computing/sound-juicer-2.21.91.tar.bz2),
or from the [GNOME FTP
servers](ftp://ftp.gnome.org/pub/gnome/sources/sound-juicer/2.21/). A
few features, before we hit the deep freeze.

-   Write extracted audio to a temporary file and then rename
    (Matthew Martin)
-   Disable the Eject button if the drive cannot eject (David Meikle)

