Title: Sound Juicer "Stab Stab Stab! This Is More Than A Message" 2.23.3
Date: 2008-09-08 18:01
Tags: tech
Slug: sound-juicer-stab-stab-stab-this-is-more-than-a-message-2-23-3

Sound Juicer "Stab Stab Stab! This Is More Than A Message" 2.23.3 has
been released. Tarballs are available [on
`burtonini.com`](http://www.burtonini.com/computing/sound-juicer-2.23.3.tar.bz2),
or from the [GNOME FTP
servers](ftp://ftp.gnome.org/pub/gnome/sources/sound-juicer/2.23/).

-   Don't crash when exiting
-   Don't distribute full GFDL with docs
-   Correctly parse CDDA URLs (\#550131, thanks Matthew Martin)

