Title: Sound Juicer Sub-optimal?
Date: 2005-10-03 20:57
Tags: tech
Slug: sound-juicer-sub-optimal

GStreamer-hero Andy Wingo [recently
blogged](http://wingolog.org/archives/2005/09/30/suboptimality) about
his experience with Sound Juicer. He went as far as pinging me on IRC,
so I presume he'd like a full and thorough reply.

 <cite>So. Someone puts in a CD. A while later, although there's no indication something is happening, sound-juicer pops up.</cite>

:   This is a bug in gnome-volume-manager: it doesn't give you any
    feedback that it is probing the new media, or starting Sound Juicer.
    I think a notification area icon for "new media inserted" would be
    good whilst it probes, and then the standard startup notification
    animation/panel entry when it starts up Sound Juicer.

<cite>1. The process of getting tracks from wherever blocks the rest of the UI. I should be able to play while this is happening.</cite>

:   The lookup of tracks does happen in a seperate thread, and starts
    when the UI unblocks. The interface blocks because Sound Juicer
    called `open` on the device node, and that is blocking until the
    drive has spun up. I may be able to put that section of code in
    another thread. Filed as
    [\#317860](http://bugzilla.gnome.org/show_bug.cgi?id=317860).

 <cite>2. If I press 'play', nothing happens. I have to select the first track, then press play.</cite>

:   Weird. \[looks for CD to test\] Works for me, what version are you
    using? The 2.11.x series had lots of fixes for various issues like
    this, so you may need to upgrade.

 <cite>3. While playing, the extract button is insensitive. So it seems I can't extract while playing? Fine, maybe I can play while extracting... No, can't do that either... It should be possible -- the first thing you want to do with a new CD is play it, and ripping it would be a close second. Two birds, one stone would be nice.</cite>

:   My stock argument for requests like this is that the playback
    functionality in Sound Juicer is intended as a guide to find the
    track the user wants to rip and not as a fully-featured player.
    However, that's just an excuse in this case for "looks tricky". I'm
    not expecting GStreamer to sanely handle playback and ripping at the
    same time, but maybe I'll give this a go when I switch to `libcdio`.
    Filed as
    [\#317862](http://bugzilla.gnome.org/show_bug.cgi?id=317862).

 <cite>4. If there is an error in the disc, the whole UI blocks for a long time.</cite>

:   \[Insert stock grumble about cdparanoia\] The extraction is in
    another thread so is about as far removed as it can get from the
    mainloop as I can get, without forking.

 <cite>5. When the ripping is finished, the disc is ejected. Great. When I put another in, it blocks the sound-juicer UI without any stated reason for 4-5 seconds, and then doesn't even read my new CD. I have to fiddle in menus to manually tell s-j to reread the disc. Duh...</cite>

:   The interface blocking is the same as (1). Sound Juicer used to
    re-read CDs correctly, so something has broken. I'm tempted to blame
    HAL, but I'm not sure if that is correct. I see this too so will try
    and get it fixed in 2.12.3. Filed as
    [\#317864](http://bugzilla.gnome.org/show_bug.cgi?id=317864).

 <cite>6. Then, neither Rhythmbox nor Jamboree automatically adds these files to their libraries. There should be some way of adding files as they are finished writing, so that I can play them immediately. At the very least they should all be added when ripping finishes. Not sure how to do this though.</cite>

:   The standard solution here is that the applications should monitor
    the directories they read from with `dnotify`/`inotify`/whatever, so
    they are notified when a new file is created.

Hopefully this answered all of the complaints. Not a bad tally: two bugs
not in Sound Juicer, a duplicate, a regression, and some bugs which are
very non-trivial to fix.
