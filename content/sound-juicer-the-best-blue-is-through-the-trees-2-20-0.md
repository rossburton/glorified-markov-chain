Title: Sound Juicer "The Best Blue Is Through The Trees" 2.20.0
Date: 2007-09-17 17:39
Tags: tech
Slug: sound-juicer-the-best-blue-is-through-the-trees-2-20-0

Sound Juicer "The Best Blue Is Through The Trees" 2.20.0 is out.
Tarballs are available [on
`burtonini.com`](http://www.burtonini.com/computing/sound-juicer-2.20.0.tar.bz2),
or from the [GNOME FTP
servers](ftp://ftp.gnome.org/pub/gnome/sources/sound-juicer/2.20/).

-   Remove a work around now that GStreamer is fixed, and update
    requirement (\#472650, Jaap Haitsma)

Also thanks to the tireless translation team: Djihed Afifi (ar), Ihar
Hrachyshka (be), Alexander Shopov (bg), Runa Bhattacharjee (bn\_IN),
Jordi Mallach (ca), Ask Hjorth Larsen (da), Hendrik Richter (de),
Tshewang Norbu (dz), Kostas Papadimas (el), David Lodge (en\_GB), Jorge
González (es), Ivar Smolin (et), Iñaki Larrañaga Murgoitio (eu), Ilkka
Tuohela (fi), Christophe Benz (fr), Ignacio Casal Quinteiro (gl), Ankit
Patel (gu), Eyal Mamo (he), Gabor Kelemen (hu), Francesco Marletta (it),
Takeshi AIHANA (ja), Young-Ho Cha (ko), Erdal Ronahi (ku), Žygimantas
Beručka (lt), Raivis Dejus (lv), Arangel Angov (mk), Kjartan Maraas
(nb), Wouter Bolsterlee (nl), Tomasz Dominikowski (pl), Og Maciel
(pt\_BR), Duarte Loreto (pt), Mugurel Tudor (ro), Nickolay V. Shmyrev
(ru), Danishka Navin (si), Matic Žgur (sl), Elian Myftiu (sq), Милош
Поповић (sr), Daniel Nylander (sv), Dr.T.Vasudevan (ta), Theppitak
Karoonboonyanan (th), Baris Cicek (tr), Maxim Dziumanenko (uk), Clytie
Siddall (vi), Funda Wang (zh\_CN), Chao-Hsiung Liao (zh\_HK, zh\_TW).
