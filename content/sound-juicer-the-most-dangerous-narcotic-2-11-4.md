Title: Sound Juicer "The Most Dangerous Narcotic" 2.11.4
Date: 2005-07-14 20:45
Tags: tech
Slug: sound-juicer-the-most-dangerous-narcotic-2-11-4

Sound Juicer "The Most Dangerous Narcotic" 2.11.4 is out. Tarballs are
available [on
`burtonini.com`](http://www.burtonini.com/computing/sound-juicer-2.11.4.tar.gz),
or from the [GNOME FTP
servers](ftp://ftp.gnome.org/pub/gnome/sources/sound-juicer/2.11/).

Again, another cool release with nice new features. Everyone give it a
go, and see what you think of the new interface.

-   Thread the extracting pipeline for faster rips
-   Add a volume control when playing (Ronald Bultje)
-   Remove the progress dialog (Raj M Madhan)
-   Register our custom icons as stock so themes can set them
    (Luca Cavalli)
-   Use Disc instead of CD in the menu
-   Disable Play button when extracting (Raj)
-   Set the pipeline to NULL when cancelling (Raj)
-   Use gnome-common (Ali Akcaagac)

Thanks to the translators: Adam Weinberger (en\_CA), Francisco Javier F.
Serrador (es), Frank Arnold (de), Michiel Sikkes (nl), Miloslav Trmac
(cs), Paisa Seeluangsawat (th), Priit Laes (et), Takeshi AIHANA (ja),
and Tommi Vainikainen (fi).

<small>NP: <cite>Brain Freeze</cite>, DJ Shadow & Cut Chemist</small>
