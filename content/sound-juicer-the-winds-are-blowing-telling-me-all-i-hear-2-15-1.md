Title: Sound Juicer "The Winds Are Blowing Telling Me All I Hear" 2.15.1
Date: 2006-04-25 01:15
Tags: tech
Slug: sound-juicer-the-winds-are-blowing-telling-me-all-i-hear-2-15-1

Sound Juicer "The Winds Are Blowing Telling Me All I Hear" 2.15.1 is
out. Tarballs are available [on
`burtonini.com`](http://www.burtonini.com/computing/sound-juicer-2.15.1.tar.gz),
or from the [GNOME FTP
servers](ftp://ftp.gnome.org/pub/gnome/sources/sound-juicer/2.15/). This
is the first release in the 2.15.x development series and isn't that
exciting, sorry:

-   Only manipulate the track store if there is stuff in it (\#333402)
-   Fix progress bar calculation (\#339062, John Thacker)
-   Check for cdio &gt;= 0.70 (\#339303, John Laliberte)
-   Use new intltool for po/LINGUAS magic (\#339197)
-   Use GString when creating the paths (\#336725, James Livingston)
-   Update the file/path pattern documentation (\#337638)
-   Update the about dialog (\#328181, Brian Pepple)
-   Save and restore playback volume (\#334170, Marinus Schraal)
-   Improve --help (\#335016, Vincent Untz)
-   Translate an untranslated string (\#334509)

<small>NP: <cite>Out Of Season</cite>, Beth Bibbons and Rustin
Man</small>
