Title: Sound Juicer "They Keep Hiding The Truth And Rights" 2.10.1
Date: 2005-04-05 01:27
Tags: tech
Slug: sound-juicer-they-keep-hiding-the-truth-and-rights-2-10-1

Sound Juicer "They Keep Hiding The Truth And Rights" 2.10.1 is out [as
usual](http://www.burtonini.com/computing/sound-juicer-2.10.1.tar.gz),
fixing a couple of crashers and adding more translations.

-   Initialise audio profiles earlier (John Palmieri)
-   Remove bad checks which can crash (Andrei Yurkevich)

Translations by Adam Weinberger (en\_CA), Ahmad Riza H Nst (id),
Canonical Ltd (xh), Jyotsna Shrestha (ne), Mugurel Tudor (ro), Raphael
Higino (pt\_BR), and Steve Murphy (rw).
