Title: Sound Juicer "This Is The Bright Frontier" 2.12.1
Date: 2005-09-19 00:02
Tags: tech
Slug: sound-juicer-this-is-the-bright-frontier-2-12-1

Sound Juicer "This Is The Bright Frontier" 2.12.1 is out. Tarballs are
available [on
`burtonini.com`](http://www.burtonini.com/computing/sound-juicer-2.12.1.tar.gz),
or from the [GNOME FTP
servers](ftp://ftp.gnome.org/pub/gnome/sources/sound-juicer/2.12/). A
few fixes have been made:

-   Add --device command line argument to override GConf
-   Set the maximum read speed on every extract
-   Fix an i18n bug
-   Fix typo in documentation
-   Add a [DOAP](http://usefulinc.com/doap) file to the distribution

Thanks to the ever-working translators: Baris Cicek (tr), Clytie Siddall
(vi), Francesco Marletta (it), Frank Arnold (de), Ignacio Casal
Quinteiro (gl), Iñaki Larrañaga Murgoitio (eu), Rostislav "zbrox" Raykov
(bg), Theppitak Karoonboonyanan (th).
