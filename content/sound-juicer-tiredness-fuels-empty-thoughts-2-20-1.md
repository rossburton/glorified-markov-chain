Title: Sound Juicer "Tiredness Fuels Empty Thoughts" 2.20.1
Date: 2007-10-14 22:30
Tags: tech
Slug: sound-juicer-tiredness-fuels-empty-thoughts-2-20-1

Sound Juicer "Tiredness Fuels Empty Thoughts" 2.20.1 is out. Tarballs
are available [on
`burtonini.com`](http://www.burtonini.com/computing/sound-juicer-2.20.1.tar.bz2),
or from the [GNOME FTP
servers](ftp://ftp.gnome.org/pub/gnome/sources/sound-juicer/2.20/).
Hopefully this fixes the common crashes that people are seeing, if it
does then I'll backport the fixed to 2.16 so that more distributions can
release updated packages.

-   Unset temporary iterators after ripping, hopefully fixing a very
    common crash (\#403870).
-   Only lock the drive when extracting, and ensure its unlocked
    when finished. This should fix another common crash (\#484535).
-   Fix logic bug when creating directories (\#481025, thanks
    William Lachance).
-   Reference the initial profile to stop crashing when profiles are
    edited (\#440400, Stefan Röllin)

<small>NP: <cite>Aerial</cite>, Kate Bush</small>
