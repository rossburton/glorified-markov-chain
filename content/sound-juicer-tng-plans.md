Title: Sound Juicer: TNG Plans
Date: 2004-06-16 00:00
Tags: tech
Slug: sound-juicer-tng-plans

I have grand plans for Sound Juicer which, when I find the time, will
make it a replacement for `gnome-cd`. Basically I'm going to change the
user interface quite dramatically so that the main window is a read-only
view of the CD, and add playback controls. I hope this will be an easier
interface for CD playing than the current GNOME CD player (with its
design based on a physical CD player). Then the information editing will
be taken out into a dialog, which means that more options can be
available as they are only visible when required.

Of course, pictures speak a thousand words, so here are some provisional
mockups.

![Sound Juicer main
window](http://www.burtonini.com/computing/screenshots/sj2-mockup-main.png){}

![Sound Juicer album
editor](http://www.burtonini.com/computing/screenshots/sj2-mockup-editor.png){}

Observant people will notice certain similarities with Totem... So, that
is my aim for the future of SJ, once I've figured out how best to use
the GNOME 2.6 audio profiles library of course (hi Thomas!). Comments
are welcome (as long as they are positive).

<small>NP: [<cite>Tanto Tempo</cite>, Bebel
Gilberto](http://www.amazon.co.uk/exec/obidos/ASIN/B00006F1L6)</small>
