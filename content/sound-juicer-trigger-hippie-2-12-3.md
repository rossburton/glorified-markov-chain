Title: Sound Juicer "Trigger Hippie" 2.12.3
Date: 2005-11-24 01:54
Tags: tech
Slug: sound-juicer-trigger-hippie-2-12-3

Sound Juicer "Trigger Hippie" 2.12.3 is out. Tarballs are available [on
`burtonini.com`](http://www.burtonini.com/computing/sound-juicer-2.12.3.tar.gz),
or from the [GNOME FTP
servers](ftp://ftp.gnome.org/pub/gnome/sources/sound-juicer/2.12/). This
release fixes a few important bugs:

-   Wait before attempting to read a new CD to let the disc spin up
-   Protect against invalid iterators when playing
-   Handle the file chooser returning NULL
-   Fix MusicBrainz results with multiple albums (David Mandelberg)
-   Handle G\_FILENAME\_ENCODING (Colin Leroy)

