Title: Sound Juicer "Two Undernourished Egos Four Rotating Hips" 2.15.4
Date: 2006-07-24 22:33
Tags: tech
Slug: sound-juicer-two-undernourished-egos-four-rotating-hips-2-15-4

Sound Juicer "Two Undernourished Egos Four Rotating Hips" 2.15.4 is out.
Tarballs are available [on
`burtonini.com`](http://www.burtonini.com/computing/sound-juicer-2.15.4.tar.gz),
or from the [GNOME FTP
servers](ftp://ftp.gnome.org/pub/gnome/sources/sound-juicer/2.15/). Not
a lot going on, sorry.

-   Add date information to all possible albums (Alex Lancaster)
-   Improve GStreamer error handling (Tim-Philipp Muller)
-   Don't crash when re-opening the Preferences dialog
-   Clear the genre field when re-reading the disk

