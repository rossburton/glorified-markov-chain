Title: Sound Juicer Update
Date: 2003-04-23 00:00
Tags: tech
Slug: sound-juicer-update

Wow, the feedback from Sound Juicer has been great. I did a Debian
package, Frederic Crozat (@mandrake) did Mandrake package, and a Gentoo
package is being worked on too. Two different projects (lymric and
net-Rhythmbox) have both stated that they would like to integrate with
Sound Juicer. Rock!

Feature requests, and better yet, patches, come in every day which is
nice. I hope to get 0.2 out fairly shortly, with an UI to set the
extract path, bug fixes, and a dependency on GStreamer 0.6.1. 0.3 should
allow you to set the format for the ripped file. Beyond that, I don't
have a plan...
