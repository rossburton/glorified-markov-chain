Title: Sound Juicer "We Could Have Been Strangers If We Tried" 2.15.3
Date: 2006-06-14 15:51
Tags: tech
Slug: sound-juicer-we-could-have-been-strangers-if-we-tried-2-15-3

Sound Juicer "We Could Have Been Strangers If We Tried" 2.15.3 is out.
Tarballs are available [on
`burtonini.com`](http://www.burtonini.com/computing/sound-juicer-2.15.3.tar.gz),
or from the [GNOME FTP
servers](ftp://ftp.gnome.org/pub/gnome/sources/sound-juicer/2.15/).
Still very little progress...

-   Update for new nautilus-cd-burner API (William Jon McCann)

