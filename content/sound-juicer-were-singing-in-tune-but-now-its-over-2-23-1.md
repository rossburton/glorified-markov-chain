Title: Sound Juicer "We're Singing In Tune But Now It's Over" 2.23.1
Date: 2008-08-05 03:33
Tags: tech
Slug: sound-juicer-were-singing-in-tune-but-now-its-over-2-23-1

Sound Juicer "We're Singing In Tune But Now It's Over" 2.23.1 has been
released. Tarballs are available [on
`burtonini.com`](http://www.burtonini.com/computing/sound-juicer-2.23.1.tar.bz2),
or from the [GNOME FTP
servers](ftp://ftp.gnome.org/pub/gnome/sources/sound-juicer/2.23/).
Nothing that amazing here, sorry:

-   Fix play+pause+play (\#523182, thanks Matthew Martin)
-   Add %ay, album year (\#522909, Juan F. Giménez Silva)

