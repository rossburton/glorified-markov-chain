Title: Sound Juicer "Where Beer Does Flow And Men Do Chunder" 0.6.0
Date: 2005-02-04 22:01
Tags: tech
Slug: sound-juicer-where-beer-does-flow-and-men-do-chunder-0-6-0

Sound Juicer "Where Beer Does Flow And Men Do Chunder" 0.6.0 is
available -- download the [tarball
here](http://www.burtonini.com/computing/sound-juicer-0.6.0.tar.gz).
Debian packages available in [my
repository](http://www.burtonini.com/debian) shortly and may be
buildable for Sid when GTK+ 2.6 is in (tomorrow I'd say).

This is the first release in the 0.6 series. It's a bit rough, but
please file any crashes you find in it. In particular, you can only
encode to Ogg Vorbis and FLAC unless you create new audio profiles.
However, it will hopefully morph into Sound Juicer 2.10.0 rapidly and be
part of the Desktop release.

-   Now uses Audio Profiles for encoding
-   Expanded metadata tags
-   HAL-enabled builds actually compile
-   Time remaining calculations actually work (hondaguru@earthlink.net)
-   Handle filename conversion failures (Frederic Crozat)
-   Handle empty results from MusicBrainz (FC)
-   Disable Select/Deselect All when extracting (Raj Madhan)
-   Don't crash at startup when there is no profile defined (RM)
-   Handle unset audio profile keys (RM)
-   Ensure Select/Deselect All reflect the state of the selections
    (Nirmal Kumar)
-   Warn the user if they try and exit whilst ripping (RM)
-   The Close button is the default when extracting is complete

<small>NP: <cite>100^th^ Window</cite>, Massive Attack</small>
