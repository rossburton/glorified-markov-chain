Title: Sound Juicer "Why Should You Know Better By Now" 2.24.0
Date: 2008-09-21 20:41
Tags: tech
Slug: sound-juicer-why-should-you-know-better-by-now-2-24-0

Sound Juicer "Why Should You Know Better By Now" 2.24.0 has been
released. Tarballs are available [on
`burtonini.com`](http://www.burtonini.com/computing/sound-juicer-2.24.0.tar.bz2),
or from the [GNOME FTP
servers](ftp://ftp.gnome.org/pub/gnome/sources/sound-juicer/2.24/).
