Title: Sound Juicer Wiki and Bug Smashing
Date: 2005-02-06 00:00
Tags: tech
Slug: sound-juicer-wiki-and-bug-smashing

As Sound Juicer will be [part of the Desktop
release](http://mail.gnome.org/archives/desktop-devel-list/2005-February/msg00027.html)
in GNOME 2.10, I quickly knocked up a page on the [GNOME
Wiki](http://live.gnome.org/SoundJuicer) listing my priorities for the
next stage of development. Simultaneously I hit Bugzilla, closing about
15 bugs. Finally back down to under 50 bugs!
