Title: Sound Juicer "Wish I Could Stop You From Talking" 2.16.2
Date: 2006-11-19 20:58
Tags: tech
Slug: sound-juicer-wish-i-could-stop-you-from-talking-2-16-2

Sound Juicer "Wish I Could Stop You From Talking" 2.16.2 is out.
Tarballs are available [on
`burtonini.com`](http://www.burtonini.com/computing/sound-juicer-2.16.2.tar.bz2),
or from the [GNOME FTP
servers](ftp://ftp.gnome.org/pub/gnome/sources/sound-juicer/2.16/).
Loads of fixes!

-   "None" directory path shouldn't create blank directory name
    (\#374831, Adam Petaccia)
-   Don't disable re-read when playing (\#347218, Stephen Cook)
-   Update bacon volume (\#369490)
-   Set a11y relationships (Robin Sonefore, \#364386)
-   Fix prefs dialog spacing (\#332561, Christian Persch)
-   Fix dates with 0s in (\#364976, Alex Lancaster).
-   Replace spaces with underscores in path\_patterns and file\_patterns
    when "Strip special characters" is selected (\#357111,
    Luca Cavalli).
-   Port to automake 1.9, update desktop file for new Bug Buddy
    (\#357682, Christian Persch)
-   Reset the extract speed on every track just in case (\#343544).
-   Initialise the authentication manager (\#356578)
-   Show error messages when the pipeline doesn't link (Tim-Philipp
    Muller, \#361151)
-   Add 32x32 icon

