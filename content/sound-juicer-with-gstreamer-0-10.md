Title: Sound Juicer with GStreamer 0.10
Date: 2006-01-14 20:20
Tags: tech
Slug: sound-juicer-with-gstreamer-0-10

I just committed to CVS (on the `SJ_GSTREAMER_10` branch) an initial
port to GStreamer 0.10. There is no playback functionality yet (patches
welcome!) but ripping should work, and should be faster too. Everyone
running GNOME 2.13, please switch to this branch and give it a go!
