Title: Sound Juicer
Date: 2003-03-28 00:00
Tags: tech
Slug: sound-juicer

I've been working on <ref>Sound Juicer</ref>, a GTK+/GStreamer CD
ripping tool. Basically, I was fed up with Grip's crapness, and wanted
to learn GStreamer. My goals are:

-   Clean interface
-   Automattic tagging of files

The code is going well. I've been prototyping the modules and currently
have some small modules, and a very large mess of code which will be
refactored as I am satisfied with it.

Of course, everyone wants screenshots! Feast your eyes on the visual joy
of the [multiple albums
found](/computing/screenshots/sj-multiple-albums.png) dialogue, the
[main window](/computing/screenshots/sj-main.png), and the [preferences
dialog](/computing/screenshots/sj-prefs.png).
