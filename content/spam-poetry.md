Title: Spam Poetry
Date: 2006-04-07 17:15
Tags: life
Slug: spam-poetry

I could blog about work, or Sound Juicer, or new albums, or my trip to
the Very Very Cold Helsinki, or the subsequent cold/lost voice I've
developed, but no. Instead, have some Spam Poetry:

> Their death row raid  
> in means lethargy high jinks  
> as exhumation fault.  
> dynamo or spring chicken

Inspired.

<small>NP: <cite>He Has Left Us Alone, but Shafts of Light Sometimes
Grace the Corners of Our Rooms...</cite>, A Silver Mt. Zion</small>
