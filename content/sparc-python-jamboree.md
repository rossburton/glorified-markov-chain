Title: SPARC, Python, Jamboree
Date: 2003-09-29 00:00
Tags: tech
Slug: sparc-python-jamboree

What a fun day. Played with Jamboree for a minute, which passes our
medium-sized music archive test (7gig of music over NFS). It indexed the
lot in a minute, where with Rhythmbox builds I generally did it in
pieces, or over lunch. Send a massive list of requests to Johan, and am
looking forward to the new RhythmDB work Colin Walters' has done, to
compare.

Python is fun. I'd forgotten a lot of it but in my mission to write a
tool to compare trees of supposedly identical Java interfaces, I've
ended up with a hash of hashes, where some of the elements in the
sub-hashes are actually lists of hashes. The children in this chaotic
data structure are function prototypes, and I have a feeling these will
be classes soon.

I interrupt your reading pleasure for this side note: my train home and
the current time of dusk is perfectly suited to lovely going-home
sunsets. Tonight is another lovely one, but it will never beat last
Wednesday's deep orange through to purple clouds, reflected in the lake
at Broxbourne. Resuming normal programming.

Also got to play with our Debian/SPARC box again. At some point in the
past 64-bit `gcc` started to work, but after some testing it turned out
that something was up. `gcc -o test test.c` produced a 64-bit binary,
but `./test` said it wasn't a valid binary. After a quick chat with Ben
Collins, it turns out that the kernel needs updating: the userspace
tools detect that I can run 64-bit processes, but the kernel is was
32-bit only. A quick update and reboot later, and I'm running 64-bit
"Hello, World!". What an overkill. Still have the problem that `gcc`
builds 64-bit by default, whereas `g++` builds 32-bit...
