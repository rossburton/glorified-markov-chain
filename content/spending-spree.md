Title: Spending Spree!
Date: 2004-04-14 00:00
Tags: life
Slug: spending-spree

I think I'm starting to mentally associate Easter with massive
spending... I took last Thursday off to relax and we finally bought our
wedding rings (platinum bands, mine [looks like
this](http://www.ernestjones.co.uk/images/products/details/3121496.jpg)),
which obviously are not cheap. Then we popped into Virgin and discovered
a DVD sale: bought [K-PAX](http://www.imdb.com/title/tt0272152/), [High
Fidelity](http://www.imdb.com/title/tt0146882/) and [Starship
Troopers](http://www.imdb.com/title/tt0120201/).

Of course, two rings does not make a spending spree. On Saturday we went
down to London so I'd have something nice to wear to the wedding (as the
groom I thought I'd best make an effort) and picked up a lovely black
suit from [Austin Reed](http://www.austinreed.co.uk/), found HMV also
had a DVD sale (massive restraint here, only bought
[Léon](http://www.imdb.com/title/tt0110413/)) and went to Zizzi's for
dinner. Finally on Sunday, as a break from the all the travelling to get
to the shops, we bought a [new
TV](http://products.sony.co.uk/productdetail.asp?id=2_1_4444) online.
Now all I need is for the GNOME Foundation to send me the Bounty
money...

NP: Liquid Swords, GZA.
