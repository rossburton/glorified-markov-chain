Title: SSH Tip Of The Day
Date: 2008-12-11 12:00
Tags: tech
Slug: ssh-tip-of-the-day

Do you regularly ssh into machines which have dynamic IP addresses, and
get really annoyed with OpenSSH warning that the IP's key doesn't match
the host key? I certainly do, with machines announce their names using
mDNS and a DHCP server in my router. Today I finally checked the
documentation and found out how to skip this check.

The magic option is `CheckHostIP`, which you can set in `.ssh/config` on
a per-host level. I've got this in my `config`:

    Host *.local
      CheckHostIP no

Now all machines I ssh into using a `.local` domain won't have their
IP's key checked against the host key, because the IP is dynamic.
Sorted!

<small>NP: <cite>Music Like Amon Tobin</cite>, Last.fm</small>
