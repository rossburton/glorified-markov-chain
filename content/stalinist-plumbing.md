Title: Stalinist Plumbing
Date: 2004-03-11 00:00
Tags: life
Slug: stalinist-plumbing

A totally irrelevant title for a totally irrelevant posting -- my good
mate [Allen Morgan](http://www.monkey-spanking.co.uk) has finally
started creating a [web site](http://www.monkey-spanking.co.uk).

Why "Stalinist Plumbing"? Well, I just started reading [The Kindness Of
Strangers](http://www.amazon.co.uk/exec/obidos/ASIN/075531073X) by the
BBC War Correspondent Kate Aide. It would take to long to explain the
context, but she said it and it made me smile.
