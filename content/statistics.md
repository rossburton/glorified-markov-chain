Title: Statistics
Date: 2005-03-24 17:41
Tags: life
Slug: statistics

Some random statistics of the last fortnight:

-   Number of emails to download: 6274
-   Number of photos taken: 626
-   Disk space used by photos: 1.7G
-   Number of photos printed: 266
-   Percentage of buses and autorickshaws in Delhi which use 'green'
    fuels: 100%
-   Duration of outbound flight: 8 hours
-   Duration of inbound flight: 13 hours
-   Number of different 'most important fort in India' seen: 3
-   Cheapest meal for two with drinks: 380 Indian Rs (£4.64)
-   Most expensive meal: ~2500 Indian Rs (~£30)
-   Biggest Buddha (height): 30 metres

We're mid-way through sorting the photos out, but to get something
online now, here is one.

[![Taj Mahal](http://www.burtonini.com/photos/Misc/thumb-taj.jpg)](http://www.burtonini.com/photos/Misc/taj.jpg)
