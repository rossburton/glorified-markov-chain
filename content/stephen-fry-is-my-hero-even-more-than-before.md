Title: Stephen Fry Is My Hero (Even More Than Before)
Date: 2007-09-20 13:10
Tags: tech
Slug: stephen-fry-is-my-hero-even-more-than-before

[Stephen Fry](http://en.wikipedia.org/wiki/Stephen_Fry) has long been a
hero of mine for his acting and comedy, but I've just discovered that
not only is he a geek, but he also *gets it*. From [his
blog](http://www.stephenfry.com/blog/) regarding the iPhone:

> Server side apps only. No, no, no, no, no. This is NOT good. It's one
> thing to want to keep the proprietary system closed, but to present a
> device sealed in digital Araldite is a Bad Idea. An Ubuntu flavoured
> Linux for mobiles is in the works, and you don't get more open source
> than that.

The full article is rather long but a very good read. Now I just want
him to stop fawning over his iPhone and get back to making another
series of QI, damnit!

<small>NP: <cite>Digital Shades Volume 1</cite>, M83</small>
