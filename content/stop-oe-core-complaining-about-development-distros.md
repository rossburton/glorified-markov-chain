Title: Stop oe-core complaining about development distros
Date: 2012-08-08 10:22
Tags: tech, yocto
Slug: stop-oe-core-complaining-about-development-distros

As anyone who runs <s>oe-core</s> Poky on a development distribution, you'll
get a warning when you start `bitbake` because the distribution is
unsupported:

`WARNING: Host distribution "Debian GNU/Linux unstable (sid)" has not been validated with this version of the build system; you may possibly experience unexpected failures. It is recommended that you use a tested distribution.`

Fair enough, but I'm tough enough to deal with that. Luckily you can
silence this warning. Take the distribution identifier out of the
warning and then append it to `SANITY_TESTED_DISTROS` in your
`local.conf`, for example:

`SANITY_TESTED_DISTROS += "Debian GNU/Linux unstable (sid)"`

Voilà, no more spurious warnings.
