Title: Strange Office
Date: 2004-06-08 00:00
Tags: life
Slug: strange-office

I work in a very strange office. On the way in to work someone in the
train in front on mine collapsed due to the heat, and today there is a
leather jacket hanging on the coat rack. Yes, someone wore a leather
jacket to work today, when the [Met Office](http://www.metoffice.com)
thinks it will hit 28° C.

<small> NP: <cite>[Worldwide
Undergroud](http://www.amazon.co.uk/exec/obidos/ASIN/B0000CG3PB)</cite>,
Erykah Badu </small>
