Title: Straw: Moving On
Date: 2005-01-16 14:15
Tags: tech
Slug: straw-moving-on

Spooky: just as [Juri announces that he is giving
up](http://www.helsinki.fi/~pakaste/blog/straw_moving_on.html) as the
maintainer of Straw, I started looking for someone to take over
maintaining the Debian packages. Between switching to Bloglines and
being a lax maintainer of late, the guilt has been building up every
time a bug report came in and I've finally snapped. I'll file a formal
RFA next week, but if anyone wants to take over then [mail
me](mailto:ross@debian.org).

<small>NP: <cite>Bueno Vista Social Club</cite></small>
