Title: Straw Orphaned
Date: 2005-01-21 00:00
Tags: tech
Slug: straw-orphaned

I finally did it: Straw has [been
orphaned](http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=291539).
Hopefully someone will be able to pick it up shortly and ensure it's in
a fit state for Sarge.

<small>NP: <cite>O</cite>, Damien Rice</small>
