Title: Stuff Happens
Date: 2003-07-25 00:00
Tags: life
Slug: stuff-happens

<div class="title">

Music

</div>

The Red Hot Chilli Peppers again! Will the torture never end? The RHCP
albums are continually playing at work, and when they are not its The
Cardigans. We have two weeks of music here, and it's the same three
albums over and over and over...

No wonder I've started listening to Secret Agent and Groove Salad (from
[SomaFM](http://www.somafm.com/)) recently!

<div class="title">

Conversation

</div>

It struck me today how redundant much of chatting is. This is a fragment
of a real conversion I overheard this lunch time (with an attempt at the
distinctive Croydon twang):

> Person 1: I just lost my mobile phone, didn't I.  
> Person 2: What do ya mean ya lost it?  
> Person 1: Well, I can't find it can I!

Is online conversion more terse as you actually have to type the
letters? This sort of conversation is common when spoken, but rare in my
experience online.

<div class="title">

Conversation

</div>

Finally managed to get the Kefalonia photos back. I'll scan some more
when I have my own scanner working, but here is a photos of [the local
goat](/photos/Kefalonia/goat.jpg), resting in a tree. I can't decide
whether the goat is thinking "look at me I'm a cute goat" or "you
looking at me?"...

Speaking of photos, why is it every time I go to Jessops it's raining.
Seriously, *every* time.
