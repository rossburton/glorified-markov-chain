Title: Suboptimality
Date: 2005-10-07 11:05
Tags: tech
Slug: suboptimality

Whilst I was away at LinuxWorld Expo I see
[Jeff](http://primates.ximian.com/~fejj/blog/archives/000034.html) and
[Christian](http://blogs.gnome.org/view/cneumair/2005/10/06/0) carried
on the discussion started by Wingo.

Jeff is correct and if I'd thought about it at the time I'd have
realised that g-v-m is only responding to the "new media" signal from
HAL, which has to wait for the drive to spin up. One idea is that maybe
HAL should emit a pre-device-added signal so that g-v-m can pop up a
notification area icon saying that *something* has been inserted and
it's being probed.

However I'm not a great fan of Christian's proposal. Personally I'd
prefer we invented some MIME types, i.e. application/x-device-cdda for
"can play audio CDs" and g-v-m can then display lists of applications
based on the MIME type keys, and also use the startup notification
information in the .desktop files.
