Title: Success!
Date: 2003-11-05 00:00
Tags: tech
Slug: success

Despite the continued chronic tiredness, today isn't going badly.

My tool to search the Evolution address book [is
working](http://www.burtonini.com/computing/screenshots/evolution-panel-1.png).
The next stage is to clean up the UI (I'm stealing the vCard widgets
from Evolution) and evolve (boom boom) it into a panel applet.

Finally managed to get a LDAP server running at work, thanks to Jeff
Waugh and Chris Toshok. I'm glad to see that Evolution's use of LDAP is
far, far superior to Outlook Express or Mozilla's.

And to finish it all off:

>     $ ./vmpTest
>     Running suite(s): Direct
>     100%: Checks: 185, Failures: 0, Errors: 0
>       

That, my son, is a comprehensive test suite. The full suite contains 195
tests but 10 of them are not relevant here.

**Dave**: great news about the new Nautilus plugin API. I suggest you
attempt a Subversion plugin first though using the lovely Subversion
client API, otherwise you'll send half of your time parsing the damn
`CVS/*` files...
