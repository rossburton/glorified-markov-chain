Title: Summer Photos
Date: 2005-05-27 18:04
Tags: life
Slug: summer-photos

The combination of Spring and working from home is a nice combination.
I've taken quite a few photos of our rather small garden and when I've
made Tate work again I'll put them online, but here is one to keep you
going.

[![](http://www.burtonini.com/photos/Random/thumb-img_2397.jpg){
}](http://www.burtonini.com/photos/Random/img_2397.jpg)

Vicky titled it <cite>A Bee's Feast</cite>.
