Title: Summertime!
Date: 2006-06-12 13:15
Tags: life
Slug: summertime

Summer finally arrived in England last week, after a pretty miserable
and wet spring. Annoyingly it arrived a few days late as we were hoping
that it would be glorious whilst we were in Paris, but alas it was only
good for the last two days.

Of course summer means sitting outside with glasses of cold drinks and
lazying around. We did a bit of that over the weekend but it was a
little too sticky then, as today is much fresher I dug around the pit of
doom that is our garden and built some of the (to be replaced very soon)
garden furniture.

![My Office](http://burtonini.com/photos/Misc/img_5628.jpg){}

Lovely. However as the sun makes its way up the shadows to work in get
smaller and smaller, we really need to get a sunshade.

<small>NP: Lounge stream, Digitally Imported Radio</small>
