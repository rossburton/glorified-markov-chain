Title: Sun Vendor Patches
Date: 2004-04-30 00:00
Tags: tech
Slug: sun-vendor-patches

Glynn says:

> <cite> I think I may have created the first vendor patch against the
> new file chooser. Fear me. </cite>

Hmm, I wonder what it does. Maybe it fades the dialog to black and back
again using their secret patented algorithm when the user changes
location.

Also Hubert has [ZeroConf name
lookup](http://www.advogato.org/person/hub/diary.html?start=223)
working. I think I'll poke him for information and use it at home, my
ADSL router will give out IPs via DHCP but won't let me assign names to
the mac addresses, and I don't really have a server, just a laptop and a
desktop. Multi-cast DNS appears to be the solution to my naming problem
and has been on my To Do list for a long time.

NP: <cite>From The Choirgirl Hotel</cite>, Tori Amos. This is my Tori
Amos mega-minidisc. :)
