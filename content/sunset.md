Title: Sunset
Date: 2005-01-25 00:00
Tags: life
Slug: sunset

On Sunday we had a lovely purple and red sunset, so I decided to wander
around town trying to find a good point to photograph it from without
there being *too* many houses in the way. Luckily I discovered a small
park and an allotment, so managed to get some nice pictures.

[![](http://www.burtonini.com/photos/Random/thumb-img_0379.jpg){
}](http://www.burtonini.com/photos/Random/img_0379.jpg)
[![](http://www.burtonini.com/photos/Random/thumb-img_0388.jpg){
}](http://www.burtonini.com/photos/Random/img_0388.jpg)
[![](http://www.burtonini.com/photos/Random/thumb-img_0390.jpg){
}](http://www.burtonini.com/photos/Random/img_0390.jpg)

I'm *really* loving the 300D...
