Title: Super 7
Date: 2003-08-14 00:00
Tags: life
Slug: super-7

Super 7 is my sister's boyfriend's band (vocals and guitarist) -- and
they rock. They recorded a gig recently which was shown on Sky last
night -- a movie of it is available
[here](http://luke.supapawa.com/bands/super7.htm) (its a WMV but Xine
plays it fine). They buggered up the aspect ratio, so manually select
anamorphic.

If anyone out there thinks they are good, then feel free to email
<info@trinityforceltd.com> and tell them how great you think they are!
:)
