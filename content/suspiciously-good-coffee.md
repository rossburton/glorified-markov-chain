Title: Suspiciously Good Coffee
Date: 2004-06-12 00:28
Tags: life
Slug: suspiciously-good-coffee

I was feeling in need of a drink on the way home so I stopped at
<cite>Ritazza</cite>, the micro-coffee shop on platform 1 at East
Croydon station. Ordered a double expresso and whilst the smiling man
was making it the honey flapjacks were calling to me... so I got one of
those too. Then it started to get all weird.

The flapjack is made from oats, syrup, butter, sugar and honey. You know
-- like you'd do at home -- but nothing like what you get in the shops
usually. This cheered me up no end (being partial to a good
flapjack\[1\]), and indeed it was lovely, until I noticed my coffee...

It's *perfect*. Good temperature, a solid crema, and nicely strong. The
only downside is that it comes in a whacking great paper cup, but I'm
sure I'll live with that. I've a feeling this could be the end of me...

<small> \[1\] Of course true "good" flapjacks don't have chocolate bits,
fruit bits, sugared bits or any other bits added, nor have they been
dipped in another of the above. </small>
