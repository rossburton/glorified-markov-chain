Title: Sweet, Sweet Commits
Date: 2005-07-28 15:48
Tags: tech
Slug: sweet-sweet-commits

Some source code commits are boring, like bug fixes and documentation,
while some are exciting, like new features. Today I got to do one of the
latter:

> `$ svn commit -m "Adding DBus port of the Calendar" eds-dbus/calendar`

I can't take any credit for this, all praise must go to [Chris
Lord](http://www.chrislord.net/) for the port, who is interning at
Opened Hand over the summer.

<small>NP: <cite>Birth Of The Cool</cite>, Miles Davis</small>
