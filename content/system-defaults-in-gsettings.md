Title: System Defaults in GSettings
Date: 2011-07-04 15:45
Tags: tech
Slug: system-defaults-in-gsettings

**Note:** Florian in the comments points me at the documentation (albeit
rather concise) for this [in the API
documentation](http://developer.gnome.org/gio/stable/GSettings.html#GSettings.description)
under <cite>Vendor Overrides</cite>.

GSettings, like GConf before it, allows the administrator of a system to
override the default settings or lock down keys to particular settings.
This is [well documented in the GNOME
wiki](https://live.gnome.org/dconf/SystemAdministrators).

However GConf didn't really have the concept of vendor patches.
Traditionally if a Vendor wanted to change a default (say, the
wallpaper) they'd have to patch the GConf schemas directly. Luckily for
people who maintain distributions, GSettings provides a way of
installing vendor overrides directly. It's not documented as far as I
can tell so consider this a first draft at the manual...

First, find the setting you want to override, `dconf-editor` is useful
for this. Say you're making a work-orientated custom distribution so you
want the shell's popup calendar to show the week number by default. Some
digging in `dconf-editor` leads us to the `org.gnome.shell.calendar`
folder (the "schema") with a boolean key `show-weekdate` that defaults
to `false`. By changing the default of this to `true`, all users will
have work week shown unless they explicitly set it to `false`.

Now we've found the information we need we can write the override file.
Create a new file with the extension `.gschema.override`, such as
`mydistro-tweaks.gschema.override`. This file is a .ini-style keyfile,
logically mapping schemas to groups and key/value pairs to (predicable)
key/value pairs. The value needs to be in the [GVariant serialisation
format](http://developer.gnome.org/glib/stable/gvariant-text.html), but
for things like booleans, numbers and strings these are fairly obvious.
So we'd have a file that looks a little something like this:

    [org.gnome.shell.calendar]
      show-weekdate=true

Note that you can set multiple keys in multiple schemas in the same
override file, so if we also wanted to show the date in the panel we'd
have this:

    [org.gnome.shell.calendar]
    show-weekdate=true

    [org.gnome.shell.clock]
    show-date=true

Now the file is ready to be installed. Put it in a package, install to
`$prefix/share/glib-2.0/schemas` and finally run
`glib-compile-schemas $prefix/share/glib-2.0/schemas` in the
post-install/post-remove hooks. Done!

(many thanks to Ryan Lortie for telling me how vendor patches work)
