Title: Tasks 0.1
Date: 2007-02-23 11:38
Tags: tech
Slug: tasks-0-1

I've just tagged and rolled a Tasks 0.1 tarball. Tasks is a simple To Do
manager, using GTK+ and libecal (part of Evolution Data Server). It
isn't very featureful at the moment, but it's progressing nicely: I'm
using it already.

![Tasks](http://burtonini.com/computing/screenshots/tasks.png)

I'll be adding this to the [OpenedHand
Projects](http://projects.o-hand.com) site shortly, but for now [here is
a tarball](http://burtonini.com/computing/tasks-0.1.tar.gz).
