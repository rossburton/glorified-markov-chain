Title: Tasks 0.10
Date: 2007-07-11 12:39
Tags: tech
Slug: tasks-0-10

I'm pleased to announce that Tasks 0.10 is now available from the
[Pimlico Project](http://pimlico-project.org). There are a few fixes
here, but the big news is an initial port to Maemo (thanks to Rob
Bradford for the bulk of this work).

-   Basic Maemo port (currently for the N800 only, but should build for
    the 770)
-   Internal refactoring of the grouping architecture
-   Don't allow tabbing in the Notes field (\#381)
-   Make the Notes tab label bold if there is a note

The Maemo port of Tasks is a prime place for anyone who wants to learn
about programming Maemo. The Maemo-specific code is currently 595 lines
long (a chunk of which need to be factored out), so filling in the
missing pieces (such as starting a web browser) is easy. If anyone wants
to get their hands dirty, [mail me](mailto:ross@burtonini.com).

<small>NP: <cite>Burial</cite>, Burial</small>
