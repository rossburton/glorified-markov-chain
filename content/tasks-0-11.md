Title: Tasks 0.11
Date: 2007-08-13 21:35
Tags: tasks, tech
Slug: tasks-0-11

I'm pleased to announce that Tasks 0.11 is now available from the
[Pimlico Project](http://pimlico-project.org). A few bug fixes:

-   Update the window title in an idle callback, fixing crashes (\#401)
-   Persist and restore the window position (\#397)
-   Fix compile with non-gcc compilers (\#408, \#405)
-   Set a title on the New Group dialog
-   Fix package checks for the Maemo port
-   Implement open URL in the Maemo port
-   Hide button and menu images in Maemo port

