Title: Tasks 0.12
Date: 2007-09-23 16:45
Tags: tasks, tech
Slug: tasks-0-12

I'm pleased to announce that Tasks 0.12 is now available from the
[Pimlico Project](http://pimlico-project.org). Hot new feature is
undo/redo support, which lets you undo any change to the tasks. If an
action is found which cannot be undone and then redone, please file a
bug.

-   Add undo/redo support (\#312, \#508, \#510)
-   Disable actions which require a selected task when no task is
    selected
-   Port OpenMoko UI to the New World Order (Rob Bradford)
-   Add a 24x24 icon (\#475)

