Title: Tasks 0.14
Date: 2008-09-29 08:45
Tags: tasks, tech
Slug: tasks-0-14

It's been nearly 10 months after the previous Tasks release, for which I
profusely apologise. I wanted to fix one final bug before releasing,
which sadly took five months to get around too... I eventually fixed it
last night, so here is [Tasks
0.14](http://pimlico-project.org/tasks.html).

-   Magic date parser when adding tasks
-   Support libunique in the GTK+ port (Jonny Lamb)
-   Support OWL window menus in the GTK+ port
-   Save and restore the edit window size
-   Add a clickable note icon (\#548)
-   Ellipsize the undo/redo menu items (\#741)
-   Make sure the date popup doesn't go off the screen (\#752)

The most interesting change in this release is the magic date parser,
which first landed back in March. This lets you use <cite>Google
Calendar</cite> style descriptive tasks such as "release tasks today",
"do shopping next tuesday" or "pay bills on 2nd". There are many
patterns that are matched but I need two things from any users of Tasks.

1.  Translations. At the moment there are only English and French
    translations for the strings, which are critical for the parser
    to work. Translators, please update the translations!
2.  Feedback. The parser handles all of the natural language expressions
    that I thought would be useful. There are probably plenty more which
    are not handled, so if you find one which isn't handled (or is
    handled incorrectly) then please [file a
    bug](http://bugzilla.openedhand.com).

Oh, and one last thing. The OpenMoko and Maemo ports have likely
bitrotted. New functionality has been added to the platform abstraction
and I don't think those ports were updated. If someone actively uses
Tasks on either Maemo or OpenMoko and is willing to test builds before
release, please [contact me](mailto:ross@burtonini.com).
