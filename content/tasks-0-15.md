Title: Tasks 0.15
Date: 2009-03-30 12:00
Tags: tasks, tech
Slug: tasks-0-15

Just a small few fixes, translation updates, and little features in
Tasks 0.15.

-   Add --edit-task
-   Use gtk\_show\_uri if available
-   Lots of translation updates
-   Add magic patterns "in x days" and "in x weeks"

As usual, download from the [Pimlico
Project](http://pimlico-project.org/tasks.html).
