Title: Tasks 0.16
Date: 2009-07-13 08:27
Tags: tasks, tech
Slug: tasks-0-16

Some stability fixes, translation updates, and small new features in
Tasks 0.16.

-   Don't crash if you edit a task and then delete it
-   Lots of translations
-   Don't use SexyIconEntry
-   Move task ellipsising to the middle
-   Show tooltips for tasks with notes

As usual, download from the [Pimlico
Project](http://pimlico-project.org/tasks.html).
