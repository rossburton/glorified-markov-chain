Title: Tasks 0.18 (and 0.17)
Date: 2010-07-12 20:00
Tags: tasks, tech
Slug: tasks-0-18-and-0-17

Whilst [Tasks](http://www.pimlico-project.org/tasks.html) isn't exactly
under active development, I'm still maintaining it because I actually
use it (unlike certain other projects, ahem). So, Tasks 0.18 is
released.

-   Huge translation update, including several missing strings
-   Add a "tomorrow" button to the date popup
-   Support adding tasks from the command line
-   Use "category" over "group" consistantly
-   Ensure the entry is correctly styled
-   Ellipzies categories in the combo box
-   Correctly encode non-ASCII notes
-   Fix compilation with GTK+ 2.18

Tarballs and more information as usual are available at the [Pimlico
Project](http://www.pimlico-project.org/tasks.html) web site.

In related news, we're slowly migrating over to the GNOME
infrastructure. We've migrated the source code, next up is the tarballs
and bugzilla.
