Title: Tasks 0.19
Date: 2011-02-18 13:20
Tags: tasks, tech
Slug: tasks-0-19

Shock news: a [Tasks](http://www.pimlico-project.org/tasks.html)
release! Announcing 0.19:

-   Lots of translations
-   Fix i18n in the about dialog

Yeah, it's all go on the Tasks front... Tarballs on the Pimlico site, or
gnome.org.

<small>NP: <cite>Yanqui U.X.O.</cite> - Godspeed You! Black
Emperor</small>
