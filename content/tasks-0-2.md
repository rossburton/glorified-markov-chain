Title: Tasks 0.2
Date: 2007-02-28 18:20
Tags: tech
Slug: tasks-0-2

[Tasks 0.2](http://projects.o-hand.com/tasks) is ready to roll. Tasks is
a simple To Do manager, using GTK+ and libecal (part of Evolution Data
Server). No great new features over 0.1, but several bug fixes. Now that
I've been putting off categories by fixing up code, I'll have to them.

![Tasks](http://burtonini.com/computing/screenshots/tasks-0.2.png)

-   Sort tasks based on the locale
-   Persist window size
-   Allow GTK+ themes to change the task colours
-   Move the cursor to new tasks
-   Remove the None priority from the interface
-   Add a frame around the date popup (thanks Luca)
-   Don't close the date popup when the month is changed (thanks Kris)
-   Pop down the combo when the button is pressed (thanks Kris)
-   Add a faded label to the entry explaining what it is for
-   Fix memory leaks

For 0.3 I invite a dedicated member of gnome-i18n to be the first person
to send a patch to i18n the source and provide a translation. Fame and
glory can be yours!
