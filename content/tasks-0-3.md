Title: Tasks 0.3
Date: 2007-03-05 21:16
Tags: tech
Slug: tasks-0-3

Tasks 0.3 is out! The main new features are internationalisation and
category filtering. A complete list of user-visible changes:

-   Translatable (thanks Jordi Mallach)
-   Add a category combo in the edit dialog to select categories, and a
    category filter in the main window
-   When adding new tasks if the view if filtered, put the task in that
    category (Emmanuele Bassi)
-   Don't embolden completed high priority tasks
-   Show task count in the title bar
-   Use the stock Add button
-   Enable building on GTK+ 2.8

I also had a few translations:

-   nl (Espen Stefansen)
-   nl (Wouter Bolsterlee, Chrisph Brill)
-   pl (Tomasz Dominikowski)
-   id (Arief Utama)
-   pt\_BR (Bruno Boaventura)
-   fr (Bruno Bord)
-   gl (Alejandro Riveira Fernández)
-   sk (ja@disorder)
-   vi (Nguyen Thai Ngoc Doy)
-   el (Stagros Giannouris)
-   cs (Vitezslav Kotrla)
-   et (Priit Laes)
-   eu (Mikel Olasagasti)
-   sv (Andreas Henriksson)
-   it (Emmanuele Bassi)
-   tr (Tuyan Ozipek)
-   es (Adolfo González Blázquez)
-   de (Christoph Brill)
-   ca (Jordi Mallach)

Sources are available on the [OpenedHand Projects
site](http://projects.o-hand.com/tasks), and packages for Ubuntu Edgy,
Ubuntu Feisty, and Debian Sid are [in my
repository](http://burtonini.com/debian/). Adam Williamson has also
packaged Tasks into Mandriva Contrib.
