Title: Tasks 0.4
Date: 2007-03-16 09:30
Tags: tech
Slug: tasks-0-4

Tasks 0.4 is released! This release has some good new features:

-   Add a menu bar with new features like Remove Completed.
-   Mark the current day on the calendar popup
-   Stop duplicate groups appearing in the task editor (\#239)
-   Only count uncompleted tasks for the titlebar

![Tasks](http://burtonini.com/computing/screenshots/tasks-0.4.png){}

Sources are available on the [OpenedHand Projects
site](http://projects.o-hand.com/tasks), which also links to Debian
packages.

<small>NP: <cite>You Are Free</cite>, Cat Power</small>
