Title: Tasks 0.5
Date: 2007-05-19 12:30
Tags: tech
Slug: tasks-0-5

Tasks 0.5 is now released. This release has features a port to OpenMoko
and several bug fixes.

-   Add an OpenMoko frontend (Rob Bradford)
-   New icons from Andreas Nilsson to match the Pimlico style
-   Use a SexyIconEntry for the URL entries if available (Diego
    Escalante Urrelo, \#241)
-   Fix layout of widgets in the task editor (Diego Escalante
    Urrelo, \#236)
-   Chain up focus events in the new task label fixing warnings and IM
-   Add some padding around the editor dialog (\#323)
-   Improve messages for translation
-   Translate the menu bar
-   Fix license to refer to Tasks not Sound Juicer
-   Remove Application from the desktop file

More information, screeenshots, and tarballs can be downloaded from the
[Pimlico site](http://pimlico-project.org). There are no packages yet,
but I hope to have those online shortly.
