Title: Tasks 0.7
Date: 2007-05-27 18:55
Tags: tech
Slug: tasks-0-7

It's a Bank Holiday weekend, so what better to do that to implement a
great new feature in Tasks, and then attack the bug list? It's now
Sunday evening, and Tasks 0.7 is available from the [Pimlico web
site](http://pimlico-project.org/tasks.html) as usual.

-   When adding a new task, parse ! or + as high priority, - as low
    priority, and @foo as the group foo
-   Make the details window wider (\#314)
-   Fix mnemonic activation on the description field
-   Close the New Group dialog on enter (\#347)
-   Add a menu shortcut for Mark Complete (\#346)
-   Don't conflict the Description and Delete mnemonics (\#345)
-   Enable interactive searches in the task view (\#315)
-   Allow multiple editor windows to be open (\#317)
-   Add a keybinding for Delete

Best Tasks release *ever!*
