Title: Tasks 0.8
Date: 2007-06-14 14:20
Tags: tech
Slug: tasks-0-8

After all of the mad Tasks hacking that I did over the last few weeks, I
thought it really was time to make a release. So without much ado, here
is [Tasks 0.8](http://pimlico-project.org/tasks.html).

-   Allow filtering on uncategorised tasks (\#370, thanks
    Andrey Tatarinov)
-   Fix off-by-one in the window title when deleting tasks (\#353)
-   Display day names if the task is due in the next week
-   Don't use properties that only exist in GTK+ 2.10
-   Fix builds against Glib &lt; 2.10
-   Add a label to the Category combo (\#320)
-   When editing a task, raise any existing windows (\#350)
-   Display a confirmation when deleting tasks from the editor (\#351)
-   Set the task editor title to the task summary (\#349)

Tarballs and packages for Etch, Edgy and Feisty are on
[debian.o-hand.com](http://debian.o-hand.com/). Packages for Sid are
uploaded already.

As a teaser, using the experience from working on both the Nokia
addressbook and Tasks I've started working on a framework for the next
generation of [Contacts](http://pimlico-project.org/contacts.html).
Large amounts of the code are from Tasks but with `s/task/contact/g`
applied, and it works quite well already.

![Hito
screenshot](http://burtonini.com/computing/screenshots/hito.png){}

That is a view of my entire addressbook, filtered on the OpenedHand
group. There is no widget to select groups yet, so you'll have to trust
me that it's live!

To anyone who noticed that not all of OpenedHand are in the list, and
are wondering if I'm subtly dissing people: No I'm not. We have an
internal LDAP server, so I stopped keeping my local addressbook up to
date some time ago.

<small>NP: <cite>What's Come Inside Of You?</cite>, Freescha</small>
