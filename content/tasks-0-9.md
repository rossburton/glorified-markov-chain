Title: Tasks 0.9
Date: 2007-06-26 21:15
Tags: tech
Slug: tasks-0-9

After fixing a few bugs, cleaning up the source code, and even adding a
few new features, [Tasks 0.9 is
ready](http://pimlico-project.org/tasks.html).

Most of the changes were internal, but there are a few user-visible
changes.

-   Show icon in the main task view when a task has a URL
-   When clicking on the URL icons, start a web browser
-   Revert the group when cancelling New Group
-   Make the editor dialog two pane, with Notes on a seperate pane

