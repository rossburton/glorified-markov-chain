Title: Tasks i18n
Date: 2007-03-01 08:10
Tags: tech
Slug: tasks-i18n

Fame and Glory go to [Jordi
Mallach](http://oskuro.net/blog/freesoftware/tasks-i18n-2007-02-28-23-26)
for the i18n patch and Catalan translation of Tasks. Go Jordi!

Translators, the gauntlet has been thrown. Tasks isn't in GNOME SVN
which is a pain for you I know (though I do have a cunning plan for
that...) but the SVN is [publically
available](http://projects.o-hand.com/tasks). I'll greatly welcome each
and every translation.
