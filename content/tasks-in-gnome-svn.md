Title: Tasks In GNOME SVN
Date: 2008-10-17 20:15
Tags: tasks, tech
Slug: tasks-in-gnome-svn

Thanks to the heroic work of Olav and Thomas, Tasks (along with Contacts
and Dates) is now in GNOME SVN. Translators, feel free to do your thing.
Oh, and would it be possible to get Tasks added to Damned Lies?
