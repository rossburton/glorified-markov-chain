Title: Tasks Packages
Date: 2007-03-02 13:44
Tags: tech
Slug: tasks-packages

Thanks to the magic of CDBS and my `rebuild` script, I've now got
packages of Tasks 0.2 for Debian Sid, Ubuntu Edgy and Ubunty Feisty [in
my repository](http://burtonini.com/debian). The Sid packages are also
in the NEW queue so should hit Debian in... about three weeks.

Thanks to Andrew Kerr who mailed me an initial packaging, although I
think I did end up rewriting about 80% of it.

<small>NP: <cite>Yoshimi Battles the Hip-Hop Robots</cite>, The
Kleptones</small>
