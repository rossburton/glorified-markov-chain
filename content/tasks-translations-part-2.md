Title: Tasks Translations (part 2)
Date: 2007-03-01 14:30
Tags: tech
Slug: tasks-translations-part-2

-   Czech, thanks Vitezslav Kotrla
-   Estonian, thanks Priit Laes
-   Basque, thanks Mikel Olasagasti
-   Swedish, thanks Andreas Henriksson
-   Italian, thanks Emmanuele Bassi

I may give up actually coding in the future and just ask for things to
be done on my blog.
