Title: Tasks Translations
Date: 2007-03-01 12:20
Tags: tech
Slug: tasks-translations

    2007-03-01  Ross Burton  <ross@openedhand.com>

        * tr.po:
        Turkish translation, thanks Tuyan Ozipek.

        * es.po:
        Spanish, thanks Adolfo González Blázquez.

        * de.po:
        German, thanks Christoph Brill.

        * nl.po:
        Dutch, thanks Max Beauchez.

Thanks guys!
