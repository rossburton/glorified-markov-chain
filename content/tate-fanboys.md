Title: Tate Fanboys
Date: 2005-01-20 00:00
Tags: tech
Slug: tate-fanboys

It appears Tate already [has a
fanboy](http://naeblis.cx/rtomayko/2005/01/11/ross-taint-erp-rawke),
despite not even having a release yet. Of course, the fact that Ryan
wrote [Kid](http://splice.sf.net/kid), and Kid is a fundamental part of
Tate, has nothing to do with this.

<small>NP: <cite>The Doct Of The Bay</cite>, Otis Redding</small>
