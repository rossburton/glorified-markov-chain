Title: Testers Wanted
Date: 2008-03-18 11:00
Tags: tech
Slug: testers-wanted-2

Over the weekend I hacked on a clone of [Marco
Polo](http://www.symonds.id.au/marcopolo/) for GNOME. The idea is that
you define a set of contexts, such as "work", "in meeting" or "home".
The current context is determined by a set of rules, for example being
on the "Burton" wireless network means I'm in the "home" context, the
time being between 09:00 and 18:00 means the "daytime" context, and so
on. Finally, when entering or leaving a context actions can be executed,
such as muting the sound card, mounting a remote drive, or changing the
default printer. So far I have sources for the time of day and wireless
network name, and actions to run a command and set a GConf key.

Now that the basics are in place, I'm looking for other alpha-testers.
Experience with Python is a requirement at the moment as there is no UI
or configuration file yet. That said, if this application sounds like it
could be useful to you then please [email
me](mailto:ross@burtonini.com).

<small>NP: <cite>!K7</cite>, Various</small>
