Title: Testers Wanted
Date: 2007-01-07 21:00
Tags: tech
Slug: testers-wanted

I've got a new version of Postr ready for release, but would like
someone else to test it before I release it as quite a lot of the
important code has been, well, rewritten.

So, to any potential testers: using Bazaar clone [this
repository](http://burtonini.com/bzr/postr/postr.dev), install Twisted
(`python-twisted-core` on Debian/Ubuntu), delete
`~/.flickr/c53cebd15ed936073134cec858036f1d/auth.xml` (the cached
authentication tokens) and try and upload a photo. If anyone gives it a
go please leave comments or email me, so I can fix any bugs.

Thanks!
