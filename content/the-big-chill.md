Title: The Big Chill
Date: 2004-01-27 00:00
Tags: life
Slug: the-big-chill

Since the Met Office mentioned last week that this week could [be a bit
chilly](http://www.metoffice.gov.uk/corporate/pressoffice/2004/pr20040122.html),
the media have been in full-on tabloid-frenzy mode. "Big Chill To Hit In
48 Hours" for example. To be honest, if we don't get a full-on blizzard
and proper arctic conditions -- I'm thinking at least -10°C before
wind-chill and a foot of snow piled up, I'm going to be very
disappointed.
