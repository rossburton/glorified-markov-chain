Title: The Book Meme
Date: 2004-05-04 00:00
Tags: life
Slug: the-book-meme

It appears I'm [being
blamed](http://advogato.org/person/async/diary.html?start=104) for the
book meme/fad on [Planet GNOME](http://planet.gnome.org). Don't blame
me! I just followed the trend on [Planet
Debian](http://planet.debian.net) which was already going strong,
honest!
