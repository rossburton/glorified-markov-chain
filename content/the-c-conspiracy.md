Title: The C# Conspiracy?
Date: 2005-08-15 18:26
Tags: tech
Slug: the-c-conspiracy

Oh fun, the great Novell vs Red Hat war rears it's ugly head again. Now
that Sound Juicer and Totem both play CDs we're nicely on route to
dropping gnome-cd totally. To this end, a few weeks ago Ronald [filed a
bug](http://bugzilla.gnome.org/show_bug.cgi?id=312828) with
gnome-volume-manager asking that the default be changed to Sound Juicer
(I'm thinking this should be Totem, but that's just my opinion).

Today the bug was closed. Hooray, I thought, SJ is the new default CD
player. But no:

> we're going to default to Banshee instead.

Pardon? We, the gnome-volume-manager maintainers, who happen to be
Novell employees, have decided that the default CD player in GNOME will
be [Banshee](http://sonance.aaronbock.net/). Not only is this not part
of GNOME, but it's written in C\#. I'm pissed off as this isn't a valid
reason to close the bug, considering it isn't even proposed for Desktop,
and I bet Red Hat will be pissed off if C\# enters the Desktop.

And on that note, I'm going to cook.

**Update:** all sorted now. Totem is the default CD player. Die gnome-cd
die!
