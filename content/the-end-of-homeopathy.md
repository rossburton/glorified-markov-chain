Title: The End Of Homeopathy?
Date: 2007-11-16 08:45
Tags: life
Slug: the-end-of-homeopathy

Dr Ben Goldacre hits the jackpot, moving from a column on Saturday's
Guardian to a [cover story in Friday's
Guardian](http://www.badscience.net/2007/11/a-kind-of-magic/). It's a
very balanced and reasoned argument against homoeopathy for people who
don't understand what a fair trial is, how placebo works, or just how
stupid homoeopaths sound when they try and explain it. [It's down to
permeated
nano-particles](http://www.guardian.co.uk/g2/story/0,,2209998,00.html),
apparently.

I recommend reading the article to anyone who thinks that homoeopathy is
better than placebo or agrees that homoeopathy has a place in treating
AIDS and malaria. Or people like me, who like watching homoeopathy get
slapped down. For the medical geeks out there, there is the companion
[article in the
Lancet](http://www.badscience.net/2007/11/the-lancet-benefits-and-risks-of-homoeopathy/)
too.
