Title: The "GNOME Wishlist"
Date: 2003-12-10 00:00
Tags: tech
Slug: the-gnome-wishlist

So the every amusing OSNews is running a [GNOME
Wishlist](http://www.osnews.com/story.php?news_id=5361&page=1). Sigh.
Where do I start?

Nautilus Scripts/Addons
-----------------------

Nautilus 2.6 will support funky new plugins, with a clean API and decent
menu merging. I though Eugenia was keeping up with Nautilus development,
she certainly posts on nautilus-list now and again.

Spatial Mode
------------

It's not finished -- either help make it totally rock by
commenting/fixing/patching, or wait until is it finished.

Metacity Features
-----------------

The standard rant about "viewports" and "workspaces". *Again*. Jesus
people, ask for a feature and not the name for a subset of available
features that you have used before. Please. I'm about to ask God to
extend his kitten-killing to Metacity... For glueing windows to the
corner of the screen... well, press `shift` once you are close to the
corner and it will magically glue itself, without snapping to the
windows it sees en route.

File Selector
-------------

Eugenia's comments are pretty useless: "...is pretty bad". Not really a
comprehensive UI review, but thanks anyway. But that's not what I'm
confused about -- I'm confused about the number of people who think that
the "Frobnicate this file" check box in Frederico's example screenshot
is part of the default UI! *Please engage brain before posting*. What
would "frobnicating" do to a document I opened in gedit? In galeon? Did
you ever consider the possibility that this widget is an example of an
extra widget the developer can add to the file selector?

Volume and Showdesktop Icons
----------------------------

Honestly, compared against some of the bugs in GNOME this is laughable.
Extract the patch from Red Hat, and file it as a bug upstream. It will
probably be applied. Not Hard Work.

Development Tools
-----------------

"Glade is junk, end of story" -- Eugenia. Right. Personally I consider
Glade to be a wonderful interface designer, and makes coding GTK+
interfaces trivial. I hope this isn't referring to the "Generate Source"
button in Glade, which is generally considered to be A Bad Idea when
using C.

Personally, I hope that Eclipse's C support will mature and someone
integrates Glade somehow, even if it is just a button to launch the
binary. But I'm happy with Glade + libglade + xemacs.

Copy/Paste still misbehaves after all these years
-------------------------------------------------

File bugs! There is no fundamental reason why copy and paste shouldn't
work, as is shown by the recent [gaim
hacking](http://gaim.sourceforge.net/sean/clipboard.png) to copy right
text to/from the gaim chat window into the Evolution composer.

GConf Editor
------------

A search button for gconf-editor could be handy, but generally tried
looking in `/apps/[app-name]`?

Samba on Nautilus
-----------------

GNOME 2.4 I believe had a new `smb:` implementation, and in GNOME 2.6 it
will rock even more.

Rhythmbox and Totem
-------------------

"Use the XMMS visualization plugins" -- not possible. It is impossible
to link GTK+ 1.2 and GTK+ 2.x code in the same binary.

"Totem ... use either Gstreamer or Xine on the fly" -- why? I'd say that
everything GStreamer can play, Xine can play. If you want a player for
everything, use Xine. In the future when GStreamer has the required
features, we'll all be able to switch over to that instead.

"I would like Totem to recognize the file format and show an alert to
the user "would you like to download from the web these formats and
install them?" -- Totem already does this when it can, and has done so
for a long time.

Epiphany
--------

The usual minor issues which get blown out of all proportion as
show-stopping bugs for the entire desktop. File a bug, create a patch,
do something!

Text and Video Messaging Integration
------------------------------------

I think the gnomemeeting maintainer covered this one...

Burning Application
-------------------

GNOME the desktop is going towards tools to help end-users. Thus we have
nautilus-cd-burner which is wonderful for the very common task of "burn
these files". I have a patch (honest, I do) for Rhythmbox which lets
that burn audio CDs from playlists. I don't see the need for a
fully-featured 100% coverage CD burning tool in the GNOME desktop.
