Title: The Good Stuff
Date: 2006-04-21 14:50
Tags: life
Slug: the-good-stuff

This arrived in the post today:

![Has Bean
coffee](http://burtonini.com/photos/Misc/has-bean.jpg){}

[Brazil Sitio Boa
Sorte](http://www.hasbean.co.uk/product_info.php?cPath=44_40&products_id=614)
coffee, freshly ground yesterday. It won Cup Of Excellence last year and
[Has Bean](http://www.hasbean.co.uk/) bought the entire supply. It's
*good*. At £4.99 a packet it's not really expensive, but it's not cheap.
I must avoid buying the other Cup Of Excellence winners, for example [El
Salvador San
Roberto](http://www.hasbean.co.uk/product_info.php?cPath=44_40&products_id=423)
which is *fourteen pounds* a bag.

I'm probably going to buy most of my coffee from Has Bean in the future:
the coffee is good and the delivery is fast. They freshly grind on
demand so it's always fresh, and it comes in airtight re-sealable bags
(none of that fold it over and sticky bit of plastic nonsense) that sit
nicely in the fridge. All in all: heavily recommended.

<small>NP: <cite>Buena Vista Social Club</cite></small>
