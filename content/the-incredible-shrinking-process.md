Title: The Incredible Shrinking Process
Date: 2005-10-20 16:50
Tags: tech
Slug: the-incredible-shrinking-process

Some time ago I was very pleased when I found one of the causes for the
[Incredible Expanding
Evolution](http://burtonini.com/blog/computers/eds-2005-05-16-16-55). At
the time I was positively pleased that the Evolution Data Server's heap
usage was under 1 megabyte when in use (the benchmark was to run 20
bookviews).

Well, things have come on a long way since then:  
![Massif
chart](http://www.burtonini.com/computing/screenshots/eds-massif-20051020.png)

Now it's peaking at just under 400Kb! The 200Kb chunk is the Berkeley DB
cache for my addressbook, and the size of that is tunable. After that
come `add_module` and `add_alias` from `libc`'s `gconv` implementation
of `iconv`. Now I'm pretty sure the data they are loading should be made
constant and shared among all processes. Anybody know of existing work
in this area?

<small>NP: <cite>Fear Of Fours</cite>, Lamb</small>
