Title: The Monarchy
Date: 2003-06-25 00:00
Tags: life
Slug: the-monarchy

So a comedian managed to break into Prince William's (next but one to
the throne I believe) birthday party a few days ago (dressed as Osama
Bin Ladin in a dress). The news following this was amusing in a way. The
police did the sensible thing of apologising and promising never to do
it again; the tabloids called the comedian a nut, a lunatic, a dangerous
fool. It was pointed out that if this person was carrying a bomb, he
could have potentially killed the next six people in line for the
throne, passing it onto Prince Andrew who was in Canada I believe.

Apparently this is not good. Now, I totally agree that someone breaking
into a "secure" party and killing six people is not a good thing, and am
not advocating the assassination of the monarchy. I am a peaceful
person, against killing in general.

However, this touches a nerve I have regarding the monarchy. People
discuss whether Prince Charles is fit to be the next King, if maybe we
should skip him and go straight to his son Prince William (who started
out being a relatively normal person, thanks to his mother Diana, but
has turned into another fox hunting Barbour jacket wearing upper-class
royal over time). Like we have a choice. England is a monarchy. The
people don't pick the succession. A select group of advisers doesn't
pick. The government doesn't pick. *God picks*. The succession of a
monarchy is not based on the next King or Queen being a particularly
brave, strong, clever, cunning or insightful person, but that He picked
them. Of course, He is infallible so is always right in the long run,
even if He appears to make strange moves. He is never wrong.

If the comedian did turn out to be a terrorist and killed the next six
people in line to the thrown, then that is what He wanted. A disease
could kill the immediate royal family and leave a distant young child as
the King, and that is what He wanted (something similar has happened in
the past). This is screwing around with the idea of free will, as
whatever happens was meant to happen. Extrapolate this up to the entire
nation, and suddenly our lives are just small parts of the grand plan,
decided in advance. I don't agree with this, and from the polls in the
tabloids (I'm looking mainly at <cite>The Sun</cite> here), neither does
the majority of the nation. So why do we still have this ancient system?

Well, in England at the moment the Royal family's roles consist of:

-   Waving
-   Doing a speech at Christmas which nobody watches
-   Attracting tourists
-   Being rich

They don't really serve a good purpose any more, as the running of the
country has been delegated to the elected government. Your comments: so
why does England still have a monarchy?
