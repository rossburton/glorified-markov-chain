Title: The Return Of A Legend
Date: 2004-06-22 00:00
Tags: life
Slug: the-return-of-a-legend

Oh lordy. [<cite>The Hitch-Hikers Guide To The
Galaxy</cite>](http://www.bbc.co.uk/radio4/hitchhikers/index_new.shtml)
is back for a whole new series on BBC Radio 4 later this year. I knew
there was a reason why I paid my license fee!

NP: <cite>Bebel Gilberto</cite>, Bebel Gilberto.
