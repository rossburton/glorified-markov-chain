Title: The SynCh
Date: 2006-11-09 18:30
Tags: tech
Slug: the-synch

Yesterday I plugged my SynCh into a wall socket and charged my Nokia
6230i. Then I charged an iPod Nano. This morning I plugged my SynCh into
my laptop and charged up my Nokia 770. I'm loving my
[SynCh](http://www.the-synch.co.uk)!

Basically The SynCh is a extendable charger and USB hub, with a USB plug
at one end for plugging into a laptop, and a custom power socket and USB
socket on the other. Into one of those you can plug a number of
adaptors: I have a mini-USB plug for data transfer, a Nokia old-style
charger, a Sony PSP charger (for the Zaurus), and an iPod charger. I
also have the travel adaptor, which lets me plug the SynCh into UK, EU
or US sockets for charging from the mains.

It's pretty cool, and I recommend that anyone who ends up taking seven
chargers away with them look at getting one. It's also a lot cheaper
than the iGo! My only problem is that the iPod charger won't charge my
3G iPod, but handles 4G and newer fine. Apple must have done something
odd...
