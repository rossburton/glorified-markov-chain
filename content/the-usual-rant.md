Title: The Usual Rant
Date: 2004-09-28 00:00
Tags: life
Slug: the-usual-rant

Being a Brit I generally talk about the weather or the trains. The
weather isn't bad for this time of year (although this week is
distinctly darker when I get home, compared to last week) so I'm going
to talk about the trains.

Yesterday the wonderful Leaf Fall timetable kicked in, which is a rather
pathetic substitute for cleaning the tracks more often. The deal is that
trains leaving for London in the morning leave slightly earlier (8
minutes in my case), so that when the wrong sort of leaves fall with the
wrong sort of rain, the trains can still get to London on time. Well,
that is the theory. Today the leaves haven't dropped yet, the train left
8 minutes early, and yet managed to arrive in London 10 minutes late.

Larry Wall will tell you that the principle virtues of a programmer are
Laziness, Impatience and Hubris. I think the platform staff at Liverpool
Street heard this and misunderstood: they have the impatience to tell
the hordes waiting on the concourse that their train is arriving shortly
on platform 7 instead of waiting for it to clear first, and hubris to
think this was a good idea, and the laziness to just sit there when they
realise that two full commuter trains have just arrivied on the same
platform, and the gates have just gridlocked.

Argh -- I don't want to be obsessed by trains, but the state of the rail
network seems to enforce it at times.

\[calming pause\]

In other news, [Luis](http://tieguy.org/blog/index.cgi/190) is clearly
mad. The "New Jeff" is a pale imitation of the Original Jeff, clearly
added as they couldn't get Jeff back and they knew he was the main
attraction for viewers. The forth series of <cite>Coupling</cite> should
have been taken outside and shot without trial for its own good, it was
that bad. I would recommend you to watch out for <cite>Green Wing</cite>
to travel across the Atlantic, but I don't know if Channel 4 are able to
sell programming across the pond.

<small>NP: <cite>Keep It Unreal</cite>, Mr. Scruff</small>
