Title: The Usual
Date: 2003-12-15 00:00
Tags: tech
Slug: the-usual

It's the usual Monday morning. Currently I've got a pile of patches for
evolution-data-server sitting in my checkout, which are getting
increasingly harder to separate for the lists. The contact lookup applet
finally works using EBookView (don't free stuff you don't own, stupid),
but sadly uses an API which is only in the previously mentioned patches.
I've been talking to Chris Toshok about this API change, hopefully he
will approve the commit today. And the video conferencing addition to
EContact. And the improved API docs. I need patch approval love -- why
did I start hacking on Evolution so close to the holidays! `:(`
