Title: Things That Annoy Me
Date: 2003-08-08 00:00
Tags: life
Slug: things-that-annoy-me

Several thing in the world annoy me.

-   People who stand at the top of the Moorgate tube station exit,
    trying to hand "Work from Home!" flyers, whilst standing totally in
    the way so that everyone coming out and me going in have to
    squeeze past.
-   Overground trains which don't have air conditioning, or even
    air circulation.
-   People which walk down busy pavements/corridors/walkways and just
    stop and turn around abruptly. Often the same people will wave an
    arm through the air, saying "Gee Dwayne, will ya look at that!"
    whilst concussing anyone within arms reach.
-   Pink tops which have been washed several times and haven't run in
    the wash before, but suddenly decide to run after all and dye two of
    my linen shirts and a top of Vicky's a lovely pale pink colour.
-   People who write <cite>Things That Annoy Me</cite> lists.

