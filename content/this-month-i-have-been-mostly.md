Title: This month I have been mostly...
Date: 2004-01-26 08:57
Tags: life
Slug: this-month-i-have-been-mostly

...reading [<cite>Carter Beats The
Devil</cite>](http://www.amazon.co.uk/exec/obidos/ASIN/0340794992) by
Glen David Gold, [<cite>Strange Places, Questionable
People</cite>](http://www.amazon.co.uk/exec/obidos/ASIN/033035566X) by
John Simpson and [<cite>Last Chance to
See</cite>](http://www.amazon.co.uk/exec/obidos/ASIN/0330320025) by
Douglas Adams and Mark Carwardine.

<small>\[Apologies to <cite>The Fast Show</cite>\]</small>

<cite>Carter</cite> is an exciting 1920s story about a magician, the
death of the President, the invention of television and personal
tragedy. Overall this is a good fun book, which kept me from blogging as
I read it on the train.

<cite>Strange Places</cite> is a very interesting read. It is basically
an account of John Simpson's work (a BBC journalist) over the last 30 or
so years. The range of people he has met is amazing, but also the
personal details of the people are very interesting. At times he comes
over as a little pompous, but on the whole he is very down-to-earth and
manages to retain the legendary BBC impartiality. Overall an interesting
book offering a rare personal view of the world's politicians and
leaders, which often meant I was reading until my bath water turned
cold.

<cite>Last Chance</cite> is the classic Douglas Adams doing his great
"comedy sci-fi author travels around world, looks confused" act. This
time he is traveling with Mark Carwardine, trying to find a range of
endangered species. DNA's writing matches the standards set by
<cite>Hitch-Hikers</cite>, and often led to me trying to control my
laughter on the train. Overall a enjoyable and enlightening read about
the state of the world and the destruction we seem to inflict wherever
we go.
