Title: Three Things I Love
Date: 2006-06-14 15:30
Tags: life
Slug: three-things-i-love

1.  Espresso shots in a glass of ice-cold milk. Quick and easy coffee
    boost and cooling drink in one.
2.  [<cite>Remembranza</cite>](http://musicbrainz.org/album/94f09962-8981-4bd6-a833-5630378ecbc9.html),
    by Murcof (Fernando Corona). Similar technically to his first album
    <cite>Martes</cite> but less electronic. The soundscape is far
    smoother whilst remaining as sparse, and the album feels a lot
    more relaxed. Fernando is a magician.
3.  Benchmarks that say `before: 6.5s` and then `after: 3.3s`. Yay for
    rewrites!

<small>NP: <cite>Remembranza</cite>, Murcof</small>
