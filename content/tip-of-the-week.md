Title: Tip Of The Week
Date: 2007-08-17 11:20
Tags: tech
Slug: tip-of-the-week

This fragment of shell doesn't do what you'd expect at a quick glance:

    if [ $? -ne 0 ]; then
        exit $?
    fi

The act of checking `$?` causes `$?` to be set to the result of the
test, so this executes `exit 0` if `$?` is non-zero.

<small>NP: <cite>Cosmos</cite>, Murcof</small>
