Title: Today's Geohack
Date: 2008-05-13 10:50
Tags: tech
Slug: todays-geohack

Following hot on the heels of Yahoo's announcement of their [Internet
Location Platform](http://developer.yahoo.com/geo/), I wrote a quick
20-line Python hack to convert from latitude and longitude to a place
name. Because the ILP doesn't yet expose the ability to go from a
position to a <acronym title="Where On Earth ID">WOEID</acronym> we have
to ask the Flickr web services to do this first (as Flickr is owned by
Yahoo this is using the same backend). Once we have the WOEID, it can be
then be looked up on the ILP and useful information obtained. Example
speak more than words:

    $ python geohack.py 
    Using position 51.872330 0.161950
    Got WOEID 12775
    Got town Bishop's Stortford

Now to write a [GeoClue](http://geoclue.freedesktop.org) provider which
will fill in the locality information from the position. Long-term grand
plans involve integrating all of this geo magic into Postr, somehow.

<small>NP: <cite>Third</cite>, Portishead</small>
