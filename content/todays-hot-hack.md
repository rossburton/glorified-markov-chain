Title: Today's Hot Hack
Date: 2007-11-07 11:10
Tags: tasks, tech
Slug: todays-hot-hack

Today I got fed up of writing *yet another* `GQueue` and idle function
to perform tasks incrementally in the background, with all of the
bookkeeping that needs to be done. So, I wrote `taku_idle_queue_add`.

Using it is simple, create a `GQueue` and then call
`taku_idle_queue_add`, passing the queue and a callback function. When
the queue has items in it, the callback gets called. Easy! Just remember
not to return `FALSE` from the callback unless you are sure the queue
will never be used again.

I'd appreciate anyone who knows `GSource` programming in detail to
review [the code](http://burtonini.com/computing/taku-queue-source.c).
Maybe I should even try and get this into Glib?

<small>NP: <cite>Cliqhop IDM</cite>, Soma.fm</small>
