Title: Today's Second Geohack
Date: 2008-05-13 15:15
Tags: tech
Slug: todays-second-geohack

I managed to wangle a Fire Eagle invitation this morning, so over lunch
I grabbed the Python API Kit and threw it at the sample Gypsy client.

    $ ./gypsy-fireeagle.py 00:0B:0D:88:A4:A3
    got 51.861145 0.156275
    Updated FireEagle

The first line is me running my script (this one is 64 lines, but it is
half whitespace), telling it where my GPS is. The second line is the
current position that my rather cheap and nasty GPS determined. The
third line tells me that Fire Eagle has been updated with those
coordinates.

Suffice to say I'm very impressed with Yahoo's geocoding software. My
GPS never settles to an accurate reading and will happily jitter around
a 20 metre wide circle for hours, but the location Fire Eagle is
reporting me at is *two doors away*. I'm not exaggerating: it says
number 9 on my street when it should be number 5. That is some
incredibly accurate mapping they have.
