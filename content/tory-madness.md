Title: Tory Madness
Date: 2005-04-27 21:30
Tags: life
Slug: tory-madness

The [Tories](http://www.toryscum.com) have gone "truth" crazy of late,
first by continually banging on about Blair's lies about the Iraq war
(but we won't mention how they wanted to go to war before Labour did),
and now with [`libdempolicy.com`](http://www.libdempolicy.com): a site
showing <cite>what lies in the small print of their policies</cite>.
Personally I thought they had been pretty open, but let's see what the
Tories can make of their policies.

 <cite>There should be no upper limit on the numbers of refugees accepted by EU countries</cite>

:   Oh My God.

    First, I thought that Kennedy had been saying this for ages, but
    that isn't the real point. Of course there should be no upper limit:
    if suddenly there was an atrocity and people were on the run for
    their lives, we should say "sorry, but the inn is full"? The sheer
    balls of a party that mixes immigration and asylum into a single
    über-policy of basically stopping it all, whilst having a leader
    whose parents are immigrants, is frankly quite impressive. Obviously
    the spin guys are doing something right as I just can't understand
    why no-one (apart from the audience in Question Time last week
    that is) is pointing out that they are totally unrelated subjects.

 <cite>Liberal Democrats would 'introduce fair benefits for asylum seekers'</cite>

:   This is a terrible policy as asylum seekers are sub-human
    scum, obviously. Best to leave them in France until we know they
    haven't got AIDS.

    I still don't know why asylum seekers cannot go and get a job,
    instead of having to try and live on some pathetic token
    state benefit.

 <cite>The Liberal Democrat Home Affairs spokesman, Mark Oaten, says he is 'absolutely convinced that prison is a complete and utter waste of time'</cite>

:   Sounds about right. Put a load of sane people in a small boring room
    for a few years and they probably won't be well-adjusted when they
    come out, doing the same to people who are not well-adjusted to
    start with is madness. Most people in prison need assistance which
    isn't provided by a magnolia wall.

<cite>We will put your taxes up</cite>

:   Well, durrr. Is there anyone who *didn't* know that the Tories only
    real policy is to lower taxes? Personally I think the Lib Dem policy
    of a 50% income tax on income over £100,000 is a great idea and have
    thought so ever since I started nudging the current upper tax
    boundary, and realised that if my earnings doubled I wouldn't be
    paying a higher rate. The tax system needs a bloody good overhaul
    (along with the Council tax, being based on property prices from
    1980), and this is a good start.

<cite>They would 'work positively towards the conditions for British entry to the euro'</cite>

:   I'm glad at least one main party is actually mentioning the Euro
    this year, although to be honest if the other two started to talk
    about it the election propaganda would turn into even more of a
    farce than it already is.

Well I expect to get a few comments on this posting, so I'll leave it
here with a small pointer towards [Tactical
Voter](http://www.tacticalvoter.net/).
