Title: Toshok's vCard Parser
Date: 2006-01-08 12:40
Tags: tech
Slug: toshoks-vcard-parser

Toshok: as you are obviously [proud of your vCard parser in
evolution-data-server](http://squeedlyspooch.com/blog/archives/002019.html),
how about going back into e-d-s for a minute and [reviewing a patch
which makes it twice as
fast](http://bugzilla.gnome.org/show_bug.cgi?id=312581). Thanks!
