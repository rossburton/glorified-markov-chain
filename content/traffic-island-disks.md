Title: Traffic Island Disks
Date: 2004-02-11 00:00
Tags: life
Slug: traffic-island-disks

[Traffic Island Disks](http://www.traffic-island.co.uk/index.php) is a
pretty cool idea.

> <cite> We roam the streets looking for people wearing headphones, stop
> them, and interview them while recording whatever they are listening
> to. The result is a half hour tour of an area of London, heard through
> people's personal tastes and rhythms. </cite>

I have a funny feeling I'm going to spend a lot of money on
[Shazam](http://www.shazam.com) finding out what some of the songs
are...

NP: [Moon Safari](http://www.amazon.co.uk/exec/obidos/ASIN/B0000262YS/),
by Air.
