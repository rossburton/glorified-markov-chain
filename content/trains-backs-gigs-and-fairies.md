Title: Trains, Backs, Gigs and Fairies
Date: 2003-09-09 00:00
Tags: life
Slug: trains-backs-gigs-and-fairies

Continuing the finest British tradition of complaining, I must moan
about the trains. Occasional signal problems I can deal with, but one
signal failure on the way to work Monday, another one on the way back,
and yet another Tuesday morning is just taking the piss, really. The
conspiracy theorist in me notices how all of this is occurring just as
the new signalling equipment has been installed and will be turned on
next month -- there is nothing like a train service which is much better
than usual to make the public think it was all worth it...

Off to see Heather Nova on Friday, the new album was out yesterday but I
didn't know in time to get to the shops. Hopefully I'll get it today so
I can at least bounce along to the songs. The venue looks stunning: it's
an old church in Islington called the [Union
Chapel](http://www.unionchapel.org.uk/).. Hopefully I'll be there in
time to meet [Edd](http://www.usefulinc.com) and have a pint first.

The back is much better now -- I'm still taking ibuprofen every morning
as I wake up with a very stiff neck, but that is far better than not
being able to move. At last that is over with.

Finally I saw another fairy on the tube today. I'm glad they are still
around, they bring an air of excitement and mystery to London. For 99.9%
of the people in the world who have no idea what I'm on about, [this is
the closest](http://www.dace.co.uk/artist.htm#tale) I can get to a link
about them.
