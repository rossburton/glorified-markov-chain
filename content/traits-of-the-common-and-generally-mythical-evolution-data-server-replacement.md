Title: Traits of the Common and Generally Mythical Evolution Data Server Replacement
Date: 2008-03-18 17:00
Tags: tech
Slug: traits-of-the-common-and-generally-mythical-evolution-data-server-replacement

When not writing media centres or GL toolkits, it appears that the
latest trend in open source is to write Evolution Data Server
replacements. There is a fairly common pattern forming.

First, implementation details will be announced as a major, if not the
main, feature. The shining example is "based on DBus". Yes, DBus is
great. Yes, ORBit is a dying technology for something as simple as
transfering a few strings between two processes. But this is *an
implementation detail*. I'd prefer a project using DBus instead of
another [incredibly complicated
IPC](http://en.wikipedia.org/wiki/Internet_Message_Access_Protocol), but
implementation details are typically not something to get excited about.

Often this first point gets out of control and suddenly the point of the
project is to design a DBus interface, not to write real working code.
Of course, an interface without any code behind it, without any
reference implementation, without several applications and different
users, is bound to be broken somewhere. But you'll never know until it
is too late and you've labelled the interface as STABLE. Learn from DBus
itself, anyone who followed the project before 1.0 knows that the core
concepts were rewritten several times before it was finally marked as
stable.

Spreading basic FUD is fairly common too. "EDS is not efficient
concerning network bandwidth" doesn't make sense, because EDS is a local
daemon. When it does talk over the network, it's fairly sensible. The
LDAP (and Groupwise/Exchange I believe) backend maps EDS searches to
native searches so that only the requested items are fetched. Backends
such as WebCal have no option but to fetch the entire file, because that
is how they work. "EDS is not efficient concerning memory usage" is
rather vague, and if you interpret it as "private dirty memory usage is
unreasonably high when in use" then in my opinion that is untrue and I
have Massif logs to back me up.

If these points were true, they'll generally be fixable within EDS. The
default local calendar backend is implemented as an iCalendar file on
disk, which is parsed into memory in its entirety on startup. This
certainly works well for a basic implementation but should be replaced
with a database of some sort, a simple one which stores a hash of UID to
event would reduce memory usage for large calendars. Add to that a cache
of start and end times to optimise that common case and the end result
is probably both faster and uses less memory, for a few days work.

Occasionally complaints are spot-on, but EDS isn't immutable and whilst
starting a new project from scatch may be more fun, please think of
everyone else. EVCard is over-complicated and yet tragically crippled,
whilst EContact tries to be clever but generally gets in the way.
Luckily we can write a new contact object which is easier to use. The
query language is limited, but Milan Crha of Red Hat fame has been
chipping away and now it's more flexible without breaking existing
applications. Maybe someone can come up with a good replacement
language, and the old language deprecated.

I'll summarise what I'm trying to say.

-   EDS isn't perfect, we all know that.
-   However, EDS also isn't immutable. It can be fixed.
-   If you find bugs or bad design in EDS, please file a bug report.
-   If you have spare time to start a replacement project, please
    briefly consider the possibility of working on EDS first. The code
    isn't that scary, honest (especially the DBus port when I get around
    to merging it).
-   If you still want to start a replacement project, at least be polite
    and inform the evolution-hackers mailing list that you are starting
    a project to replace it. You never know, there might be common
    ground that we can both work on.

<small>NP: <cite>Kharah System</cite>, Hereill</small>
