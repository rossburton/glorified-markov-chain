Title: TV Shorties
Date: 2005-11-03 12:46
Tags: life
Slug: tv-shorties

[<cite>Broken News</cite>](http://www.bbc.co.uk/comedy/brokennews/) is
absolutely fantastic, and I just discovered that there is a third series
of [<cite>Peep
Show</cite>](http://en.wikipedia.org/wiki/Peep_Show_(television))
"coming soon". Cashback!

**Update**: just found out that the new series will start on the 11^th^
November. Back of the net!
