Title: UEFI Validation
Date: 2014-05-19 10:07
Tags: tech, yocto
Slug: uefi-validation

The [Linux UEFI Validation Project](https://01.org/linux-uefi-validation) was announced recently:

> Bringing together multiple separate upstream test suites into a
> cohesive and easy-to-use product with a unified reporting framework,
> LUV validates UEFI firmware at critical levels of the Linux software
> stack and boot phases.
>
> LUV also provides tests in areas not previously available, such as the
> interaction between the bootloader, Linux kernel and firmware.
> Integrated into one Linux distribution, firmware can now be tested
> under conditions that are closer to real scenarios of operation. The
> result: fewer firmware issues disrupting the operating system.

Of course that "one Linux distribution" is built using the [Yocto
Project](https://www.yoctoproject.org/), so it's trivial to grab the
source and patch/extend it, or rebuild it for different processors if
for example you want to validate UEFI on
[ARM](https://wiki.linaro.org/ARM/UEFI) or a [new CPU that mainstream
distros don't fully support](http://en.wikipedia.org/wiki/Intel_Quark).
