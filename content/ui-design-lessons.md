Title: UI Design Lessons
Date: 2004-05-11 00:00
Tags: tech
Slug: ui-design-lessons

Mikael discovered [the user interface gem which is
MLDonkey](http://micke.hallendal.net/archives/000149.html), and suggests
that we learn a thing or two from it. I agree, and hunted around for
more prime examples of UI design lessons we should learn from.
Especially for Mikael, I found a Windows Jabber client called Exodus.
This is the connect dialog:

![Exodus Connect
dialog](http://www.burtonini.com/computing/screenshots/exodus-connect.png){}

A beginner will notice that the terminating button layout is clearer as
`OK | Cancel` rather than `Cancel | Connect` we'd normally use. However,
a closer look reveals finer details than we are used to. The Details
button opens a dialog to edit the current profile, and the blue
underlined label is not a web link but a pop-up menu to create new
accounts. Finally, successful integration of the web and the destktop is
here!

<small>NP: <cite>Liquid Swords</cite>, GZA</small>
