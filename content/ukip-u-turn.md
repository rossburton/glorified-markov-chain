Title: UKIP U-Turn
Date: 2006-05-09 09:20
Tags: life
Slug: ukip-u-turn

Amusing news yesterday, as it's revealed that Roger Knapman, the leader
of [UKIP](http://en.wikipedia.org/wiki/UKIP) (a single-issue party who
dislikes the EU, and basically wants to reduce, or remove, immigration),
hires Polish builders to work on his country mansion. The bonus
punchline: the builders are hired through a Polish-registered company
that specialises in hiring eastern Europeans to work in Britian, which
is run by Mr Knapman's son.

> Mr Knapman, an MEP and the UKIP's leader since 2002, recruited the
> workers through his son, whose company specialises in bringing foreign
> labour to Britain.
>
> The men have been at Mr Knapman's grade-II listed home in the village
> of Coryton, west Devon, for 11 months. They sleep dormitory-style in
> the attic.
>
> The company run by Mr Knapman's son claims that the imported labour
> costs up to 50 per cent less than British workers would. Mr Knapman,
> speaking to an undercover reporter from The Sunday Times, praised the
> men as "they work so much harder".

(from [The
Independent](http://news.independent.co.uk/uk/politics/article362721.ece))

<small>NP: [<cite>Boards of
Pays-Bas</cite>](http://www.jamendo.com/uk/album/560/), Poxfil</small>
