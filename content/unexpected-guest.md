Title: Unexpected Guest
Date: 2005-05-26 15:30
Tags: life
Slug: unexpected-guest

Whilst doing my best to shake off my nasty cold, an unexpected guest
flew past me in the garden.

[![](http://www.burtonini.com/photos/Misc/thumb-butterfly.jpg){
}](http://www.burtonini.com/photos/Misc/butterfly.jpg)

Apparently that is a Holly Blue, also known as <cite>Celastrina
argiolus</cite>. Sadly that is not a scaled image, but is just cropped.
I think I need a [macro
lens](http://www.canon.co.uk/for_home/product_finder/cameras/ef_lenses/macro_lenses/ef-s_60mm_f2.8_macro_usm/)...
