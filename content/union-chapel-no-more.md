Title: Union Chapel No More
Date: 2004-12-14 00:00
Tags: life
Slug: union-chapel-no-more

Sad news today: the [Union Chapel](http://www.unionchapel.org.uk), art
and music venue extraordinaire, is closing. The Church membership held a
vote which banned the consumption of alcohol in the main auditorium,
consequently wiping out the main income stream. There are plans to
recover it, but the initial appeal has already been refused. I only
managed to go to one gig at the Chapel, but it was amazing: the
acoustics were excellent and the atmosphere in the small chapel was
amazing. Finger's crossed...

<small>NP: <cite>[Zwischen zwei und einer
Sekunde](http://www.thinnerism.com/releases.php?r=thn038)</cite>,
krill.minima</small>
