Title: Update for Mikael
Date: 2003-08-05 00:00
Tags: life
Slug: update-for-mikael

This is an update for [Mikael](http://micke.hallendal.net/). Hi Mikael!

I've been feeling rather ill for the last few days -- I blame the eye
infection/medicine personally. The infection is nearly gone but its
still sore in the morning until I take the ointment, which makes the
soreness go... but brings with it a headache as it makes it hard to see
out of my left eye!

Damn its getting warm too. Yesterday it hit 31 degrees celcius, and
today its likely to reach 34. The weather men are speculating that
Wednesday could break the English record and go for 100 degrees
Farenheit. I know in the scheme of things that's not really hot, but in
England it is. Our high-speed train tracks are buckling in the heat...

In other news, I was busy fixing Sound Juicer bugs on the way to work
this morning, so should be able to do a new release this lunch time.
Hopefully this one won't totally suck...

Oh, and it turns out that my fiance's mum's old friend's daughter was
Anne Boleyn in [Kevin and Perry Go
Large](http://uk.imdb.com/Title?0205177). Well there you go.
