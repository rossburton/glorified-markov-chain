Title: Update
Date: 2003-01-23 00:00
Tags: tech
Slug: update

<div class="title">

Work

</div>

Dammit, back coding Java again, this time right at the bottom of the
system with the native methods. Yuck. I've totally forgotten our
language, popping and pushing operand stacks is dull and was really
looking forward to some serious C++/gtkmm hacking. Oh well.

<div class="title">

Python

</div>

Started playing with Python again, this time I wanted to write a small
system monitor using the notification tray. I don't really fancy working
out how to send X events in Python, so I investigated how to use the
PyGTK wrapping tools.

Many thanks to Johan Dahlin, who essentially guided me step by step
through wrapping a `GObject`. After a few small issues (don't expect
programs to parse empty files) I am nearly there -- I can
`import TrayIcon from trayicon` but I cannot construct it. Watch this
space.

So what do I expect to produce? Let us see:

-   A bug report about the code generator not moaning when it is given a
    .h file instead of a .defs file.
-   A small HOWTO on wrapping `GObject`s.
-   A system monitor program which will silently sit in the background,
    putting an icon in the notification tray if for example disk space
    runs low.

<div class="title">

gnome-games

</div>

Huzzah, `gnome-games` 2.2.0 is out. Hopefully GNOME 2.2, with its moody
splash screen, isn't too far away. Of course I'll be replacing it with
the disco balls of the ¡GARNOME! splash screen, as my login sound is now
a the first bar of <cite>Disco Nights</cite>.

For `gnome-games` 2.4.0 I plan on removing many games. The exact list
changes as random people should "I love that game" but I want to remove
the buggy crap games and concentrate on the rest. The games I'm removing
will be copied into a new module so other people can maintain them, but
don't expect me to work too hard on them...

<div class="title">

Debian

</div>

I finally finished my Debian Tasks & Skills and Philosophy & Procedures
tests, and have passed. When Matt Hope gets back from the conference in
Perth, he can start the final ball rolling. Maybe later this year I'll
be `ross@debian.org`...
