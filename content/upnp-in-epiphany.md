Title: UPnP in Epiphany
Date: 2008-06-12 22:10
Tags: tech
Slug: upnp-in-epiphany

One of the more useful features of the UPnP specification is that
devices have a standard way of specifying a "presentation URL", a
human-readable web page representing the device. For example, my
SoundBridge has a web page which shows the currently playing music and
lets me switch radio station, whilst my router's presentation URL is the
administration page.

Useful, but not exposed anywhere. Until now...

![GUPnP in
Epiphany](http://burtonini.com/computing/screenshots/ephy-upnp.png){}

This is a small Epiphany extension which adds all presentation URLs it
finds to the <cite>Nearby Sites</cite> menu, just like the URLs
discovered using Avahi. It needs a bit more work as it doesn't yet
handle being unloaded or devices disappearing, but it is certainly
usable now.

If anyone else wants to have a go with it, the source can be fetched
using Bazaar from [here](http://burtonini.com/bzr/ephy-gupnp/). Watch
out for the currently hard-coded paths...
