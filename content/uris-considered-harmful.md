Title: URIs Considered Harmful...
Date: 2003-11-03 00:00
Tags: tech
Slug: uris-considered-harmful

Many people, specifically the Nautilus maintainers, but including me,
consider the use of
<abbr title="Uniform Resource Identifiers">URIs</abbr> in GNOME
something which should be hidden from the user.

For example, to access the CD burning tools in Nautilus, you can
navigate to `burn:///`. This is ugly and has zero discoverability, i.e.
the user has to be told that it exists. The same for `network:///`,
`fonts:///` and `applications:///`. These are all magic URIs, once the
user knows they exist, they should be able to remember them, but the
point is that shouldn't have to. A good example of this is the
integration of magicdev with nautilus-cd-burner, instead of having to go
to `burn:///` (or selecting "CD Creator" in GNOME 2.4) to open the
correct location, magicdev will open it automatically when a blank CD is
inserted on the assumption that you are likey to want to burn something
to it. Joe User can just use it, and Lee T. Hacker remembers the URI.

Thats all well and good, and often-covered material. However, URIs have
one thing going for them: at least they are not
<abbr title="Universal Unique Identifiers">UUIDs</abbr>. I found this
gem in the Windows 2000 Resource Kit:

> To open a new Explorer window which displays My Documents, use this as
> the command line:
>
>     %SystemRoot%\explorer.exe /e,::{450D8FBA-AD25-11D0-98A8-0800361B1103}

Check out that snappy UUID...
