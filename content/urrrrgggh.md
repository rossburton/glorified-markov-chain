Title: Urrrrgggh
Date: 2003-10-29 21:47
Tags: life
Slug: urrrrgggh

Why am I so tired? Last night I went to bed around 10:30, and this
morning I was struggling to keep my eyes open. I've been like this since
last Thursday or so, and I hope it stops. Maybe I'm getting a cold, just
*really* slowly.

More linkage: [Why Comic Sans](http://www.connare.com/comic.htm),
[politicians being grown-up as
usual](http://www.guardian.co.uk/uklatest/story/0,1271,-3323474,00.html),
[don't remind anyone this
happened](http://thomas.apestaart.org/gallery/view_photo.php?set_albumName=GU4DEC&id=ack&PHPSESSID=24e8d150ab3e7a15a5f3c252f0ea7f96),
[no
comment](http://thomas.apestaart.org/gallery/view_photo.php?set_albumName=GU4DEC&id=aed&PHPSESSID=24e8d150ab3e7a15a5f3c252f0ea7f96),
and after the [Face Of Sun](http://www.gnome.org/~jdub/guadec3/108.jpg),
we finally have the [Face of
Imendio](http://thomas.apestaart.org/gallery/view_photo.php?set_albumName=GU4DEC&id=aef&PHPSESSID=24e8d150ab3e7a15a5f3c252f0ea7f96)
(well, the face of half of Imendio)
