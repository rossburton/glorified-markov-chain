Title: Useless Java Tools?
Date: 2004-03-10 00:00
Tags: tech
Slug: useless-java-tools

At [work](http://www.180sw.com/) I've spent a couple of days this week
writing a tool to compare the public interface of two independent code
bases. Yes, they should both inherit from a common set of interfaces in
the common case, but this is not possible here -- we have two separate
codebases which need to look identical to objects which subclass the
relevant objects, and *cannot* share any code.

It's quite cool if I do say so myself -- you can mark members as being
specific to a particular codebase (so are ignored), and I've even got a
sweet pygtk UI with icons, treeviews and suchlike. I'm thinking about
GPLing it (my boss is OSS-friendly, we've contributed to several
projects so far), but will anyone else out there use this tool? I'm not
entirely sure.

In more GNOMEy news, the Fun Bug Of The Day is the inability to open dot
files with the new GTK+ 2.4 file selector. Obviously I consider this
rather urgent for 2.4.0... Apart from that the interface is pretty sweet
-- great work `jrb` and `federico`.

NP: Dub Side Of The Moon, Easy Star All Stars. This is a *great* album.
