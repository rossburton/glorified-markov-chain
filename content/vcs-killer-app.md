Title: VCS Killer App
Date: 2006-01-09 18:30
Tags: tech
Slug: vcs-killer-app

I've been wanting something like [bzr
shelve](http://riva.ucam.org/~cjwatson/blog/2006-01-09-bzr-shelve.html)
for CVS and Subversion for a long time. Now that it exists for `bzr` I
am very tempted to join Colin in trying to make a generic version...
