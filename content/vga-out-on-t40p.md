Title: VGA Out on T40p
Date: 2006-06-21 15:00
Tags: tech
Slug: vga-out-on-t40p

Dear Lazyway...

Next week I'm giving a presentation at GUADEC and would like to avoid
embarrassing last-minute configuring of X by setting up VGA out on my
laptop before I leave. This is harder than it sounds as I don't actually
own any monitors any more.

If anyone out there has the VGA port working on a Thinkpad T40p (or
similar) in clone mode, with GL acceleration, hopefully with automatic
down-scaling to 1024x768, then can you please [mail
me](mailto:ross@burtonini.com) your `xorg.conf`. I have S-video out
sort-of working but it's not downscaling the image and I've lost GL
acceleration, which is a great pain.

Note: any comments about how I'm evil for using a non-free driver will
be ignored.

<small>NP: <cite>Ágætis Byrjun</cite>, Sigur Rós</small>
