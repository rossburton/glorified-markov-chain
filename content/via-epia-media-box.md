Title: Via EPIA Media Box?
Date: 2006-12-03 17:00
Tags: tech
Slug: via-epia-media-box

I'm considering building myself a Via EPIA-based media/NAS box in the
new year, as a replacement for the LinkStation NAS I have upstairs (my
latest Unison run took 24 hours due to the limited RAM) and the ThinkPad
X22 I have under the hifi (as it only plays music, and doesn't do
video).

I notice that all of the EPIAs have hardware MPEG acceleration, though
I'm not sure what models have what features. I plan eventually on using
the box to play DVDs and random MPEG streams from (say) BitTorrent, and
play and record DVB-T streams. Can anyone say if the Via EPIA MII 6000
is capable of this, or should I go for something a little more powerful
like the SP8000EG? The former has an "MPEG Accelerator" whereas the
latter explictly states accelerated MPEG decoding and encoding. Please
email me or leave comments, thanks!
