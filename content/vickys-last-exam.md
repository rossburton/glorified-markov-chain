Title: Vicky's Last Exam
Date: 2003-05-28 00:00
Tags: life
Slug: vickys-last-exam

Last Friday (the 23^rd^) was Vicky's last exam -- what a day.

[WAGN](http://www.wagn.co.uk) was doing engineering works that day, they
advised people not go travel to London as there would only be four very
slow trains an hour. I decided not to go in and work from home but
obviously V had to go in, so we picked up a copy of the temporary
timetable and made plans. She was up at 06:40, and down the station at
07:30. This is allowing plenty of time to get to London for her 10:00
exam even if the train was delayed. I got up, had a bath, and started to
work...

At 10:10 the phone rung -- I was expecting a call from my boss this
morning so I picked it up.  

> Ross: Hello?  
> </br> Vicky: Hi  
> </br> Ross: Oh, hi darling.. Hang on! What are you doing phoning me!
> </p>

Obviously I was a little confused as to why Vicky was phoning me from
inside her exam. It turns out that the train made good speed for the
first few miles, quite empty as most people didn't go in, and about a
minute past Harlow Mill stopped. The driver came on after a few minutes,
telling everyone that they would be there for a while, there is a queue
of trains up to the next station and a suspected broken track. His
estimate of how long they would be waiting went up and up until 2 hours
later the train pulled into Harlow Town station, normally an 8 minute
journey from home. Vicky jumped off, called me, and tried to make
alternative plans.

My mission was to tell the university -- I managed to get hold of the
examinations department but they wouldn't say any more than "tell her to
come to room 140". Vicky didn't take her mobile phone with her so I had
to wait for her to call back from a pay phone to pass the message on.
Luckily she was thinking calmly and got a letter from the station
stating what had happened. However she was two hours away from college
on a good day, and the exam had started. The race was on to get to
college as soon as possible!

Cue a replacement bus to Epping, Central line tube to Stratford, Jubilee
line to Canada Water then East London line to New Cross, followed by a
run along the road. She made it in for 12:05 and the procedures all
kicked in -- write down what happened, we'll try and get you a seat in
the next exam sitting, stay here until 14:30.

The exam actually went well.

In the meantime I spent most of the day worrying where she was, telling
myself that no news is good news -- she is too busy to call as she
storms across London. I got a call at 16:40, she had just finished her
exam, and was going to come back home. I relaxed. Another call at 18:40,
she was at Tottenham Hale and waiting for the last journey home. That is
a 30 minute journey usually, add 10 minutes for the train to turn up etc
-- I said I'd order a pizza to arrive about 15 minutes after she gets
back.

What a optimistic fool I am.

At 20:30 Vicky finally makes it home after the train was stuck again.
This time the driver even told everyone to get out, stretch their legs,
and he'll tell them when the train is about to go again! We had arranged
to go to the local pub at 20:30, so V ate the pizza (which had been
sitting in the oven) and quickly changed while everyone arrived, then we
went down the Castle and relaxed for the first time all day. The evening
was good fun, saw my friends who I haven't seen for a while, had a few
drinks and managed to borrow the boxed set of the Star Wars Trilogy.
