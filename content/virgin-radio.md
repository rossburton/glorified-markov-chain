Title: Virgin Radio
Date: 2006-02-01 11:40
Tags: tech
Slug: virgin-radio

Reminded by Christian's blog about the [positive experience with Virgin
Radio's technical
people](http://blogs.gnome.org/view/uraeus/2006/02/01/0), I popped along
to their streaming site to see if they had read the email I sent them
last week. Indeed they have, as they now [list Totem as a recommended
player](http://www.virginradio.co.uk/thestation/listen/streams.html) on
Linux.

Yay Virgin Radio! Now if they stopped playing the same damn songs over
and over...

<small>NP: <cite>Music For The Mature B-Boy</cite>, DJ Format</small>
