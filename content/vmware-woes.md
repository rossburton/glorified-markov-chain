Title: VMWare Woes
Date: 2005-12-05 16:05
Tags: tech
Slug: vmware-woes

I'm currently trialing VMWare 5.5 on my laptop, to run Windows XP so
that Vicky can use iTunes to manager her iPod (no, <cite>gtkpod</cite>
isn't an option, and neither is <cite>SharpMusique</cite>). However,
it's not working. `:(`

I've installed iTunes, but when I plug in the iPod XP detects first an
iPod and then a USB Mass Storage device. It appears that the Mass
Storage device is overriding the iPod drivers so iTunes doesn't notice
the iPod. And so I ask the Lazyweb: has anyone else seen this, and is
there a way around it?

**Update:** looks like the problem is caused by VMWare exposing my USB 2
controller as a USB 1.1 controller, which iTunes refuses to support as
it would be too slow. Dammit! Any known workarounds?

**Update 2:** there are suggestions that this may be fixed by using an
older version of iTunes, so does anyone have a Windows iTunes 5
installer to hand? Apple are only providing the latest release on their
site.

<small>NP: <cite>Laika Come Home</cite>, Space Monkeyz vs.
Gorillaz</small>
