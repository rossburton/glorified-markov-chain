Title: Volunteers Needed!
Date: 2012-01-18 17:45
Tags: tech
Slug: volunteers-needed

Those lovely people over at Flickr have finally bitten the bullet and
are [turning off
FlickrAuth](http://code.flickr.com/blog/2012/01/13/farewell-flickrauth/)
in the summer, meaning that all applications that use the Flickr API
need to use OAuth.

This is something I totally agree with, whilst FlickrAuth works and was
clearly an important influence on OAuth, it's a single-service protocol
when OAuth has managed to get massive adoption and a huge developer
base.

The problem I've got is `libsocialweb`, which has a Flickr module that
allows both fetching of your contact's recent photos and uploading
images. This uses FlickrAuth so at the end of July will suddenly stop
working. I've got enough on my plate at the moment and would love for
more people to understand how the entire social spagetti works, so this
is a call for a volunteer to work on this, for which I'll obviously be
available to offer any guidance and mentoring required.

There are two ways of approaching this, the easy way and the slightly
harder way.

The easy way is to update `libsocialweb` to use an `OAuthProxy` instead
of a `FlickrProxy`, and update the module metadata so that Bisho uses
the generic OAuth flow instead of a Flickr-specific flow. This should be
fairly simple and needs to happen soon so that any distributions that
are using `libsocialweb` don't break in the summer.

The harder way is to add Flickr support to `gnome-online-accounts`,
using the Twitter service as an example, and then port the Flickr
service in libsocialweb to use `gnome-online-accounts` to authenticate.
I've a proof of concept for the librest-goa integration which will be a
useful starting point. This is more of a proof of concept for
`libsocialweb`, we've been looking at moving away from Bisho but haven't
done anything substantial yet.

Ideally both of these happen, so the current code will continue to work
in the future and the GOA work demonstrates how GOA and `libsocialweb`
would work together. So, anyone interested?
