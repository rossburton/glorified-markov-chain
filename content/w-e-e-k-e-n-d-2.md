Title: W-E-E-K-E-N-D
Date: 2005-06-26 17:40
Tags: life
Slug: w-e-e-k-e-n-d-2

W-E-E-K-E-N-D, it's weeeekend.

On Saturday we went down to London to go and see the most excellent
[<cite>Earth From The Air</cite>](http://www.earthfromtheair.com/) (also
know as <cite>Earth From Above</cite>) on the South Bank, next to the
half-gerkin GLA building. The photography is amazing, and I recommend
that everyone go.

[![](http://www.burtonini.com/photos/Random/thumb-img_2513.jpg){
}](http://www.burtonini.com/photos/Random/img_2513.jpg)
[![](http://www.burtonini.com/photos/Random/thumb-img_2514.jpg){
}](http://www.burtonini.com/photos/Random/img_2514.jpg)

As you can see, the weather was pretty rough. I'd have liked to taken
more photographs, but we were starting to get frostbite...

Then on to Oxford Street, to check out the HMV sale. Bad move. I found
some bargains, and got <cite>Trouser Jazz</cite> (Mr Scruff),
<cite>Between Darkness And Wonder</cite> (Lamb), <cite>Pre-Millennium
Tension</cite> (Tricky) and <cite>Laika Come Home</cite> (Space Monkeyz
& Gorillaz) for £20, so rewarded myself with <cite>Bubba Ho-Tep</cite>.
Then we stopped off at a local pub for a pint or two (for future
reference it's <cite>The Green Man</cite> on Berwick Street), before
heading out to find [<cite>The Red
Onion</cite>](http://www.red-onion-restaurant.co.uk/). The food there is
excellent (thanks to Russel for mentioning it in the past), and
reasonably priced: two starters and mains with a bottle of wine was £50.
My only complaint is that the service was a little slow, but we did
arrive the same time as a party of 20.

Sunday is laze day: a bit of cleaning and listening to the new music. As
a second reward to myself for being relatively good in HMV I bought
another three albums in the morning: <cite>Out From Out Where</cite>
(Amon Tobin), <cite>Sounds From The Verve Hi-fi</cite> (Thievery
Corporation), and <cite>Birth Of The Cool</cite> (Miles Davis). I best
stay away from Amazon for a while now...

<small>NP: <cite>One Offs... Remixes and B-Sides</cite>, Bonobo</small>
