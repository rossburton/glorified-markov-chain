Title: W-e-e-k-e-n-d
Date: 2004-03-09 00:00
Tags: life
Slug: w-e-e-k-e-n-d

Weeeeekend. Well, it was. I'm starting to really suck at updating this.

Saturday was good fun, we went down to see John's new flat in Queens
Park, London. Very nice flat, lots of polished wood, a Smeg cooker and a
resident cat who refuses to acknowledge the land lord has moved. We ate
a huge amount of fresh bread and cheese, with huge amounts of red wine,
followed by a huge bowl of veggie chilli and rice. Turned out to be very
full, but I think "satisfied" is a suitable word.

Some interesting news for the day: MacDonald's new salads contain [more
fat than a hamburger and
chips](http://www.timesonline.co.uk/article/0,,2-1031425,00.html). I'll
have to pop into a MacDonalds one day (that's a scary thought) and see
how they are being marketed. After the recent claims and actions, I
doubt they are actually saying "healthy" but letting the word "salad"
imply it.

In a semi-related note, I was pleased to see Co-op pushing
[FairTrade](http://www.fairtrade.org.uk/) coffee at the London train
stations recently. However, they don't seem to have the pull for the
commuters that the recent X-Box stand had... which might have had
something to do with the fact that the girls were in CFM heels and
skirts so short it was, well, shocking.

Finally, enraged to see that the stupid EU Intellectual Property Rights
Enforcement Directive is still stupid and going after the little man.
It's a little late, but London's <cite>Metro</cite> (a free semi-trashy
tabloid) has a positive (from my point of view) article on the front
page:

> <cite> It was originally intended to target big-time music and film
> pirates and organised criminals but was rewritten to target
> individuals. </cite>
>
> ...
>
> <cite> Critics say the legislation was hijacked by the record
> industry. It was put forward by French Euro MP Janelly Fourtou, wife
> of the head of Vivendi Universal -- which owns several record labels.
> </cite>
