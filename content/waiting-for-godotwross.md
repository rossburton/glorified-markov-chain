Title: Waiting For Godot^WRoss
Date: 2005-06-13 11:01
Tags: tech
Slug: waiting-for-godotwross

I just joined `#gnome-debian` to find the following topic:

` TODO: gnome-cups-manager sj zenity | The Independent: "Rabid Debian users on a search and destroy mission against Ross Burton. 'Yes,' one of them said, 'he doesn't give us our Zen.'" `

Hmm, yesterday the To Do list was several lines long. Now it's just
three packages, and they are all mine. Damn. Looks like it's time to
update my Sid machine...

<small>NP: <cite>X & Y</cite>, Coldplay</small>
