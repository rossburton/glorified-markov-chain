Title: Wanted
Date: 2005-11-27 14:20
Tags: tech
Slug: wanted-2

Dear Lazyweb. I want a new keyboard.

I'm after a new keyboard: it needs to have very good action (there is a
reason my current keyboard is about 7 years old), be black or grey (to
match my new Logitech Cordless Click! Plus mouse), and ideally have an
integrated USB hub (into which I can plug the dongle for the mouse) or
be wireless (and share the dongle that came with my mouse). It must not
be a "natural" keyboard, although the Microsoft Comfort range is
interesting. The only keyboard I've found so far which has all of these
is an IBM USB keyboard, which costs £60...

Any ideas? [Mail me](mailto:ross@burtonini.com)!

**Update:** so I'm probably going to order the Apple USB keyboard (even
though it's white). Does anyone know how the keyboard layout differs
from the "standard" PC British layout?

**Update:** Apple USB keyboard ordered. Thanks for the suggestions!
