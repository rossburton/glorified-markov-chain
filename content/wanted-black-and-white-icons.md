Title: Wanted: Black and White Icons
Date: 2007-08-06 10:30
Tags: tech
Slug: wanted-black-and-white-icons

As people who have seen my laptop know, I use the wonder Darkilouche
theme by Jakub. To complement it I'm after some decent black and white
(ideally `#F5F5F5` as the primary colour) icons for Network Manager,
seahorse-agent and so on. I just tried redrawing the NM status icons in
Inkscape, and basically it's obvious that I should *never* attempt to
draw an icon *ever again*. I thought that the High Contrast icon theme
would have what I want, but it doens't cover Network Manager yet. Does
anyone know any suitable icons?

<small>NP: <cite>Backup 01</cite>, TRS-80</small>
