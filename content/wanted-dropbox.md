Title: Wanted: Dropbox
Date: 2006-05-08 14:40
Tags: tech
Slug: wanted-dropbox

**Wanted:** a stand-alone, secure, simple file drop-box, that I can run
on my external-facing machine and allow people to send me files. Ideally
it would have a HTML forms based interface if someone does a `GET /`,
but allow direct WebDAV connections to `PUT` files. Obviously deleting
and so on should not be allowed, and all uploads constrained to a
specific directory.

Any suggestions?

<small>NP: <cite>Takk</cite>, Sigur Rós</small>
