Title: Wanted: GStreamer Knowledge
Date: 2006-06-14 17:40
Tags: tech
Slug: wanted-gstreamer-knowledge

This afternoon I discovered that the FAAC and FAAD encoders have been
ported to GStreamer 0.10, so I hastily installed
`gstreamer0.10-plugins-bad-multiverse` and started writing a profile so
that I could encode straight to AAC files from Sound Juicer, for my
iPod. This is where the world would rejoice, if it were so simple.

My first worry was that the `faac` element doesn't implement the tagging
interface, so I may have AAC data but there is no track information. I
then noticed that `file` wasn't identifying the files as AAC, but as
just data. Starting to panic, I dived into `#gstreamer` and discovered
some bad news. By setting an option on the encoder (`outputformat=1`) I
get AAC data that is recognised, and Xine will play it, but it's not
wrapped in a Quicktime container so can't be tagged and probably won't
play on an iPod.

This is my challenge. If someone can write enough of a Qt muxer that
supports tagging so that straight from Sound Juicer I can create `.m4a`
files that play on my iPod without any hassle, I'll give you a free
Sound Juicer t-shirt and my eternal gratitude. Anyone interested?

<small>NP: <cite>Remembranza</cite>, Murcof. Yes, again, it's
great.</small>
