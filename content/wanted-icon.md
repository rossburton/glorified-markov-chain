Title: Wanted: Icon
Date: 2008-06-18 09:40
Tags: tech
Slug: wanted-icon

I'm hacking on a small tool at the moment and need an icon for the
launcher. A simple icon of a cow's head would be perfect: anyone know of
something like this, or willing to quickly draw one for me?
