Title: Wanted: Non-European Tasks Users
Date: 2007-05-25 20:00
Tags: tech
Slug: wanted-non-european-tasks-users

I've added a hot new feature to
[Tasks](http://pimlico-project.org/tasks.html), so that you can specify
a priority and group when creating a task instead of having to create
and then edit it. It works like this:

<dl>
<dd>
Some Task

</dd>
<dt>
Create a task with the summary <cite>Some Task</cite>

</dt>
<dd>
+ Some Task

</dd>
<dd>
! Some Task

</dd>
<dt>
Create a high priority task with the summary <cite>Some Task</cite>

</dt>
<dd>
- Some Task

</dd>
<dt>
Create a low priority task with the summary <cite>Some Task</cite>

</dt>
<dd>
! @Work Some Task

</dd>
<dt>
Create a high priority task in the <cite>Work</cite> category with the
summary <cite>Some Task</cite>

</dt>
</dl>
Now, I've tried to be i18n-aware, and use GLib's UTF-8 functions to
manipulate the string, but I'd like someone to check this. Can a
non-English speaker test this out with some interesting locales,
specifically with UTF-8 characters which contain ASCII whitespace in
their byte representation such as 0x20. Thanks!

**Update:** thanks to Simon for pointing out that my paranoia is
unfounded, UTF-8 was designed to stop this sort of problem. However,
people checking this code works would still be useful!

<small>NP: <cite>Music Is Rotted One Note</cite>, Squarepusher</small>
