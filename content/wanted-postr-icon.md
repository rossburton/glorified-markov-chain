Title: Wanted: Postr Icon
Date: 2007-04-04 10:15
Tags: tech
Slug: wanted-postr-icon

Wanted: icon for [Postr](http://burtonini.com/blog/computers/postr). I'm
not sure what sort of design I'd like for a Flickr uploader, maybe a
photo frame in front of the "web browser" globe icon would do the job.
Thanks to anyone who does a rocking icon!

<small>NP: <cite>Kaleidoscope</cite>, DJ Food</small>
