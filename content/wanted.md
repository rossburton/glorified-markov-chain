Title: WANTED
Date: 2007-03-07 10:30
Tags: tech
Slug: wanted

This may sound a little strange but does anyone have
`linux-image-2.6.20-6-generic` and the matching restricted modules
packages for Ubuntu Feisty? They've fallen out of the Ubuntu archive,
the morgue doesn't exist any more, and newer kernels don't work with
Scratchbox. Thanks!

<small>NP: <cite>Oneiric</cite>, Boxcutter</small>
