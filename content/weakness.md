Title: Weakness
Date: 2006-01-16 22:10
Tags: life
Slug: weakness

On the whole I'm an organic, fresh, free-range, no-MSG, tree-hugging
leftie kind of guy. I prefer Marks and Spencer's Red Leicester and
Spring Onion crisps as they contain potato, oil, cheese and onion. When
I buy chicken it's free-range. Eggs are **always** free-range, and the
fact that the supermarkets still sell "value" eggs upsets me. This
weekend in a "top 10" conversation, I discovered that I have several
weaknesses:

-   Nik-Naks (Rib 'n' Saucy)
-   Super Noodles (Mild Curry)
-   Jelly Babies

More MSG, artificial flavourings and additives than you can shake a
stick at. Please say I'm not alone... or am I just weak-willed?

In other news, Henry is three months old today. He is all jabbed up now
so can finally leave the house, walks are fun (for certain definitions
of fun that is) as he continually pulls on the lead and is obsessed with
cars driving past. So far we're averaging two random people talking to
us about Henry on every walk...

[![Henry](http://www.burtonini.com/photos/Henry/thumb-img_4108.jpg){
}](http://www.burtonini.com/photos/Henry/img_4108.jpg)
[![Henry](http://www.burtonini.com/photos/Henry/thumb-img_4115.jpg){
}](http://www.burtonini.com/photos/Henry/img_4115.jpg)
[![Henry](http://www.burtonini.com/photos/Henry/thumb-img_4117.jpg){
}](http://www.burtonini.com/photos/Henry/img_4117.jpg)
