Title: Web Developing Under IE6
Date: 2003-12-24 00:00
Tags: tech
Slug: web-developing-under-ie6

...is a Right Royal Pain In The Arse.

The pain started when I needed to be in Windows and thought I'd check
how my web site looked in IE6. Bad, is the answer. Firstly, IE6 doesn't
know what a "thin" border looks like and renders it several pixels
thick. That isn't too bad, but then IE6 also decides to ignore the
`margin-right:` statement and lets the content spread across the entire
page, over the side bar. Finally I made the mistake of using a PNG with
an alpha channel, which of course IE renders with solid 50% grey
background. There is a [work-around for this
bug](http://support.microsoft.com/default.aspx?scid=kb;en-us;Q294714),
but its incredibly ugly. Developing for Internet Explorer causes the red
mist to appear...

Thomas Vander Stichele: web shopping in .es sounds painful. I was
surprised by this as in .uk we have many online stores, and its pretty
rare to find a site which does not work correctly. The normal problem is
JavaScript assuming IE and some parts of the site not working correctly.
Much of our Christmas shopping was done at Amazon.co.uk, which was
delivered quickly as usual, even if in amazingly over-sized boxes (a
trait [Dabs](http://www.dabs.com) appears to have pioneered with 2' × 1'
× 0.5' boxes for a stick of SDRAM)
