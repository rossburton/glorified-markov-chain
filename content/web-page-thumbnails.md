Title: Web Page Thumbnails
Date: 2004-06-14 00:00
Tags: tech
Slug: web-page-thumbnails

Matt Biddulph wrote a nice little hack to [create a thumbnail of any
URL](http://www.hackdiary.com/archives/000055.html) using Mozilla and
Python. In awe of the coolness this could give, I grabbed it and had a
quick play. Matt and myself both spotted the deliberate error he
introduced in the script he uploaded, then I replaced the usage of PIL
with GDK (thus doing the rescale in memory without going to disk), and
fiddle the size of the thumbnail. Et volia:

![Thumbnail of
burtonini.com](http://www.burtonini.com/computing/screenshots/burtonini-thumbnail.png)

There are still issues: sadly Mozilla won't render the page to a window
without the window being visible on screen. Maybe a lower-level API
would allow this, but for now it does the the job rather well. Well done
Matt. My hacked copy is [available
here](http://www.burtonini.com/computing/screenshot-tng.py).

<small>NP: <cite>Liquid Swords</cite>, GZA.</small>
