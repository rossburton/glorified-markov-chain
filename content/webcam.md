Title: Webcam
Date: 2006-01-13 17:45
Tags: tech
Slug: webcam

This week I gave in and bought a cheap webcam (a Logitech QuickCam for
Notebooks Deluxe or something), primarily for the upcoming OpenedHand
VOIP service (**update:** this is not a public service, just SIP
addresses for OH employees!), but also to record what
[Henry](http://www.burtonini.com/photos/Henry) is up to when we leave
(we want to know how long he cries for when we go out). Happily it
worked straight out of the box with Ubuntu Dapper (using the `spca5xx`
module):

![Me!](http://www.burtonini.com/photos/Misc/webcam-capture.jpg)

It works well in GnomeMeeting too, so all I need now is a good simple
tool to record from the webcam to disk, ideally in Ogg Theora. Any
suggestions? A GStreamer pipeline would probably do the job for now, as
I can wrap that up in a nice Python UI if I need to.

<small>NP: <cite>Storm</cite>, Heather Nova</small>
