Title: Wedding Photography
Date: 2004-06-06 00:00
Tags: life
Slug: wedding-photography

(yes, people, my laptop disk is back and I'm blogging again. More later)

Yesterday Russel (great bloke, my boss and our wedding photographer)
came up to look at the venue for the wedding ceremony and reception to
see how the light falls, where to take what shots, etc. Whilst doing
this obviously he took many photos and when we got home I kept a copy
when we were looking at them.

These are taken in the Rose Garden behind the registry office. It's a
small circle of bushes with a rose arch and little stone seats. All very
nice and romantic.

[![photo](http://www.burtonini.com/photos/WeddingPhotoPlanning/thumb-img_0152.jpg.png)](http://www.burtonini.com/photos/WeddingPhotoPlanning/img_0152.jpg)
[![photo](http://www.burtonini.com/photos/WeddingPhotoPlanning/thumb-img_0154.jpg.png)](http://www.burtonini.com/photos/WeddingPhotoPlanning/img_0154.jpg)
[![photo](http://www.burtonini.com/photos/WeddingPhotoPlanning/thumb-img_0155.jpg.png)](http://www.burtonini.com/photos/WeddingPhotoPlanning/img_0155.jpg)
[![photo](http://www.burtonini.com/photos/WeddingPhotoPlanning/thumb-img_0156.jpg.png)](http://www.burtonini.com/photos/WeddingPhotoPlanning/img_0156.jpg)
[![photo](http://www.burtonini.com/photos/WeddingPhotoPlanning/thumb-img_0157.jpg.png)](http://www.burtonini.com/photos/WeddingPhotoPlanning/img_0157.jpg)
[![photo](http://www.burtonini.com/photos/WeddingPhotoPlanning/thumb-img_0158.jpg.png)](http://www.burtonini.com/photos/WeddingPhotoPlanning/img_0158.jpg)
[![photo](http://www.burtonini.com/photos/WeddingPhotoPlanning/thumb-img_0159.jpg.png)](http://www.burtonini.com/photos/WeddingPhotoPlanning/img_0159.jpg)
[![photo](http://www.burtonini.com/photos/WeddingPhotoPlanning/thumb-img_0160.jpg.png)](http://www.burtonini.com/photos/WeddingPhotoPlanning/img_0160.jpg)
[![photo](http://www.burtonini.com/photos/WeddingPhotoPlanning/thumb-img_0161.jpg.png)](http://www.burtonini.com/photos/WeddingPhotoPlanning/img_0161.jpg)
[![photo](http://www.burtonini.com/photos/WeddingPhotoPlanning/thumb-img_0163.jpg.png)](http://www.burtonini.com/photos/WeddingPhotoPlanning/img_0163.jpg)
[![photo](http://www.burtonini.com/photos/WeddingPhotoPlanning/thumb-img_0164.jpg.png)](http://www.burtonini.com/photos/WeddingPhotoPlanning/img_0164.jpg)
[![photo](http://www.burtonini.com/photos/WeddingPhotoPlanning/thumb-img_0170.jpg.png)](http://www.burtonini.com/photos/WeddingPhotoPlanning/img_0170.jpg)

Me looking remarkable happy at finding a tree.  
[![photo](http://www.burtonini.com/photos/WeddingPhotoPlanning/thumb-img_0166.jpg.png)](http://www.burtonini.com/photos/WeddingPhotoPlanning/img_0166.jpg)

"Hey Russel, I've magicked a door from Ross' pocket!"  
[![photo](http://www.burtonini.com/photos/WeddingPhotoPlanning/thumb-img_0168.jpg.png)](http://www.burtonini.com/photos/WeddingPhotoPlanning/img_0168.jpg)

After Vicky's magic trick we went to the Cloisters at the registry
office, where every photographer has a field day playing with their
flash guns to get good lighting on the arches.

[![photo](http://www.burtonini.com/photos/WeddingPhotoPlanning/thumb-img_0172.jpg.png)](http://www.burtonini.com/photos/WeddingPhotoPlanning/img_0172.jpg)
[![photo](http://www.burtonini.com/photos/WeddingPhotoPlanning/thumb-img_0173.jpg.png)](http://www.burtonini.com/photos/WeddingPhotoPlanning/img_0173.jpg)
[![photo](http://www.burtonini.com/photos/WeddingPhotoPlanning/thumb-img_0176.jpg.png)](http://www.burtonini.com/photos/WeddingPhotoPlanning/img_0176.jpg)
[![photo](http://www.burtonini.com/photos/WeddingPhotoPlanning/thumb-img_0177.jpg.png)](http://www.burtonini.com/photos/WeddingPhotoPlanning/img_0177.jpg)
[![photo](http://www.burtonini.com/photos/WeddingPhotoPlanning/thumb-img_0178.jpg.png)](http://www.burtonini.com/photos/WeddingPhotoPlanning/img_0178.jpg)
[![photo](http://www.burtonini.com/photos/WeddingPhotoPlanning/thumb-img_0179.jpg.png)](http://www.burtonini.com/photos/WeddingPhotoPlanning/img_0179.jpg)
[![photo](http://www.burtonini.com/photos/WeddingPhotoPlanning/thumb-img_0180.jpg.png)](http://www.burtonini.com/photos/WeddingPhotoPlanning/img_0180.jpg)
[![photo](http://www.burtonini.com/photos/WeddingPhotoPlanning/thumb-img_0186.jpg.png)](http://www.burtonini.com/photos/WeddingPhotoPlanning/img_0186.jpg)
[![photo](http://www.burtonini.com/photos/WeddingPhotoPlanning/thumb-img_0189.jpg.png)](http://www.burtonini.com/photos/WeddingPhotoPlanning/img_0189.jpg)
[![photo](http://www.burtonini.com/photos/WeddingPhotoPlanning/thumb-img_0192.jpg.png)](http://www.burtonini.com/photos/WeddingPhotoPlanning/img_0192.jpg)
(we were weeding, okay?)
[![photo](http://www.burtonini.com/photos/WeddingPhotoPlanning/thumb-img_0194.jpg.png)](http://www.burtonini.com/photos/WeddingPhotoPlanning/img_0194.jpg)
[![photo](http://www.burtonini.com/photos/WeddingPhotoPlanning/thumb-img_0197.jpg.png)](http://www.burtonini.com/photos/WeddingPhotoPlanning/img_0197.jpg)

I practise eloping and marrying the Invisible Woman:  
[![photo](http://www.burtonini.com/photos/WeddingPhotoPlanning/thumb-img_0181.jpg.png)](http://www.burtonini.com/photos/WeddingPhotoPlanning/img_0181.jpg)

Ross and Vicky, circa. 1850.  
[![photo](http://www.burtonini.com/photos/WeddingPhotoPlanning/thumb-img_0187.jpg.png)](http://www.burtonini.com/photos/WeddingPhotoPlanning/img_0187.jpg)

Ross and Vicky engaging in their little known pursuit of Shakespearian
theatre.  
[![photo](http://www.burtonini.com/photos/WeddingPhotoPlanning/thumb-img_0188.jpg.png)](http://www.burtonini.com/photos/WeddingPhotoPlanning/img_0188.jpg)

This is Little Hallingbury Village Hall, where we are having our
reception. To the right you can just see the large garden behind the
hall, where we will be having a barbecue. Inside we'll have a jazz band
for the evening entertainment.

[![photo](http://www.burtonini.com/photos/WeddingPhotoPlanning/thumb-img_0199.jpg.png)](http://www.burtonini.com/photos/WeddingPhotoPlanning/img_0199.jpg)
