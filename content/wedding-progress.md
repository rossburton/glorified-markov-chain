Title: Wedding Progress
Date: 2003-08-13 00:00
Tags: life
Slug: wedding-progress

Last night we filled in the notice of marriage, the first official step
towards getting married. We have to answer questions like address and
fathers occupation for each other first, which is a little odd. The
registrar said this was a way of protecting against people who don't
really know each other getting married (i.e. marriages for passports),
but it wouldn't be too hard to memorise the answers.

We also discovered that "spinster" (a non-married woman, the female
equivalent of bachelor) has its roots in the Crusades, when the wives of
the knights would spin cotton until they returned. Or so the registrar
said, anyway.
