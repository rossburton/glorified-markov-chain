Title: Weekend
Date: 2004-03-15 00:00
Tags: life
Slug: weekend-2

I'm determined to post my weekend blog before Wednesday this week, so I
best get writing!

This weekend was good fun — bumped into an old friend, Sarah Mason, in
the shops on Friday night. I wasn't even home yet and she'd already been
to the gym *and* got a bit drunk — some people seem to fit more hours in
the day than I can. Saturday involved re-arranging the furniture, so
I've now got a sore back from picking up the fish tank (and its many
gallons of water...). That evening we met
[Allen](http://www.monkey-spanking.co.uk) and went down to one of the
many local Indian restaurants. Wonderful food — must go again soon.

Spent many an hour with Allen and Vicky afterwards, lubricated with
Bangla, chatting about the usual — [disappearing
socks](http://www.livejournal.com/users/groovyal/1030.html), stag nights
and profit-making schemes involving the Bible...

In other not-really-related-news, I noticed numerous ~~pigs~~police men
on the way to work today, and [now I know
why](http://www.guardian.co.uk/uklatest/story/0,1271,-3862379,00.html).
Someone has found an [easter egg in the BMW
M3](http://www.gizmodo.com/archives/first_automotive_cheat_code.php).
Finally, an interesting blog entry by someone whose daughter [knows
nothing but
Tivo](http://www.kokogiak.com/gedankengang/gedanken_arch.asp#1111200389).

NP: [Personal
Journals](http://www.amazon.co.uk/exec/obidos/ASIN/B000063Y38), Sage
Francis.
