Title: Weekend
Date: 2006-08-29 11:00
Tags: life
Slug: weekend

Yay for Bank Holiday weekends. It's been a busy one, in no particular
order we:

-   Saw <cite>Snakes On A Plane</cite>, which was good. It managed to be
    tongue-in-cheek without being silly, and Samuel L Jackson didn't
    disappoint at all. Great fun film.
-   Saw <cite>Pirates Of The Caribbean 2</cite>, which wasn't bad. As
    ever it proves the rule that the sequel is never as good as the
    original, was a little too long, and the abrupt ending setting the
    stage for the third film was a little annoying. I didn't think it
    was bad though, but you really needed to have seen the first film.
-   Had a swift pint (or two) with Allen before the cinema. He's been
    travelling around with work a lot recently (Manchester,
    Sheffield, Delhi) so catching him is quite tricky these days.
-   Went out to dinner with Caroline to [De
    Luca](http://www.delucacucina.co.uk/). Very good Italian,
    highly recommended.
-   Cooked a roast chicken and so on for Pete and Jayne, before they go
    back to uni. This summer has flown by and we didn't really see them
    much, so it was good to spend half the day with them. I also failed
    to poison anyone with roast chicken, which is always good.

<small>NP: <cite>\[THN014\] Zu Hause^2^ LP</cite>, Digitalverein</small>
