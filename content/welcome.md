Title: Welcome
Date: 2010-11-13 08:00
Tags: life
Slug: welcome

Say hello to Isla Daisy Burton.

[![Isla Daisy
Burton](http://farm2.static.flickr.com/1077/5147009874_d5ca18f785_z.jpg){
}](http://www.flickr.com/photos/rossburton/5147009874/ "Isla Daisy Burton")

Born at home (planned) on Thursday 4th November 2010 at 19:08, weighing
6 pounds 14.5 ounces. Both mother and baby are well. I'm now on leave
until the end of December, so expect delayed responses to my personal
email and vacation autoreplies to my Intel email.
