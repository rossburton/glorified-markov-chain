Title: We're Hiring!
Date: 2008-04-16 14:45
Tags: tech
Slug: were-hiring

Here at OpenedHand Towers we've just announced some more [job
openings](http://o-hand.com/jobs/), so if you have skills in any of the
kernel, X.org, GTK+, Clutter or OpenEmbedded then please have a look.
We're also after user interface/interaction designers, junior designers
(print/web/UI), and have an student internship for a programmer. Pretty
much something for everyone!
