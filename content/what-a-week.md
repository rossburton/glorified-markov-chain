Title: What a Week
Date: 2004-05-07 00:00
Tags: life
Slug: what-a-week

Blimey, what a week or so this has been. We've finally finished sorting
out the wedding gift list and I'm pretty pleased with what we've found
&mdash most things essential apart from a kitchen sink (which was an
option with the service we've got) such as kitchenware, book cases, new
hifi for the bedroom, towels, etc. "Domesticated", and possibly
"homely", are very much the words.

Last Saturday we were due to be planning the wedding photography, but
that was cancelled so we spent the day doing some shopping. Well, by
some I mean a few very large items, including a wonderful framed print
of <cite>Farbstudie Quadrate Mit Konzentrischen Ringen</cite> by
Kandinsky. Stortford has a wonderfully large Arthouse store, but there
was a man in there who started by telling Vicky off for holding a print
badly and then was probably trying to be efficient, but ended up almost
framing a few prints we had barely looked at. Thankfully the woman who
was putting the prints into the frames was far nicer, and snapped at him
a few times.

We caught up on films over the weekend: finally watched the excellent
[<cite>Kill Bill Vol. 1</cite>](http://imdb.com/title/tt0266697/) (I
found <cite>Jackie Brown</cite> a bit of a let-down, so I was pleased
that I enjoyed <cite>KB</cite> as much as I did) and went to see
[<cite>Eternal Sunshine of the Spotless
Mind</cite>](http://imdb.com/title/tt0338013/), which was good. Finally
to finish the weekend we watched
[<cite>K-PAX</cite>](http://imdb.com/title/tt0272152/), and had the
usual is he/isn't he debate. You can get all three <cite>K-PAX</cite>
books in one now, which is very tempting.

Last night there was an excellent <cite>Dispatches</cite> on Channel 4,
about the battle some residents in Lee were having with the government
about an asylum seeker holding center which was being built in their
own. Lots of hot <acronym title="Not In My Back Yard">NIMBY</acronym>
action and some amusing (if they were not so appalling) quotes: "I don't
want them anywhere in England, they are dangerous" and "He looked
hostile and, you know, foreign. Had a bit of a beard" (when asked "how
did you know he was an asylum seeker"). Jesus, people. I can feel myself
entering a Garrettesque rant, so I'll stop now. `:-)`

Oh, and Murray Cumming was right: Tasking **are** the enemy.

NP: <cite>O</cite>, Damien Rice
