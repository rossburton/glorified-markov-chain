Title: What's New, Pussy Cat
Date: 2003-03-10 00:00
Tags: tech
Slug: whats-new-pussy-cat

Still not a Debian Developer. Passed all the tests, but the
report-writing stage takes time, or something. I don't think I could be
a Debian Account Manager, far too much work with people expecting
results quickly. Poor dobey...

My mission to ITP for Debian everything on the planet is going well. I'm
waiting for *GNOME PDF*, *GNOME Chat* and *Gossip* to have tarball
releases for packages. Impatient people can try the CVS snapshot
packages in my [experimental](/debian/experimental) repository.
