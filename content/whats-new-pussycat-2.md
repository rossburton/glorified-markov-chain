Title: What's New, Pussycat
Date: 2004-12-02 00:00
Tags: life
Slug: whats-new-pussycat-2

Erm, yeah. So it's been a while since I last blogged. My only excuse is
that I've been busy with work...

I had a birthday meal a few weeks ago which was cool, and missed the
last train home which wasn't so cool. It was good to see old faces again
and all that. For my birthday I got cool DVDs (<cite>Father Ted</cite>,
<cite>Black Books</cite>, <cite>24</cite> and <cite>Boyz In the
Hood</cite>) from most people and a kick arse coffee machine from Vicky.
No more putting up with instant (albeit passable Fairtrade instant), I
can go for espresso or filter now. I'm not to bad at a latte either, but
I've not yet summoned the courage to froth milk.

Quick snippets: the new Netscape browser [looks, erm,
busy](http://gemal.dk/misc/nsb05.png). Probably the worse application
I've seen for a long time. Bush turns an [education act into a military
recruitment
drive](http://www.motherjones.com/news/outfront/2002/11/ma_153_01.html),
and [Galloway won his second libel
case.](http://news.bbc.co.uk/1/hi/uk_politics/4061165.stm)

<small>NP: <cite>Dub Come Save Me</cite>, Roots Manuva</small>
