Title: What's new, pussycat...
Date: 2003-10-24 00:00
Tags: tech
Slug: whats-new-pussycat

Here I am, sitting on the train, again, trying to think about finally
using the status bar in Sound Juicer. But I'm tired and I hurt: my back
is disagreeing with sitting down after just having got up, and a few
days ago I managed to bruise the tendons in my right hand. Oh joy.

Yesterday I totally rewrote `sj-extracting.`, the cause of many bugs in
Sound Juicer. I'll test and commit it today (after resisting the urge to
commit without testing last night), and will then do a new release,
which will truly rule, of course.

A few days ago Helix released the first beta of their media player. I
have fond memories of Bastien building their SDK on this iBook,
muttering away, before cursing and taking his iBook to the Helix stand
to poke them about the latest stupidity he found. There were plans to
try and get Helix to use Totem as a front end for their player, as it
has a pluggable backend (currently it can use Xine or GStreamer) and a
totally sweet interface, but they didn't in the end. Turns out they've
written a poor Totem clone, really. Totem doesn't have to start
[\[image\]](http://www.burtonini.com/computing/screenshots/hxplay-1.png)
worrying
[\[image\]](http://www.burtonini.com/computing/screenshots/hxplay-2.png)
yet
[\[image\]](http://www.burtonini.com/computing/screenshots/hxplay-3.png).

In Debian news, we've managed to get GNOME 2.2 into testing and GNOME
2.4 into unstable, a feat which only took a week. Many thanks to
Sebastien Bacher who did most of the initial work!
