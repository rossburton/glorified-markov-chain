Title: Why I Don't Use GnomeFiles
Date: 2006-02-13 12:40
Tags: tech
Slug: why-i-dont-use-gnomefiles

My Top Five Reasons as to why I don't update the information for [Sound
Juicer](http://gnomefiles.org/app.php?soft_id=426) in
[GnomeFiles](http://gnomefiles.org) often:

1.  No programmatic way of uploading new versions. Freshmeat has a
    XML-RPC interface and sample implementation,
    [freshmeat-submit](http://freshmeat.net/projects/freshmeat-submit).
    This means I could integrate posting new versions into my
    `make release` script, which also uploads source tarballs to
    `ftp.gnome.org` and puts the latest
    [DOAP](http://usefulinc.com/doap) file on `burtonini.com`. Ideally
    GnomeFiles will let me specify a DOAP URL and the server will fetch
    it regularly, updating the information when it changes.
2.  When adding a new version I need to fill in the download location
    field from scratch. For every Sound Juicer release this is going to
    look like
    `http://ftp.gnome.org/pub/gnome/source/sound-juicer/2.[something]/sound-juicer-2.[something].tar.gz`.
    It's a good assumption that if one version was at some URL, the next
    version will be at a very similar URL, so why not pre-fill that
    field with the previous version's URL? Freshmeat does this, and it
    turns typing in a long URL into changing a single number.
3.  I need to fill in the file size of the file at the URL. Why can't
    the server do that? If the download is over HTTP it could do a
    `HEAD` request and extract the `Content-Length` header.
4.  No support for branches. There are two branches of Sound Juicer
    development at any moment: stable and development. I have to pick
    one of these to publish, so am either publishing only the stable
    release, or the development releases. People who want to know about
    the other releases can't find this information out.
5.  This one is petty, but it bugged me. <cite>Description of the file
    to download eg. "Binary" or "gnomefiles.org Server"</cite>. One of
    those is a description of the file, the other is a description of
    the site.

<small>NP: <cite>Black Diamond</cite>, Angie Stone</small>
