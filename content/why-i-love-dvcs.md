Title: Why I Love DVCS
Date: 2007-02-28 10:30
Tags: tech
Slug: why-i-love-dvcs

Over the last four days I've had patchy Internet connectivity but I've
been hacking on Tasks a bit in the evening and on flights. The result:

    $ svn diff | diffstat
     README                        |   33 +++++++++++++++++++++++++++
     src/Makefile.am               |    1 
     src/koto-group-filter-model.c |    4 +++
     src/koto-task-editor-dialog.c |   40 +++++++++++++++++---------------
     src/koto-task-editor-dialog.h |    5 +---
     src/koto-task-editor.c        |   47 +++++++++++++++++++++++---------------
     src/koto-task-store.c         |   37 ++++++++++++++++++++++++------
     src/koto-task-store.h         |    3 ++
     src/koto-task-view.c          |   45 ++++++++++++++++++++++++++++++++-----
     src/koto-task.c               |   51 ++++++++++++++++++++++++++++++++++++++++++
     src/koto-task.h               |   26 +++++++++++++++++++++
     src/test.c                    |   48 ++++++++++++++++++++++++++++++++++-----
     12 files changed, 281 insertions(+), 59 deletions(-)

Now I wish I had the foresight to clone the Tasks repository into a
Bazaar branch so that I could commit as I went, and then merge it all
back when I got home. But no, I have several bug fixes and new features
in this mega-diff, and I'm too anal to commit it in a single go. Expect
a Tasks 0.2 soon!
