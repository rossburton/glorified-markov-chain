Title: Why I Love Jon Snow
Date: 2008-01-09 22:18
Tags: life
Slug: why-i-love-jon-snow

There are many reasons why I love [Jon
Snow](http://en.wikipedia.org/wiki/Jon_Snow): his eclectic ties and
socks, his personal morals and ethics (his biography is pretty good),
and of course
[Snowmail](http://www.channel4.com/news/snowmail/?intcmp=news_wp).
Today's Snowmail contained this referential gem:

“These are not just bad sales figures. These are M&S bad sales figures.”

<small>NP: <cite>Herbstlaub</cite>, Marsen Jules</small>
