Title: WiFi Madness
Date: 2005-05-26 12:00
Tags: tech
Slug: wifi-madness

I've *finally* got my new D-Link DWL-G650+ wifi card working on my
laptop. I have revision B.1 of this card, which makes a big difference,
the earlier revisions use a different chip. This card has an ACX111 chip
inside, which is somewhat supported by Linux, but its not trivial.

Hoary comes with the `acx_pci` module builtin, and a collection of
firmware. However, the module will try and load `TIACX111.BIN` to the
card, which is wrong. It will successfully load, but the card won't
work. The trick on Hoary is to steal `FwRad16.bin` from the driver CD
and put it in `/lib/hotplug/firmware`, and then delete `TIACX111.BIN`
from the same location. Only then will the right firmware be uploaded.

Sid is more fun, the module has to be built yourself (there is a package
which makes this very easy using `module-assistant`), but don't do what
the guides suggest and install `acx111-firmware`. Take `FwRad16.bin` and
move it to `/lib/firmware/TIACX111.BIN`. Unlike the earlier versions the
module in Sid will not fall back onto other firmware images.

Now that it is working, I've just got wait for the performance to
increase: in a side-by-side test my cheapo Actiontec Orinoco PCMCIA card
is transferring files 100Kb/s faster.

<small>NP: <cite>Wrath Of The Math</cite>, Jeru The Damaja</small>
