Title: WinXP Madness
Date: 2006-03-21 09:45
Tags: tech
Slug: winxp-madness

Recently I had to reboot my laptop into Windows so that Vicky could use
iTunes. After a few minutes, this little dialog popped up.

![Windows XP restart
dialog](http://www.burtonini.com/computing/screenshots/winxp-restart.png){}

Some automatic updates had been downloaded once I'd logged in, and I now
**had** to restart. The day before I had a similar dialog which had
disabled the <cite>Restart Later</cite> but didn't have a countdown, so
I moved the dialog to the side and carried on (luckily it wasn't system
modal). However a five minute timer is just amazingly bad: did
installing the security fix break the system so much that it has to be
rebooted now, and applications will start to crash and die shortly?

<small>NP: <cite>Maxinquaye</cite>, Tricky</small>
