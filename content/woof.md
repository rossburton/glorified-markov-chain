Title: Woof!
Date: 2005-11-20 19:20
Tags: life
Slug: woof

In a month's time we'll have a new member of the Burton clan, an
incredibly cute cream Labrador Retriever. We've had to pick a first and
second choice, and this is our first (currently to be named either Henry
or Louie):

![](http://www.burtonini.com/photos/Misc/img_3130.jpg)
![](http://www.burtonini.com/photos/Misc/img_3132.jpg)

The trip to see him was... a challenge. In theory a train to Norwich
only takes 2:20, but it ended up taking us over 3:40... and Ely station
(Station Of The Year 1987, don't you know) is surprisingly cold when
waiting there for half an hour.

We can't wait for him/her to arrive (current favourite names for the
second choice, a girl, are Ella or Bess), and really hope the house move
hurries along, we're getting the dog on the 17th December and were
planning on completing on the 1st, but that is looking more and more
unlikely.
