Title: Working Life
Date: 2003-03-10 00:00
Tags: tech
Slug: working-life

Work is looking good at the moment... I'm just finishing off a Java
program (version 0.9.4 so far, its got be done soon), and keep on nearly
working on our new project, which is GTKMM 2 based, yay! I get to use
GTKMM at work, and exercise my C++ skills.

However, being a part-time admin sometimes sucks. Dan and myself had to
go into the office over the weekend and upgrade the servers, Gallahad
(Compaq Proliant) and Lovel (some Dell desktop machine). Poor machines:
not enough RAM and running RH62. We threw in more RAM, gigabit ethernet
cards (w00t), bigger disks and upgraded to RH73.

Now, I hope we find the money to upgrade the desktop machines. We
currently have archaic Dell el-cheapo desktops which are barely
surviving, and the current plan is to replace them with Mini-ATX boxes
and TFT screens. Yum -- no more headaches! Along with the upcoming RH81,
this should be a nice work machine.
