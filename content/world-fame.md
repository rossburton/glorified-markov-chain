Title: World Fame
Date: 2006-06-13 09:50
Tags: life
Slug: world-fame

Some people may have heard that my sister's partner, Elliot, started
working a new shopping channel last month called Deal TV. It's only on
Sky so we can't get it, but my sister took a picture and mailed it to me
last night. Ladies and gentlemen, I present the early career of a future
media superstar.

![Elliot on DealTV](http://burtonini.com/photos/Misc/Elliot.jpg)

<small>NP: <cite>Black Diamond</cite>, Angie Stone</small>
