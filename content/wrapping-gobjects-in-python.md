Title: Wrapping GObjects in Python
Date: 2003-03-13 00:00
Tags: tech
Slug: wrapping-gobjects-in-python

My first article for IBM, *Wrapping GObjects in Python*, [went
online](http://www.ibm.com/developerworks/linux/library/l-wrap/ "Wrapping GObjects in Python")
yesterday.

I am really pleased with this, and plan to write another article. I find
it is a good way to a) make money, and b) ensure I actually know what I
am talking about. If I can't write an article about something, I
obviously don't know the details.
