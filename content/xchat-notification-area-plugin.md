Title: XChat Notification Area Plugin
Date: 2005-04-05 20:30
Tags: tech
Slug: xchat-notification-area-plugin

After installing
[xchat-systray](http://blight.altervista.org/index.php?s=&act=Systray)
and then quickly removing it in shock and disgust, I decided to do a
quick hack using the XChat Python plugin. Lo and behold, a notification
area plugin for XChat which displays an icon in the notification area on
interesting events, and *nothing else*.

It's short, simple, and straight to the point. When someone talks to
you, an icon appears in the notification area. When you switch to the
relevant tab, the icon disappears. There is nothing to configure, and it
does nothing else. Pure simplicity, and a whole 70 lines of source.

It requires the Python XChat plugin and `python-gnome-extras`, but
assuming you have that just drop [this
file](http://www.burtonini.com/computing/notify.py) into `~/.xchat2/`
and restart XChat. Voila! At this point you'll notice that I've not even
bothered to use a decent icon, but I will soon, honest.
