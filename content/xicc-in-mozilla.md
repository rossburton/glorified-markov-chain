Title: XICC in Mozilla
Date: 2007-03-15 11:40
Tags: tech
Slug: xicc-in-mozilla

I just learnt via [The Daily
Chump](http://dailychump.org/2007/03/15/2007-03-15.html#1173938233.855396)
that [Firefox 3 should have support for ICC profile
handling](https://bugzilla.mozilla.org/show_bug.cgi?id=16769) when
displaying images. The initial patch was to use ColorSync which won't
work on Linux, but the recent patches by Tim Rowley also use the
`_ICC_PROFILE` from my [ICC Profiles In
X](http://burtonini.com/blog/computers/xicc) specification when running
on Linux. Excellent news, I hope this gets integrated soon.

Speaking of which, now that more applications are using this property, I
should get around to extending the Screen Resolution setting in GNOME to
include a colour profile selector...

<small>NP: <cite>Kaleidoscope</cite>, DJ Food</small>
