Title: XICC in UFRaw
Date: 2007-11-16 17:30
Tags: tech
Slug: xicc-in-ufraw

I just got an email from Udi Fuchs of UFRaw fame. The latest version of
UFRaw, 0.13, supports the [XICC
specification](http://burtonini.com/blog/computers/xicc) so it can
adjust the image for correct display on your monitor. Great, thanks Udi!

<small>NP: <cite>Milieu</cite>, Tape</small>
