Title: Xoo
Date: 2005-09-05 16:35
Tags: tech
Slug: xoo

I know there are some [Xoo](http://projects.o-hand.com/xoo/) fanatics
out there^\[1\]^, so I thought I best tell the world that I've just
rolled Xoo 0.7. We never actually made a tarball of 0.6 so this release
is quite an improvement over 0.6. I'll also be updating the Debian
packages shortly.

<small>NP: <cite>Do You Want More?!!!??!</cite>, The Roots</small>

<small> \[1\] Well, maybe just two^\[2\]^  
\[2\] That's me and Matthew </small>
