Title: Xrandr, postscript
Date: 2007-02-09 17:10
Tags: tech
Slug: xrandr-postscript

If anyone out there has been using my Edgy packages for the new Xrandr
love, then you'll notice that I've just deleted them...

Daniel Stone, X hacker extraordinaire, has built updated packages. As he
can actually use git I trust them more than I trust my own. They may
blow up your machine and so on, but they are available here:

    deb http://www.fooishbar.org/packages/ randr-1.2/edgy/$(ARCH)/

He also kindly built AMD64 debs, something that I cannot do.
