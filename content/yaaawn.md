Title: Yaaawn
Date: 2003-09-19 00:00
Tags: life
Slug: yaaawn

It's early and for the first time since last Friday I'm sitting on the
train going to work, as again I've been ill. But first, All About
Heather Nova.

<div class="title">

Heather Nova

</div>

The Heather Nova gig last Friday was excellent. The Union Chapel is a
lovely building, everyone sits on the pews, there is a stained glass
window behind the artist, and a nice bar to the side (which I presume
wasn't part of the original design). We turned up too late to meet Edd
in the pub, so once I'd met Vicky we sat down, only for Edd to sit down
in his seat directly in front of me a minute later... The support band,
Grand Drive, were very good, three men, each with an acoustic guitar.
They were plugging the CDs they were selling at the back, which we
totally forgot to buy but is on order from Amazon. Then Heather Nova
came onto the stage, which had been decorated with fairy lights and was
wonderfully lit. Her, the lighting, and the stained glass window behind
looked stunning. No wonder there was a large video camera at the back,
this is an excellent venue to record the <cite>Storm</cite> DVD at.

The band for this tour is quite cut-down, Heather on vocals, a drummer,
a bassist (either double bass or a normal bass guitar), and the most
amusing band member on keyboards and backing vocals. He was really
enjoying playing, started by swaying to the music but as the songs get
going he shuts his eyes, grins madly and leans right over the keys. This
4 piece band is best suited for <cite>Storm</cite>, but were good enough
to perform most songs very well. The acoustics were also perfect,
everything was audible, the bass was strong but not drowning out
anything, and we could hear every single instrument. It was very easy to
listen too, after being aurally attacked by sound engineers at the
Brixton Academy.

<div class="title">

Urrgh

</div>

The next day, Saturday, Vicky went off shopping for a wedding dress. She
tried many on, didn't like all of any of them, so has gained lots of
knowledge for next time. That evening, we met with her mum and planned
some of the wedding, went through our ideas, etc. I sneezed now and
again, felt a little cold, and we went home. Sunday was far more
interesting, that evening I started shivering whilst burning up and had
developed an all-over rash.

Because of this I didn't feel up to the office commute until Thursday,
but a BT engineer was going to turn up Thursday to fix my ADSL, so I
worked from home then... I'm becoming quite a rare site in the office
recently! Maybe I can do an entire week next week.
