Title: Yay/Boo
Date: 2003-04-07 00:00
Tags: tech
Slug: yayboo

Things to say "yay" too: I'm onto the last stage of the Debian New
Maintainer process; Sound Juicer is going very well; it appears Jimbob
is hacking on Gnome Chat again; I'm not coding Swing at work any more.

Things to say "boo" too: BBC reporter John Simpson (second only to John
Snow as a Top Class News Person in my opinion) was very nearly killed by
"friendly fire" yesterday, as a bomb dropped by the US Air Force hit 12"
away. He had a piece of shrapnel embedded in his flak jacket next to his
spine... He was very lucky, their translator was not, as he was killed
by the blast. My thoughts go out to the families of the journalists who
risk their lives to bring us the news.
