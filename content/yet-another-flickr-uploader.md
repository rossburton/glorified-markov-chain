Title: Yet Another Flickr Uploader
Date: 2006-08-15 21:55
Tags: tech
Slug: yet-another-flickr-uploader

Following the grand tradition of rewriting new programs instead of
improving existing ones, I've written a Flickr upload tool in Python.
It's called Postr, but luckily that string isn't exposed in the UI.

![Flickr Uploader](http://burtonini.com/computing/screenshots/postr-1.png)

It's not yet ready for a 0.1 release, although it's close. You can
drag-and-drop images to the application, set the title, description, and
tags, and then upload. It tries to be clever: the EXIF thumbnail is used
in the interface if it exists to avoid loading the entire image, and if
the image contains an EXIF ImageDescription field that is used for the
description automatically. My next task is to fetch the list of tags and
present them to the user in a useful way.

If anyone wants to have a play with it, then the source is available via
`bzr` in [this repository](http://burtonini.com/bzr/postr/).
