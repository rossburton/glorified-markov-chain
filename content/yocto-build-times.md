Title: Yocto Project Build Times
Date: 2012-11-15 16:20
Tags: tech, yocto
Slug: yocto-build-times

Last month our friends at Codethink were guests on FLOSS Weekly,
[talking about Baserock](http://twit.tv/show/floss-weekly/230).
[Baserock](http://www.baserock.com/) is a new embedded build system with
some interesting features/quirks (depending on your point of view) that
I won't go into. What caught my attention was the discussion about build
times for various embedded build systems.

> Yocto, again, if you want to do a clean build it will take days to
> build your system, even if you do an incremental build, even if you
> just do a single change and test it, that will take hours.

(source: [FLOSS Weekly \#230, timestamp
13:21](http://twit.tv/show/floss-weekly/230), slightly edited for
clarity)

Now "days" for a clean build and "hours" for re-building an image with a
single change is quite excessive for the Yocto Project, but also quite
specific. I asked Rob Taylor where he was getting these durations from,
and he corrected himself on Twitter:

\[tweet https://twitter.com/robtaylor78/status/263300739089178624\]

I'm not sure if he meant "hours" for both a full build and an
incremental build, or whether by "hours" for incremental he actually
meant "minutes", but I'll leave this for now and talk about real build
times.

Now, my build machine is new but nothing special. It's built around an
Intel [Core i7-3770](http://ark.intel.com/products/65719/) CPU
(quad-core, 3.4GHz) with 16GB of RAM (which is overkill, but more RAM
means more disk cache which is always good), and two disks: a 250GB
Western Digital Blue for `/`, and a 1TB Western Digital Green for
`/data` (which is where the builds happen). This was built by [PC
Specialist](http://pcspecialist.co.uk/) for around £600 (the budget was
\$1000 without taxes) and happily sits in my home study running a
nightly build without waking the kids up. People with more money stripe
`/data` across multiple disks, use SSDs, or 10GB `tmpfs` filesystems,
but I had a budget to stick to.

So, let's wipe my build directory and do another build from scratch
(with sources already downloaded). As a reference image I'll use
`core-image-sato`, which includes an X server, GTK+, the Matchbox window
manager suite and some demo applications. For completeness, this is
using the 1.3 release - I expect the current master branch to be
slightly faster as there's some optimisations to the housekeeping that
have landed.

`$ rm -rf /data/poky-master/tmp/ $ time bitbake core-image-sato Pseudo is not present but is required, building this first before the main build Parsing of 817 .bb files complete (0 cached, 817 parsed). 1117 targets, 18 skipped, 0 masked, 0 errors. ... NOTE: Tasks Summary: Attempted 5393 tasks of which 4495 didn't need to be rerun and all succeeded.`

real 9m47.289s</code>

Okay, that was a bit *too* fast. What happened is that I wiped my local
build directory, but it's pulling build components from the "shared
state cache", so it spent six minutes reconstructing a working tree from
shared state, and then three minutes building the image itself. The
shared state cache is *fantastic*, especially as you can share it
between multiple machines. Anyway, by renaming the `sstate` directory it
won't be found, and then we can do a proper build from scratch.

`$ rm -rf /data/poky-master/tmp/ $ mv /data/poky-master/sstate /data/poky-master/sstate-old $ time bitbake core-image-sato Pseudo is not present but is required, building this first before the main build ... NOTE: Tasks Summary: Attempted 5117 tasks of which 352 didn't need to be rerun and all succeeded.`

**real 70m37.298s**  
user 326m45.417s  
sys 37m13.304s  
</code>

That's a full build from scratch (with downloaded sources, we're not
benchmarking my ADSL) in just over an hour on affordable commodity
hardware. As I said this isn't some "minimal" image that boots straight
to busybox, this is building a complete cross-compiling toolchain, the
kernel, X.org, GTK+, GStreamer, the Matchbox window
manager/panel/desktop, and finally several applications. In total, 431
source packages were built and packaged, numerous QA tests executed and
flashable images generated.

My configuration is to build for Intel Atom but a build for an ARM,
MIPS, or PowerPC target would also take a similar amount of time, as
even what could be considered "native" targets (targeting Atom, building
on i7) doesn't always turn out to be native: for example carrier-grade
Xeon's have instructions that my i7 doesn't have, and if you were
building carrier-grade embedded software you'd want to ensure they were
used.

So, next time someone claims Yocto Project/OpenEmbedded takes "days" or
even "hours" to do a build, you can denounce that as FUD and point them
here!
