Title: Fact-checking ChatGPT with Yocto Project release names
Date: 2025-02-07 17:03
Tags: tech, yocto
Slug: yocto-llm

For fun, I decided to ask to "explain the themes in the Yocto Project release names". It went about as well as I expected, to be honest:

>The Yocto Project follows a thematic naming convention for its release names. The themes have evolved over time:

So far so good.

>Types of Birds (Older Releases)
>Early Yocto Project releases were named after different bird species. Examples include:

Incorrect, that was not one of the themes.

>Dizzy (a reference to Dizzy Gillespie, but also sounds like a bird name)

A jazz musician that "sounds like a bird name"?  One hell of a stretch, and not the reference.

>Fido (a breed of pigeon)

From what I can tell, Fido is not a breed of pigeon. Also not the reference.

>Jethro (a reference to Jethro Tull, but also resembles a bird-like theme)

Jethro Tull the rock band? Not the reference.

I do wonder why it decided the theme was types of birds, then made up one bird breed and listed two musicians that "sound like" bird names.

Continuing on...

>Muppets Characters (Mid-Series)
>The Yocto Project later transitioned to naming releases after characters from The Muppets, a popular puppet show created by Jim Henson. Examples include:

No, it didn't.

>Krogoth (a lesser-known Muppet)

Krogoth is so lesser-known that the Muppets Wiki doesn't mention it, so I think this is made-up.

>Morty (another Muppet character)

According to the Muppets Wiki "Morty is Rick Moranis' agent. He is seen in Muppets Tonight episode 202 [in 1997]".

Considering how incorrect everything else is I actually think this is more likely to be a coincidence than an actual reference to reality. There's been a lot of characters over the decades of shows.

>Rocko (potentially a reference to Rocko from Rocko’s Modern Life, though not strictly Muppet-related)

"not strictly"? A cartoon from the 1990s by some of the people that later gave us Spongebob Squarepants is not the reference for this release.

>Sci-Fi and Fantasy References (Current Theme)
>The most recent trend in Yocto release names leans toward science fiction and fantasy references. Examples include:

Surprisingly, this is not the current theme.

>Langdale (a reference to a location, possibly inspired by British landscapes, but also fits a fantasy tone)

Oh you're close, go on you can do it!

>Mickledore (a name with a Tolkien-esque or fantasy feel)

I can only imagine why that is...

>Nanbield (continuing the theme of mysterious, fantasy-like names)

I give up.

Remember people: verify _everything_ that LLMs say, as they are first-rate bullshit artists.