Title: Zebu 0.1
Date: 2008-06-22 14:20
Tags: tech
Slug: zebu-0-1

As one of the maintainers of
[debian.o-hand.com](http://debian.o-hand.com/) I use the always
wonderful `pbuilder` and `cowbuilder` to rebuild packages originally
build for Debian Sid for Debian Etch, Ubuntu Gutsy, Hardy, and so on.
Continually typing the commands to update the cowbuilders can get
tiresome fast so last week I scratched the itch and produced
<cite>Zebu</cite>.

![Zebu](http://burtonini.com/computing/screenshots/zebu-0.1.png)

As of version 0.1 it is barely functional but it does let you update or
login to a cowbuilder. It requires that the cowbuilders are named
`/var/cache/pbuilder/*.cow` and doesn't support "traditional" pbuilder
rootstraps yet, but that is planned. Anyway, cowbuilders are the future.

If anyone else thinks this could be useful there is a
[tarball](http://burtonini.com/computing/zebu-0.1.tar.gz) and a [Bazaar
repository](http://burtonini.com/bzr/zebu). I must also thank the
wonderful Ulisse Perusin for the rocking icon he created.

<small>NP: <cite>Cosmos</cite>, Murcof</small>
