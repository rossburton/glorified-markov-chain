Title: ZeroConf Bookmarks in Epiphany
Date: 2006-02-11 17:50
Tags: tech
Slug: zeroconf-bookmarks-in-epiphany

Yesterday I noticed that Epiphany in Ubuntu (and probably Debian too) is
now build with Zeroconf support, which means it will detect any web
sites exported over Zeroconf and add them to the bookmarks.

This totally rocks, as instead of having a number of bookmarks for
various administration pages on my machines, the machines can export
them over Zeroconf and when I am at home and those machines are on,
they'll appear magically.

The first step to having Zeroconf enlightenment is to ensure
`avahi-daemon` is installed on all machines. It doesn't matter if there
are some machines that it cannot be installed on (i.e. router running
embedded software), as other machines can publish their name and
services on their behalf.

The first service I published is the web-based admin pages for my
printer. The printer is connected to a small server called Melchett (a
Buffalo LinkStation) which is running CUPS. By creating
`/etc/avahi/services/cups.service` on Melchett with the following
contents, the web site is published:

    <?xml version="1.0" standalone='no'?>
    <!DOCTYPE service-group SYSTEM "avahi-service.dtd">
    <service-group>
      <name>Epson Stylus Photo R220 Admin</name>
      <service>
        <type>_http._tcp</type>
        <port>631</port>
        <txt-record>path=/printers/epson-stylus-photo-r220</txt-record>
      </service>
    </service-group>

Each `.service` file in `/etc/avahi/services` defines a group of
services. A group has a name, and a set of services. Each service has a
number of properties:

Type
:   Specifies what the type of the service is (`_http._tcp` is HTTP
    over TCP). There is a [canonical list of types
    available](http://www.dns-sd.org/ServiceTypes.html). This is a
    required element.

Port
:   What port the service is listening on. This is a required element,
    there is no default port 80 for `_http._tcp` services.

Text Record
:   This sets arbitary key-value pairs, which are interpreted on a
    per-service manner. For `_http._tcp` services the valid keys are `u`
    (username), `p` (password) and `path`.

There are also `domain-name` and `host-name` properties, but these are
not used in this service description as the service is on the local
machine.

In summary, you can see that I have specified that there is a web page
accessible via HTTP on this machine, available on port 631 at the path
`/printers/epson-stylus-photo-r220`. When other machines search the
network for web pages they'll find this service entry (with the hostname
properties filled in by Avahi), and generate the URL
`http://melchett.local:631/printers/epson-stylus-photo-r220` A quick pop
into Epiphany shows that this is indeed the case, there is now a
<cite>Local Sites</cite> category in the bookmarks containing
<cite>Epson Stylus Photo R220 Admin</cite>.

There is one other thing I need to explain: how to publish services on
behalf of other machines. Until this afternoon this required something
to run `avahi-publish-address` to give the machine a Zeroconf name, but
this afternnon Trent committed [static name
mapping](http://lathiat.livejournal.com/34943.html) to Avahi. When this
is in a released version, I'll continue this article.
