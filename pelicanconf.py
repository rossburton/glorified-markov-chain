#!/usr/bin/env python3
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals
import os

AUTHOR = u'Ross Burton'
SITENAME = u'Glorified Markov Chain'
SITEURL = ''
RELATIVE_URLS = True

PATH = 'content'

TIMEZONE = 'Europe/London'

DEFAULT_LANG = u'en'

#SHOW_SOCIAL_ON_INDEX_PAGE_HEADER = True
SOCIAL = (('twitter', 'https://twitter.com/rossburton'),
          ('github', 'https://github.com/rossburton'),
          ('flickr','https://www.flickr.com/photos/rossburton/'),
          ('envelope','mailto:ross@burtonini.com'))

# Ten entries to a page, limit of two orphans for the final page.
DEFAULT_PAGINATION = 10
DEFAULT_ORPHANS = 2

# Don't want author or category indexes as I am only myself and there are no
# categories.
DIRECT_TEMPLATES = ('index', 'tags')

DEFAULT_DATE_FORMAT = '%B %-d, %Y'

FEED_MAX_ITEMS = 10
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

THEME = "themes/pelican-clean-blog"
HEADER_COVER = "images/top.jpg"
DISPLAY_PAGES_ON_MENU = False
DISPLAY_CATEGORIES_ON_MENU = False
MENUITEMS = (("Tags", "tags.html"),
             ("Talks", "pages/talks.html"),
             ("About", "pages/about.html"))

FOOTER_INCLUDE = 'myfooter.html'
THEME_TEMPLATES_OVERRIDES = ["templates",]

# Turn this on when it doesn't run on the <head><title> too
TYPOGRIFY = False

PLUGINS = ('pelican_flickrtag',)

FLICKR_TAG_TEMPLATE_NAME = "flickr"
FLICKR_API_KEY = "b7c81d0a76b74182befe778deaa564c6"
FLICKR_API_SECRET = "d48c521da71561f3"
FLICKR_API_TOKEN = ""
FLICKR_TAG_CACHE_LOCATION = "flickrtag-cache"

# Don't generate these indexes
AUTHOR_SAVE_AS = ""
CATEGORY_SAVE_AS = ""

ARTICLE_URL = '{date:%Y}/{date:%m}/{date:%d}/{slug}/'
ARTICLE_SAVE_AS = '{date:%Y}/{date:%m}/{date:%d}/{slug}/index.html'
# TODO: link to these somewhere (archives link?)
YEAR_ARCHIVE_SAVE_AS = '{date:%Y}/index.html'
MONTH_ARCHIVE_SAVE_AS = '{date:%Y}/{date:%m}/index.html'
